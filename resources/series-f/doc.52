<!-- www.lowe.org/data/1/1910.txt -->
CAN YOU MAKE MONEY WITH YOUR IDEA OR INVENTION?
Doc #1910

By John Tavela, Editor
Management Publications
U.S. Small Business Administration

Summary

Innovative ideas are essential to business progress. It is very difficult,
however, for innovators to get the kind of financial and management support
they need to realize their ideas.

This Aid, aimed at idea people, inventors, and innovative owner-managers of
small companies, describes the tests every idea must pass before it makes
money.

You've Got an Idea? Great!

So, you've had an idea for an invention or an innovative way of doing
something that will boost productivity, put more people to work, and make
lots of money for you and anyone who backs you? As you've probably heard,
you're the kind of person your country needs to compete in world markets
and maintain its standard of living. You're the cutting edge of the future.

You are another of those individuals on whom progress has always depended.
We all know that it hasn't been huge corporations that have come up with
the inventions that have revolutionized life. As the discoverer of
penicillin, Sir Alexander Flemming, said, "It is the lone worker who makes
the first advance in a subject: The details may be worked out by a team,
but the prime idea is due to the enterprise, thought and perception of an
individual." Innovators like you are business's lifeblood.

Owner-managers who have started companies on new ideas know first hand
about the innovation process. They also know that you can expect to hear....

You've Got an Idea? So What?

In the first place, the chances that you are the first to come up with a
particular innovation are somewhere between slim and none. Secondly, even
if you have come up with the better mouse trap, nobody--but nobody--is
going to beat a path to your door. In fact, in the course of trying to
peddle your BMT, you'll beat up plenty of shoe leather wearing paths to
other people's doors. You'll stand a good chance of wearing out your
patience and several dozen crying towels as well.

Why is it so hard to find backers for your brainchildren? One consultant
put it: "Nobody wants unproven ideas. Nobody wants to be first. Everybody
wants to be second." Why this fear of the new?

Well, new product failure rates are estimated conservatively to be between
50 and 80 percent. One survey of major companies with millions of dollars
to spend on R & D, market research, and product advertising, and with
well-established distribution systems found that of 58 internal proposals
only 12 made it past initial screening. From these 12 only one successful
new product emerged.

Another group set up to help innovators has found that of every 100 ideas
submitted 85 have too many faults to bother with. They can be eliminated
immediately. Of the remaining 15, maybe five will ever be produced. One of
those might--only might--make money.

With odds like 99 to 1 against an idea being a monetary success, is it any
surprise that your idea is greeted with a chorus of yawns?
People--companies, investors, what have you--are basically conservative
with their money. Ideas are risky.

Does that mean you should forget about your idea? Of course not. It merely
means that now you're beginning to see what Edison meant, when he said,
"Genius is one per cent inspiration and ninety-nine percent perspiration."

Again, those of you who own small firms started on innovations are well
aware of the truth of Edison's words. You've been through the hard work.

Can You Exploit Your Idea?

Although coming up with what you think is a sure-fire idea is the biggest
step, it's still only the first one. You've got the other thousand miles of
the journey to success still ahead of you.

Many things remain to be done before you can expect to realize the first
dollar from your invention or other innovation. You should be prepared for
the unhappy discovery that the end of the line for your idea may turn up
well before the point you needed to reach to make money from it.

At a bare minimum, your idea will have to pass the following tests:

Is it original or has someone else already come up with it?

Can someone produce and distribute it if it's an invention or other
product, or use it if it's a marketing innovation, a new use for an
existing product, or the like?

Will it really make money? (Will someone buy it?)

Can you protect your idea?

That seems to be a modest enough list, and it is. The problems arise from
the dozens of underlying questions that must be answered before the major
questions can be resolved. Here, for example, are the 33 areas that the
University of Oregon's Innovation Center runs each submitted idea through
to determine if it has commercial merit:

Legality
Safety
Environmental Impact
Societal Impact
Potential Market
Product Life Cycle
Usage Learning
Product Visibility
Service
Durability
New Competition
Functional Feasibility
Production Feasibility

Stability of Demand
Consumer/User Compatibility
Marketing Research
Distribution
Perceived Function
Existing Competition
Potential Sales

Development Status
Investment Costs
Trend of Demand
Product Line Potential
Need
Promotion
Appearance
Price
Protection
Payback Period
Profitability
Product Interdependence
Research and Development

Now that is not a modest list. However, for the moment let's ignore the 33
and look at the four broad questions.

Is Your Idea Original?

Obviously, if somebody has already come up with and produced as good an
item or a better one, if would be pointless for you to pursue a similar
idea any further. You'd only be wasting your time and money.

There are lots of places to look to find out. If your idea is for a
consumer product, check stores and catalogs. Check trade associations and
trade publications in the field into which your invention or innovation
fits. Visit trade shows relevant to your idea. Look in the business and
popular press. (Here, you can consult The Reader's Guide to Periodical
Literature to help you in your search. Your public library has a copy.)

Don't be afraid to ask people in the field if they've ever heard of
anything along the lines of your idea. In the pure idea stage it's not very
likely that somebody will steal your idea--all the hard work still has to
be done. Besides, you can ask general sorts of questions and keep the
details of your idea to yourself if you're really anxious that your idea
will be pirated. Patent rights to an idea in major foreign countries will
be jeopardized by uncontrolled disclosure prior to filing a patent
application in the United States.

Obviously, if what you've come up with is an invention or an idea that can
be put into patentable form, you'll eventually have to make a patent
search. You could do that in this early stage, but it's probably a better
idea to hold off until you've taken a look at your idea in the light of the
next two questions.

How Will the Invention Be Produced and Distributed?

The first thought many innovators have is to take their ideas to a big
national company. Provide the dazzling idea, they think, and let the giant
work out the details. After all, the national company has the money, the
production capability, and the marketing know-how to make this surefire
profit maker go.

Unfortunately, the big companies are almost never interested in ideas from
outsiders. Whether that's because, as one innovation broker has suggested,
that outside technology is "a risk, a threat," or simply because large
corporations need potential sales of an item to be in the tens of millions
of dollars, doesn't matter. The cold fact is that selling a big firm on
your idea is in the 100,000 to 1 shot range.

On the other end of the scale, you may be able to produce some items
yourself, working out of your home and selling by mail order. This method
can be a good way to get started, but after a while you may find yourself
getting tired of having 200,000 better mouse traps stashed in your bedroom.

To be sure, if you can start (or already have) your own company, you will
be better off. It's easier to sell a company than a patent, even if the
company is losing money.

Many potential buyers understand a company much better than they understand
the technology of an invention. Business people usually look at the
profit-and-loss possibilities differently from the way an innovator does.

Many of these business people follow what one innovator has called "the
`Anyhow' theory of economics": "We have a plant anyhow. We have a sales
force anyhow. We advertise anyhow. We're smarter anyhow." Such business
people also know that by the time they purchase a company most of the bugs
are out of the technology and customers exist.

Between the extremes of starting your own company or having big business
buy you out is taking your idea to small and medium-sized businesses. Such
firms would be happy to produce an item producing sales in amounts that
simply don't interest large companies. Smaller firms may lack marketing and
distribution expertise, but again your major problem is even finding one
that can help you realize your idea and is interested in trying.

Will Your Idea Make Money?

This is the question that worries everybody. Here is where the risk arises
that makes it so difficult to interest people in backing your idea. It's a
question that's really impossible to answer with any assurance. After all,
major corporations even with massive market studies hit clinkers all the
time. Remember the Edsel? On the other hand, an idea so seemingly stupid
that you'd think it was somebody's idea of a silly joke might make
millions. Don't you wish you'd thought of the pet rock?

So many factors need to be considered to answer this question. Is there a
market? Where is it? Is it concentrated or dispersed? Could the size of the
market change suddenly? Will competition drive you out? These questions are
by no means the bottom of the iceberg. Yet, answering the money question to
the satisfaction of potential backers is the key to the other questions.

Can You Protect Your Idea?

Once you've come up with tentatively satisfying answers to the originality,
production and distribution, and salability questions, it's time to consider
protecting your idea. After all, it looks like you may have something.

If you do have a patentable item, it's time to look into trying to protect
it under the patent laws. Here briefly are the steps you'll need to follow:

Get a close friend (who understands your invention) to sign his or her name
on a dated diagram or written description of the invention. Or, you can
file a "disclosure document" with the Patent Office. Taking one to these
measures will provide evidence of the time you came up with your invention
in case of a dispute with other inventors over who conceived it first.
Sending yourself a registered letter describing the invention is useless as
evidence. Filing a disclosure document does not give you any protection.
Get patent protection as soon as possible.

Make a patent search to see whether or not the invention has already been
patented in as good or better a version. You can make a search yourself.
The only place to make such a search efficiently is at the Patent and
Trademark Office in Arlington, Virginia. The staff at the Office will help
you. You may find, however, that the only practical way to proceed from
patent search on is with the help of a patent attorney.

If the invention has not been patented, prepare a patent application and
file it with the Patent and Trademark Office.

Again, you can do this yourself, following the pattern you find in similar,
recent patents, though, again, a patent attorney will be helpful. If you
have an attorney prepare your application, go through the exercise
yourself, anyway. Compare your application with your attorney's. Make sure
all of the points you regard as important are covered and that the attorney
has written what you want to say. Work out differences together.

Promptly file amendments or additional patent applications with the Office
if you make important changes in your invention.

Having a patent won't mean you have absolute protection. In fact, one
survey found that in over 70% of the infringement cases brought by patent
holders to protect their patents, the patent itself was held invalid.

Defending your patent can be very expensive. If you don't have a patent,
however, the probability of successfully protecting your invention
approaches zero.

Mere ideas or suggestions can't be patented. Some of these you may be able
to be put in patentable form, but for those that you can't it's pretty much
do-it-yourself. Consult with a patent attorney or the Patent Office about
the classes of patentable subject matter.

Say, for example, you think you have a great gimmick for selling more of
Company A's products. Leaving aside the likelihood that Company A won't be
interested, how do you approach Company A with your idea with any assurance
they won't simply use it without paying you a cent?

About the best you can do is write them a letter telling them you have a
promotional (or whatever) idea and, without giving them any details, offer
to send it to them. Include in your letter a statement to be signed and
returned by a Company A representative promising they won't divulge your
idea or make use of it without compensation (to be negotiated between them
and you), if they'd like to know the details of your plan. They'll probably
say thanks but no thanks or that they can't promise any such things without
seeing the idea, but it's the only course open to you.

Is There Any Hope?

Each section of this Aid seems to be packed with bad news, but the Aid
wouldn't be doing you any favors by raising false hopes. The point is, you
need to be more than an idea person to make money out of an invention or
other innovation.

Many small businesses have been doomed from the start because of false
hopes. Those of you who already operate going firms have avoided wishful
thinking in other business areas. You need to avoid it where innovation is
concerned, too.

What are potential idea and invention backers looking for? If you read
around in the subject, you'll run across many comments to the effect that:

What we want is an entrepreneur, someone who cannot only invent a product
but find capital and a way of getting the product on the market.

It's better to have a fair new product and a great manager than the other
way around.

Management is the most important element for success of an invention.

Edison wasn't only an inventing genius. He was also a promoting genius, a
publicity genius, a capital-raising genius, a genius at seeing potential
markets for inventions.

Have you ever heard of Joseph Swan? A strong case could be made for saying
he invented the electric light eight months before Edison. Who got the
patents? Who got the bulb to the market? Edison. Who invented the electric
light bulb? Edison.

Few of us are Edisons. We may have brilliant product ideas, but we aren't
usually knowledgeable, let alone brilliant, in all the of the areas that
need to be covered. We need help.

Where Can you Go for Help?

While you probably still have to invest considerable perspiration yourself,
you can get help with some of the sweating. Even Edison had some help.

Patent Attorneys and Agents. Attorneys and agents can help you make patent
searches and applications, if you can't do them yourself. The U.S. Patent
Office has geographical and alphabetical listings of such people, but
doesn't make recommendations or assume any responsibility for your
selection from their lists. You can also find attorneys and agents by
looking in the classified section of your telephone directory under
"Patents."

Invention Promotion Firms. Also likely to be listed in the "Patents."
section of the directory are firms that offer--for a fee--to take on
the whole job of protecting and promoting your idea. Caution is necessary
in dealing with such promoters.

Federal Trade Commission investigations found that one firm, which charged
fees ranging from $1,000 to $2,000, had ten clients who made money on their
inventions--that was out of a total of 35,000. Another firm with 30,000
clients had only three with successful inventions. If you elect to use an
idea promotion firm, make sure:

They can provide you with solid evidence of their track record--not just
a few flashy success stories, but verifiable statistics on the number of
clients they've had and the number who have actually made money.

They don't collect the entire fee in advance.

They will provide you with samples of their promotional materials and lists
of companies to whom they've sent it. (Then check with those companies
yourself.)

You check the promotion firm's reputation with the local Better Business
Bureau, Chamber of Commerce, a patent attorney, or a local inventors or
innovators club.

Invention Brokers. Brokers work for a portion of the profits from an
invention. They may help inventors raise capital and form companies to
produce and market their inventions. They often provide sophisticated
management advice. In general, you can expect these brokers to be
interested in more complex technology with fairly large sales potential.

University Innovation/Invention/Entrepreneurial Centers. These centers,
some funded by the National Science Foundation, show promise for helping
inventors and innovators. The best known one, the University of Oregon's
Experimental Center for the Advancement of Invention and Innovation (The
Innovation Center no longer exits), for example, evaluated an idea for a
very modest fee. The Center evaluated an idea on 33 criteria (listed
earlier in the Aid) to help inventors weed out bad ideas so they won't
waste further time and money on them.

The Center also identified trouble spots that required special attention
in planning the development or commercialization of a potential new
product. If an idea looked like it had merit and was commercially feasible,
the Center tried to link the innovator with established companies or
referred him or her to sources of funds.

The Small Business Administration. The SBA's Small Business Institutes
(SBI's) are located at more than 450 colleges and universities around the
country. While currently few SBI schools can provide much help with the
technical R & D aspects of innovations, they certainly can provide the
market research, feasibility analysis, and business planning assistance
necessary to make an innovation successful.

SBA field offices (see your local telephone directory under "U.S.
Government") can provide you with information about the SBI program. You
may find other management assistance programs offered at the field offices
of help in realizing your idea as well.

National Bureau of standards. The Office of Energy-Related Inventions in
the U.S. Department of Commerce's National Bureau of Standards will
evaluate non-nuclear energy-related inventions and ideas for devices,
materials, and procedures without charge. If the office finds that the
invention or idea has merit, it will recommend further study by the
Department of Energy. The Department of Energy may provide support for the
invention if it shows promise. This process may take from nine months to a
year.

Inventor's Clubs/Associations/Societies. You may have such clubs in your
locality. You can share experiences with kindred spirits and get good
advice, low cost evaluation, and other help.

Talking with other inventors is probably the most helpful thing you can do.
Find someone who has been through the entire routine of patents, applied
R&D, and stages of financing. It doesn't matter if the end result was a
financial success or failure. Getting the nitty-gritty of the process is
what's important.

Are You Being Unreasonable About Your Chances?

If you have read this Aid and still think you can make money with your
idea, some people might think you've missed the point. If you continue to
believe in your idea after looking at the odds and obstacles, you ore being
unreasonable.

That's exactly what you should be. You're in good company.

All progress is made by unreasonable people, George Bernard Shaw observed.
Reasonable people adapt to the world around them; unreasonable people try
to change it.

