<!-- www.service.com/tcc/corp6.html -->
<header>
<title>Common Questions About Incorporating</title>
</header>
<body bgcolor="#ffffff">  

<hr>
<center>
<table border=0>
<tr> 
<td>&lt;--<a href=/tcc/select2.html>Previous Page</a></td> 
<td>&nbsp;</td>
<td><a href=/tcc/home.html>Home</a></td>   
<td>&nbsp;</td>
<td><a href=/tcc/select4.html><b>Incorporate NOW!</b></a></td> 
<td>&nbsp;</td>
<td><a href=/tcc/tccorder.html>Free Brochure</a></td>
<td>&nbsp;</td>
<td><a href=/tcc/tccorder.html>Next Page</a>--&gt;</td> 
</tr> 
</table>
</center>
<hr>
<p>


<h1>Q & A : Common Questions About Incorporating and Forming an LLC</h1><p>

<dl>
<dt><strong>What is a registered agent and why do I need one?</strong>
<dd>State laws require that a location that is open and staffed during business hours
be designated in the state of incorporation to receive and forward all official documents
from the state or service of process and other legal notifications.  Also a registered agent will
forward franchise tax reports to your corporation.  
<b>As your registered agent, TCC can:</b>
<br>- obtain certified copies of documents filed with the state.
<br>- order Certificates of Good Standing for the corporation.
<br>- file amendments and other documents.
<br>- provide ongoing customer support to your corporation.
<br>- answer your questions about, and help with any changes to your corporate status.
<p>

<dt><strong>What's the difference between an "S" corporation and a "C" corporation?</strong>
<dd>An "S" corporation is a very attractive entity. Available to small
companies (up to 75 shareholders, all of whom must be citizens or
residents of the U.S), it provides the benefits of incorporation, while
eliminating "double taxation." Rather than being taxed at the corporate
level, profits and losses are included in your personal return
(claiming business losses can reduce your personal tax bill -
especially in the early years of a company). These extra tax advantages
are not available to shareholders in a regular "C" corporation.  An "S"
corporation though can only issue one class of stock.  TCC can apply for
<a href="/tcc/corp5.html#a">"S" status</a> for your corporation for the nominal fee of $35.

<p>

<dt><strong>If I incorporate in Delaware, can I do business in another U.S. state?</strong>
<dd>Yes. The vast majority of our clients incorporate in Delaware and
also choose to formally qualify to do business as a "foreign
corporation" in other U.S.  states. We would be happy to assist you
with those filings as well. 
Please see our section on <a href=/tcc/corp13.html>qualifications</a> or
call our representative toll free
in the U.S. at 1-800-542-2677 ext. IDS for state qualification fees and 
further information. International customers call 001-302-575-0440 ext. IDS.
<p>

<dt><strong>What income taxes do I have to pay to the state of Delaware?</strong>
<dd>None for out-of-state shareholders. Delaware collects an annual
Franchise Tax based on the number of shares of stock and par value of
each share. Minimum tax for 1500 shares at no par value is $30 plus $20
for the annual Franchise Tax Report. Sign up for our 
<a href=/tcc/corp5.html#b>Tax-on-Time service</a>, 
and we'll take care of this for you.
<p>

<dt><strong>Is 1313 N. Market Street my legal
mailing address in Delaware?</strong>
<dd>Yes and no. Yes for receipt of any legal documents, such as a
summons or subpoena or any documents from the Secretary of State. No
for general mail. We do provide a separate 
<a href=/tcc/corp5.html#c>Mail Forwarding Service</a>
at a small additional cost. Call for details toll free in the U.S. at 
1-800-542-2677 ext. IDS or send email to corp@incorporate.com.  
International customers call 001-302-575-0440 ext. IDS.
<p>

<dt><strong>Does one stock certificate represent one share of stock?</strong>
<dd>One stock certificate can represent any number of shares up to the amount
authorized, as stated on the Certificate of Incorporation.
<p>

<dt><strong>How many directors and officers do I have to have?</strong>
<dd>Only one officer or director if you are incorporating in Delaware. Other
states vary in their requirements, but usually require no more than three. We
can advise you by phone.
<p>

<dt><strong>What is the difference between PAR and NO PAR value stock?</strong>

<dd>Par value stock has a stated value on its face which is the minimum amount
contributed by the shareholder. No par value stock has no stated value, allowing
the company to issue it for any amount per share which the Board of Directors
determines to be appropriate.
<p>

<dt><strong>How much is my Registered Agent service and when do I pay it?</strong>

<dd>Registered Agent service is based on a calendar year, due January 1
of each year. First year in Delaware is $45 (this amount accompanies
your incorporation documents), second year is $75 and the third year
the fee is $99. For companies incorporated in all other states, the fee
is $150 the first year, $125 thereafter.
<p>
<dt><strong>Is there a fee for you to reserve the corporation name in Delaware?</strong>

<dd>No. We provide this service at no charge. Through our direct electronic
access to the state's database, we can immediately advise you if a name is
available and reserve it for you. For a fee of only $20 plus state filing fees,
we can reserve a corporate name for you in any other state. It usually takes a
minimum of 2 weeks to reserve a name in other states. 
<p>

<dt><b>What is the difference between an S Corporation and an LLC?</b>
<dd>The most important differences are the following:
<p>
<dd>a. An S Corporation has <b>most</b> of the tax benefits of operating as an individual
proprietorship or a partnership but LLC has <b>all</b> of the tax benefits.
<p>
b. The number (75) and types of investors in an S Corporation are limited by the Internal
Revenue Code.  There are no comparable limitations on LLC's.
<p>
c. S Corporations usually have perpetual life.  The duration of an LLC is initially
limited to a period of not more than 30 years, but the members may later extend the duration.
<p>

<dt><b>How may persons do you need to form an LLC?</b>
<dd>Delaware and several other states require only one.  Other states require at least two.
<p>

<dt><b>How do you show ownership in an LLC?</b>
<dd>An LLC can issue certificates showing the holders' percentages of ownership.
<p>

<dt><b>Does an LLC have stock?</b>
<dd>In most cases an LLC cannot issue stock.
<p>

<dt><b>Why does there have to be a dissolution date for "LLC's"?</b>
<dd>For federal tax purposes an LLC must be more like a partnership than a corporation.
Corporations generally have perpetual life.
<p>

<dt><b>Does an LLC offer the same protection as a Corporation?</b>
<dd>Yes.  Both provide owners with Limited Liability.
<p>

<dt><b>Can I switch my present Corporation to an LLC?</b>
<dd>Not without some tax risks.  Do not attempt to do so without consulting your tax advisor.
<p>

<dt><b>After an LLC is formed can it take on new members?</b>
<dd>Yes. There are no limitations as to when members can join.
<p>

<dt><b>Can Foreigners be managers in a LLC?</b>
<dd>Yes.  There are no restrictions as to who can be a member.
<p>

<dt><b>Can an LLC own an S Corporation?</b>
<dd>No.  Only individuals and specified kinds of trusts may be shareholders in a S Corporation.

</dl>

<p>
<strong>MORE QUESTIONS? WANT IMMEDIATE ANSWERS?</strong> Call us toll free
in the U.S. at 1-800-542-2677 ext. IDS, Fax us at 302-575-1346, Dept. IDS, or
e-mail to corp@incorporate.com.  International customers call 001-302-575-0440
ext. IDS.
<p>


<pre>

</pre>
<hr>
<center>
<table border=0>
<tr> 
<td>&lt;--<a href=/tcc/select2.html>Previous Page</a></td> 
<td>&nbsp;</td>
<td><a href=/tcc/home.html>Home</a></td>   
<td>&nbsp;</td>
<td><a href=/tcc/select4.html><b>Incorporate NOW!</b></a></td> 
<td>&nbsp;</td>
<td><a href=/tcc/tccorder.html>Free Brochure</a></td>
<td>&nbsp;</td>
<td><a href=/tcc/tccorder.html>Next Page</a>--&gt;</td> 
</tr> 
</table>
</center>
<hr>
 
<p>


</body>

