<!-- www.catalog.com:80/napmsv/edi.htm -->
<HTML>
<HEADER>
<title>Getting Started with EDI</title>
</HEADER>
<BODY>
<img src="rain_lin.gif"><p>
<img src="pw.gif"><p>
<img src="rain_lin.gif"><p>
<h3>Getting Started with EDI</h3><p>
 
<address><b>Susan Stecklair</b> is president of Silicon Valley Solutions, a company that<br>
specializes in EDI implementations and training.  Please send your comments,<br>
questions, or suggestions to her at (408) 996-7492 or at Susanps@AOL.COM</address><p>
<img src="black_li.gif"><p>

<h4>Are you in charge of the new EDI program?  Did your biggest <br>
customer just dictate that you must be on EDI before April of next <br>
year?  What is EDI?  How can you get started?</h4>

<img src="black_li.gif"><p>

<b>Definitions  </b><br>

EDI is the acronym for Electronic Data Interchange and is defined as the<br>
computer-to-computer exchange of business documents in a standard format.  <br>
A common example in Silicon Valley is the use of a Purchase Order standard<br>
developed by the Accredited Standards Committee (ASC) X12.  This document<br>
consists of an outer "envelope" containing the sender and receiver ID's which<br>
indicates the "mailbox" for the network. Within the body of the transmission<br>
there is a series of segments, and standard codes which defines the data such<br>
as the part number, cost, tax info, shipment methods, bill-to-location,<br>
ship-to-location, and contacts.   The beauty of this EDI PO is that it can be<br>
generated from a MRP system, sent via networks to the supplier, processed by<br>
the supplier's system, and create pick-lists before the normal work day<br>
begins.  Human intervention may occur if there is a hot PO to expedite, or if<br>
an exception report indicates a problem.  The cost savings from this type of<br>
implementation can be quite impressive.  Transaction intense programs such as<br>
Just-in-Time (JIT) are augmented by and often necessitate EDI programs.<br>
AMD's Vice-President of Distributor Operations, <b>Al Frugaletti</b>, credited AMD's<br>
EDI program with developing stronger ties with their distributors, improving<br>
business processes, and shrinking their cycle-time from 8 weeks to 5 days.   <br><p>

Electronic Commerce is a broader umbrella which includes EDI as-well-as<br>
technologies such as bar-coding, electronic imaging, FAX'ing, and satellite<br>
communications.  The Electronic Commerce capabilities will expand as the<br>
X.400 communications protocols become more common.   X.400 and X.435 <br>
allows for a group of information to be transmitted for one item.  In a <br>
hypothetical case of a health-care transaction, one transmission could <br>
contain information on John Smith such as a digitized version of his X-ray, <br>
a voice-message explaining his X-ray, an EXCEL file, a FAX with the written <br>
diagnosis, and an EDI invoice for John Smith's radiology services.<br><p>

<b>Publications</b><br>

A valuable EDI resource is the magazine <i>EDI World</i> (305-925-7533) <br>
which is published monthly at a cost of $29.95 for twelve issues.  The<br>
November 1994 issue contains a list of Regional Users Groups (more on <br>
this later); available books and videos; a list of EDI translation products; <br>
and a transportation directory of major air, rail, motor and ocean carriers and<br>
their detailed usage of EDI.  EDI World has also published a directory of<br>
translation software packages including basic package cost, typical<br>
time-to-install, annual maintenance costs, source code, and supported<br>
computer platforms.  <br><p>  

Another gem is <b>EDI FORUM: The Journal of Electronic Commerce</b><br>
(708-848-0135).  The magazine is published quarterly with a $250 annual<br>
subscription fee, and tends to be more academic and global in nature.  Each<br>
issue contains a theme.   Recent issues have focused on the following:  "EDI<br>
and Reengineering", "Global EDI", and "EDI in Healthcare and Insurance."<br>
Future themes will include:  "EDI Products and Services", "Retail EDI", and<br>
"EDI and the Government".<br><p>

<b>Standard Publications</b><br>

The bibles for the EDI standards are published by Washington Publishing<br>
Company (800-334-4912).  They publish the ASC X12 and UN/EDIFACT <br>
(global) Standards.  The cost for the ASC X12 (Version 3.4) is $365 for a <br>
non-member. You should not attempt an EDI program without the official <br>
standards. Washington Publishing Company also publishes the valuable <br>
implementation guidelines for specific transactions developed by various <br>
industry groups. <br><p>

<b>Regional User Groups</b><br>

One avenue for insight and support for your new EDI program is the Northern<br>
California EDI Users' Group.  It holds quarterly morning meetings at various<br>
locations in the Bay Area.   For example, the May 1994 meeting was at HP in<br>
Santa Clara and had presentations on "EDI Infrastructure" and "Advanced Ship<br>
Notices".  The recent September meeting was at the World Trade Club in San<br>
Francisco with presentations on "Global Logistics", "Business Realities of<br>
the 90's", and "Global Supply Chain Management."  The cost to join is free.<br>
FAX <b>Sigrid Marmann</b> (415-258-0256) to get on the mailing list for future<br>
events.  These meetings are especially useful for organizations on a tight<br>
budget.  Employees impacted by EDI can attend the meetings and get a flavor<br>
of what other organizations and industries are doing.  They are exposed to<br>
the terminology, create contacts, and generate new ideas.  <br><p>

We are especially fortunate that there are a number of different types of<br>
industries doing EDI in the area.  Some, such as the retail and<br>
transportation industries, are typically more advanced in their usage of EDI<br>
than "high tech" electronic or software firms.<br><p>

<b>Industry Groups</b><br>

Another source of valuable contacts and information is your EDI Industry<br>
Group.  Nearly 60 industry groups have been formed to create a forum for the<br>
discussion of EDI and related issues specific to their industry.  The<br>
administrative arm which maintains the list of the industry groups and<br>
regional user groups is the <i>Data Interchange Standards Association, Inc.</i><br>
(DISA) at (703) 548-7005.   Joining an industry group is highly recommended<br>
as part of developing an EDI implementation plan.  The cost may be several<br>
thousand dollars, plus travel expenses for the key members of your EDI team.<br>
However, EDI project managers agree that they gain valuable insights and<br>
training by attending these meetings.  Generally, the more senior members of<br>
the EDI project team attend these meeting as compared to the Regional Users'<br>
Groups. <br><p>

As a service, the <i>Electronic Industry Data Exchange Association</i> (EIDX)<br>
offers a reference outline for trading partner information, a resource<br>
packet, a presentation on  "EDI 101", an AMA trading partner agreement<br>
sample, and a "EDI Yellow Pages" subscription.  Current discussions/workshops<br>
include:  "EDI Control Issues", "EDIFACT Training and Mapping", "EDI Basics",<br>
"Distributor Alliance", and "Internet EDI".   <br><p>

Following is a sample of EDI industry groups.  For a more comprehensive list,<br>
contact DISA at (703) 548-7005.<br><p>

<b>Group Contact Phone Number</b><br>

Electronics (EIDX) Patty Rusher (703) 527-7002<br>
Financial  (NACHA) Deborah Shaw (703) 742-9190<br>
Health  (HIBCC) Robert Hankin (602) 381-1091<br>
Retail (UCC) Christine Frapwell (513) 435-3870<br>
Transportation  (ATA/MSC) Douglas Anderson (703) 838-1721<br><p>

<b>Get Ready</b><br>

Many of you are already involved in EDI processes.  For those of you not<br>
involved, get ready!  On October 28, 1993, <b>President Clinton</b> issued a<br>
memorandum on "Streamlining Procurement Through Electronic Commerce."  In<br>
this memorandum he states, "I am committed to fundamentally altering and<br>
improving the way the Federal Government buys goods and services by ensuring<br>
that electronic commerce is implemented for appropriate Federal purchases as<br>
quickly as possible. ...by September 1994, establish an initial electronic<br>
commerce capability to enable the Federal Government and private vendors to<br>
electronically exchange standardized requests for quotations, quotes,<br>
purchase orders, and notice of awards and begin Government-wide<br>
implementation...."  This commitment to implement electronic commerce by the<br>
U.S. Government will impact the more than 300,000 vendors currently doing<br>
business with the U.S. Government. This decision is anticipated to have an<br>
enormous rippling effect throughout the economic community, as these vendors<br>
to remain competitive also request EDI and electronic processing by their<br>
supplier base. <br><p>

<address><b>Susan Stecklair</b> is president of Silicon Valley Solutions, a company that<br>
specializes in EDI implementations and training.  Please send your comments,<br>
questions, or suggestions to her at (408) 996-7492 or at Susanps@AOL.COM</address><p>

<i>Copyright, Silicon Valley Solutions, 1994</i><p>
 
<img src="rain_lin.gif"><p>
<a href="welcome.html"><img align=middle src="home.gif"> NAPM - Silicon Valley Home Page</a><p>

<img src="rain_lin.gif"><p>
Copyright 1994<br>
<strong>National Association of Purchasing Management - Silicon Valley, Inc.</strong><br>
P.O. Box 32156, San Jose, CA 95152-2156<br>
Phone : 408 / 929 - NAPM [ 6276 ], FAX : 408 / 929 - 6277<br>
Email : napmsv@aol.com , URL : http://www.catalog.com/napmsv<br><p>

You may freely view this material through web browsers and <br>
related communications programs.<br>
All other rights reserved.<p>

<img src="rain_lin.gif"><p><p>
<address>Last update, December 13, 1994</address>

</BODY>
</HTML>





