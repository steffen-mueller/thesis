<!-- www.mel.nist.gov/msid/sima/mse.htm -->
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<TITLE>Systems Integration for Manufacturing
Applications Program:  PROJECTS (MSE)</TITLE>

<META NAME="GENERATOR" CONTENT="Internet Assistant for Microsoft Word 2.04z">
</HEAD>
<BODY BGCOLOR="FFFFFF">
<P>
<A NAME="BM560330"><FONT SIZE=5>Systems Integration for Manufacturing
Applications</FONT></A><FONT SIZE=5> <BR>
(<a href="http://www.nist.gov/sima">SIMA</a>) Program: PROJECTS<p>
</FONT>
<P>
<a href="http://www.nist.gov/sima"><img src="simaljsp.gif" BORDER=0></a><P>


<P>
<B><FONT SIZE=4>Manufacturing Systems Environment</FONT></B></A>
<P>
<A NAME="OLE_LINK1"><B><FONT SIZE=2>(see also <A HREF="ttte.htm"><B>Testbeds and Technology Transfer Environment </B></A><B>(TTTE)
&amp; <A HREF="sde.htm"><B>Standards Development Environment </B></A>(SDE))</B></FONT></B></A>
<UL>
<LI>[<A HREF="#mse1">MSE1</A>] Electronic Commerce of Component
Information (ECCI)
<LI>[<A HREF="#mse2">MSE2</A>] Information Protocols for Design
<LI>[<A HREF="#mse3">MSE3</A>] Framework Integration
<LI>[<A HREF="#mse4">MSE4</A>] Manufacturing Enterprise Integration
<LI>[<A HREF="#mse5">MSE5</A>]  Operator Interfaces for Virtual
and Distributed Manufacturing
<LI>[<A HREF="#mse6">MSE6</A>]Process Planning Applications
<LI>[<A HREF="#mse7">MSE7</A>] Process Plant Engineering and Construction
<LI>[<A HREF="#mse8">MSE8</A>] Production and Product Data Management
<LI>[<A HREF="#mse9">MSE9</A>] Rapid Response Manufacturing (RRM) Intramural
<LI>[<A HREF="#mse10">MSE10</A>] Reference Model Architecture
<LI>[<A HREF="#mse11">MSE11</A>] STEP Application Protocols for
Polymers Test Data
<LI>[<A HREF="#mse12">MSE12</A>] STEP for the Process Plant Industry
<LI>[<A HREF="#mse13">MSE13</A>] Supply Chain Integration
<LI>[<A HREF="#mse14">MSE14</A>] Unified Process Specification
Language
<LI>[<A HREF="#mse15">MSE15</A>] Virtual Environments and Visualization
for Manufacturing <BR>
</UL>
<P>
<A NAME="mse1"><B>[<A NAME="mse1">MSE1</A>] Electronic
Commerce of Component Information (ECCI)</B></A>
<br>
<Address>
Project Manager: James St. Pierre
<A HREF="mailto: james.st.pierre@nist.gov">  james.st.pierre@nist.gov</A>
</ADDRESS>
Telephone: (301) 975 - 4124
<P>
<B>Summary Description:</B> The objective of this project is to actively contribute to the technical development of neutral product data exchange specifications and
component information infrastructure for the electronics industry.
Towards this goal the project works to resolve conflicts among
competing and conflicting standardization efforts within the electronics
industry. ECCI staff work with industry and other government laboratories
to promote the transfer of technical information between the manufacturers
of electronic parts and those who need parts for the design, manufacture,
and repair of electronic systems. The scope of work includes research,
development of prototype solutions, and information specification
development.<BR>
<P>
<A NAME="mse2"><B>[<A NAME="mse2">MSE2</A>] <A HREF="http://www.nist.gov/msid/projs/97projs/infexch3.htm">Information Protocols
for Design</B></A>
<br>
<Address>
Project Manager: Ram Sriram<A HREF="mailto:ram.sriram@nist.gov">  ram.sriram@nist.gov</A>
</ADDRESS>
Telephone: (301) 975 - 3507
<br>
<P>
<B>Summary Description:</B> The objective of
this project is to develop standards and protocols enabling interoperability
among design applications and between design applications and
other engineering applications, such as process planning and production
engineering. An international standard -- called STEP -- has been
focusing on developing various protocols for exchanging product
data. However, STEP does not fully address the advanced information
exchange requirements necessitated by leading-edge CAD and CAM
systems. Further, emergence of new computer-based design support
systems will introduce additional interoperability issues. This
project aims to extend STEP and related standards to address the
above problems. The focus will be on design content and knowledge/design
rationale representation. Specific tasks address constraint and
feature-based information (parametric representations), a solid
interchange format for rapid prototyping machines, engineering
ontologies, design and process planning interfaces, and interfaces
between virtual reality-based CAD and traditional CAD systems.
<BR>
<P>
<A NAME="mse3"><B>[<A NAME="mse3">MSE3</A>] <A HREF="http://www.nist.gov/msid/projs/97projs/framewrk.htm">Framework Integration</B></A>
<br>
<Address>
Project Manager: Neil Christopher<A HREF="mailto:neil.christopher@nist.gov">  neil.christopher@nist.gov</A>
</ADDRESS>
Telephone: (301) 975 - 3888
<P>
<B>Summary Description:</B> The objective of
this project is to analyze, implement, validate,
and report on standards and pre-normative specifications developed
by industry. This project performs detailed analyses of industry
developed specifications as they apply to manufacturing systems
integration. Validation testing of these specifications against
manufacturing operations in NIST's laboratory setting will be
performed. The installation of an object oriented communications
infrastructure for use by the other projects of the SIMA MSE program
will be completed. The project will also work to develop a framework
of specifications based on enterprise integration modeling requirements
emerging from ISO TC184 /SC5 /WG1.<BR>
<P>
<A NAME="mse4"><B>[<A NAME="mse4">MSE4</A>] <A HREF="http://www.nist.gov/msid/projs/97projs/mfgent2.htm">Manufacturing Enterprise Integration</B></A>
<br>
<Address>
Project Manager: Jim Nell<A HREF="mailto:jim.nell@nist.gov">  jim.nell@nist.gov</A>
</ADDRESS>
Telephone: (301) 975 - 5748
<P>
<B>Summary Description:</B> Manufacturing enterprises
are seeking ways to improve their infrastructure and processes
to enable more effective operation in a more agile, virtual-enterprise
mode. The investment justification for doing this varies by specific
company, but reasons focus upon higher quality products, better
repeatability, lower cycle time, and more customized production
capability. Re-engineering should not occur in a vacuum. There
should be careful analysis to define the total domain of enterprise
representation. Also needed is a guideline regarding how to subdivide
the domain logically. This project will define a scenario for
representing an enterprise according to a framework, an architecture,
and models. The project will also address life cycles of enterprises
and models. The program will identify what, of the above, should
be standardized. Much work is being done on this challenge by
consortia around the world. This project will coordinate the work
to the extent that existing groups and groups planning to work
a sector of the domain are able to identify what has or is being
done so that they can leverage their work.<BR>
<P>
<A NAME="mse5"><B>[<A NAME="mse5">MSE5</A>] <A HREF="http://isd.cme.nist.gov/brochure/OperatorInterface.html">Operator Interfaces
for Virtual and Distributed Manufacturing</B></A>
<br>
<Address>
Project Manager: Ernie Kent<A HREF="mailto:ernest.kent@nist.gov">  ernest.kent@nist.gov</A>
</ADDRESS>
Telephone: (301) 975 - 3460
<P>
<B>Summary Description:</B> The objective of
this project is to develop interface specifications enabling monitoring
and control of geographically distributed manufacturing systems.
The operator interface to such manufacturing systems are virtual
renditions of the processes being monitored and controlled. The
operator interface specifications resulting from this project
will be based on methods for collecting and presenting information
required to interact with remote manufacturing systems via wide-area
networks.<BR>
<P>
<A NAME="mse6"><B>[<A NAME="mse6">MSE6</A>] <A HREF="http://www.nist.gov/msid/projs/97projs/ap2132.htm">Process Planning
Applications</B></A>
<br>
<Address>
Project Manager: Shaw Feng<A HREF="mailto:shaw.feng@nist.gov">  shaw.feng@nist.gov
</ADDRESS>
</A>
Telephone: (301) 975 - 3551
<P>
<B>Summary Description:</B> The objective of
this project is to develop and test information specifications
enabling integration of various manufacturing applications with
process planning systems. There are two principal areas of work
in the project. The first is to define a preliminary framework
of open interface specifications for collaborative design and
process planning. This framework incorporates the draft STEP application
protocol for feature-based design and process planning (&quot;AP224&quot;),
a message model, and a process planning application programming
interface. The second principal area of work is to test, validate,
improve, and advance the draft STEP application protocol for process
plans for numerical controlled (NC) machining (&quot;AP213&quot;)
to international standard status. As part of this effort, testing
of the AP213 specification will be performed through pilot implementations
with process planning and NC programming systems vendors. <BR>
<P>
<A NAME="mse7"><B>[<A NAME="mse7">MSE7</A>] Process Plant
Engineering and Construction</B></A>
<br>
<Address>
Project Manager: Kent Reed<A HREF="mailto: kent.reed@nist.gov"> kent.reed@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 5852
<P>
<B>Summary Description:</B> The objective of
this project is to work with U.S. industry to develop a technical
capability to represent and exchange information supporting the
design and construction of structural systems using internationally
accepted protocols. The U.S. construction industry seeks to improve
its use of computerized systems through integration, e.g., automation
of the exchange and sharing of information among systems. The
evolving international standard ISO 10303---Product Data Representation
and Exchange---known as STEP, is providing the base technology.
The U.S. structural engineering community has recognized the advantages
of STEP and seeks to make STEP exchange viable in its computerized
design, analysis, and fabrication systems.<BR>
<P>
<A NAME="mse8"><B>[<A NAME="mse8">MSE8</A>] <A HREF="http://www.nist.gov/msid/projs/97projs/simapp2.htm">Production and
Product Data Management</B></A>
<br>
<Address>
Project Manager: Chuck McLean<A HREF="mailto:charles.mclean@nist.gov">   charles.mclean@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 3511
<P>
<B>Summary Description:</B> The objective of
this project is to develop models, interfaces, techniques, and
prototypes for integrating production system engineering, production
management, simulated production facilities, and product data
management systems with each other and with other manufacturing
applications and support systems. This project has three sub-projects:
1) integration of production management systems; 2) integration
of production system engineering applications (e.g., plant layout
with manufacturing simulation); and 3) integration of product
data management with other manufacturing applications. As part
of the production management subproject, the focus will be on:
1) validation, consensus building, and technology transfer of
an information model and messaging specification, and 2) the development
of additional specifications for work orders, routings, schedules,
and dispatch lists. As part of the production system engineering
subproject, the focus will be on: 1) development of a plant layout
to simulation interface, and 2) developing the assembly process
specification to simulation interface. The product data management
subproject will focus on the development of an interface to product
data management systems to support version control, authorization,
references, engineering change orders and document management
states.<BR>
<P>
<A NAME="mse9"><B>[<A NAME="mse9">MSE9</A>] <A HREF="http://www.nist.gov/rrm/">Rapid Response Manufacturing (RRM) Intramural</B></A>
<br>
<Address>
Project Manager: Kevin Jurrens<A HREF="mailto:jurrens@nist.gov">   jurrens@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 5486
<P>
<B>Summary Description:</B> The objective of
this project is to facilitate standardization and implementation of a proposed electronic representation for manufacturing resource data. The representation will cover
a limited scope of manufacturing resource data, including milling
and turning machine tools; cutting tools appropriate to the processes
of milling, drilling, boring, reaming, tapping, and turning; cutting
tool inserts; and the tool holding and assembly components required
to mount the cutting tools to the machines. ISO and ANSI standardization
efforts for cutting tool data have been initiated with NIST staff
serving in key roles. Industrial collaborations have been formed
to review and implement the MR data structure.<BR>
<P>
<A NAME="mse10"><B>[<A NAME="mse10">MSE10</A>] <A HREF="http://isd.cme.nist.gov/brochure/RefModelArch.html">Reference Model
Architecture</B></A>
<br>
<Address>
Project Manager: Harry Scott<A HREF="mailto: harry.scott@nist.gov"> harry.scott@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 3437
<P>
<B>Summary Description:</B> The objective of
this project is to develop a reference model architecture for
intelligent control of manufacturing processes. Further, the project
will demonstrate, validate and evaluate the reference model architecture
through analysis and performance measurements of a simulated and
prototype implementation. A first implementation of the architecture
uses an inspection workstation facility as a testbed. This project
builds on the work to date in examining and understanding alternative
architectural approaches and required functionality for implementing
intelligent control systems in the manufacturing domain. In particular,
the architecture will bring together multiple manufacturing system
architectural elements under a single consistent reference specification
with standard interfaces developed or adopted for communication
between architectural components.<BR>
<P>
<A NAME="mse11"><B>[<A NAME="mse11">MSE11</A>] STEP Application
Protocols for Polymers Test Data</B></A>
<br>
<Address>
Project Manager: Joe Carpenter<A HREF="mailto: joseph.carpenter@nist.gov">    joseph.carpenter@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 6397
<P>
<B>Summary Description:</B> The objective
of this project is the development of a formal ISO standard
for exchanging data generated by polymers testing. Other existing
or developing standards provide prototype tests of the exchange
of materials information in a superficial way (mostly a material's
name, specification (if any), and limited properties), they do
not provide the details of the data resulting from materials testing.
Specifically the project intends to work in conjunction with industry
to develop an ISO 10303 specification (i.e., an application protocol)
aimed at handling polymer test data collected according to industry-adopted
methodologies.<BR>
<P>
<A NAME="mse12"><B>[<A NAME="mse12">MSE12</A>] STEP for the
Process Plant Industry</B></A>
<br>
<Address>
Project Manager: Mark Palmer<A HREF="mailto: mark.palmer@nist.gov">    mark.palmer@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 5858
<P>
<B>Summary Description:</B> The objective of
this project is to work with U.S. industry to develop, test, and
demonstrate a coordinated suite of ISO 10303 (STEP) application
protocols that meet the information needs of the process plant
industries. The U.S. process plant industries seek to improve
their use of computerized systems through integration of information
systems, e.g., automation of the exchange and sharing of information
among systems. The many computerized systems in use can be integrated
only at great cost because of their incompatible proprietary representations
for information. Standard, neutral information representations
and exchange methods are needed that allow system vendors to be
innovative and yet allow system users to exchange and share information
about process plants coherently. In NIST workshops and industry
meetings, owners, engineers and constructors, information technology
providers, and parts suppliers identified the design and fabrication
of plant piping systems and the delivery of process engineering
information as top priorities for application protocol development.
Starting with these requirements, industry and NIST are working
to develop these specifications and to participate in the development
of other application protocols needed by the process plant industries.
<BR>
<P>
<A NAME="mse13"><B>[<A NAME="mse13">MSE13</A>] Supply Chain
Integration</B></A>
<br>
<Address>
Project Manager: Mary Beth Algeo<A HREF="mailto: mary.algeo@nist.gov">    mary.algeo@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 2888
<P>
<B>Summary Description:</B> The objective of
this project is to identify potential information exchange and
interface protocol solutions in order to improve supply chain
integration. Supply chain integration equates to getting the right
information to the right people in the supply chain at the right
time, thereby achieving equilibrium between supply and demand.
An optimized supply chain is one which moves a product from the
point-of-origin to that of consumption in the least amount of
time and at the smallest cost. The thrust of this project is to
develop a broad understanding of supply chain integration and
its implications on manufacturing systems. This project will provide
an overall view of supply chain integration and will seek to identify
those technology and standards gaps which could be addressed in
future work.<BR>
<P>
<A NAME="mse14"><B>[<A NAME="mse14">MSE14</A>] <A HREF="http://www.nist.gov/psl/">Unified Process
Specification Language</B></A>
<br>
<Address>
Project Manager: Amy Knutilla<A HREF="mailto:knutilla@nist.gov"> knutilla@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 3514
<P>
<B>Summary Description:</B> The objective of
this project is to identify or create a process representation
that is common to all manufacturing applications, generic enough
to be decoupled from any given application, and robust enough
to be able to represent process information for any given application.
This project is designed specifically to promote outreach and
consensus through activities such as the establishment of electronic
discussion lists, active industry review and feedback, and on-line
project status through the World Wide Web.<BR>
<P>
<A NAME="mse15"><B>[<A NAME="mse15">MSE15</A>] <A HREF="http://www.itl.nist.gov/div894/ovrt/">Virtual Environments
and Visualization for Manufacturing</B></A>
<br>
Project Manager: Sandy Ressler<A HREF="mailto:sressler@nist.gov"> sressler@nist.gov
</ADDRESS></A>
Telephone: (301) 975 - 3549
<P>
<B>Summary Description:</B> The objective of
this project is to create new visualization environments which
assists the manufacturing industry in achieving systems integration
of manufacturing applications. This is being accomplished by enhancing
several SIMA applications through Virtual Environment (VE) technology
of several kinds. Current limitations of both immersive and non-immersive
VE technology are being examined. VE standards which will enhance
the manufacturing process are being investigated as well. In addition
to &quot;traditional&quot; VE using a computer generated polygonal
environment, image based VEs are being explored. Existing proprietary
software tools are being used as mechanism to create portable
interoperable environments using non-proprietary standard representations
such as VRML. Finally, the introduction of virtual humans into
the factory floor will enhance the dying practice of ergonomic
and human motion time studies, a necessary factor in the design
of production spaces.<BR>
<P>
<ADDRESS>
For more information on the SIMA Program, contact SIMA Program Manager Jim Fowler at
<A href="mailto:jefowler@nist.gov">jefowler@nist.gov</A>.<BR>
</ADDRESS>
<P>
<a href="http://www.nist.gov/sima"><img src="simaljsp.gif" BORDER=0></a><P>
<FONT SIZE=2>We welcome your <A href="comments.htm"><I>comments &amp; suggestions</A> about these pages. </I></FONT><BR>
<A href="/msidstaff/parker.julie.html"><FONT SIZE=2>Julie Parker</FONT></A><FONT SIZE=2>, <A href="mailto:webmaster@cme.nist.gov">Webmaster</A></FONT> and by <A href="/msidstaff/bthomas.htm"><FONT SIZE=2>Brenda L. Thomasson</FONT></A>SIMA Program Secretary<BR>
<FONT SIZE=2>Revised March 17, 1997.</FONT>

<HR size=6>
<P>
<B><FONT SIZE=2>U.S. Department of Commerce<BR>
</FONT></B><FONT SIZE=2>Technology Administration<BR>
National Institute of Standards and Technology<BR>
Manufacturing Engineering Laboratory </FONT>

</BODY>
</HTML>


