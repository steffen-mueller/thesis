<!-- www.mel.nist.gov/msid/projs/97projs/advsys3.htm -->
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<TITLE>ADVANCED MANUFACTURING SYSTEMS AND NETWORKING TESTBED (AMSANT) Project Page
</TITLE>

<META NAME="GENERATOR" CONTENT="Internet Assistant for Microsoft Word 2.04z">
</HEAD>
<BODY>
<P>
<B>Title: ADVANCED MANUFACTURING SYSTEMS AND NETWORKING TESTBED
(AMSANT)</B> 
<P>
<B>Staff:</B> (1.5 years) ROBERT DENSOCK, *Don Libes, *Micky Steves,
*Vacant
<P>
<B>Objective and MEL Thrust(s) Supported:</B> The objective of
the AMSANT project is to provide a distributed, multinode facility
at NIST that will enable collaborative development of technologies
and standards to support distributed and virtual manufacturing
enterprises through an advanced computing and communications infrastructure.<I>
Improving Operational Effectiveness</I> 
<P>
<B>Need(s) Addressed:</B> The AMSANT project will address the
high performance computing and communications needs of the SIMA
and NAMT programs. High performance computers and communications
are necessary building blocks to effectively integrate the various
applications within a distributed virtual manufacturing enterprise
and to promote new forms of collaborative engineering and electronic
commerce. The AMSANT facilities will fulfill the need for a research
and development laboratory where researchers from NIST and other
government, industrial, and academic organizations can work together
to solve the problems associated with manufacturing systems integration.
The facility also fulfills the need for a place to train SIMA
researchers on sophisticated software applications and to demonstrate
SIMA accomplishments.
<P>
<B>Technical Approach:</B> The AMSANT project started in FY94
and was tasked with the design and construction of a state-of-the-art
computer laboratory that would support the computing, communications,
and demonstrations needs of the SIMA program. The first AMSANT
lab opened its doors in early FY95. This lab houses many high
performance computer workstations and servers, ATM networking
capabilities, and large screen computer display systems. Since
its completion, the AMSANT lab has been host to daily research
activities, several large demonstrations, and several software
training classes. In late FY95, plans were made to build a similar
lab to support the National Advanced Manufacturing Testbed (NAMT)
program. Work in FY96 included construction of the NAMT lab and
expansion of the AMSANT ATM network. The network supports high-speed
data, voice, and full-motion video between the labs and several
project sites around NIST. This years activities involve further
expansion of the computing and communications infrastructure to
support new requirements and new projects. The ATM network will
be expanded to support realtime audiovisual communications between
all AMSANT/NAMT project sites. The Molecular Beam Epitaxy (MBE)
lab in building 225 will be connected to the ATM network as a
new project site. Also, a network management system will be installed
to monitor and control the ATM network.
<P>
<B>FY96 Accomplishments:</B>
<UL>
<LI>Conducted annual requirements analysis leading to FY96 hardware
purchases 
<LI>Designed and constructed similar computing and communications
infrastructure for National Advanced Manufacturing Testbed (NAMT).
NAMT Laboratory completed 9/96 
<LI>Designed and installed a fiberoptic ATM network that spans
four buildings at NIST. The network is used to carry real time
voice and video as well as high-speed data 
<LI>Evaluated several collaborative tools and installed &quot;Showme&quot;
over ATM to enable realtime, full-motion video conferencing from
NAMT project sites during demos
<LI>Completed AMSANT overview document 
</UL>
<P>
<B>FY97 Plans:</B>
<UL>
<LI>Develop a network management system to monitor and control
the AMSANT/NAMT communications infrastructure 
<LI>Continued testing of video conferencing tools for use in AMSANT/NAMT
<LI>Conduct annual requirements analysis to determine FY97 hardware
purchases 
<LI>Assist with demos utilizing AMSANT resources and assure smooth
operation of AMSANT facility 
<LI>Enhance computing and communications infrastructure to provide
continued, leading edge capabilities 
</UL>
<P>
<B>Five-Year Plan Goals vs. Fiscal Years:</B>
<P>
<TABLE border=1>
<TR><TH><CENTER><B>ADVANCED MANUFACTURING SYSTEMS AND NETWORKING TESTBED (AMSANT)</B></CENTER>
</TH><TH><CENTER><B>97</B></CENTER></TH><TH><CENTER><B>98</B></CENTER>
</TH><TH><CENTER><B>99</B></CENTER></TH><TH><CENTER><B>00</B></CENTER>
</TH><TH><CENTER><B>01</B></CENTER></TH></TR>
<TR><TD>Install SNMP Network Management System to monitor and control AMSANT communications infrastructure.
</TD><TD>****</TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>
<TR><TD>Install new infrastructure capabilities requested in FY97.
</TD><TD>****</TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>
<TR><TD>Increase the number of communications links to external testbeds in support of distributed manufacturing research and related standards development.
</TD><TD>****</TD><TD>****</TD><TD>****</TD><TD>****</TD><TD>****
</TD></TR>
<TR><TD>Migrate proven computing, networking, and collaboration technologies to the desktop of NIST researchers.
</TD><TD>****</TD><TD>****</TD><TD>****</TD><TD>****</TD><TD>****
</TD></TR>
<TR><TD>Upgrade the design of other NIST computing labs based on the AMSANT design in order to allow cooperative research among the NIST laboratories.
</TD><TD>****</TD><TD>****</TD><TD>****</TD><TD>****</TD><TD>****
</TD></TR>
</TABLE>
</BODY>
</HTML>

