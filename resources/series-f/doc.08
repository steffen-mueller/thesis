<!-- itrc.on.ca/research/research-math.html -->
<html><head>
<html>
<head>
<title>ITRC Research Projects : Networks for Information Services</title>
</head>
<body bgcolor="#FFFFFF" vlink="#A20000">
<b><img src="/icons/researchers.gif" alt="ITRC :  Research Projects">
</b>
<h2>
<a name="RTFToC1">Project Descriptions
</a>
</h2>



<ul>
<li>Networks for Information Services
<ul>

<li><a href="#RTFToC2">Development of Network Security Functions and Authentication Methods</a>
<li><a href="#RTFToC3">ATM Network Resources Management Architecture</a>
<li><a href="#RTFToC4">Over-sampled Techniques in Mobile Communication</a>
<li><a href="#RTFToC5">Interconnection of Wireless and Wired ATM-Based Networks</a>
</ul></ul>
<hr>
<h3>
<a name="RTFToC2">Development of Network Security Functions and Authentication Methods
</a></h3>
Project Leader: 
<ul>
<li>		<b>Gord B. Agnew, Electrical & Computer Engineering, University of Waterloo</b>
<li>		<b>Scott A. Vanstone, Combinatorics and Opt., University of Waterloo</b></ul>
Team Members:
<ul>
<li><b>		R. C. Mullin, Combinatorics and Opt., University of Waterloo</b></ul>

<ul>Security is becoming a necessity in the areas of communications and computer networks. 
The installation of very high speed networks (such as ATM) has created a need for very 
fast and efficient authentication and encryption methods to protect users' data. 
Currently, it is difficult to design and implement secure encryption methods that 
will operate at the speeds required for ATM networks.<p>

A new trend in communication systems is the introduction of wireless network access. 
This technology introduces many attractive features including the elimination of (wired) 
network installation costs, ease of user relocation, etc. This environment also introduces 
increased vulnerability for interception of users' data, theft of services, and spoofing of 
users' identities. In these types of networks, speed is not the issue. It is envisioned 
that many subscribers will use battery powered, portable devices. In this case, efficiency 
of size and power are extremely important. <p>

Over the past few years, we have developed fast and efficient implementations of public key 
cryptographic systems based on elliptic curves. These systems have the advantage that high 
levels of security can be realized with much smaller block sizes than other public key systems. 
Based on our experience, we believe that implementations using an elliptic curve public key 
system would be ideal for both high speed systems and for efficient, lower power systems.<p>

For many years, the use of photographic (visual) identification has been used for authentication 
of an individuals identity. This method has been subject to misuse and counterfeiting in many 
instances. A solution to the problem would be to introduce a coding scheme that would allow 
visual inspection and electronic verification of the image.</ul><p>

<hr><h3>
<a name="RTFToC3">ATM Network Resources Management Architecture
</a></h3>
Project Leader: 
<ul>
<li><b>		A. Leon-Garcia, Electrical & Computer Engineering, University of Toronto</b></ul>
Team Members:
<ul>
<li><b>		George Kesidis, Electrical & Computer Engineering, University of Waterloo
<li>	Jon Mark, Electrical and Computer Engineering, University of Waterloo
<li>	Rajesh Pankaj, Electrical and Computer Engineering, University of Toronto
<li>	Johnny Wong, Electrical and Computer Engineering, University of Waterloo
</b></ul>


<p>
<ul>The emergence of home multimedia PCs, the deployment of cable and satellite digital 
television systems, the unprecedented growth of network-based services based on the 
World Wide Web, and the unprecedented collaboration of the computer and telecommunications 
industry in the ATM Forum have assured the evolution of telecommunications networks towards 
an infrastructure based on ATM technology that will provide the support for existing and 
emerging information and entertainment services. The resulting communications infrastructure  
will combine the ubiquity and the quality of service guarantees of the telephone network with 
the ease with which the Internet supports the development of new applications and provides 
connectivity across a network of networks.<p>

The communications infrastructure that is emerging will be unprecedented in scale and will 
constitute the largest distributed computing system ever built. A key and urgent research 
challenge is to develop network resources management and control procedures that will handle 
diverse services requirements in this environment. In this major project we propose a research 
program to develop an integrated network resources management (NRM) architecture to support the 
aforementioned emerging services as they mature and are deployed in large scale.<p>

An integrated set of five projects have been formulated to develop this network resources 
management architecture. The lead project is intended to ensure the development of a coherent 
and integrated NRM architecture. The project is also responsible for basic research associated 
with the challenges of NRM scalability, and will address issues such as the applicability of 
object-oriented modeling as a specification tool for large-scale NRM systems, growth strategies 
and distribution of controls. An early milestone will be to develop an NRM reference model 
which identified possible resource allocation schemes for each level in the control hierarchy 
and which describes the interactions between levels in the hierarchy. The development of the 
actual NRM architecture will involve the selection of specific procedures for each level in 
the hierarchy. Projects Two, Three, and Four address the selection of these specific procedures 
at the cell, call, Virtual Path, and Virtual Network levels. Project Five characterizes video 
traffic flows and Quality of Service requirements for future ATM networks.</ul><P>

<hr><h3>
<a name="RTFToC4">Over-sampled Techniques in Mobile Communication
</a></h3>
Project Leader: 
<ul>
<li><b>		Bosco Leung, Electrical & Computer Engineering, University of Waterloo</b></ul>

<ul>The field of mobile communications encompasses huge technological breadth. The proposed research 
focuses on the component needs of wireless transceiver that promises large volume of manufacture 
with the additional constraints of being mostly low power/low voltage driven. Potential 
applications include wireless communication devices such as cellular telephones, pagers, 
wireless LANs and consumer applications such as AM, FM radio. Some of these devices are also 
likely to have a set of multi-media input/output functions which provide pen, speech, audio 
and/or video interfaces.<p>

A typical block diagram of such a wireless transceiver is shown in fig. 1. These include in 
the receive direction an antenna and a RFD anti-imaging filter, low-noise RFD input amplifier 
(LAN), a frequency synthesizer to provide the local oscillator (LO) signal, a mixer that 
translates the RF signal either to base-band directly or to an IF signal, IF gain (AGC) and 
filtering (BPF or LPF), in most cases a second LO signal to perform the mixing as well as 
in-phase (I) and quadrature phase (Q) separation, and the base-band digital signal processing 
circuits (DSP) to perform timing and symbol recovery. <p>

The output data Dout is then routed through the multiplexer/demultiplexer (INTERFACE) to the 
proper low voltage low power (LV/LP) analog/digital converters that produce the appropriate 
multi-media signals. In the transmit direction, the multi-media signals are converted by the 
proper codecs in Din which is used to generate symbols by the DSP. Digital to analog conversion 
(DAC) and reconstruction filter (LPF) are performed on these symbols at the IF and then translated 
to the RF via a mixer. The resulting signal is amplified by a power amplifier (PWR AMP) which then 
drives a transmit/receive multiplexer (T/R).</ul><p>

<hr><h3>
<a name="RTFToC5">Interconnection of Wireless and Wired ATM-Based Networks
</a></h3>
Project Leader: 
<ul>
<li><b>		Weihua Zhuang, Electrical & Computer Engineering, University of Waterloo</b></ul>
Team Members:
<ul>
<li><b>		Jon W. Mark, Electrical & Computer Engineering, University of Waterloo</b></ul>

<ul>Wireless personal communication networks (PCNs) based on new digital technologies have emerged 
as an important field of activity in telecommunications. It was estimated that integrated 
wireless office system would grow from zero in 1992 to over $1-3 billion in 1997. [1]. 
Although the present wireless technology is mainly geared for voice and data communications, 
it is recognized that future wireless networks will extend the services provided to mobile 
terminals to a broad range of multimedia services such as voice, data, graphics, colour 
facsimile, low-resolution video [2]-[3]. <p>

Next-generation wireless PCNs will co-exist with fiber-optic based broadband communication 
networks. These wired broadband systems will offer constant bit-rate (CBR), variable bit-rate 
(VBR), and packet transport services designed to support multimedia applications. In order 
to avoid a serious mismatch between future wired and wireless networks, it is necessary to 
begin consideration of broadband wireless systems with similar service capabilities. Future 
wireless systems should provide new services features such as high-speed transmission, 
flexible bandwidth allocation, VBR/CBR packet modes, quality-of-service selection, etc. 
Clearly, implementation of these broadband features on the wireless medium is a more difficult 
technical challenge than that of fiber due to radio propagation channel impairments and limited 
radio spectrum. Although wireless systems may not be able to provide services with both quality 
and quantity the same as those of wired networks, it is important to aim at system designs that 
provide qualitatively similar attributes. <p>

To implement a completely wireless network requires a large investment, while partly using 
co-existing wired networks is more cost efficient. Asynchronous Transfer Mode (ATM) technology 
is a promising solution for multimedia communications. It allows various types of data being 
transmitted over the same network. A network, which uses a wireless network segment as the local 
user network and an ATM network as the backbone, combines the advantages of wireless and wired 
networks. The wireless front-end of the network provides flexibility for users who move around 
the office building, while the ATM backbone provides high transmission quality and capacity. 
When fully developed, the future personal wireless network, with access to wired ATM-based 
broadband networks, will be able to deliver a broad range of communication services to a 
portable hand-held unit at any desired location. The wireless system will allow users to 
have completely tetherless and mobile access to the network for services for home, office, 
shopping mall, airport, etc. Therefore, it is expected that the future wireless networks will 
be interconnected to wired broadband networks using ATM transmission.<p>

Recently, research and development (R&D) have been conducted in every aspect of indoor wireless 
communications and ATM-based wired networks, respectively. The CITR project "Broadband Indoor 
Wireless Communications" (with David D. Falconer as the major project leader) is one of the 
examples. The CITR project is to design a broadband indoor wireless digital communications 
system using millimeter wave frequency bands, with emphasis on the development of wireless 
transmission technologies in the frequency band of 20 to 60 Ghz. ATM-based transport architecture 
for multi-service wireless personal communication networks has been proposed to NEC, USA [4]. In 
Europe (at Siemens AG, Germany, and GPT, U.K.), R&D activities are taking place on integrating 
third-generation mobile systems (TGMS) and universal personal telecommunication (UPT) into ATM 
networks [5]. All the work on integration of wireless personal communication networks with ATM 
networks are still at an early stage. Details of research on the design of the interface between 
wireless and ATM wired networks are not available yet. It is the intention of this project to 
develop such an interface for the interconnection between an indoor wireless and ATM wired 
networks.</ul><p>



<hr>

<a href="/home.html"><img hspace=3 src="/icons/home.gif" alt="Home Page" 
border=0></a>
<a href="/new.html"><img hspace=3 src="/icons/neoteric.gif" alt="What's New"
border=0></a>
<a href="/membership.html"><img hspace=3 src="/icons/members.gif"
alt="Membership"  border=0></a>
<a href="/map.html"><img hspace=3 src="/icons/map.gif" alt="Map" border=0></a>

<a href="/feedback.html"><img hspace=3 src="/icons/feedback.gif" alt="Feedback" 
border=0></a>
</body></html>

