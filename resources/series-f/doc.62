<!-- www.mel.nist.gov/msid/projs/97projs/mfgent2.htm -->
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<TITLE>Manufacturing Enterprise Integration Project Page</TITLE>

<META NAME="GENERATOR" CONTENT="Internet Assistant for Microsoft Word 2.04z">
</HEAD>
<BODY>
<P>
<B>Title: MANUFACTURING-ENTERPRISE INTEGRATION </B>
<P>
<B>Staff:</B> (.75 years) *Neil Christopher, JAMES NELL 
<P>
<B>Objective and MEL Thrust(s) Supported:</B>Support the development
and implementation of enterprise models and modeling methodology.
Define the role and requirements for enterprise-reference architectures.
Coordinate and publicize the work to the extent that existing
groups and groups planning to work a sector of the domain are
able to identify what has or is being done so that they can leverage
their work. Through ICEIMT'97 (International Conference for Enterprise
Integration Modeling Technology) cooperation identify the barriers,
develop solutions to problems, communicate results, and help to
justify the technology to industry so that it can be moved profitably
from the international R&amp;D community to broadly based implementation.
<I>Manufacturing Systems Integration</I> 
<P>
<B>Need(s) Addressed:</B> Manufacturing enterprises need ways
to improve their infrastructure and processes to enable more effective
operation in a more agile, virtual-enterprise mode so that enterprises
can produce higher-quality products, better repeatability, lower
cycle time, and more customized production capability. Meeting
this need involves applying enterprise-integration technologies
and integrating business discourse internationally. However, investment
in these technologies on a large scale has been slowed by a lack
of business justification, a plethora of seemingly conflicting
solutions and terminology, and by an insufficient understanding
of the technology by the end-user community. Industry, therefore,
also needs tools, reference architectures, well-placed standards,
and methodology to help develop credible ways to justify integration-related
investments. 
<P>
<B>Technical Approach:</B> Develop, in WG1, a strategic standardization
plan that defines the role of international standards in the field
of integration technology. This project will assist that by coordinating
a careful analysis to define the total domain of enterprise representation
and some guidelines regarding how to subdivide the domain logically.
This project will define a scenario for representing an enterprise
according to a framework, an architecture, and models; and will
address life cycles of enterprises and models. The project also
will attempt to define which of the artifacts above should be
standardized, which should be recommendations, and which should
be left to software-product developers. 
<P>
Plan program of work and convene TC184 SC5 WG1, Modeling and Architecture
<P>
Advance ISO 14258 to International-Standard status and produce
a working draft of the Requirements for A Generalized Enterprise
Reference Architecture and Methodology. 
<P>
Convene five workshops for the ICEIMT'97 and plan the conference
for November 1997. 
<P>
<B>FY96 Accomplishments:</B>
<UL>
<LI>Convened four ISO TC184 SC5 WG1 meetings. Created a World-Wide-Web
site for WG1 activities. (http://www.nist.gov/sc5wg1/)
<LI>Received international approval to advance committee draft
of ISO 14258, Concepts and Rules for Enterprise Models, to draft-international
standard. 
<LI>Convened the Joint Workshop for Standards for the Use of Models
that Define the Data and Processes of Information Systems. All
preparatory and resultant information for this JTC-1 sponsored
workshop was distributed entirely on the World-Wide Web and email
exploder. (http://www.nist.gov/workshop/jtc1-96/)
<LI>Planned for the ICEIMT'97 (International Conference on Enterprise
Integration Modeling Technology). NIST and the European Community
ESPRIT Program are sponsoring. 
<LI>Maintained and Upgraded STEP on a Page. Published SOAP on
the World-Wide Web. (http://www.nist.gov/sc5/soap/)
</UL>
<P>
<B>FY97 Plans:</B>
<UL>
<LI>Plan program of work and convene TC184 SC5 WG1, Modeling and
Architecture. 
<LI>Produce a working draft of the Requirements for a Generic
Enterprise-Reference Architecture and Methodology. 
<LI>Register ISO CD 14258, Concepts and Rules for Enterprise Models,
as a draft-international standard; then ballot and register the
document as an international standard. 
<LI>Plan and convene the five ICEIMT'97 workshops and plan the
conference. 
<LI>Lead WG1 in work to create a road map for WG1 standards; and
to define the domain of enterprise-integration frameworks, enterprise-reference
architectures, enterprise models, and enterprise modeling. 
</UL>
<P>
<B>Five-Year Plan Goals vs. Fiscal Years:</B>
<P>
<TABLE border=1>
<TR><TH><CENTER><B>Manufacturing Enterprise Integration</B></CENTER>
</TH><TH><CENTER><B>97</B></CENTER></TH><TH><CENTER><B>98</B></CENTER>
</TH><TH><CENTER><B>99</B></CENTER></TH><TH><CENTER><B>00</B></CENTER>
</TH><TH><CENTER><B>01</B></CENTER></TH></TR>
<TR><TD>Convene TC184 SC5 WG1; Advance ISO 14258 to International-Standard status; produce a working draft of NP for Requirements for A Generalized Enterprise Reference Architecture and Methodology. 
</TD><TD>****</TD><TD>****</TD><TD></TD><TD></TD><TD></TD></TR>
<TR><TD>Convene five International-Conference-for-Enterprise-Integration-Modeling-Technology workshops and plan the conference for November 1997.
</TD><TD>****</TD><TD>****</TD><TD></TD><TD></TD><TD></TD></TR>
<TR><TD>Develop, in WG1, a strategic standardization plan that defines the role of international standards in the field of integration technology.
</TD><TD>****</TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>
<TR><TD>Advance GERAM from NP through DIS status.</TD><TD>****
</TD><TD>****</TD><TD>****</TD><TD></TD><TD></TD></TR>
</TABLE>
</BODY>
</HTML>

