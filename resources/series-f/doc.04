<!-- design.eng.clemson.edu/dmg/ -->
<body bgcolor ="ffffff">
<CENTER>
<IMG SRC="/gif/logo.gif">
</CENTER>
<HR>

<TITLE>Clemson University Design Group</TITLE>

<H1>CLEMSON UNIVERSITY </H1>
<H1>DESIGN METHODOLOGY GROUP</H1>

<hr>
       <p>Our research efforts target two main areas in the product realization
 	process, namely: design methodology/optimization and more recently,
	rapid prototyping and virtual reality for design.</p> 

 	<p>These two areas are described in the outline below:</p>
<HR>
<H2>(1) DESIGN METHODOLOGY / OPTIMIZATION</H2>

        <p>Study the design process from early conception to pre-production. 
 	Investigate ways to represent a design through form function
	relationships.  Study how to represent affordability issues early in the design </p>

	<p>Develop mathematical approximation techniques that improve the
	efficiency of traditional structural optimization methods while
	providing key parameters that carry information about the relations
	describing a design process. 
<A HREF= "../mdo/mdo.html"> Multidisciplinary
 optimization </A> - Identify
	cross disciplinary links that must be considered when concurrent design
	is performed.  Develop methods for the description and for the
	evaluation of sensitivities.  Develop design data decomposition tools.
 	Study data representation methodologies (object oriented - relational
	data bases) and their links to geometric models.  Investigate the use
	of genetic algorithms, neural networks, simulated annealing techniques
	in the design process.</p>
<HR>
<H2>(2) RAPID PROTOTYPING - CAD - VIRTUAL REALITY</H2>

        <p>Study the computer aided design (CAD) issues related to rapid
	prototyping and develop algorithms to improve the speed, accuracy and
	speed of use of rapid prototyping systems.  Determine the shortcomings
	of current CAD systems for design and develop techniques to address and
	resolve these issues. (involves solid modeling, optimization and
	expert systems).
Develop interfaces between virtual reality and rapid prototyping, and explore the use of VR as a truly 3D CAD modeler. See <a HREF="http://fantasia.eng.clemson.edu/vr/"> Virtual Reality Research </a> at Clemson.
</p>
	See also the "Clemson Intelligent Design Environment for Stereolithography" (<A HREF= "../rp/cides/cides.html">CIDES</A>, for short) and the <A HREF= "../rp/prl.html">Product Realization Lab</A> here at Clemson.

<hr>

<H1><IMG SRC="/gif/arrow.gif">
<A HREF="./people/people.html" >People</A> </H1>
All the current members of the group, their research , background and points of interest.
<P>

<H1><IMG SRC="/gif/arrow.gif">
<A HREF="./projects/projects.html">Projects</A></H1>

List and descriptions of the different projects the CUDMG is working on.
<P>

<H1><IMG SRC="/gif/arrow.gif">
<A HREF="./mainpage/activities.html">Activities</A></H1>

The current activities of the group.
<P>

<H1><IMG SRC="/gif/arrow.gif">
<A HREF="../rp/prl.html">Product Realization Lab</A></H1>

Some of the work is in association with the Product Realization Lab here at Clemson.  The lab focuses on the use of rapid prototyping or free form fabrication to produce 3D parts directly from CAD models.
<P>
<H1><IMG  SRC="/gif/arrow.gif">
<A HREF="./papers/papers.html">
Papers</A></H1>

These are some of the papers published by Group members, in postscript form.
<P>

<H1><IMG  SRC="/gif/arrow.gif">
<A HREF="./mainpage/swhw.html">Hardware and Software</A></H1> available in the design Labs

<P>


<H1><IMG SRC="/gif/arrow.gif">
<A HREF="./mainpage/news.html">News</A></H1>

The last moment information through the  News
<P>

<H1><IMG SRC="/gif/arrow.gif">
 <A HREF="./people/alumni.html">
Alumni</A></H1>

The list of the graduates of the program, their whereabouts, addresses and
phone numbers when available.
<P>
<H1><IMG SRC="/gif/arrow.gif">
<A HREF="./mainpage/misc.html">Misc.</A></H1>
Conferences, Deadlines, due dates of papers in the areas of design, optimization  and rapid prototyping.

<P>
<H1><IMG SRC="/gif/arrow.gif">
Other relevant Information</H1>
Look up these other net sites:
<p>
<dl>
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://epims1.gsfc.nasa.gov/engineering/engineering.html">
The World-Wide Web Virtual Library: Engineering</a>

<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://cdr.stanford.edu/html/WWW-ME/home.html">
The World-Wide Web Virtual Library: Mechanical Engineering</a>
<br><br>

 <dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://hypatia.gsfc.nasa.gov/NASA_homepage.html">
NASA Information Services via World Wide Web</a> NASA HQ
 <dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://endo.sandia.gov/AIAA_MDOTC/main.html"> AIAA MDO TC</a><br>
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://www.aero.ufl.edu/~get/issmo.htm"> ISSMO </a>International Society for Structural and Multidisciplinary Optimization <br>
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://fmad-www.larc.nasa.gov/mdob/">
Multidisciplinary Design Optimization Branch (MDOB) </a> NASA Larc

<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://mijuno.larc.nasa.gov/">
Design for Competitive Advantage</a> NASA Larc

<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://guinan.gsfc.nasa.gov/W3/VR.html">
WebStars: Virtual Reality</a> NASA site
<br><br>

<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://www.sandia.gov/agil/home_page.html">
Agile Manufacturing</a> Sandia National Laboratories
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="	http://elib.cme.nist.gov/edl/html/edl.html">Engineering Design Lab
</a>NIST
<br><br>
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://cdr.stanford.edu/">
Center for Design Research Home Page </a>Stanford University.

<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://smartcad.me.wisc.edu/"> I-CARVE</a> Laboratory for Integrated Computer Aided Research in Concurrent and
Virtual Engineering, University of Wisconsin.
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://cs.wpi.edu/Research/aidg/AIinD-hotlist.html">WPI AI in Design Webliography</a> Worcester Polytechnic Institute.
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://cadlab.mit.edu/"> CAD laboratory </a>MIT
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://www.cs.cmu.edu/afs/cs.cmu.edu/project/anneal/www/lab_home.html"> Carnegie Mellon University</a> Computational Design Lab
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://ASUdesign.eas.asu.edu"> Arizona State University </a> Design Automation Laboratory 
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://isl.msu.edu/GA/"> Michigan State University </a> GARAGe  -- GA related research.
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://www.vislab.iastate.edu"> Iowa State University Vislab</a>
<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://cs.wpi.edu/Research/aidg/AIinD-hotlist.html">
AI in design</a> WPI
<br><br>
<dt><IMG SRC="/gif/yellow-ball.gif"><a 
HREF="http://www.cms.dmu.ac.uk:9999/People/cph/vrstuff.html">
Virtual Reality</a> United Kingdom

<dt><IMG SRC="/gif/yellow-ball.gif"><a HREF="http://www.cs.clemson.edu/~paragon/"> EPSCoR grant and Paragon </a> Research at Clemson University<br>



</dl>
<hr>
Check also the <A HREF="//saul2.u.washington.edu:8080/Hot/html.html">Writing HTML </a> pages at the University of Washington.
<HR>
<H2>
Our Statistics: 
</H2>
<A HREF="./logs/dmglog.html"> Most Current Statististics</A>
<p>
<A HREF="./logs/dmglogold.html"> Older Statististics</A>
<HR>
<A HREF="http://www.clemson.edu/"><IMG SRC="/gif/home.xbm"></a>Home to <b>Clemson University</b> home page <p>
<A HREF="http://www.eng.clemson.edu/"><IMG SRC="/gif/home.xbm"></a>Home to Clemson University <b>College of Engineering</b> home page 
<HR>
<address>Last updated 15Mar95 by George Fadel at gfadel@eng.clemson.edu</address>

