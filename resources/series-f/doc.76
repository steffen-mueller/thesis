<!-- www.paperfree.com/edi/ediclint.htm -->
<!doctype html public "-//IETF//DTD HTML//EN">
<HTML>

<HEAD>

<TITLE>I.A EDI and Implementing Server-Server Technologies</TITLE>

<META NAME="GENERATOR" CONTENT="Internet Assistant for Word 1.0Z">
<META NAME="AUTHOR" CONTENT="Dan Kazzaz">
</HEAD>

<BODY>

<H1>EDI and Server-Server Technologies </H1>

<P>
<U><I><B>Introduction</B></I></U> 
<P>
The continuing improvement on the price/performance of computer
hardware has lead organizations to the conclusion that distributed
processing and distributed databases are the most logical way
to implement systems today. This is not a new idea. The trend
started with the advent of &quot;mini&quot; computers linked to
&quot;mainframe&quot; computers. It has always been more cost
effective to link groups of users to smaller machines and have
higher speed links to the larger machines.
<P>
This technology was successfully implemented by defining &quot;transactions&quot;
which could be captured and edited on the smaller machines. These
&quot;transactions&quot; were then passed to the host machine
where they were processed. In the rush to embrace client server
technology, many implementors have yet to realize that they must
be transaction based in order to remain open for interconnection.
This paper postulates that as the reality of transaction based
client-server takes hold, the EDI standards will emerge as the
methodology of choice.
<P>
Perhaps the easiest way to describe the state of EDI and its implications
to the large corporate user, is to start with a historic perspective.
There are several trends to be observed; the proliferation of
computers and systems; the evolution of EDI; the evolution of
computer languages. These three are linked in the evolution of
distributed client service corporate information systems.
<P>
<U><I><B>Proliferation of Computers and Systems</B></I></U> 
<P>
As computers became more cost effective, they became ubiquitous
within the enterprise. The sheer number of computers poses a problem
for communicating business documents. At first, there was one
computer for the Enterprise, then one per department. Now every
individual is likely to have at least one computer. Remember,
those devices which scan the bar code at your local grocery check
out stand are computers, which in turn, talk to the cash register
which is yet another computer.
<P>
This vast number of computers represents a great temptation for
every programmer. Many are likely to try to create the very product
your enterprise has been missing all these years. Many of these
developers succeed at creating a product and persuading someone
in the organization to purchase it. These products must now communicate
with each other in a supportable manner. In other words, two people
have purchased two different application systems, these systems
must communicate with one another and you will have to support
the interface.
<P>
Most organizations have many applications, none of which talk
to another, or only with great effort. This trend is accelerating
with client server technology.. The state of client server is
such that the design of the applications is geared around the
model of connecting directly to the database via SQL calls. This
works well when the system designer is architecting a closed system.
That is to say, that each of the user interfaces is talking directly
to a known database.
<P>
The question becomes how to develop applications that must communicate
with many diverse databases. These applications must interconnect
with systems where: the schema is not known; the database is not
SQL compliant; or simply operate more efficiently under a transaction
processing scheme, rather than an SQL scheme.
<P>
As an example, if an organization has a CICS based server and
clients that are PC based does an interface occur which makes
sense? There must be a transaction communicated which can be translated
to a known CICS call.
<P>
<U><I><B>Evolution of EDI</B></I></U> 
<P>
Almost as soon as computers were invented, information had to
be exchanged between them. Punch cards and paper tape, dinosaurs
of the computer age, are mysteries to most computer users today.
For many years it was the only method of exchange from computer
to computer.
<P>
The computer revolution in information exchange started in the
late sixties with magnetic tape and better telecommunications.
In the early days, baud rates of 110 were common, telephone lines
were noisy and difficult to use. Data networks were established
because phone companies were too difficult to work with and too
expensive. In particular, packet switch networks were set up for
both reliability and cost purposes.
<P>
As computers proliferated, information had to be exchanged between
many organizations. It quickly became apparent that standardization
on an industry basis was required. Some of these early organizations
included TDCC, UCS, NACHA and others. In 1979, several organizations
came to the conclusion that a CROSS INDUSTRY standard was required.
These organizations petitioned ANSI for accreditation and X12
was formed.
<P>
The success of X12 has been industry dependent. Whenever an industry
perceived a threat to its livelihood, one of the tactics employed
to reduce cost and improve efficiency was to implement EDI. The
primary effort to push EDI has been strictly economic and extremely
successful by any economic measure. Of particular note are the
Automotive and Textile industries and their strategic partners.
<P>
<U><I><B>Evolution of Computer Languages</B></I></U> 
<P>
On a parallel level and in a very different sphere, there has
been a revolution in language theory, its practice and implementation.
With the creation of the first computers, it was believed that
computers could be made to translate from one spoken language
to another, such as English to French. It was also believed that
they could be made to understand sound versus typed speech.
<P>
It was quickly learned what kind of languages computers could
or could not understand. A whole theory and practice grew around
creating and maintaining computer languages. The early languages,
COBOL and Fortran, were data dictionary independent. Some of the
later languages such as Total, Image and SQL were data schema
dependent. Regardless of the type of computer language, whether
it was a first, second, third or fourth generation language, a
compiler was developed for the language for the host environment.
<P>
Developing compilers has become almost trivial, since we know
so much about how to do it. There is however one factor that has
is still an art. The problem is that given the requirements to
translate from one grammar to another, how do they relate? The
problem is akin to the following: picture yourself as just arriving
in a country where you do not speak the language and they do not
speak yours, how do you communicate?
<P>
Clearly, if everyone could agree on a common language (remember
Esperanto?) communicating would be much easier. In relating tokens,
objects, messages or transactions between systems, having them
in the same format, like EDI, makes interpreting them much easier
and maintenance and audit much more efficient.
<P>
<U><I><B>EDI as a standard</B></I></U><I><B>.</B></I> 
<P>
Someone once said that &quot;the nice thing about standards is
that there are so many to choose from.&quot; Why should the ANSI
EDI or EDIFACT structure be chosen over the myriad of others available?
The answer has multiple parts, as follows:
<P>
<I><B>A.)</B></I> It is a cross industry standard developed on
a consensus basis. In other words, there is no single dictator
of the standard. Many of the earlier &quot;standards&quot; were
created by a single organization, be it a software company or
industry group. No one individual or small organization could
possibly create a message standard that everyone could adhere
to. The problem is akin to having a single organization dictate
what is correct English. It simply cannot be done.
<P>
<I><B>B.)</B></I> It is a compressed standard. Several standards
have fixed width fields, making it more expensive (in terms of
bytes) to transmit. Remember, some of these messages correspond
to tax returns. There is a large difference in cost when considering
all those required to file taxes. It is much less expensive to
transmit the information in a compressed delimited format.
<P>
<I><B>C.)</B></I> Envelopes. Effective world wide application
messaging requires envelopes that allow users to send information
directly to the appropriate application. There are some standards
that do not contain both the sender and the receiver codes, the
message type, security code and other fields required to accomplish
a fully integrated solution.
<P>
<I><B>D.)</B></I> Data Dictionary. There is a universal data dictionary
associated with the standard. The context of the field can be
determined positionally. This is contrast with standards that
require that parties agree to key words in the transmission. For
example, BEG*01*BY*1234 says that this is an original purchase
order, it is a Buying Order and the Purchase Order Number is 1234.
Other standards require that key words be sent and adhered to.
Consequently, the same information would have to be Transaction
Type: Original, Order Type: Buying, PO Number: 1234. This is clearly
more information.
<P>
It is much easier to integrate messages when you know exactly
what the field is, where it is, how is it being used and what
its minimum and maximum sizes are.
<P>
<U><I><B>EDI tool sets.</B></I></U> 
<P>
The integration of EDI into the corporate environment has spurred
several organizations to create EDI tool sets. These tool sets
come in a large variety of prices and capabilities. The objectives
of the tool sets are to make the integration as easy as possible.
<P>
These tool sets provide functions such as monitoring, archiving,
mapping, mailboxing, generation of test data and many more. With
the introduction of a standard is has been practical for many
companies to develop software that is easily integrated into a
complete corporate solution.
<P>
<U><I><B>Examples:</B></I></U> 
<P>
The airline industry, which is probably both the best and the
worst example, have historically implemented interfacing between
reservation systems with &quot;screen scraping&quot; and other
technologies. Now, the shift to interactive EDI is saving them
millions of dollars in their re-engineering efforts. Since they
are using EDI for interairline communication, they are now beginning
to use it for their internal communication projects.
<P>
The insurance industry has always been computer intensive. They
must take in a great deal of data to perform the analysis required.
Their shift towards EDI for external and internal communication
is already accruing tremendous savings.
<P>
<U><I><B>EDI challenges</B></I></U> 
<P>
There are many challenges in both the business and technical implementations
of EDI. The two major ones are the differences in dialect, coded
lists and differences in structure.
<P>
<U><I><B>Dialect.</B></I></U> 
<P>
The differences in dialects come about in any language. In order
for the language to be used it must be rich and flexible. The
EDI standard has both these attributes. The flexibility allows
for many ways of expressing exactly the same content. Anyone who
has implemented EDI is well aware of the myriad of small differences
that exist in the manner which an order is presented to an organization.
<P>
<U><I><B>Code lists.</B></I></U> 
<P>
One of the more successful implementations of EDI has been at
the retail level, in particular grocery. Most of the goods purchased
have a standard part number, known as the UPC, printed on the
box. This makes the common identification of goods a reasonable
task. Most of the rest of the world has yet to standardize (if
it is even possible) on code lists. Some examples are: the customer
number held by all companies with mass billing. Physician Id numbers
held by insurance companies. Tax Id numbers held by all taxing
authorities.
<P>
<U><I><B>Differences in structure.</B></I></U> 
<P>
There is a prevailing assumption that all documents of a particular
business nature are the same. An excellent example of the differences
of like documents is to examine the invoices. A telephone invoice
detail lists telephone numbers and length of time. A utility invoice
deals with gas units and electric units. A shipping invoice deals
with destinations, quantities and weights. If there is no accounting
to be done by the organization receiving the invoice, then simply
the invoice total is sufficient. However, there are generally
cost savings and allocations to be performed on the invoices.
These algorithms differ greatly depending on the source of the
invoice.
<P>
<U><I><B>Conclusion.</B></I></U> 
<P>
Thus with the emergence and acceptance of EDI as the standard
transaction language, the tools available for routing, application
integration, communication software, Transaction Processing monitoring
and archival, EDI has become the data standard for Server-Server
interactions. It has already begun a revolution in inter and intra-enterprise
communication. EDI will reduce the pain and cost of Client-Server
technology, causing yet another evolution in the corporate computing
environment.
<hr>
<i><a href="/edi/index.htm">Home page </A></i>
<br>
<i>Brought to you by <a href="/edi/pfs.htm">PaperFree Systems</A></i>
</BODY>

</HTML>

