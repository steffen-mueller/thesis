<!-- www.law.cornell.edu/uscode/42/2000e-2.html -->
<HTML><HEAD><base target="_top"><TITLE>42 USC Sec. 2000e-2 (01/16/96)</TITLE></HEAD>
<BODY BACKGROUND="/icons/USCodeBG.jpg">
<!-- TITLE 42 - THE PUBLIC HEALTH AND WELFARE -->
<!-- CHAPTER 21 - CIVIL RIGHTS -->
<!-- SUBCHAPTER VI - EQUAL EMPLOYMENT OPPORTUNITIES -->
<!-- -HEAD- -->
<H4>&#167; 2000e-2. Unlawful employment practices    </H4>
  <UL><LI>(a) Employer practices  <P>It shall be an unlawful employment practice for an employer - <UL>
<LI>(1) to fail or refuse to hire or to discharge any individual, or otherwise to
discriminate against any individual with respect to his compensation, terms, conditions, or privileges of employment, because of such individual's race, color, religion, sex, or national origin; or <LI>(2) to limit, segregate, or classify his employees or applicants for
employment in any way which would deprive or tend to deprive any individual of employment opportunities or otherwise adversely affect his status as an employee, because of such individual's race, color, religion, sex, or national origin. </UL>
<LI>(b) Employment agency practices  <P>It shall be an unlawful employment practice for an employment agency to fail
or refuse to refer for employment, or otherwise to discriminate against, any individual because of his race, color, religion, sex, or national origin, or to classify or refer for employment any individual on the basis of his race, color, religion, sex, or national origin. <LI>(c) Labor organization practices  <P>It shall be an unlawful employment practice for a labor organization -
<UL>
<LI>(1) to exclude or to expel from its membership, or otherwise to discriminate
against, any individual because of his race, color, religion, sex, or national origin; <LI>(2) to limit, segregate, or classify its membership or applicants for
membership, or to classify or fail or refuse to refer for employment any individual, in any way which would deprive or tend to deprive any individual of employment opportunities, or would limit such employment opportunities or otherwise adversely affect his status as an employee or as an applicant for employment, because of such individual's race, color, religion, sex, or national origin; or <LI>(3) to cause or attempt to cause an employer to discriminate against an
individual in violation of this section. </UL>
<LI>(d) Training programs  <P>It shall be an unlawful employment practice for any employer, labor organization,
or joint labor-management committee controlling apprenticeship or other training or retraining, including on-the-job training programs to discriminate against any individual because of his race, color, religion, sex, or national origin in admission to, or employment in, any program established to provide apprenticeship or other training. <LI>(e) Businesses or enterprises with personnel qualified on basis of 
religion, sex, or national origin; educational institutions with personnel of particular religion Notwithstanding any other provision of this subchapter, (1) it shall not be an unlawful employment practice for an employer to hire and employ employees, for an employment agency to classify, or refer for employment any individual, for a labor organization to classify its membership or to classify or refer for employment any individual, or for an employer, labor organization, or joint labor-management committee controlling apprenticeship or other training or retraining programs to admit or employ any individual in any such program, on the basis of his religion, sex, or national origin in those certain instances where religion, sex, or national origin is a bona fide occupational qualification reasonably necessary to the normal operation of that particular business or enterprise, and (2) it shall not be an unlawful employment practice for a school, college, university, or other educational institution or institution of learning to hire and employ employees of a particular religion if such school, college, university, or other educational institution or institution of learning is, in whole or in substantial part, owned, supported, controlled, or managed by a particular religion or by a particular religious corporation, association, or society, or if the curriculum of such school, college, university, or other educational institution or institution of learning is directed toward the propagation of a particular religion. <LI>(f) Members of Communist Party or Communist-action or  Communist-front
organizations <P>As used in this subchapter, the phrase ''unlawful employment practice'' shall
not be deemed to include any action or measure taken by an employer, labor organization, joint labor-management committee, or employment agency with respect to an individual who is a member of the Communist Party of the United States or of any other organization required to register as a Communist-action or Communist-front organization by final order of the Subversive Activities Control Board pursuant to the Subversive Activities Control Act of 1950 (50 U.S.C. 781 et seq.). <LI>(g) National security  <P>Notwithstanding any other provision of this subchapter, it shall not be an
unlawful employment practice for an employer to fail or refuse to hire and employ any individual for any position, for an employer to discharge any individual from any position, or for an employment agency to fail or refuse to refer any individual for employment in any position, or for a labor organization to fail or refuse to refer any individual for employment in any position, if - <UL>
<LI>(1) the occupancy of such position, or access to the premises in or upon
which any part of the duties of such position is performed or is to be performed, is subject to any requirement imposed in the interest of the national security of the United States under any security program in effect pursuant to or administered under any statute of the United States or any Executive order of the President; and <LI>(2) such individual has not fulfilled or has ceased to fulfill that requirement.
</UL>
<LI>(h) Seniority or merit system; quantity or quality of production; 
ability tests; compensation based on sex and authorized by minimum wage provisions Notwithstanding any other provision of this subchapter, it shall not be an unlawful employment practice for an employer to apply different standards of compensation, or different terms, conditions, or privileges of employment pursuant to a bona fide seniority or merit system, or a system which measures earnings by quantity or quality of production or to employees who work in different locations, provided that such differences are not the result of an intention to discriminate because of race, color, religion, sex, or national origin, nor shall it be an unlawful employment practice for an employer to give and to act upon the results of any professionally developed ability test provided that such test, its administration or action upon the results is not designed, intended or used to discriminate because of race, color, religion, sex or national origin.  It shall not be an unlawful employment practice under this subchapter for any employer to differentiate upon the basis of sex in determining the amount of the wages or compensation paid or to be paid to employees of such employer if such differentiation is authorized by the provisions of section 206(d) of title 29. <LI>(i) Businesses or enterprises extending preferential treatment to 
Indians <P>Nothing contained in this subchapter shall apply to any business or enterprise
on or near an Indian reservation with respect to any publicly announced employment practice of such business or enterprise under which a preferential treatment is given to any individual because he is an Indian living on or near a reservation. <LI>(j) Preferential treatment not to be granted on account of existing 
number or percentage imbalance <P>Nothing contained in this subchapter shall be interpreted to require any
employer, employment agency, labor organization, or joint labor-management committee subject to this subchapter to grant preferential treatment to any individual or to any group because of the race, color, religion, sex, or national origin of such individual or group on account of an imbalance which may exist with respect to the total number or percentage of persons of any race, color, religion, sex, or national origin employed by any employer, referred or classified for employment by any employment agency or labor organization, admitted to membership or classified by any labor organization, or admitted to, or employed in, any apprenticeship or other training program, in comparison with the total number or percentage of persons of such race, color, religion, sex, or national origin in any community, State, section, or other area, or in the available work force in any community, State, section, or other area. <LI>(k) Burden of proof in disparate impact cases  <UL>
<LI>(1)(A) An unlawful employment practice based on disparate impact is established
under this subchapter only if - <UL>
<UL>
<LI>(i) a complaining party demonstrates that a respondent uses a particular
employment practice that causes a disparate impact on the basis of race, color, religion, sex, or national origin and the respondent fails to demonstrate that the challenged practice is job related for the position in question and consistent with business necessity; or <LI>(ii) the complaining party makes the demonstration described in subparagraph
(C) with respect to an alternative employment practice and the respondent refuses to adopt such alternative employment practice. </UL>
<LI>(B)(i) With respect to demonstrating that a particular employment practice
causes a disparate impact as described in subparagraph <UL>
<UL>
<LI>(A)(i), the complaining party shall demonstrate that each particular
challenged employment practice causes a disparate impact, except that if the complaining party can demonstrate to the court that the elements of a respondent's decisionmaking process are not capable of separation for analysis, the decisionmaking process may be analyzed as one employment practice. </UL></UL></UL>
<LI>(ii) If the respondent demonstrates that a specific employment practice does
not cause the disparate impact, the respondent shall not be required to demonstrate that such practice is required by business necessity. (C) The demonstration referred to by subparagraph (A)(ii) shall be in accordance with the law as it existed on June 4, 1989, with respect to the concept of ''alternative employment practice''. (2) A demonstration that an employment practice is required by business necessity may not be used as a defense against a claim of intentional discrimination under this subchapter. <LI>(3) Notwithstanding any other provision of this subchapter, a rule barring
the employment of an individual who currently and knowingly uses or possesses a controlled substance, as defined in schedules I and II of section <A HREF="102.shtml">102(6)</A> of the Controlled Substances Act (21 U.S.C. 802(6)), other than the use or possession of a drug taken under the supervision of a licensed health care professional, or any other use or possession authorized by the Controlled Substances Act (21 U.S.C. 801 et seq.) or any other provision of Federal law, shall be considered an unlawful employment practice under this subchapter only if such rule is adopted or applied with an intent to discriminate because of race, color, religion, sex, or national origin. </UL>
<LI>(l) Prohibition of discriminatory use of test scores  <P>It shall be an unlawful employment practice for a respondent, in connection
with the selection or referral of applicants or candidates for employment or promotion, to adjust the scores of, use different cutoff scores for, or otherwise alter the results of, employment related tests on the basis of race, color, religion, sex, or national origin. <LI>(m) Impermissible consideration of race, color, religion, sex, or 
national origin in employment practices <P>Except as otherwise provided in this subchapter, an unlawful employment
practice is established when the complaining party demonstrates that race, color, religion, sex, or national origin was a motivating factor for any employment practice, even though other factors also motivated the practice. <LI>(n) Resolution of challenges to employment practices implementing 
litigated or consent judgments or orders <UL>
<LI>(1)(A) Notwithstanding any other provision of law, and except as provided in
paragraph (2), an employment practice that implements and is within the scope of a litigated or consent judgment or order that resolves a claim of employment discrimination under the Constitution or Federal civil rights laws may not be challenged under the circumstances described in subparagraph (B). <UL>
<LI>(B) A practice described in subparagraph (A) may not be challenged in a claim
under the Constitution or Federal civil rights laws - <UL>
<LI>(i) by a person who, prior to the entry of the judgment or order described in
subparagraph (A), had - (I) actual notice of the proposed judgment or order sufficient to apprise such person that such judgment or order might adversely affect the interests and legal rights of such person and that an opportunity was available to present objections to such judgment or order by a future date certain; and (II) a reasonable opportunity to present objections to such judgment or order; or <LI>(ii) by a person whose interests were adequately represented by another
person who had previously challenged the judgment or order on the same legal grounds and with a similar factual situation, unless there has been an intervening change in law or fact. </UL></UL>
<LI>(2) Nothing in this subsection shall be construed to -  <UL>
<LI>(A) alter the standards for intervention under rule 24 of the Federal Rules
of Civil Procedure or apply to the rights of parties who have successfully intervened pursuant to such rule in the proceeding in which the parties intervened; <LI>(B) apply to the rights of parties to the action in which a litigated or
consent judgment or order was entered, or of members of a class represented or sought to be represented in such action, or of members of a group on whose behalf relief was sought in such action by the Federal Government; <LI>(C) prevent challenges to a litigated or consent judgment or order on the
ground that such judgment or order was obtained through collusion or fraud, or is transparently invalid or was entered by a court lacking subject matter jurisdiction; or <LI>(D) authorize or permit the denial to any person of the due process of law
required by the Constitution. </UL>
<LI>(3) Any action not precluded under this subsection that challenges an
employment consent judgment or order described in paragraph (1) shall be brought in the court, and if possible before the judge, that entered such judgment or order.  Nothing in this subsection shall preclude a transfer of such action pursuant to section <A HREF="../28/1404.shtml">1404</A> of title 28.   

























































































































































































































































