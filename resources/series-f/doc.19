<!-- www.bvmltd.co.uk/networks.html -->
<!DOCTYPE html PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
<TITLE>Networking with OS-9 - A Tutorial</TITLE>
<BODY>
<body background ="images/backlogo.gif">
<IMG ALIGN=right Hspace=10 SRC=images/smallogo.gif>

<h2>A Tutorial on Inter-Machine Communications</H2>
<HR ALIGN=center SIZE=6>
<p>
<b>
The methods of connecting computer systems are often mis-understood. This tutorial sets out to describe some of the options available to the designer of a real-time system using VMEbus hardware and the OS-9 Operating System and tools available from BVM.</b><BR><BR>

<FONT SIZE="+2" COLOR="#FF0000">HARDWARE</FONT>

<BR><B>RS232 </B><BR>

The simplest method of connection for point to point usage between two machines. Normally restricted to modest data rates but can drive large distances by using modems.<P>

<B>RS485</B><BR>

A two or four wire multi-drop connection using twisted pair cable. A single pair of wires carries the data signals in either direction whilst the second pair provides clocks if synchronous operation is used. The latter allows much higher data rates to be achieved. With more than two nodes on the network all transmitters must turn off and only respond to a poll from the network controller. The differential drive signals of RS485 can achieve distances up to 1.2 kilometres at reasonably high speeds. Alternatively up to 2Mbits per second can be used for synchronous operation up to fifty metres.<BR><P>

<B>Ethernet</B><BR>

Ethernet comes in two common flavours. The true ethernet uses a large diameter heavy weight coaxial cable with clamp-on taps and drop cables to attach to the systems. More common in the smaller office and industrial environments is thin ethernet or 'cheapernet' which uses low cost coax cable (similar to TV coax but 50 ohm impedance) and connects via a BNC connector. Each node must be T'd into the cable and a 50 ohm terminator connected at each end of the network. Ethernet has a theoretical transfer rate of 10Mbits per second but in reality this is rarely achieved. On all but very heavily loaded networks the transfer is very fast and relatively predictable. Early implementations of ethernet were on large D.P. sites with non-real-time systems shifting huge volumes of data. As ethernet depends on collision detection on the network rather than collision avoidance, heavily loaded systems got a reputation for being unsuitable for real-time. In reality most real-time systems transfer so little data, and ethernet is intrinsically so much faster than the alternatives, that it makes an ideal carrier in a correctly designed system architecture.<BR><P>

<B>Backplane</B><BR>

In multi-processor systems different CPU's on the same backplane can communicate using shared memory areas in each others memory map.<BR>

<HR>

<FONT SIZE="+2" COLOR="#FF0000">SOFTWARE</FONT><BR>

Whichever hardware transport mechanism is used a network connection between two systems relies on the selection of a suitable protocol. Some of the options available under OS-9 are listed below.<BR><P>

<B>SCF</B><BR>

The most common use of serial ports is communication with serial terminals. This uses the standard OS-9 Serial Character File manager (SCF). SCF operates on a character by character basis and provides simple filtering of control characters. With most systems flow control can be by software means ie XON XOFF signalling or by hardware means using the RS232 signals RTS/CTS for example. SCF is used extensively for lower speed applications but imposes considerable system overheads for high speed data links.<BR><P>

<B>IOF</B><BR>

For more rigourous serial communications BVM's IOF file manager provides a much more efficient data transfer without the overhead of character filtering provided by SCF. IOF is particularly suitable for intelligent serial cards where it off-loads much of the overhead of the host CPU rather than interrupting on a character by character basis. Again hardware or software flow control is provided in the serial driver.<BR><P>

<B>NFM Network File Manager</B><BR>

A simple network providing file and data transfer between OS-9 systems. Although the file manager is provided by Microware and is common, the lower level protocols were not defined and tend to limit communication between different vendors systems unless the appropriate elements are installed in both systems. NFM is available for use on RS232 point to point connections, ethernet or between CPUs on the same backplane.<BR> <P>

<B>ISP Internet Support Package (TCP/IP)</B><BR>

This is one of the more well known protocols and allows connection to many different types of machine. Its popularity grew in the Unix arena but it is now available under most operating systems. Apart from straightforward file transfers ISP provides 'sockets' which allow direct communications between users applications.

ISP commonly runs on an ethernet network but can also be used on serial (SLIP) or backplane connections.<BR><P>

<B>NFS Network File system</B><BR>

NFS requires ISP to be running on the system and provides an extended file system across different types of system. For example an OS-9 operator can access files on a Unix system merely by setting up the correct path name for the Unix node and file location. NFS also provides Remote Procedure Calls (RPC) allowing remote stations to initiate tasks on other machines running a variety of operating systems. For example, an OS-9 system operator could initiate a procedure on a remote Unix machine connected via a serial link and modems.<BR><P>

<B>PCLink</B><BR>

PCLink, as its name implies, originated to connect PCs to OS-9 systems. Since its creation PCLink has developed to become a totally integrated peer to peer network between any mix of OS-9, DOS, Windows 3.1 or Windows for Workgroups systems. The user is provided with a seam free connection giving full access to each systems file system and peripherals. It includes RMX a remote execution utility allowing OS-9 tasks to be initiated and controlled from a PC. Full two way data transfer is provided using OS-9 pipes and MDF, the Module Data File manager. Whether running with Windows 3.1 windows or PCLinks own windowing system under DOS an operator can control different tasks in different windows, or merely keep one window open to monitor a task whilst performing operations in another window. <P> 

At application program level PCLink is totally compatible with NFM allowing straightforward upgrade in situations where it is required to add a PC connection to an existing network.

PCLink is in common usage in both development and target environments in many different industries. It commonly uses thin ethernet and can operate in native mode on the BVME410 or BVME4000 or on top of ISP on any OS-9 system which has it installed.  PCLink is also available for use on RS232 point to point connections.<BR><P>

<B>FieldLink</B><BR>

FieldLink is  based around a low-overhead connection orientated networking system allowing the creation of a bi-directional data path between any two OS-9 processes on the same or different processors. If multiple paths are provided between different processes FieldLink will select the best and automatically re-route the link should a failure occur. These interprocess communication channels used by FieldLink are established and maintained by BVM's RCMAN (Remote Connection File Manager).<P>

FieldLink utilities include a remote login facility, a module loader and execution server with client utilities and 'C' bindings allowing OS-9 modules to be loaded and executed on a remote processor from either the command line or from users application programs.  A generic I/O call server and template based client 'C' bindings allow users application programs to open paths to remote devices and perform any I/O function supported by the remote device driver.

FieldLink is also useful in mixed network environments providing bridges between PCLink, NFM or TCP/IP.  Users can for example use a PC as the display console talking to an OS-9 control system via Ethernet/PCLink and then to an array of RP2000s on a distributed FieldLink/RS485 network. Alternatively in some applications it is possible to eliminate the OS-9 system and have the PC controlling the RP2000s directly.<BR><P>

<B>IOF For Networks</B><BR>

BVM's IOF network file manager provides a low level packet interface allowing users to specify their own higher level protocols directly from within their application. This can provide a very efficient transmission system for custom applications.

In summary the BVM hardware and software options provide great flexibility for network communications whether talking between OS-9 systems, PCs or Unix based systems. And, whats more, the above protocols can co-exist on the same hardware. This means that a single ethernet connection to an OS-9 system can simultaneously communicate with a PC using PCLink protocol and a Unix system running Internet with another connection to OS-9 using OS-9 NFM.<P>
<BR><HR WIDTH=90% ALIGN=center SIZE=5>
<center>
<A HREF="index.html"><IMG SRC="images/home.gif" border=0  ALT="home"></A>
<A HREF="news.html"><IMG SRC="images/news.gif" border=0 ALT="news"></A>
<A HREF="vmeprods.html"><IMG SRC="images/vme.gif" border=0 ALT="vme products"></A>
<A HREF="os9prods.html"><IMG SRC="images/os9but.gif" border=0 ALT="OS-9 Prods"></a>
<A HREF="mailto:sales@bvmltd.co.uk"><IMG SRC="images/mail.gif" border=0 ALT="mail us"></A>
</center><BR>
<center>
<h5>BVM Limited can be contacted by <BR>Telephone  +44 (0)1489 780144 <BR>

Fax +44 (0)1489 783589<BR>

or email <a HREF="mailto:sales@bvmltd.co.uk">sales@bvmltd.co.uk<P></h5></A>

<H6>Copyright &#169; 1996 BVM Limited. All rights reserved.                                       All Trademarks acknowledged</H6><br>
</CENTER>
</BODY>
</html>
