<!-- www.dol.gov/dol/eta/public/regs/cfr/20cfr/toc_Part600-699/0609.0002.htm -->
<html>
<title>609.2 - Definitions of terms.</title>
<h3>609.2 - Definitions of terms.</h3>
<UL>
<LI><b>Standard Number:</b>  609.2<br>
<LI><b>Standard Title:</b>  Definitions of terms.<br>
<LI><b>SubPart Number:</b>  A<br>
<LI><b>SubPart Title:</b>  General Provisions<br>
</UL>
<hr>
<body>
  For the purposes of the Act and this part:<br>
<b>(a)</b> <i>Act</i></b> means subchapter I of chapter 85, title 5, United States Code, 5 U.S.C. 8501-8508.<br>
<b>(b)</b> <i>Agreement</i></b> means the agreement entered into pursuant to the Act between a State and the Secretary under which the State agency of the State agrees to make payments of unemployment compensation in accordance with the Act and the regulations and procedures thereunder prescribed by the Department.<br>
<b>(c)</b> <i>Based period</i></b> means the base period as defined by the applicable State law for the benefit year.<br>
<b>(d)</b> <i>Benefit year</i></b> means the benefit year as defined by the applicable State law, and if not so defined the term means the period prescribed in the agreement with the State or, in the absence of an Agreement, the period prescribed by the Department.<br>
<b>(e)</b> <i>Federal agency</i></b> means any department, agency, or governmental body of the United States, including any instrumentality wholly or partially owned by the United States, in any branch of the Government of the United States, which employs any individual in Federal civilian service.<br>
<b>(f)</b> <i>Federal civilian service</i></b> means service performed in the employ of any Federal agency, except service performed_<br>
<b>(1)</b> By an elective official in the executive or legislative branches of the Government of the United States; <br>
<b>(2)</b> As a member of the Armed Forces or the Commissioned Corps of the National Oceanic and Atmospheric Administration; <br>
<b>(3)</b> By Foreign Service personnel for whom special separation allowances are provided under chapter 14 of title 22 of the United States Code;<br>
<b>(4)</b> Outside the 50 States, the Commonwealth of Puerto Rico, the Virgin Islands, and the District of Columbia, by an individual who is not a citizen of the United States;<br>
<b>(5)</b> By an individual excluded by regulations of the Office of Personnel Management from civil service retirement coverage provided by subchapter III of chapter 83 of title 5 of the United States Code because the individual is paid on a contract or fee basis;<br>
<b>(6)</b> By an individual receiving nominal pay and allowances of $12 or less a year;<br>
<b>(7)</b> In a hospital, home, or other institution of the United States by a patient or inmate thereof;<br>
<b>(8)</b> By a student-employee as defined by 5 U.S.C. 5351; that is: (i) A student nurse, medical or dental intern, resident-in-training, student dietitian, student physical therapist, or student occupational therapist, assigned or attached to a hospital, clinic, or medical or dental laboratory operated by an agency as defined in section 5351; or <br>
<b>(ii)</b> Any other student-employee, assigned or attached primarily for training purposes to such a hospital, clinic, or medical or dental laboratory operated by such an agency, who is designated by the head of the agency with the approval of the Office of Personnel Management;<br>
<b>(9)</b> By an individual serving on a temporary basis in case of fire, storm, earthquake, flood, or other similar emergency;<br>
<b>(10)</b> By an individual employed under a Federal relief program to relieve the individual from unemployment;<br>
<b>(11)</b> As a member of a State, county, or community committee under the Agricultural Stabilization and Conservation Service or of any other board, council, committee, or other similar body, unless such body is composed exclusively of individuals otherwise in the full-time employ of the United States;<br>
<b>(12)</b> By an officer or member of the crew on or in connection with an American vessel which is: (i) Owned by or bareboat chartered to the United States, and <br>
<b>(ii)</b> The business of which is conducted by a general agent of the Secretary of Commerce; and <br>
<b>(iii)</b> If contributions on account of such service are required under section 3305(g) of the Internal Revenue Code of 1954 (26 U.S.C. 3305(g)) to be made to an unemployment fund under a State law;<br>
<b>(13)</b> By an individual excluded by any other Federal law from coverage under the UCFE Program; or<br>
<b>(14)</b> By an individual whose service is covered by the UCX Program to which part 614 of this chapter applies.<br>
<b>(g)</b> <i>Federal employee</i></b> means an individual who has performed Federal civilian service.<br>
<b>(h)</b> <i>Federal findings</i></b> means the facts reported by a Federal agency pertaining to an individual as to: (1) Whether or not the individual has performed Federal civilian service for such an agency; <br>
<b>(2)</b> The period or periods of such Federal civilian service; <br>
<b>(3)</b> The individual's Federal wages; and <br>
<b>(4)</b> The reasons for termination of the individual's Federal civilian service.<br>
<b>(i)</b> <i>Federal wages</i></b> means all pay and allowances, in cash and in kind, for Federal civilian service.<br>
<b>(j)</b> <i>First claim</i></b> means an initial claim for unemployment compensation under the UCFE Program, the UCX Program (part 614 of this chapter), a State law, or some combination thereof, whereby a benefit year is established under an applicable State law.<br>
<b>(k)</b> <i>Official station</i></b> means the State (or country, if outside the United States) designated on a Federal employee's notification of personnel action terminating the individual's Federal civilian service (Standard Form 50 or its equivalent) as the individual's ``duty station.'' If the form of notification does not specify the Federal employee's ``duty station'', the individual's official station shall be the State or country designated under ``name and location of employing office'' on such form or designated as the individual's place of employment on an equivalent form.<br>
<b>(l)</b> <i>Secretary</i></b> means the Secretary of Labor of the United States.<br>
<b>(m)</b> <i>State</i></b> means the 50 States, the District of Columbia, the Commonwealth of Puerto Rico, and the Virgin Islands.<br>
<b>(n)</b> <i>State agency</i></b> means the agency of the State which administers the applicable State law and is administering the UCFE Program in the State pursuant to an Agreement with the Secretary.<br>
<b>(o)</b>(1) <i>State law</i></b> means the unemployment compensation law of a State approved by the Secretary under section 3304 of the Internal Revenue Code of 1954, 26 U.S.C. 3304, if the State is certified under section 3304(c) of the Internal Revenue Code of 1954, 26 U.S.C. 3304(c).<br>
<b>(2)</b> <i>Applicable State law</i></b> means the State law made applicable to a UCFE claimant by 609.8.<br>
<b>(p)</b>(1) <i>Unemployment compensation</i></b> means cash benefits (including dependents' allowances) payable to individuals with respect to their unemployment, and includes regular, additional, emergency, and extended compensation.<br>
<b>(2)</b> <i>Regular compensation</i></b> means unemployment compensation payable to an individual under any State law, but not including additional compensation or extended compensation.<br>
<b>(3)</b> <i>Additional compensation</i></b> means unemployment compensation totally financed by a State and payable under a State law by reason of conditions of high unemployment or by reason of other special factors.<br>
<b>(4)</b> <i>Emergency compensation</i></b> means supplementary unemployment compensation payable under a temporary Federal law after exhaustion of regular and extended compensation.<br>
<b>(5)</b> <i>Extended compensation</i></b> means unemployment compensation payable to an individual for weeks of unemployment in an extended benefit period, under those provisions of a State law which satisfy the requirements of the Federal-State Extended Unemployment Compensation Act of 1970, as amended, 26 U.S.C. 3304 note, and part 615 of this chapter, with respect to the payment of extended compensation.<br>
<b>(q)</b> <i>Week</i></b> means, for purposes of eligibility for and payment of UCFE, a week as defined in the applicable State law.<br>
<b>(r)</b> <i>Week of unemployment</i></b> means a week of total, part-total, or partial unemployment as defined in the applicable State law, which shall be applied in the same manner and to the same extent to all employment and earnings, and in the same manner and to the same extent for the purposes of the UCFE Program, as if the individual filing for UCFE were filing a claim for State unemployment compensation. <br>
</body>
</html>

