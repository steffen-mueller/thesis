<!-- www.osha.gov/oshpubs/oshfacts/91-40.html -->
<html><head>
<title>OSHA Program Highlight</title>
</head>
<body background="/images/stripe.gif">
<img src="/images/osh.gif">
<font face=arial size=2>
<h5>Program Highlight<br>
Fact Sheet# OSHA 91-40</h5>

                      <h3>OSHA Help For New Businesses</h3>

    <center>This fact sheet will give you --- the employer starting a new
business --- basic information about OSHA requirements and additional
publications to assist you in complying with those
requirements.</center><p>

<b>Responsibilities</b><br>
    As an employer you are responsible under the Occupational Safety and
Health Act to provide a workplace free from recognized hazards that are
causing or are likely to cause death or serious physical harm to your
employees.  You must comply with standards, rules and regulations issued
by OSHA under the act.  You must be familiar with the standards and make
copies available to employees for review upon their request.<p>

<b>Standards</b><br>
    Copies of various OSHA standards may be purchased through the
Superintendent of Documents, Government Printing Office, Washington, DC
20402-9325(telephone 202-783-3238).  Payment may be made by check, GPO
Deposit Account, or VISA or MasterCard.<p>

    OSHA's regulations are contained in Title 29, Code of Federal
Regulations Parts 1900-1999.  Standards covering General Industry are in
two volumes: 29 CFR Parts 1901.1 to 1910.441, GPO Order No. S/N 869-011-
00109-2; and 29 CFR Parts 1910.1000 to end, GPO Order No. S/N 869-011-
00110-6.  Standards covering the Construction Industry are in 29 CFR Part
1926, GPO Order No. S/N 869-011-00112-2.  In addition, there is a
combined volume of Construction Industry and General Industry standards
affecting the Construction Industry---29 CFR Parts 1926/1910 (OSHA 2207),
GPO Order No. 029-016-00122-1.  Standards covering the Maritime Industry
are in 29 CFR Parts 1911 to 1925, GPO Order No. 869-007-00110-9.<p>

<b>Recordkeeping</b><br>
    Most employers of 11 or more employees are required to maintain
records of occupational injuries and illnesses as they occur.<p>

    Employers with 10 or fewer employees and employers regardless of size
in certain industries are exempt from keeping such records unless they
are selected by the Bureau of Labor Statistics (BLS) to participate in
the Annual Survey of occupational injuries and illnesses.<p>

    OSHA recordkeeping is not required for employers in retail trade,
finance, insurance, real estate and service industries---Standard
Industrial Classification (SIC) 52-89 (except building materials and
garden supplies, SIC 52; general merchandise and food stores, SIC 53 and
54; hotels and other lodging places, SIC 70; repair services, SIC 75 and
76; amusement and recreation services, SIC 79; and health services, SIC
80).<p>

    Two forms are needed for recordkeeping: OSHA No. 200, Log and Summary
of Occupational Injuries and Illnesses and OSHA No. 101, Supplementary
Record of Occupational Injuries and Illnesses. Employers selected for the
BLS survey receive a form, OSHA 200S, in the mail.<p>

    Copies of OSHA recordkeeping forms and
publications on the recordkeeping requirements are available through the
OSHA Publications Office, Room N-3101, Frances Perkins Building, 200
Constitution Ave. NW, Washington, DC 20210 (telephone 202-523-9667).
The publications are "A Brief Guide to Recordkeeping Requirements for
Occupational Injuries and Illnesses" and "Recordkeeping Requirements
Under the Occupational Safety and Health Act of 1970."<p>

<b>OSHA Poster</b><br>
    Every employer must post in a prominent location in the workplace the
Job Safety and Health Protection workplace poster (OSHA 2203 or state
equivalent) which informs employees of their rights and responsibilities
under the Act.  The poster may be obtained through the OSHA Publications
Office.<p>

<b>Hazard Communication</b><br>
    The OSHA Hazard Communication Standard requires employers to inform
their workers of the potential dangers of any chemical hazards on the
job, and to train them in proper safeguards.  This includes information
on the hazards and identities of chemicals they are exposed to when
working and the protective measures available to prevent adverse
effects.<p>

    Employers who use the chemicals, rather than produce or import them,
are not required to evaluate the hazards of those chemicals.  Hazard
determination is the responsibility of the producers and  importers of
the materials, who then must provide the hazard information to employers
who purchase their products. All employers must have a written workplace
compliance program.<p>

    Copies of the Hazard Communication Standard and the publication,
"Chemical Hazard Communication," (OSHA 3084 Revised) are available
through the OSHA Publications Office.  Another publication, "Hazard
Communication Guidelines for Compliance," (OSHA 3111) can be purchased
from the Superintendent of Documents, U.S. Government Printing Office,
Washington, DC 20402-9325 (telephone 202-783-3238).  It is GPO Order No.
029-016-00127-1.<p>
    A compliance kit on the standard with more detailed information and
which can be used for filing the employer's hazard communication and
training programs, material safety data sheets (MSDSs) and requests for
MSDSs, and training or other records can be purchased from the
Superintendent of Documents, U.S. Government Printing Office, Washington,
DC 20402-9325 (telephone 202-783-3238). It is GPO Order No. 929-022-0000-
9 (OSHA 3104 Hazard Communication Compliance Kit).<p>

<b>Inspections</b><br>
    OSHA conducts workplace inspections to enforce its standards. Every
establishment covered by the Act is subject to inspection by OSHA
compliance safety and health officers, who are chosen for their knowledge
and experience in the occupational safety and health field.   A booklet,
"OSHA Inspections," (OSHA 2098) is available through the OSHA
Publications Office.<p>

<b>Regional Offices</b><br>
    OSHA has 10 Regional Offices.  A list of those office addresses and
telephone numbers, (OSHA Program Highlights No. 91-41), is available
through the OSHA Publications Office.<p>

<b>State Programs</b><br>
    The Occupational Safety and Health Act encourages states to develop
and operate their own job safety and health plans. OSHA approves and
monitors these state plans and provides up to 50 percent of an approved
plan's operating costs. States must set job safety and health standards
at least as effective as comparable federal standards. (Most states adopt
standards identical to federal ones.)  Twenty-three states or
jurisdictions operate complete state plans covering both the private
sector and state and local government employees. Two others, Connecticut
and New York, cover public employees only.  A fact sheet, "State Job
Safety and Health Programs," (OSHA Program Highlights No. 15) is
available through the OSHA Publications Office.<p>

<b>Consultation</b><br>
    Employers who want help in recognizing and correcting hazards and in
improving safety and health programs can get it from a free consultation
service largely funded by OSHA and delivered by State governments using
well-trained professional staff.  A booklet, "Consultation Services for
the Employer," (OSHA 3047) is available through the OSHA Publications
Office.<p>

<b>Training</b><br>
    Training courses in safety and health subjects are available to the
private sector through the OSHA Training Institute, 1555 Times Dr., Des
Plaines, IL 60018. For information on the subjects, dates, tuition, and
location of these courses telephone the Institute Registrar (708-297-
4913) or write to the Institute.<p>

<b>Handbook for Small Businesses</b><br>
    A booklet designed specifically to help small businesses comply with
OSHA requirements is available through the OSHA Publications Office.  It
is entitled "OSHA Handbook for Small Businesses" (OSHA 2209).<p>

<b>Publications Catalog</b><br>
    A catalog, "OSHA Publications and Audiovisual Programs," (OSHA 2019),
is available through the OSHA Publications Office.<p>

<b>New Employers Kit</b><br>
    A kit containing "free" OSHA publications may be obtained by writing
or telephoning the OSHA Publications Office, Room N-3101, Frances Perkins
Building, 200 Constitution Ave. NW, Washington, DC 20210 (telephone 202-
523-9667) or through the OSHA Regional Offices.
<p>
</body>
</html>

