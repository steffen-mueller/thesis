Graph-based text classification test protocol - Thu May 14 14:22:11 UTC 2015
============================================================================

Testing all documents in 'resources\series-f'
- doc.01: correctly classified as BRC with 65% confidence.
Intermediate result: 100% exact / 100% group success rate.
- doc.02: correctly classified as CNE with 56% confidence.
Intermediate result: 100% exact / 100% group success rate.
- doc.03: correctly classified as CNE with 100% confidence.
Intermediate result: 100% exact / 100% group success rate.
- doc.04: correctly classified as MPD with 35% confidence.
Intermediate result: 100% exact / 100% group success rate.
- doc.05: correctly group-classified as M but MIP was no direct hit in MET.
Intermediate result: 80% exact / 100% group success rate.
- doc.06: correctly classified as CNE with 100% confidence.
Intermediate result: 83% exact / 100% group success rate.
- doc.07: correctly group-classified as M but MG was no direct hit in MET.
Intermediate result: 71% exact / 100% group success rate.
- doc.08: correctly classified as CNE with 78% confidence.
Intermediate result: 75% exact / 100% group success rate.
- doc.09: correctly group-classified as M but MG was no direct hit in MET.
Intermediate result: 66% exact / 100% group success rate.
- doc.10: correctly group-classified as M but MMF was no direct hit in MG-LOS.
Intermediate result: 60% exact / 100% group success rate.
- doc.11: correctly classified as CEC with 62% confidence.
Intermediate result: 63% exact / 100% group success rate.
- doc.12: correctly classified as CEC with 56% confidence.
Intermediate result: 66% exact / 100% group success rate.
- doc.13: FAILED. Expected CEC-CED but got this:
With 14% confidence this is MG.
Other possible classifications:... 10% LPM
... 9% MET
... 8% MPD
... 8% BIP
... 8% BFI
... 7% MSI
... 6% CEC
... 6% LOS
... 5% CED
... 4% LAA
... 3% CNE
... 2% BRC
... 2% LEB


Intermediate result: 61% exact / 92% group success rate.
- doc.14: correctly classified as LAA with 100% confidence.
Intermediate result: 64% exact / 92% group success rate.
- doc.15: correctly classified as BRC with 78% confidence.
Intermediate result: 66% exact / 93% group success rate.
- doc.16: correctly classified as BRC with 64% confidence.
Intermediate result: 68% exact / 93% group success rate.
- doc.17: correctly classified as BRC with 78% confidence.
Intermediate result: 70% exact / 94% group success rate.
- doc.18: correctly classified as BRC with 79% confidence.
Intermediate result: 72% exact / 94% group success rate.
- doc.19: correctly classified as CNE with 100% confidence.
Intermediate result: 73% exact / 94% group success rate.
- doc.20: correctly classified as CED with 61% confidence.
Intermediate result: 75% exact / 95% group success rate.
- doc.21: correctly classified as CNE with 100% confidence.
Intermediate result: 76% exact / 95% group success rate.
- doc.22: correctly classified as CNE with 100% confidence.
Intermediate result: 77% exact / 95% group success rate.
- doc.23: correctly classified as LPM with 66% confidence.
Intermediate result: 78% exact / 95% group success rate.
- doc.24: correctly classified as LPM with 32% confidence.
Intermediate result: 79% exact / 95% group success rate.
- doc.25: correctly group-classified as L but LEB was no direct hit in LPM.
Intermediate result: 76% exact / 96% group success rate.
- doc.26: correctly classified as LPM with 46% confidence.
Intermediate result: 76% exact / 96% group success rate.
- doc.27: correctly classified as LPM with 100% confidence.
Intermediate result: 77% exact / 96% group success rate.
- doc.28: correctly classified as LPM with 89% confidence.
Intermediate result: 78% exact / 96% group success rate.
- doc.29: correctly classified as LPM with 56% confidence.
Intermediate result: 79% exact / 96% group success rate.
- doc.30: correctly classified as CNE with 45% confidence.
Intermediate result: 80% exact / 96% group success rate.
- doc.31: correctly classified as BFI with 88% confidence.
Intermediate result: 80% exact / 96% group success rate.
- doc.32: correctly classified as BFI with 75% confidence.
Intermediate result: 81% exact / 96% group success rate.
- doc.33: correctly group-classified as L but LPM was no direct hit in LAA.
Intermediate result: 78% exact / 96% group success rate.
- doc.34: correctly group-classified as L but LPM was no direct hit in LAA.
Intermediate result: 76% exact / 97% group success rate.
- doc.35: correctly classified as MMF with 99% confidence.
Intermediate result: 77% exact / 97% group success rate.
- doc.36: correctly classified as MMF with 100% confidence.
Intermediate result: 77% exact / 97% group success rate.
- doc.37: correctly group-classified as B but BFI was no direct hit in BRC.
Intermediate result: 75% exact / 97% group success rate.
- doc.38: correctly classified as LEB with 56% confidence.
Intermediate result: 76% exact / 97% group success rate.
- doc.39: correctly group-classified as L but LOS was no direct hit in LEB.
Intermediate result: 74% exact / 97% group success rate.
- doc.40: correctly group-classified as B but BRC was no direct hit in BFI.
Intermediate result: 72% exact / 97% group success rate.
- doc.41: correctly group-classified as B but BRC was no direct hit in BFI.
Intermediate result: 70% exact / 97% group success rate.
- doc.42: correctly classified as CED with 52% confidence.
Intermediate result: 71% exact / 97% group success rate.
- doc.43: FAILED. Expected LEB-LAA-LPM but got this:
With 100% confidence this is CED.
No other classifications possible.

Intermediate result: 69% exact / 95% group success rate.
- doc.44: correctly group-classified as L but LPM was no direct hit in LEB-LAA.
Intermediate result: 68% exact / 95% group success rate.
- doc.45: correctly classified as MIP with 97% confidence.
Intermediate result: 68% exact / 95% group success rate.
- doc.46: correctly classified as MIP with 100% confidence.
Intermediate result: 69% exact / 95% group success rate.
- doc.47: correctly classified as MIP with 74% confidence.
Intermediate result: 70% exact / 95% group success rate.
- doc.48: correctly classified as MIP with 87% confidence.
Intermediate result: 70% exact / 95% group success rate.
- doc.49: correctly classified as MIP with 100% confidence.
Intermediate result: 71% exact / 95% group success rate.
- doc.50: correctly classified as MIP with 100% confidence.
Intermediate result: 72% exact / 96% group success rate.
- doc.51: correctly classified as BIP with 92% confidence.
Intermediate result: 72% exact / 96% group success rate.
- doc.52: correctly group-classified as B but BFI was no direct hit in BIP.
Intermediate result: 71% exact / 96% group success rate.
- doc.53: correctly classified as BIP with 95% confidence.
Intermediate result: 71% exact / 96% group success rate.
- doc.54: correctly classified as BIP with 66% confidence.
Intermediate result: 72% exact / 96% group success rate.
- doc.55: correctly group-classified as M but MG was no direct hit in BIP-MIP.
Intermediate result: 70% exact / 96% group success rate.
- doc.56: correctly classified as BIP with 81% confidence.
Intermediate result: 71% exact / 96% group success rate.
- doc.57: correctly group-classified as M but MET was no direct hit in MG.
Intermediate result: 70% exact / 96% group success rate.
- doc.58: correctly group-classified as M but MSI was no direct hit in MG.
Intermediate result: 68% exact / 96% group success rate.
- doc.59: correctly group-classified as M but MPD was no direct hit in MSI-CNE.
Intermediate result: 67% exact / 96% group success rate.
- doc.60: FAILED. Expected MPD but got this:
With 53% confidence this is CED.
Other possible classifications:... 25% LOS
... 20% MSI


Intermediate result: 66% exact / 95% group success rate.
- doc.61: correctly group-classified as M but MSI was no direct hit in MET.
Intermediate result: 65% exact / 95% group success rate.
- doc.62: FAILED. Expected MSI but got this:
With 58% confidence this is CEC.
Other possible classifications:... 41% MSI


Intermediate result: 64% exact / 93% group success rate.
- doc.63: correctly classified as MET with 87% confidence.
Intermediate result: 65% exact / 93% group success rate.
- doc.64: correctly classified as MSI with 43% confidence.
Intermediate result: 65% exact / 93% group success rate.
- doc.65: correctly group-classified as M but MIP was no direct hit in MSI.
Intermediate result: 64% exact / 93% group success rate.
- doc.66: FAILED. Expected CEC but got this:
With 100% confidence this is MSI.
No other classifications possible.

Intermediate result: 63% exact / 92% group success rate.
- doc.67: correctly classified as LAA with 37% confidence.
Intermediate result: 64% exact / 92% group success rate.
- doc.68: FAILED. Expected LPM-LEB but got this:
With 32% confidence this is MG.
Other possible classifications:... 27% BRC
... 16% LOS
... 15% LEB
... 8% LPM


Intermediate result: 63% exact / 91% group success rate.
- doc.69: correctly group-classified as M but MET was no direct hit in MG.
Intermediate result: 62% exact / 91% group success rate.
- doc.70: correctly classified as MPD with 41% confidence.
Intermediate result: 62% exact / 91% group success rate.
- doc.71: correctly classified as LOS with 98% confidence.
Intermediate result: 63% exact / 91% group success rate.
- doc.72: correctly classified as LOS with 92% confidence.
Intermediate result: 63% exact / 91% group success rate.
- doc.73: correctly classified as LOS with 99% confidence.
Intermediate result: 64% exact / 91% group success rate.
- doc.74: correctly classified as LOS with 82% confidence.
Intermediate result: 64% exact / 91% group success rate.
- doc.75: correctly group-classified as L but LPM was no direct hit in LOS.
Intermediate result: 64% exact / 92% group success rate.
- doc.76: correctly classified as CED with 73% confidence.
Intermediate result: 64% exact / 92% group success rate.
- doc.77: correctly classified as CED with 73% confidence.
Intermediate result: 64% exact / 92% group success rate.
- doc.78: correctly classified as CED with 71% confidence.
Intermediate result: 65% exact / 92% group success rate.
- doc.79: correctly classified as CED with 100% confidence.
Intermediate result: 65% exact / 92% group success rate.
- doc.80: correctly classified as CED with 54% confidence.
Intermediate result: 66% exact / 92% group success rate.
- doc.81: correctly group-classified as B but BRC was no direct hit in BFI.
Intermediate result: 65% exact / 92% group success rate.
- doc.82: FAILED. Expected BRC-BFI but got this:
With 100% confidence this is CED.
No other classifications possible.

Intermediate result: 64% exact / 91% group success rate.
- doc.83: FAILED. Expected CNE but got this:
With 87% confidence this is BRC.
Other possible classifications:... 12% CNE


Intermediate result: 63% exact / 90% group success rate.
- doc.84: correctly classified as BRC with 60% confidence.
Intermediate result: 64% exact / 90% group success rate.
- doc.85: correctly classified as BRC with 77% confidence.
Intermediate result: 64% exact / 90% group success rate.
- doc.86: FAILED. Expected LAA but got this:
With 26% confidence this is MG.
Other possible classifications:... 25% LAA
... 11% MET
... 9% MMF
... 8% LPM
... 8% LEB
... 5% BFI
... 5% BIP


Intermediate result: 63% exact / 89% group success rate.
- doc.87: correctly group-classified as M but MG was no direct hit in MET.
Intermediate result: 63% exact / 89% group success rate.
- doc.88: correctly group-classified as M but MG was no direct hit in MET.
Intermediate result: 62% exact / 89% group success rate.
- doc.89: FAILED. Expected MET-MIP but got this:
With 40% confidence this is BRC.
Other possible classifications:... 28% LEB
... 18% BFI
... 12% LPM


Intermediate result: 61% exact / 88% group success rate.
- doc.90: FAILED. Expected CNE but got this:
With 100% confidence this is MSI.
No other classifications possible.

Intermediate result: 61% exact / 87% group success rate.
- doc.91: correctly classified as CNE with 85% confidence.
Intermediate result: 61% exact / 87% group success rate.
- doc.92: correctly classified as BIP with 62% confidence.
Intermediate result: 61% exact / 88% group success rate.
- doc.93: correctly classified as BIP with 51% confidence.
Intermediate result: 62% exact / 88% group success rate.
- doc.94: FAILED. Expected CED but got this:
With 70% confidence this is LEB.
Other possible classifications:... 29% BIP


Intermediate result: 61% exact / 87% group success rate.
- doc.95: correctly classified as LPM with 46% confidence.
Intermediate result: 62% exact / 87% group success rate.
- doc.96: correctly classified as LPM with 62% confidence.
Intermediate result: 62% exact / 87% group success rate.
- doc.97: correctly classified as LPM with 100% confidence.
Intermediate result: 62% exact / 87% group success rate.
- doc.98: correctly classified as LAA with 100% confidence.
Intermediate result: 63% exact / 87% group success rate.
Done. 62 exact matches, 86 group matches, 12 humiliating fails in 98 total documents.
Final result: 63% epic / 87% group success rate.
