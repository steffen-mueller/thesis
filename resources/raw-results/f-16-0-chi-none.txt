Graph-based text classification test protocol - Thu May 14 15:28:54 UTC 2015
============================================================================

Testing all documents in 'resources\series-f'
- doc.01: correctly group-classified as B but BFI was no direct hit in BRC.
Intermediate result: 0% exact / 100% group success rate.
- doc.02: correctly classified as CNE with 100% confidence.
Intermediate result: 50% exact / 100% group success rate.
- doc.03: correctly classified as CNE with 100% confidence.
Intermediate result: 66% exact / 100% group success rate.
- doc.04: correctly group-classified as M but MG was no direct hit in MPD-MSI-MET.
Intermediate result: 50% exact / 100% group success rate.
- doc.05: correctly group-classified as M but MIP was no direct hit in MET.
Intermediate result: 40% exact / 100% group success rate.
- doc.06: correctly classified as CNE with 87% confidence.
Intermediate result: 50% exact / 100% group success rate.
- doc.07: correctly group-classified as M but MG was no direct hit in MET.
Intermediate result: 42% exact / 100% group success rate.
- doc.08: correctly classified as CNE with 76% confidence.
Intermediate result: 50% exact / 100% group success rate.
- doc.09: correctly group-classified as M but MSI was no direct hit in MET.
Intermediate result: 44% exact / 100% group success rate.
- doc.10: correctly group-classified as M but MPD was no direct hit in MG-LOS.
Intermediate result: 40% exact / 100% group success rate.
- doc.11: correctly classified as CEC with 95% confidence.
Intermediate result: 45% exact / 100% group success rate.
- doc.12: correctly classified as CEC with 71% confidence.
Intermediate result: 50% exact / 100% group success rate.
- doc.13: correctly group-classified as C but CNE was no direct hit in CEC-CED.
Intermediate result: 46% exact / 100% group success rate.
- doc.14: correctly classified as LAA with 96% confidence.
Intermediate result: 50% exact / 100% group success rate.
- doc.15: correctly classified as BRC with 94% confidence.
Intermediate result: 53% exact / 100% group success rate.
- doc.16: correctly classified as BRC with 100% confidence.
Intermediate result: 56% exact / 100% group success rate.
- doc.17: correctly classified as BRC with 85% confidence.
Intermediate result: 58% exact / 100% group success rate.
- doc.18: correctly classified as BRC with 100% confidence.
Intermediate result: 61% exact / 100% group success rate.
- doc.19: correctly classified as CNE with 38% confidence.
Intermediate result: 63% exact / 100% group success rate.
- doc.20: correctly classified as CEC with 100% confidence.
Intermediate result: 65% exact / 100% group success rate.
- doc.21: correctly classified as CNE with 40% confidence.
Intermediate result: 66% exact / 100% group success rate.
- doc.22: correctly classified as CNE with 100% confidence.
Intermediate result: 68% exact / 100% group success rate.
- doc.23: correctly classified as LPM with 71% confidence.
Intermediate result: 69% exact / 100% group success rate.
- doc.24: correctly classified as LEB with 46% confidence.
Intermediate result: 70% exact / 100% group success rate.
- doc.25: correctly group-classified as L but LEB was no direct hit in LPM.
Intermediate result: 68% exact / 100% group success rate.
- doc.26: correctly classified as LPM with 76% confidence.
Intermediate result: 69% exact / 100% group success rate.
- doc.27: correctly classified as LPM with 97% confidence.
Intermediate result: 70% exact / 100% group success rate.
- doc.28: correctly classified as LPM with 58% confidence.
Intermediate result: 71% exact / 100% group success rate.
- doc.29: correctly classified as LPM with 100% confidence.
Intermediate result: 72% exact / 100% group success rate.
- doc.30: correctly classified as CNE with 100% confidence.
Intermediate result: 73% exact / 100% group success rate.
- doc.31: correctly classified as BFI with 85% confidence.
Intermediate result: 74% exact / 100% group success rate.
- doc.32: correctly classified as BFI with 88% confidence.
Intermediate result: 75% exact / 100% group success rate.
- doc.33: correctly classified as LAA with 82% confidence.
Intermediate result: 75% exact / 100% group success rate.
- doc.34: correctly classified as LAA with 93% confidence.
Intermediate result: 76% exact / 100% group success rate.
- doc.35: correctly classified as MMF with 99% confidence.
Intermediate result: 77% exact / 100% group success rate.
- doc.36: correctly classified as MMF with 98% confidence.
Intermediate result: 77% exact / 100% group success rate.
- doc.37: correctly classified as BRC with 78% confidence.
Intermediate result: 78% exact / 100% group success rate.
- doc.38: correctly group-classified as L but LAA was no direct hit in LEB.
Intermediate result: 76% exact / 100% group success rate.
- doc.39: correctly group-classified as L but LOS was no direct hit in LEB.
Intermediate result: 74% exact / 100% group success rate.
- doc.40: correctly group-classified as B but BRC was no direct hit in BFI.
Intermediate result: 72% exact / 100% group success rate.
- doc.41: correctly group-classified as B but BRC was no direct hit in BFI.
Intermediate result: 70% exact / 100% group success rate.
- doc.42: correctly group-classified as C but CEC was no direct hit in CED.
Intermediate result: 69% exact / 100% group success rate.
- doc.43: correctly classified as LEB with 100% confidence.
Intermediate result: 69% exact / 100% group success rate.
- doc.44: correctly classified as LAA with 57% confidence.
Intermediate result: 70% exact / 100% group success rate.
- doc.45: correctly classified as MIP with 100% confidence.
Intermediate result: 71% exact / 100% group success rate.
- doc.46: correctly group-classified as M but MMF was no direct hit in MIP-MMP.
Intermediate result: 69% exact / 100% group success rate.
- doc.47: correctly classified as MIP with 61% confidence.
Intermediate result: 70% exact / 100% group success rate.
- doc.48: correctly classified as MIP with 51% confidence.
Intermediate result: 70% exact / 100% group success rate.
- doc.49: correctly classified as MIP with 69% confidence.
Intermediate result: 71% exact / 100% group success rate.
- doc.50: correctly classified as MIP with 55% confidence.
Intermediate result: 72% exact / 100% group success rate.
- doc.51: correctly classified as BIP with 100% confidence.
Intermediate result: 72% exact / 100% group success rate.
- doc.52: correctly classified as BIP with 91% confidence.
Intermediate result: 73% exact / 100% group success rate.
- doc.53: correctly classified as BIP with 100% confidence.
Intermediate result: 73% exact / 100% group success rate.
- doc.54: correctly classified as BIP with 97% confidence.
Intermediate result: 74% exact / 100% group success rate.
- doc.55: correctly classified as BIP with 57% confidence.
Intermediate result: 74% exact / 100% group success rate.
- doc.56: correctly classified as BIP with 99% confidence.
Intermediate result: 75% exact / 100% group success rate.
- doc.57: correctly group-classified as M but MET was no direct hit in MG.
Intermediate result: 73% exact / 100% group success rate.
- doc.58: correctly group-classified as M but MSI was no direct hit in MG.
Intermediate result: 72% exact / 100% group success rate.
- doc.59: correctly group-classified as M but MG was no direct hit in MSI-CNE.
Intermediate result: 71% exact / 100% group success rate.
- doc.60: correctly group-classified as M but MSI was no direct hit in MPD.
Intermediate result: 70% exact / 100% group success rate.
- doc.61: correctly group-classified as M but MSI was no direct hit in MET.
Intermediate result: 68% exact / 100% group success rate.
- doc.62: correctly group-classified as M but MPD was no direct hit in MSI.
Intermediate result: 67% exact / 100% group success rate.
- doc.63: correctly classified as MET with 66% confidence.
Intermediate result: 68% exact / 100% group success rate.
- doc.64: correctly classified as MG with 44% confidence.
Intermediate result: 68% exact / 100% group success rate.
- doc.65: correctly group-classified as M but MG was no direct hit in MSI.
Intermediate result: 67% exact / 100% group success rate.
- doc.66: correctly classified as CEC with 64% confidence.
Intermediate result: 68% exact / 100% group success rate.
- doc.67: correctly classified as LAA with 54% confidence.
Intermediate result: 68% exact / 100% group success rate.
- doc.68: FAILED. Expected LPM-LEB but got this:
With 100% confidence this is CNE.
No other classifications possible.

Intermediate result: 67% exact / 98% group success rate.
- doc.69: correctly group-classified as M but MPD was no direct hit in MG.
Intermediate result: 66% exact / 98% group success rate.
- doc.70: correctly group-classified as M but MG was no direct hit in MPD.
Intermediate result: 65% exact / 98% group success rate.
- doc.71: correctly classified as LOS with 100% confidence.
Intermediate result: 66% exact / 98% group success rate.
- doc.72: correctly classified as LOS with 95% confidence.
Intermediate result: 66% exact / 98% group success rate.
- doc.73: correctly classified as LOS with 100% confidence.
Intermediate result: 67% exact / 98% group success rate.
- doc.74: correctly classified as LOS with 61% confidence.
Intermediate result: 67% exact / 98% group success rate.
- doc.75: correctly group-classified as L but LPM was no direct hit in LOS.
Intermediate result: 66% exact / 98% group success rate.
- doc.76: correctly classified as CED with 79% confidence.
Intermediate result: 67% exact / 98% group success rate.
- doc.77: correctly classified as CED with 100% confidence.
Intermediate result: 67% exact / 98% group success rate.
- doc.78: correctly classified as CED with 100% confidence.
Intermediate result: 67% exact / 98% group success rate.
- doc.79: correctly classified as CED with 100% confidence.
Intermediate result: 68% exact / 98% group success rate.
- doc.80: correctly classified as CED with 100% confidence.
Intermediate result: 68% exact / 98% group success rate.
- doc.81: correctly group-classified as B but BRC was no direct hit in BFI.
Intermediate result: 67% exact / 98% group success rate.
- doc.82: correctly classified as BRC with 100% confidence.
Intermediate result: 68% exact / 98% group success rate.
- doc.83: FAILED. Expected CNE but got this:
With 100% confidence this is LPM.
No other classifications possible.

Intermediate result: 67% exact / 97% group success rate.
- doc.84: correctly classified as BRC with 89% confidence.
Intermediate result: 67% exact / 97% group success rate.
- doc.85: correctly classified as BRC with 92% confidence.
Intermediate result: 68% exact / 97% group success rate.
- doc.86: correctly classified as LAA with 94% confidence.
Intermediate result: 68% exact / 97% group success rate.
- doc.87: correctly classified as MET with 100% confidence.
Intermediate result: 68% exact / 97% group success rate.
- doc.88: correctly classified as MET with 100% confidence.
Intermediate result: 69% exact / 97% group success rate.
- doc.89: correctly group-classified as M but MG was no direct hit in MET-MIP.
Intermediate result: 68% exact / 97% group success rate.
- doc.90: correctly classified as CNE with 100% confidence.
Intermediate result: 68% exact / 97% group success rate.
- doc.91: correctly classified as CNE with 100% confidence.
Intermediate result: 69% exact / 97% group success rate.
- doc.92: correctly classified as BIP with 100% confidence.
Intermediate result: 69% exact / 97% group success rate.
- doc.93: correctly classified as BIP with 100% confidence.
Intermediate result: 69% exact / 97% group success rate.
- doc.94: correctly classified as CED with 46% confidence.
Intermediate result: 70% exact / 97% group success rate.
- doc.95: FAILED. Expected LPM but got this:
With 62% confidence this is BFI.
Other possible classifications:... 37% LPM


Intermediate result: 69% exact / 96% group success rate.
- doc.96: correctly classified as LPM with 72% confidence.
Intermediate result: 69% exact / 96% group success rate.
- doc.97: correctly classified as LPM with 80% confidence.
Intermediate result: 70% exact / 96% group success rate.
- doc.98: correctly classified as LAA with 90% confidence.
Intermediate result: 70% exact / 96% group success rate.
Done. 69 exact matches, 95 group matches, 3 humiliating fails in 98 total documents.
Final result: 70% epic / 96% group success rate.
