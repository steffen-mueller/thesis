www.hpcc.gov/imp95/section.5.7.html
<PRE>
     
Agency:                 NIST  
     
Activity:               NREN Deployment and Performance Measures for
                        Gigabit Nets & MPP Sys.
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:		1.50	
	FY 1994 Pres. Rqst.:	3.00	
	FY 1994 Estimated:	2.20	
	FY 1995 Pres. Rqst.:	2.20	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	0.30
	NREN:	1.90
	ASTA:	
	IITA:	
	BRHR:	
     
Agency Ties:
     
	ARPA:	Partner		
	NSF:			
	DOE:	Partner	
	NASA:		
	NIH:	
	NSA:	
	NIST:	
	NOAA:	User
	EPA:	User
	ED:	
     
Activity Description:
     
     Promote the development of communications infrastructure
     and its use via information technology research,
     development, and related activities to enhance basic
     communications capabilities.  Support and coordinate the
     development of communications standards within the Federal
     government to provide interoperability, common user
     interfaces to systems, and enhanced security; work with
     other agencies to promote open system standards to aid in
     the commercialization of technology by U.S. industry. NIST
     will utilize its expertise to enable transition planning
     and deployment of ISDN and OSI-based protocols in the NREN
     and undertake R&D to support emerging Broadband ISDN
     standards. 
     
     NIST developed the MultiKron instrumentation chip to
     achieve low-perturbation data capture while assessing the
     performance of scalable high performance computer systems
     and high speed networking technologies.  Performance
     evaluation activities include measurement and
     characterization of the impact of software protocols on
     communications performance in order to minimize
     communications bottlenecks between application programs.  
     NIST's researcher have implemented a high speed
     communications testbed that integrates supercomputers,
     HIPPI interfaces, workstations, and SONET communications
     technologies, and employs NIST's VLSI-based MultiKron
     performance measurement chip to investigate the behavior
     and performance of communications protocols suitable for
     multi-gigabit/sec. transmission rates.  NIST collaborates
     with industry, academia and other interested agencies.
     
FY 1993 Accomplishments:

     Sponsorship of North American ISDN Users Forum
     continues; industry and users met four times to address
     implementation issues. 
     
     NIST designed, developed and has shared a designer and
     experimenter's kit which allows the Multikron
     instrumentation to be integrated into systems using the
     industry standard VME bus.    
     
     Devised a software-based technique for portable,
     sensitivity-to- performance measurements of software
     executing on scalable parallel computing systems.  
     
     Applied Multikron instrumented systems to assess
     performance of TCP/IP Protocols over a HIPPI-based network
     (High Performance Parallel Interface) and published
     results.
     
FY 1994 Major Milestones:

     Apply Mulitkron-based performance measurement
     technology to Asynchronous Transfer Mode (ATM) based
     networks to identify bottlenecks and publish results.  
     
     Conduct simulation studies of ATM-based protocols and
     networks to ascertain dynamic behaviors and performance;
     contribute results to appropriate standards bodies
     including the ATM Forum, Am. Nat. Standards
     Telecommunications Committee  (T1S1), and the International
     Telecommunications Union, Telecom Sector Committee.  
     
     Continue to sponsor the ISDN User's Forum; facilitate
     the ISDN demonstration entitled: "ISDN Solutions -94-" and
     participate in it.
     
FY 1995 Major Milestones:

     Collaborate with the ATM Forum to develop standardized
     conformity tests for ATM-based technology.  
     
     Collaborate with industry and users to establish an
     ATM testbed; conduct experiments and tests of evolving
     products.       

     Conduct R&D related to media access and flow control for ATM
     protocols; and congestion control algorithm.  Publish results
     and share in appropriate standards fora.

Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Development and Dissemination of Scientific
                        Software for HPCS
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:		0.60	
	FY 1994 Pres. Rqst.:	4.10	
	FY 1994 Estimated:	3.60	
	FY 1995 Pres. Rqst.:	7.60	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	
	ASTA:	4.60
	IITA:	3.00
	BRHR:	
     
Agency Ties:
     
	ARPA:			
	NSF:	Partner		
	DOE:	Partner	
	NASA:	Partner	
	NIH:	
	NSA:	
	NIST:	
	NOAA:	
	EPA:	User
	ED:	
     
Activity Description:
     
     Develop advanced mathematical, computational and
     visualization algorithms, software, methodology and tools
     which support the efficient application of computationally
     intensive science to key problems arising in the industrial
     sector.  Current application areas addressed include the
     development of improved computational methods for chemical
     processing and related advanced materials processing
     industries.   Topics of emphasis are methods for improved
     advanced product and process design and the generation of
     computationally derived fundamental data for
     environmentally sound competitive manufacturing processes.  
     Technology areas of emphasis include the interactive
     visualization of complex structures and data, parallel
     algorithms, large-scale Ab Initio and semi-empirical
     computational chemistry, Monte Carlo and molecular dynamics
     modeling and scientific database utilization. 

     This program activity also supports the development of
     modern, network-based reusable software classification and
     distribution technology for making new computational
     software readily available to industry and the public.  The
     focus of this activity is the NIST Guide to Available
     Mathematical Software (GAMS) project.  GAMS is a working
     virtual software repository which provides users convenient
     access to thousands of software modules which currently
     reside in four separate repositories.

FY 1993 Accomplishments:

     Electronic access by the public and industry to NIST's
     Guide to Available Mathematical Software (GAMS) reusable
     software database was augmented with expanded network
     access, and an improved graphical user interface.     
     
     GAMS software coverage was extended to over 8800
     software modules in 70 mathematical and statistical
     software packages, including 200 additional modules from
     the netlib repository of Oak Ridge National Laboratory.       
     
     Improved methods for evaluating and characterizing
     parallel random number generators were developed based upon
     a mapping of the point set to one dimensional strings.
     
FY 1994 Major Milestones:

     Develop and implement initial parallel algorithms for
     applications in computational chemistry including Monte
     Carlo methods, molecular dynamics, fluid dynamics,
     interactive molecular graphics and quantum chemistry.          
     
     Develop scalable algorithms for the computation of
     spin-dependent electron transport properties of magnetic
     multilayer structures used in magnetic storage devices.            
     
     Develop prototype visualization tools and scalable
     algorithms which support common needs in computational
     chemistry and materials properties.         
     
     Enhance the GAMS system interface and develop a
     client/server architecture to provide a software
     dissemination testbed.
     
FY 1995 Major Milestones:

     Develop and implement methods for applying quantum
     chemistry to the prediction of properties and reactivities
     of solvated systems, including the introduction of discrete
     and continuum solvent representations into modern quantum
     chemistry codes and the use of molecular dynamics and Monte
     Carlo methods to explore the energetics of solvent/solute
     clusters.        
     
     Develop scalable quantum molecular dynamics software
     for modeling growth of metal films on semiconductor
     surfaces, and apply to optimizing the performance of x-ray
     multilayer mirrors.              

     Demonstrate a useful and reliable GAMS service for the access
     and retrieval of reusable software which supports automated
     expert advice on high performance computer algorithms and
     software.

Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Support for Electronic Commerce
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:	2.50	
	FY 1994 Estimated:	0.95	
	FY 1995 Pres. Rqst.:	2.75	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	
	ASTA:	
	IITA:	2.75
	BRHR:	
     
Agency Ties:
     
	ARPA:			
	NSF:			
	DOE:		
	NASA:		
	NIH:	
	NSA:	
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Collaborate with industry and other agencies to develop or
     apply technologies which enable electronic commerce in
     general, and as it applies to manufacturing electronic and
     mechanical components and subsystems, including electronic
     information query, dissemination and subsequent integration
     of product specifications into computer aided design and
     manufacturing tools.  Establish supporting laboratory
     facilities to support demonstrations, evaluation, and
     inter-operation testing.  Conduct R&D to support
     development of tools which generate software for electronic
     commerce (EC) applications; develop an EC integration
     facility at NIST; develop requirements and prototype
     systems for handling complex data in Electronic Data
     Interchange (EDI); and design and develop prototype
     interfaces among EDI, Structured Query Language (SQL), and
     Remote Database Access (RDA).   Also, conduct R&D leading
     to development of application programming interfaces for
     digital signature, authentication, and other security
     services required to support electronic commerce.  
     Establish cooperative R&D agreements with researchers,
     vendors and users to better understand requirements and to
     acquire, test, interconnect and demonstrate prototype
     systems.  Collaborations will include DOE: Lawrence
     Livermore and Sandia National Labs; DOD/Defense Logistics
     Agency; Dept. of Treasury; and other interested agencies.
     
FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:

     Develop an Electronic Commerce integration and
     demonstration facility at NIST; solicit industry
     participation and demonstration of products.       
     
     Prepare for and complete basic functionality of
     electronic procurement pilot project.      
     
     Determine interoperability issues related to use of
     value added networks and electronic mail.       
     
     Determine and initiate development or integration of
     security services need; e.g., digital signature,
     authentication services.      
     
     Develop prototype software which can generate
     interfaces and mapping software for EDI applications given
     a description of a transaction set;  demonstrate selected
     applications.
     
FY 1995 Major Milestones:

     Demonstrate electronic procurement and publish
     application guidelines for government.      
     
     Continue development and integration of required
     security services.      
     
     Provide specifications for on-line catalogs for
     electronic and mechanical components.      
     
     Extend the demonstration facilities to support ad hoc
     query and response to trading partner databases via
     EDI/SQL/RDA-based standards.      
     
     With industry, publish a draft report describing a
     strategy to  use the Standard for the Exchange of Product
     Model Data (STEP), Initial Graphics Exchange Specification
     (IGES), Computer Graphics Metafile (CGM) and Programmers
     Hierarchical Interactive Graphics System (PHIGS) for
     management and presentation of product data.
     
Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Electronic Libraries and Distributed Multimedia
                        Applications
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:	2.00	
	FY 1994 Estimated:	0.85	
	FY 1995 Pres. Rqst.:	1.25	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	
	ASTA:	
	IITA:	1.25
	BRHR:	
     
Agency Ties:
     
	ARPA:			
	NSF:			
	DOE:		
	NASA:		
	NIH:	
	NSA:	
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Establish an electronic library pilot project which
     includes the creation, editorial review, production and
     dissemination of scientific and engineering papers,
     documents, data and other published information in a
     working, production environment via the library and
     scientific and engineering operating units at NIST.  Within
     this environment provide for the organization, search and
     retrieval, and dissemination of information by electronic
     means using state-of-the-art definition and dissemination
     technologies for documents and multimedia objects,
     particularly device-independent, portable, exchange formats
     for information objects.    
     
     Within this context, determine the adequacy of individual
     multimedia and database standards and possible extensions
     based on the application of object technology to
     multimedia;  develop a profile or architectural framework
     describing the required standards and their
     interrelationships; develop standards, prototypes, and
     demonstrations in collaboration with industry, academia and
     other interested parties through cooperative research and
     development agreements. 
     
     Collaborate with other parties within academia, the library
     and information communities, other agencies and industry to
     investigate the framework for an electronic, distributed,
     national engineering library.
     
FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:

     Establish a pilot project at NIST to evaluate
     alternates to traditional paper-based authoring, publishing
     and information dissemination.      
     
     Establish a cooperative R&D agreement with Adobe to
     assess Acrobat/ Portable Document Format as a document
     interchange format.      
     
     Initiate R&D on the communications services needed to
     support transfer of multimedia information objects.      
     
     Establish initial collaborations with other interested
     parties.
     
FY 1995 Major Milestones:

     Integrate and demonstrate the use of Portable Document
     Format (PDF) via network-based support tools like Mosaic.      
     
     Establish a formal integration process to enable
     authors to create and disseminate PDF-based documents via
     Mosaic accessible information bases.      
     
     Develop and publish plans to establish a model for a
     national scientific, technical and engineering information
     resource; draft initial specifications for infrastructure
     and information content.
     
Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Systems Integration for Manufacturing
                        Applications
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:	12.80	
	FY 1994 Estimated:	 9.00	
	FY 1995 Pres. Rqst.:	25.20	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	
	ASTA:	
	IITA:	25.20
	BRHR:	
     
Agency Ties:
     
	ARPA:	Partner		
	NSF:			
	DOE:	Partner	
	NASA:		
	NIH:	
	NSA:	
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Emphasis is on systems integration technologies that
     support flexible and rapid access to information for
     manufacturing applications.  A standards-based data
     exchange effort for computer integrated manufacturing
     focuses on improving data exchange among design, planning
     and production activities.  Prototype systems and interface
     specifications will be communicated to standards
     organizations.  Results will be made available to US
     industry through workshops, training materials, electronic
     data repositories and pre-commercial prototype systems that
     can be installed by potential vendors for test and
     evaluation in NIST and industry supported Advanced 
     Manufacturing Systems and Networking Testbeds. NIST will
     distribute standards reference data, technical information,
     and digital product data designs via digital library
     technologies.  

     NIST's manufacturing testbed will support R&D in high
     performance manufacturing systems and testing computer and
     networking hardware and software in a manufacturing
     environment, and will be extended to include applications
     in mechanical, electronics, construction, and chemical
     industries; electronic commerce applications in mechanical
     and electronic products; apparel manufacturing; integration
     of materials performance data into computer aided design;
     harmonization of product data exchange and electronic data
     interchange standards; technology transfer of information
     for electronics and construction industries; and an
     integrated Standard Reference Data system.  The testbed
     will serve as a demonstration site for use by industrial
     technology suppliers and users, and assist industry in the
     development and implementation of voluntary standards. 
     
     The work will support cooperative research and development
     agreements with industry and academic researchers.

FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:

     Install and evaluate state-of-the-art manufacturing
     systems for design and planning of mechanical part
     production.
     
     Implement a prototype application protocol development
     environment for product data exchange standards. 
     
     Publish report on reference architecture for
     manufacturing control systems and interface specifications
     for production systems. 
     
     Provide electronic technology transfer capabilities
     for NIST's manufacturing projects in response to industry
     request for online research  information for advanced
     manufacturing systems.
     
FY 1995 Major Milestones:

     Transfer interface specifications for production
     control systems into industry developed testbeds for
     verification purposes. 
     
     Evaluate additional state-of-the-art software systems
     for scheduling and production control of mechanical parts
     production. 
     
     Publish requirements for a computer-aided
     manufacturing engineering tools environment. 
     
     Specify application protocols for apparel and
     electronics mfg.
     
     Publish a reference model for process plant design. 
     
     Implement a manufacturing testbed with the apparel
     industry. 

     Demonstrate a minimum set of conformance
     testing capabilities for the Standard for the Exchange of
     Product Data (STEP). 
     
     Make selected standard reference data available via an
     improved human-machine interface.

Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Assurance, Reliability and Integrity of NREN
                        Objects
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:		
	FY 1994 Estimated:		
	FY 1995 Pres. Rqst.:	1.20	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	
	ASTA:	
	IITA:	1.20
	BRHR:	
     
Agency Ties:
     
	ARPA:			
	NSF:			
	DOE:		
	NASA:		
	NIH:	
	NSA:	Partner
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Conduct R&D regarding the applicability of object oriented
     design and development methodology to development of
     security technology.  Extend and apply the Common Criteria
     which is being developed by governments in North America
     and Europe.  Develop and publish criteria for specifying
     security requirements, methods for assuring requirements
     are met, and methods to perform certification and
     accreditation; and initiate development of an information
     technology security accreditation program.
     
FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:
     
     N/A
     
FY 1995 Major Milestones:

     Conduct R&D on object oriented development methods for
     improving security, reliability, and integrity requirements
     of objects.  
     
     Publish guidance on the use of Common  Criteria for
     specification of security, reliability and integrity
     requirements. 
     
     Create and publish a set of profiles which specify
     security, reliability, and integrity requirements that are
     applicable to NREN/NII objects.
     
Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Specification and Testing of High Integrity,
                        Distributed Systems
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:	1.70	
	FY 1994 Estimated:		
	FY 1995 Pres. Rqst.:	4.00	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	
	ASTA:	
	IITA:	4.00
	BRHR:	
     
Agency Ties:
     
	ARPA:			
	NSF:			
	DOE:		
	NASA:		
	NIH:	
	NSA:	
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Conduct R&D to extend methods to formally specify, verify
     and automate testing for conformity and interoperability of
     high integrity, secure, communications protocols and
     distributed software systems.  Evaluate object oriented
     methods and technologies for specification of high
     integrity systems.  Develop and extend computer aided
     design, analysis and testing methodologies to support
     protocol and software engineering through the use of object
     oriented technologies.  Software engineering environments
     (SEEs) are specifically designed to facilitate software
     design, validation, production and maintenance.  But, SEEs
     suffer from problems in interoperability and information
     sharing where the tools do not readily share information or
     communicate with each other.  Conduct research, develop
     test-suites, and demonstrate and validate concepts required
     to support an integrated software engineering environment. 
     Identify required software tool interfaces to promote
     applications interoperability and portability across a
     distributed software engineering environment.   
     
     Also, conduct R&D to define a process by which an
     organization: defines, specifies and manages the
     application of distributed systems to its operation;
     identifies standards and component technologies which can
     be used to assemble distributed systems; and establish a
     testbed to manage federated systems (distributed systems
     owned by more than one entity).
     
FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:
     
     N/A
     
FY 1995 Major Milestones:

     Conduct workshops with industry, academia and
     government. 
     
     Establish laboratory facilities to conduct R&D and
     assess candidate technologies which support formal
     specification, automated testing, and integrated software
     engineering. 
     
     Collaborate with industry and standards bodies to
     support the specification, development, validation and
     testing of standards and conforming products. 
     
     Assess requirements for integrated software
     engineering tools and object oriented specification
     technologies.  Publish a framework for object oriented
     services supporting integrated software engineering
     environments; solicit review and comment.
     
     Conduct R&D to establish a distributed systems
     definition methodology for management of federated systems.
     
Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Language, image and text processing
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:		
	FY 1994 Estimated:	1.50	
	FY 1995 Pres. Rqst.:	4.00	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	
	ASTA:	
	IITA:	4.00
	BRHR:	
     
Agency Ties:
     
	ARPA:	Partner		
	NSF:			
	DOE:		
	NASA:		
	NIH:	
	NSA:	Partner
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Conduct cooperative R&D and promote the development of
     algorithms and related reference materials necessary to
     conduct research, development, and commercialization of
     such research for target application areas in speech
     understanding, image recognition, and intelligent text
     retrieval.  NIST will collaborate with the research
     communities in spoken natural language, printed and cursive
     handwriting recognition, and text search and retrieval by
     providing speech, image and text corpora as reference
     materials, and developing performance measures and scoring
     protocols.
     
FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:

     Implement a prototype wide area information service
     (WAIS) using NIST's Probabilistic Ranking Information
     Search Engine (PRISE) system to access the data.  The PRISE
     system is a search engine that has proven user
     effectiveness on large collections of text.  
     
     Develop optical character recognition (OCR) techniques
     to segment bitmapped page images into components (text,
     figures, tables, etc.).   
     
     Establish a demonstration library to serve
     manufacturing interests, incorporating robust user
     searching methods, and providing access to CALS (Computer
     Aided Logistics Standards (CALS) information.
     
FY 1995 Major Milestones:

     Develop an initial dialogue management module which
     will provide a spoken-language query interface to library
     information databases. 
     
     Develop initial algorithms for understanding of
     documents and associated metrics for judging algorithm
     performance. 
     
     Develop a reference implementation of an enhanced wide
     area information service with appropriate protocol
     extensions for probabilistic search criteria
     (WAIS/Z39.50+).  
     
     Initiate investigations with OCR techniques to segment
     multimedia information sources.
     
Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                  NIST  
     
Activity:               Metrology for Future Generations of
                        Microelectronics
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:		
	FY 1994 Estimated:		
	FY 1995 Pres. Rqst.:	6.45	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	6.45
	NREN:	
	ASTA:	
	IITA:	
	BRHR:	
     
Agency Ties:
     
	ARPA:			
	NSF:			
	DOE:	Partner	
	NASA:		
	NIH:	
	NSA:	
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Provide advanced measurement support for semiconductors and 
     microelectronics and the measurement standards that are
     critical for such  technologies.  The support for
     semiconductor and microelectronics focuses on  measurement
     technology to:   (1) control fabrication processes in real-
     time to  produce more complex systems; (2) characterize
     semiconductor materials with  greater precision and over
     the smaller dimensions that are needed for faster  systems;
     and (3) support industry's development of advanced
     lithography to  increase both speed and functionality. 
     These efforts will ensure availability  of advanced
     technologies required for high performance components and
     systems  and will provide a metrological foundation for the
     more powerful and measurement  demanding systems to follow. 
     It also enables U.S. industry to produce the basic 
     approaches for complex configurations of workstations,
     advanced computers,  network servers, and networks.
     
FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:
     
     N/A
     
FY 1995 Major Milestones:

     Submit draft standards for the activation energy of
     electro- migration and for the acceleration factor for
     current-density stress to the Joint Electron Device
     Engineering Council. 
     
     Demonstrate in-situ process monitoring of film
     resistivity and thermal history during film deposition.  
     
     Conduct 2-D laser scatter imaging of silicon etching
     plasmas and  correlate to other plasma measurements as the
     first comprehensive 2-D measurements ever obtained in a
     well characterized plasma reactor.   
     
     Select and test components for wafer-plane
     spectroradiometers, including array  detectors, filters,
     monochromators, light collectors, diffusers, and fiber
     optics.  Select light sources to test as calibration
     sources.
     
Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

Agency:                 NIST  
     
Activity:               Metrology to support mobile and fixed based
                        communications  networks
     
Budget Account Code:    STRS for Computer Sys. 370
     
Budget  (BA, $ M):	
     
	FY 1993 Actual:			
	FY 1994 Pres. Rqst.:		
	FY 1994 Estimated:		
	FY 1995 Pres. Rqst.:	1.75	
     
HPCC Component for FY 1995 Presidential Request:
     
	HPCS:	
	NREN:	1.75
	ASTA:	
	IITA:	
	BRHR:	
     
Agency Ties:
     
	ARPA:			
	NSF:			
	DOE:		
	NASA:		
	NIH:	
	NSA:	
	NIST:	
	NOAA:	
	EPA:	
	ED:	
     
Activity Description:
     
     Provide advanced measurement support for advanced wired,
     optical, wireless and mixed telecommunications media. 
     Initial metrology will characterize high-performance
     microwave  integrated circuits and will provide for the
     timing of very high speed signals required for future
     mobile and fixed based networks.  The support for microwave
     integrated circuits emphasizes wafer-level standards,
     miniature individual components, and the dielectric and
     magnetic materials used in these components.  The support
     for telecommunications timing systems includes enhanced
     modeling of degradation of synchronization of optical
     networks, analyzing and measuring phase noise in such
     systems, coordinating a U.S. industry position on timing
     distribution, and advocating the latter through
     international committees.
     
FY 1993 Accomplishments:
     
     N/A
     
FY 1994 Major Milestones:
     
     N/A
     
FY 1995 Major Milestones:

     Enhance impedance characterization methods to allow
     characterization of crosstalk in multiple, lossy, high-
     speed, electronic interconnections using both frequency and
     time domain techniques.  Demonstrate utility of methods
     using commercial components.  Submit methods for adoption
     as industry standards.  
     
     Develop more efficient and simpler methods and
     systems for phase-noise characterization of synchronization
     components and systems proposed for the gigabit-per-second
     data rates.
     
Milestone Changes from Prior Implementation Plan:
     
     N/A

=========================================================

</PRE>

