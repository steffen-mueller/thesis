www.inmotionmagazine.com/pedro2.html
<HTML>
<HEAD>
    <TITLE>A Popular Movement for Social Justice - Pedro Noguera</TITLE>
</HEAD>
<BODY BGCOLOR="#ffffff">
<H3><A HREF="index3.html"><IMG SRC="returnicon2.gif" WIDTH="237" HEIGHT=
"49" ALIGN=bottom NATURALSIZEFLAG="3" BORDER="0"><BR>
<BR>
</A></H3>
<H3><CENTER>Defending Affirmative Action</CENTER>
</H3>
<H1><CENTER>A Popular Movement for Social Justice</CENTER>
</H1>
<H3><CENTER>by Pedro Noguera<BR>
Berkeley, California<BR>
</CENTER>
</H3>
<P><CENTER><IMG SRC="berk1.gif" WIDTH="268" HEIGHT="184" ALIGN=top NATURALSIZEFLAG=
"0"><BR>
<I>A recent rally for affirmative action<BR>
at the University of California, Berkeley.</I><BR>
Photo by Bruce Akizuki. </CENTER>
<H3><CENTER></CENTER>
</H3>
The fight for affirmative action finds us defending a policy that many had
begun to take for granted. Though it was born out of the struggle for civil
rights, affirmative action gradually gained wide acceptance at universities
and among employers in the public and private sector. In fact, because affirmative
action has been so firmly integrated into the infrastructure of mainstream
American society, many people find themselves shocked at its present vulnerability.
<BR>
<BR>
Though many people of color have been critical of affirmative action because
in many cases it has not benefited the poor as much as it has helped the
middle class, affirmative action must be defended. To date, affirmative
action constitutes the most meaningful and effective means devised by government
for delivering the promise of equal opportunity. Without a policy that holds
universities and employers accountable for who they admit the pledge to
not discriminate is meaningless, and there is little doubt that most organizations
would drift back to being mostly white. As a consequence of affirmative
action, white women and people of color have gained access to higher education
and professional jobs in such significant numbers that over the last twenty
years there has been a dramatic rise in income for a growing number of individuals
from these groups. Unfortunately, too many of those who have benefited most
from affirmative action fail to recognize its connection to the struggle
against racism in the United States. <BR>
<BR>
Now we find ourselves confronted by a full-scale attack against affirmative
action. Though thus far only the state of California has enacted anti-affirmative
action policies via the UC Board of Regents and Governor Pete Wilson's brazen
political opportunism, several states and the U.S. Congress have proposed
legislation that would undo these policies. This is occurring at a time
when progressive forces are largely unprepared to mount a counter-offensive.
Students and a variety of civil rights groups have begun to mobilize in
defense of affirmative action, but so far a mass movement capable of reversing
this trend has not materialized. <BR>
<BR>
Clearly, a mass movement will be necessary to counter not only the attacks
on affirmative action, but the broader offensive directed at poor and working
class people generally. Since the 1994 elections, right-wing Republicans
have been moving with great speed to enact their reactionary contract on
America. The shut down of the federal government is just an indication of
how determined they are to see their vision for this country realized. So
far, Clinton and his allies in the Democratic party have proven to be no
match for Newt Gingrich and his gang, both because they agree with parts
of that agenda (as demonstrated by Clinton's support for NAFTA and his administration's
unwillingness to raise the minimum wage) and because they lack the conviction
to meet the challenge forcefully.<BR>
<BR>
Defending affirmative action and responding to the broader right-wing offensive
is made difficult by the fragmentation and lack of shared vision that characterizes
progressive people in the U.S. Several of the groups and individuals that
have benefited from the civil rights struggle are now being manipulated
to lead the attack against it. Its no accident that Black conservative,
Ward Connerly, has become the spokesman against affirmative action in California,
or that Asian Americans are described as the primary victims of the &quot;reverse
discrimination&quot; that it supposedly causes. Moreover, many others are
cynical toward the policy because its existence hasn't prevented the growth
of poverty in our cities, nor has it or can it compensate for the glaring
inequities that characterize education throughout this country.<BR>
<BR>
Despite its limitations, and in spite of the obstacles, affirmative action
must be defended. Racism is alive and well in America, and there is no reason
to believe that universities and employers no longer need to be held accountable
in ways that affirmative action ensures. Groups and individuals who are
being encouraged to see affirmative action as conflicting with their interests,
must be educated about the ways in which the opportunities it creates benefit
society as a whole. This must occur within the context of a larger effort
to build a broad movement to counter the forces of reaction in this country.<BR>
<BR>
The question is how. How do we overcome the fragmentation and hopelessness
that characterizes progressive forces in many parts of the US. today? I
believe we can only do this by linking the fight for affirmative action
to the other issues that directly affect the quality of life of most people,
and through concerted and strategic organization. The only way to counter
the prevailing sentiment that each individual or group must protect their
own interests even at the expense of others, is by making it clear that
our collective efforts can be mutually supportive. Those concerned with
health care, education or labor rights, must be made to see how those issues
connect to immigrant rights, defense of affirmative action and more humane
approaches to addressing crime. This kind of coalition building was given
a boost during the Jackson campaigns in 1984 and 1988. It must be revived
in the 1990s.<BR>
<BR>
But perhaps organizing and coalition building must take a different form
in the 1990s. My experience participating in the affirmative action organizing
efforts at UC Berkeley leads me to believe that instead of focusing exclusively
on bringing groups together on the basis of their racial identity or particular
interest, that we must find ways to organize around a common set of interests
and even a common vision for the future. Differences in outlook and perspective
will certainly be present and create tensions, but a common vision is needed
to overcome the ways we have been splintered and factionalized. <BR>
<BR>
I am struck by the fact that many of the young activists I speak to are
more interested in building a broad movement than they are in finding the
comfort of a small group of people who think like they do. Some of the more
enlightened have even come to see that building unity requires suppressing
differences and finding ways to communicate with and organize people who
may not think or look like you do. Interestingly, these young activists
have learned some of these lessons on their own because so many of the veterans
of past struggles are no longer actively participating in any sort of struggle.
That too must change. In the months and years ahead, those of us who took
a hiatus from political activity to raise a family, pursue a career or get
an education, must get involved again. Its not good enough to complain about
where things are heading. Each of us must find ways to get involved again
and do what we can to build a popular movement for social justice, for as
far as I can tell there is still no substitute for organizing. <BR>
<BR>
The fight to defend affirmative action may be the best opportunity for doing
this. The issue lends itself quite easily to coalition building. Moreover,
because it was a concession won through past struggles, the effort to insure
its preservation also represents a clear sign that those who fought in the
past haven't just given up. <BR>
<BR>
Winning this fight won't be easy, but nothing worthwhile ever is. As Frederick
Douglas pointed out long ago &quot;Power concedes nothing without demand.
It never has and it never will.&quot; That was true then and its true now
as well. <BR>
<BR>
<HR><B><I>Dr. Pedro Noguera </I></B><I>is a professor of education at the
University of California, Berkeley. He is a former president of the Berkeley
School Board. He is the father of four children.</I><BR>
<BR>
<HR>
<P><CENTER>If you have any thoughts on this or would like to contribute
to an ongoing discussion in the &quot;E-mail, Opinions & Discussion&quot;
folder on the <B><I>Inside In Motion</I></B> page, <A HREF="mailto:publish@cts.com"><BR>
click here to send e-mail to publish@cts.com</A>.</CENTER>
<P><HR> 
<P><CENTER><I><A HREF="index4.html">Return to In Motion Table of Contents</A></I><BR>
<BR>
<A HREF="index4.html"><IMG SRC="imnpc.gif" WIDTH="168" HEIGHT="88" ALIGN=
bottom NATURALSIZEFLAG="3" BORDER="0"></A><BR>
<BR>
<BR>
<I><A HREF="index2.html">Visit the In Unity Book of Photographs</A></I><BR>
<BR>
<A HREF="index2.html"><IMG SRC="iucshad.gif" WIDTH="93" HEIGHT="57" ALIGN=
bottom NATURALSIZEFLAG="3" BORDER="0"></A><BR>
<BR>
<BR>
<I><A HREF="npc.html">Visit the NPC Productions home page<BR>
</A></I><BR>
<A HREF="npc.html"><IMG SRC="npcshad.gif" WIDTH="96" HEIGHT="62" ALIGN=
bottom NATURALSIZEFLAG="3" BORDER="0"></A><BR>
</CENTER>
</BODY>
</HTML>

