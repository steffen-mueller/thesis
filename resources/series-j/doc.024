www.osler.com/Resources/Intpropwin_2.html
<html><head>
<TITLE>Intellectual  Property Review, Winter 1996</TITLE>
<BODY BACKGROUND="../grapix/back.gif" LINK="#024d2f" VLINK="#ab6207" ALINK="000000">

<CENTER>
<TABLE WIDTH=555><TR><TD>


<IMG SRC="../grapix/ip.gif">
<H2>Trade-Mark Use in Comparative Advertising
</H2>
<I>Lee Webster and Lee Muirhead, Toronto</I>
<P>
Any business that uses a competitor's trade-mark in comparative advertising should ensure that its advertisements do not violate section 22 of the Canadian <I>Trade-marks Act (the "Act")</I>. Section 22(1) provides that:<P>
<BLOCKQUOTE>No person shall use a trade-mark registered by another person in a manner that is likely to have the effect of depreciating the value of the goodwill attaching thereto.</BLOCKQUOTE><P>
A series of "comparative advertising" cases in the Canadian courts has highlighted the problems associated with interpreting section 22 and has illustrated how the use of another person's trade-mark in advertisements may result in liability.<P>
There are three requirements for the application of section 22:<P>
<UL>
<LI>the trade-mark at issue must be registered;
<LI>the trade-mark must be "used" by a person other than the trade-mark owner; and
<LI>that use must have the effect of depreciating the value of the goodwill attaching to the registered trade mark.</UL><P>
The interpretation that the courts have given to these requirements has led to somewhat illogical results.
The difficulties in the application of section 22 began with the decision of the Exchequer Court in <I>Clairol International Corp. v. Thomas Supply and Equipment Co. </I>(1968) 55 C.P.R. 176 and its interpretation of the second and third requirements listed above.<P>
<CENTER<<B>"USE" OF THE TRADE-MARK</B></CENTER><P>
In <I>Clairol,</I>  Mr. Justice Thurlow interpreted "use" to mean "use" as defined in section 4 of the Act. What constitutes "use" under section 4 depends on whether the trade-mark is used in association with goods or with services. Under section 4(1), a trade-mark will be "used" in association with goods if it appears on the goods, or on the packaging of the goods or on any "point of sale" materials. Under section 4(2), however, a trade-mark will be "used" in association with services if it is used in any way with the performance or advertisement of those services.<P>
If the trade-mark has been registered for use in association with products, for example, any use by a third party on that third party's own products, or on the packaging of those products, or on any point of sale materials may attract liability under section 22. Use on advertising materials not available at the point of sale will not be restrained.<P>
If the trade-mark has been registered for use in association services, however, any use of that trade mark by a third party in association with the performance or advertisement of its services may attract liability even if that use does not occur at the point of sale.<P>
This anomaly between the application of section 22 to third party use of trade-marks used with goods and those used with services prompted Madame Justice Reed in <I>Eye Masters Ltd. v. Ross King Holdings Ltd.</I> (1992), 44 C.P.R. (3d) 459 to comment that the conclusion that section 22 could be used to restrain non point of sale advertisements only for service marks was "bizarre". <P>
<CENTER><B>DEPRECIATION OF THE VALUE OF GOODWILL</B></CENTER><P>
In Clairol, Mr. Justice Thurlow also found that goodwill could be depreciated in a number of ways in the comparative advertising context particularly if the comparisons were false or misleading. However, even if the comparisons were accurate, the goodwill might still be depreciated:<P>
<BLOCKQUOTE>[A trader] may not use his competitor's trade-mark for the purpose of appealing to his competitor's customers in his effort to weaken their habit of buying what they have bought before or the likelihood that they would buy his goods so as to secure the custom for himself, for this is not only calculated to depreciate and destroy his competitor's goodwill but is using his competitor's trade-mark to accomplish his purpose.</BLOCKQUOTE><P>
<I>Clairol, </I> therefore, provided a very broad definition of what constitutes "depreciation of the value of goodwill".<P> 
Following <I>Clairol, </I>  a series of cases have held that the value of the goodwill attaching to the trade-mark of another trader will be depreciated:<P><UL>
<LI>through deprecatory actions such as over labelling or the sale of stale product under the trade-mark; 
<LI>spoof or satire;
<LI>by the identification of similarities (rather than the differences) between the advertised brand and the comparison brand; and 
<LI>through any use of the trade-mark that attracts a customer away from the goods or services of the trade-mark owner.</UL><P>
The application of section 22 to restrain truthful comparative advertisements may create challenges to its constitutional validity. In many cases, the courts appear to start from the presumption that truthful comparative advertising is a commercially acceptable form of advertising which promotes competition and informs consumers.<P>
It is arguable, however, that a direct comparative advertisement is not an effective means of informing consumers, but are rather an advertising vehicle to allow a low market share brand to conduct a sophisticated form of consumer manipulation that is inconsistent with the recognized goals of better consumer information and competition. Indeed, some evidence suggests that direct comparative advertisements impede the consumer from making a rational purchasing decision.<P> 
A series of consumer studies, commencing in 1976, have established that direct comparative advertisements are less believable than indirect comparative advertisements or non-comparative advertisements, increase consumer scepticism about the advertiser, are less credible and generate more negative responses. Direct comparative advertisements also place "less psychological distance" between the advertised brand and the compared brand. This may not be in the public interest as studies suggest that less brand differentiation is also deceptive to the consumer.<P>
Direct comparative advertisements may increase the likelihood of a consumer misidentifying the sponsor of the advertisement. They may also be deceptive as consumers who are exposed to a direct comparative advertisement are likely to infer that the advertised brand has desirable attributes that are not featured in the advertisements but are associated with the comparison brand. Also, where a direct comparative advertisement explicitly states that the comparison brand is relatively inferior on a featured attribute, the advertisement is likely to lower the consumer's perceptions of the comparison brand with respect to this particular attribute.<P>
Many countries have banned direct comparative advertising. Perhaps truthful and accurate direct comparative advertisements are precisely the type of use of a trade-mark that section 22 was intended to address. In any event, any advertiser who engages in comparative advertising should not only take great care to ensure that the advertisement is not false or misleading but should also be aware that it might be restrained under section 22 of the Act if the advertisement features a competitor's mark.<P>  

* This article is a condensed version of a paper delivered by Lee Webster at a Canadian Institute Conference in October 1995. Copies of the full paper are available on request.<P>
  
<HR WIDTH=250 ALIGN=center><BR>
<I>This article is necessarily of a general nature and cannot be regarded
as legal advice. Upon request, members of the firm would be pleased to provide
additional details and to discuss the possible effects of matters contained
in this article in specific situations.</I> <BR>
<BR>

<CENTER>
<A HREF="../Maps/endlinks.map"><IMG SRC="../grapix/endlinks.gif" BORDER=0 ISMAP></A><BR><BR><BR>

<FONT SIZE = -1>

<A HREF="mainip.html">Intellectual Property News</A> | <A HREF="http://ohh1/Excite/AT-wholequery.html">Search</A> | <A HREF="../Firm/Firm_Profile.html">Firm Information</A> | <A HREF="../pands.html">Products and Services</A> | <A HREF="../Firm/Firm_recruit.html">Student Program</A> | <A HREF="../international.html">International</A><BR>

<A HREF="../Firm/Lawyers.html">Directory</A> | <A HREF="Resource_Guide.html">Resource Center</A> | <A HREF="../whatsnew.html">New</A> | 

<A HREF="mailto:counsel@osler.com">counsel@osler.com</A> | <A HREF="../credits.html"> Credits</A> | <A HREF="../OHH_home_page.html"> Home</A>
</FONT><BR><BR></CENTER>
<FONT SIZE=-2>copyright 1996 Osler, Hoskin & Harcourt



</TD></TR></TABLE>
</CENTER>
</BODY>
</HTML>

