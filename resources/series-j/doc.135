www.pan.ci.seattle.wa.us/business/dc/pr/11737ord.htm
<HTML>

<!--   ***  Fill in text file name: -->
<!-- Document converted from textfile named:  11737.ord     -->
<HEAD>


<!-- ***  Fill in title between the marks -->
<TITLE>King County Ordinance No. 11737 </TITLE>
</HEAD>
<BODY  BGCOLOR = "#ffffff">

<!--PAN's barmap graphic goes at the top-->
<a href="/html/panbar.map"><img align=middle src="/html/graphics/governi.gif" ismap ></a><br>

<P>
<!-- Duwamish Coalition Logo next -->
<IMG ALIGN=middle SRC="../dclogo2.gif">


<!-- *** fill in name of page as a header (between the H2 marks) -->

<H2>King County Ordinance Number 11737 </H2>


<!-- ***  insert text file below this mark -->
March 28, 1995
<BR>Introduced By: Pete von Reichbauer, Greg Nickels, Larry
Phillips, Chris Vance, Ron Sims, Kent Pullen
<BR>April 3, 1995 clerk
<BR>Proposed No.: 95 - 168
                 
 
                   <P><CENTER>ORDINANCE NO. 11737</CENTER>
          
  <BLOCKQUOTE>AN ORDINANCE providing for assistance
          in reclaiming contaminated industrial
          land in the Duwamish Corridor.</BLOCKQUOTE>
   
   <P>Findings:

   <P>1.   In its roles of regional and local government,
       economic development and job growth are high priority
       goals for King County.  The county has authority to
       engage in economic development programs under RCW
       36.01.085, which reads: "It shall be in the public
       purpose for all counties to engage in economic
       development programs. In addition, counties may
       contract with nonprofit corporations in furtherance
       of this and other acts relating to economic
       development."
   
   <P>2.   High quality family wage jobs are significant
       contributors to the quality of life of the region;
       manufacturing and industrial businesses can provide
       these high quality family wage jobs.
   
   <P>3.   Based on analysis supporting the King County
       Countywide Planning Policies for Growth Management
       and other work associated with economic development
       elements in local comprehensive plans, there is
       reported to be a limited supply of industrially zoned
       land in King County.  Such a limited supply could
       distort the market and preclude the achievement of
       the goal for creating additional family wage
       industrial jobs.
   
   <P>4.   In areas, such as the Duwamish Corridor
       (Attachment 1), where there has been long-term
       manufacturing or other industrial activities, a
       significant portion of the industrially zoned land is
       considered to be contaminated by today's standards,
       preventing or delaying use at its highest industrial
       potential.
   
   <P>5.   Reclaiming contaminated industrial land improves
       water and air quality and can reduce pressures to
       expand the urban area in this and other Puget Sound
       Region counties.
   
   <P>6.   The high risks of liability for costly clean-up
       of contamination can discourage owners from
       developing or redeveloping such industrial
       properties; financial institution are not willing to
       share this risk.
   
   <P>7.   The current law which makes past and current
       owners and financial institutions joint and severally
       liable for clean-up often leads to no action to
       remediate contaminated land and, therefore,
       continuing water and air pollution and, at times, the
       abandonment of such properties.
   
   <P>8.   Contaminated industrially zoned land in the
       Duwamish Corridor can be reclaimed and developed or
       redeveloped for businesses that provide high quality
       family wage jobs.
   
   <P>9.   The Duwamish Coalition established pursuant to
       Motion 9234 is a private-public partnership focused
       on the Duwamish Corridor whose Mission is "to
       preserve and reclaim industrial land for the purpose
       of expanding the manufacturing and industrial job
       base, and protecting and enhancing the natural
       environment.  The Coalition will further address
       regulatory, infrastructure and institutional barriers
       to economic growth and environmental protection in
       the corridor.  The mission will be accomplished
       through a private and multi-jurisdictional public
       partnership." The Coalition provides a unique
       opportunity for the county to complement the work of
       other jurisdictions, business, labor, environmental
       interests and community groups in preserving and
       reclaiming industrial land.
   
   <P>10.  The Duwamish Coalition has been studying the
       extent of contamination of soil, sediment and ground
       water in the Corridor and the obstacles to cleanup of
       properties. The Coalition is also reviewing examples
       of how obstacles could be overcome.  An example of a
       major cleanup is the Port of Seattle's Southwest
       Harbor Project. This  Project had the advantage of
       dealing with a large property; of having significant
       technical and financial resources to conduct analysis
       and generate remedies for contamination which
       related to actual conditions and risks rather than
       generic, hypothetical risk scenarios; of regulatory
       and permitting agencies providing dedicated staff,
       and of the ability to coordinate cleanup and
       development efforts.
   
   <P>11.  The Duwamish Coalition is engaged in
       investigating and  evaluating the impact of cleanup
       regulations and how they are applied to industrial
       land.  The Coalition is expected to propose changes
       in regulations. One major  objective of the changes
       would be having cleanups not default to generic risks
       but rather protect human health and the environment
       from the specific risks of any identified
       contamination.  For example, a study of the complex
       chemical mixture of total petroleum hydrocarbons
       (TPH) could produce data to enable regulators to
       approve risk-specific remedies.
   
   <P>12.  The Duwamish Coalition is generating action
       plans that are expected to include demonstration
       projects of how the changes in regulations can
       facilitate property remediation.

<P>    13. The county wishes to provide direct technical and
       financial support to a demonstration project
       involving cleanup of properties either through a
       purchase, clean up and resale, or through an area-
       wide cleanup by owners of smaller properties. A major
       objective of this demonstration project would be the
       establishment of scientifically defensible
       alternative cleanup standards for protecting the
       environment and human health.  The alternative
       standards could significantly lower cleanup costs,
       thus rendering such reclamation economically viable.
<BR>


<P>  BE IT ORDAINED BY THE COUNCIL OF KING COUNTY:

<P> SECTION 1.  Purpose. The purpose of this ordinance is to

provide technical and financial resources to demonstrate how

cleanup of contaminated land can be facilitated by (1)

evaluating contamination on property for its specific risks

to human health and the environment, and conducting risk-

specific cleanup; and (2) implementing an initiative to

assist smaller property owners to form voluntary temporary

partnerships with the specific purpose of achieving an area-

wide clean up.

<P>  SECTION 2.  Need for the county to provide technical and

financial resources.  The county is a regional government and

must assume a major role in economic development programs,

complementing the efforts of other governmental entities and

acting in partnership with business, labor and other

interested parties.  With respect to the specific area of the

Duwamish Corridor, the county will offer significant

technical and financial resources to complement those of the

cities of Seattle, Tukwila and Renton, the Port of Seattle

and private sector members of the Duwamish Coalition to

produce property ready for development or redevelopment for

industrial uses.

<P>  SECTION 3.  Implementing agency.  The Economic

Development Section of Parks, Planning and Resources

Department, or its successor, shall be responsible for

generating the work program required by Section 4, and

implementing the approved work program.

<P>  SECTION 4.  Work Program. A work program shall be

generated by the Economic Development Section of Parks,

Planning and Resources Department, or its successor, and

submitted, by May 1, 1995, to the council for approval.  The

work program shall include but not be limited to the

following elements:

<P>  A. Identification of a demonstration project(s).  A

demonstration project or projects relating to cleaning up

industrially-zoned property shall be identified.  The

project(s) could be to purchase, clean up and resell

property, or to assist owners of smaller properties to form

voluntary temporary partnerships with the specific purpose of

achieving an area-wide cleanup.

<P>  B. Selection and feasibility study of a site or sites

for implementing the demonstration project or projects.  The

Criteria for selecting a site or sites for implementing the

demonstration project(s)shall be proposed.  These criteria

are critical to a successful and efficient demonstration of

reclaiming industrial land for business operations and jobs

creation and, therefore, shall include but not be limited to

the following:

<P>  1) Whether the project was proposed by Duwamish

Coalition Subcommittee and approved by the Coalition Steering

Committee;

<P>  2) Whether the site(s) is available for a demonstration

project through (a) current county ownership, (b) purchase or

(c) property owners forming a partnership to achieve cleanup;

3) Whether there is support from the governmental entity with

land use jurisdiction for the demonstration project on a

particular site(s);

<P>  4) The likelihood that the site(s) involves contaminants

where risk-specific evaluation can be conducted and risk-

specific remedies implemented.  Contaminants which have been

the subject of Duwamish Coalition work would be of particular

importance;

<P>  5) The cost of project implementation on the site(s),

including both private and public sector costs; and

<P>  6) Whether the site(s) predicted "after project value"

shows a favorable return on investment

<P>  7) Whether the site(s) would support expansion of an

existing industrial/manufacturing business which would not be

viable without program assistance, and/or whether the site(s)

would promote establishment of a new industrial/manufacturing

business which would not be viable without program

assistance; and

<P>  8) Whether the site(s) would yield information and

experience which would provide valuable assistance for the

development of regulatory reform initiatives or future

demonstration projects.

<P>  The investigation of economic viability should include

what incentives the county could provide, including how the

site(s) would be priced after cleanup, to insure that buyers

who would operate an industrial/manufacturing business could

purchase and profitably operate on the site.

<P>  C. Project Evaluation.  A project evaluation plan,

including such information as number and type of jobs

created, cost of cleanup and other investments of the public

and private sectors (except for proprietary information),

shall be prepared.

<P>  D. Schedule for implementing the demonstration project.

A schedule of major milestones shall be proposed.

<P>  E. Resources required.  A project budget for technical

staff and its support shall be proposed.

<P>

     SECTION 5. There is created a new fund entitled the

"Industrial Land Reclamation Fund" for the sole purpose of

accumulating and disbursing financial resources for the

reclamation of industrial land so that the manufacturing and

industrial job base can be expanded and the natural

environment protected and enhanced.

<P>  INTRODUCED AND READ for the first time this 6th day of

March, 1995.

<P>  PASSED by a vote of 13 to 0 this 3rd day of April,

1995.<BR>

<BR>                           KING COUNTY COUNCIL
<BR>                           KING COUNTY, WASHINGTON
<BR>
<BR>
                               _________________________
<BR>                           Chair
<BR>ATTEST:
<BR>
<BR>
_________________________
<BR> Clerk of the Council
<BR>
<BR>
<BR>APPROVED this 12th day of April, 1995.
<BR>
<BR>
                               ___________________________
<BR>                               King County Executive
<BR>
<BR>
Attachments:   1. Map of the Duwamish Corridor

<BR>


<!--  Below this line are the arrows and footer-->
<HR>

<BR><A HREF="prpage.htm"><IMG ALIGN=middle SRC="../arrowl.gif">   
  Preserve and Reclaim Industrial Land</A>


<!--This one is always the same -->
<BR><A HREF="../default.htm"><IMG ALIGN=middle SRC="../arrowh.gif"> 
Duwamish Coalition Home Page</A>
<P>
<ADDRESS>

<!-- *** Be sure to change this date whenever file is updated -->
last updated:  September 15, 1995
</ADDRESS>


<!--  PAN's standard footer goes at the bottom -->
<HR>
<CENTER><A HREF="#TOP">Top of Page</A> &#149; <A HREF="/html/help.htm">Tips</A>

 &#149; <A HREF = "/html/feedback.htm">Feedback</A> &#149; <A href = "/html/director">Search/Directory</A>

 <BR><A HREF="/default.htm">Home Page</A> &#149; <A href = "/html/visitor">Visitor Center</A>

 &#149; <A HREF = "/html/citizen.htm">Citizen Center</A> &#149; <A href = "/html/business">Business Center</A>

 </center><br>
<HR>
<em>Duwamish Coalition</em><br>
</BODY>
</HTML>














