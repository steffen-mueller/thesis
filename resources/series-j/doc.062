www.disa.mil/DISN/docs/mns.html
<html>
<head>
   <title></title>
   <meta name="GENERATOR" content=" Internet Assistant for Word 1.0Z ">
   <meta name="AUTHOR" content="Rose Thomas">
</head>
<body>
<!doctype html public "-//IETF//DTD HTML//EN">
<p><b>THE JOINT STAFF</b> </p>

<p><b>WASHINGTON, D.C. 20318-8000</b> </p>

<p><b>JOINT REQUIREMENTS OVERSIGHT COUNCIL</b> </p>

<p>JROCM 047-95 </p>

<p>30 March 1995 </p>

<p>MEMORANDUM FOR THE UNDER SECRETARY DEFENSE FOR ACQUISITION AND TECHNOLOGY
</p>

<p>Subject: Defense Information System Network (DISN) Mission Need Statement
(MNS) </p>

<p>On 14 February 1995, the Joint Requirements Oversight Council (JROC)
reviewed and validated the Defense Information System Agency's Mission
Need Statement for a Defense Information System Network (DISN). Recommend
the a Major Automated Information System Review Council (MAISRC) be convened
to consider how to fulfill this joint requirement in lieu of a Defense
Acquisition Board. An accelerated acquisition process is essential in this
case to take advantage of commercial capabilities. The JROC decided to
retain approval authority for future MNS-related Operational Requirements
Documents (ORDs). </p>

<p><i>[SIGNATURE]</i> </p>

<p>W. A. Owens </p>

<p>Vice Chairman of the Joint Chiefs of Staff </p>

<p>JROC Chairman </p>

<p>Enclosure </p>

<p>
<hr width="100%" ></p>

<p><b>MISSION NEED STATEMENT (MNS)</b> <b>FOR</b> </p>

<p><b>DEFENSE INFORMATION SYSTEM NETWORK (DISN)</b> </p>

<p>1. <U><b>Background</U></b>. This MNS responds to the requirements identified
in the Defense Planning Guidance (DPG) (FY1996-2001), and to requirements
for implementing C4I for the Warrior (C4IFTW) concepts. An integrated transmission
infrastructure is needed to support the military's move into the 21st Century
information age of dynamic bandwith allocation and to replace the individual
legacy communications systems with a seamless transport capability that
can keep pace with evolving technology and meet the changing C4I demands
of the warfighter. To date, C4I strategic and tactical connectivity for
the warfighter has been provided via costly interfaces between disjointed,
disparate, and stovepiped wide and local area networks which are duplicative
and inefficiently consume limited bandwith and shrinking fiscal resources.
</p>

<p>1.1 The Command, Control, Communications, Computers and Intelligence
(C4l) and Space-Based Systems section of the DPG specifies the following
requirements: </p>

<p>a. Integrated connectivity to all theater and tactical elements through
modernized, jam-resistant telecommunications network support. </p>

<p>b. Joint and combined interoperability in all mission areas to facilitate
Joint and Combined Task Force (JTF/CTF) operations. </p>

<p>c. Improved systems to integrate strategic, theater and tactical intelligence
with joint/combined operating forces. </p>

<p>d. Intelligence systems that are fully integrated with C4 systems. </p>

<p>1.2 To effectively and economically support these requirements, a common
user transmission infrastructure with integrated wide area networking interfaces
to local area networks, integrated network and system management capabilities,
and value-added services transport capability must be fielded. The integrated
capability must ensure the warfighter has responsive and reliable command
and control, intelligence, and support information anywhere, anytime, to
support any mission. </p>

<p>2. <U><b>Mission and Threat Analysis</U></b> </p>

<p>2.1 <U>Mission</U>. The primary C4l systems support mission is to enhance
the warfighter's effectiveness by providing an integrated global communications
infrastructure with sufficient bandwith for high volume exchange of voice,
data, imagery, and video transmission services for strategic and tactical
mission areas. The capability that provides transport of this mission critical
C4I information must be global and seamless in connectivity, scaleable
in capacity, flexible in provisioning, easily extended to any location
in the world, and capable of accepting technology insertions and value-added
services in support of future warfighting requirements. When contingencies
and conflicts arise, warfighters will need continuous C4l connectivity
as they transition from the sustaining base to their respective areas of
operation. The network providing C4l support must be flexible enough to
handle rapid expansion of connectivity/bandwidth (dynamic reallocation)
requirements to support all JTF/CTF operations. </p>

<p>2.2 <U>Objectives</U>. The primary objectives of all DoD efforts to
implement these capabilities are to ensure the network and provided C4l
support can: </p>

<p>a. Provide a stable migration path to the 21st Century that exploits
information age technology for direct warfighter support. </p>

<p>b. Support two (2) Major Regional Contingencies (MRC), in addition to
peacetime, daily worldwide operational requirements. </p>

<p>c. Provide transport capability of value-added services of Global Command
and Control System (GCCS), the Defense Message System (DMS), common C2
and intelligence information transfer network, video/textual teleconferencing
network, voice networks, Integrated Tactical Strategic Data Network (ITSDN),
and other programs, systems, and initiatives enhancing the warfighter real
time information exchange and processing. </p>

<p>d. Support afloat, airborne, and ground Joint military operations/forces
in all theaters, worldwide. </p>

<p>e. Meet C4l systems demands for joint and combined US military operations
at local, regional, theater, and global levels. </p>

<p>f. Support the exchange of national and theater intelligence and/or
combat sensor information among combat and C2 systems. </p>

<p>g. Meet demands of sustaining support bases/post/camps/stations providing
mission support for deployable forces. This includes being the interface
point and providing the long-haul backbone. </p>

<p>h. Meet demands for transition from sustaining support bases to the
JTF deployed area of responsibility. This includes the transition over
the interfaces between the strategic and tactical environment, such as
the Standardized Tactical Entry Point (STEP) and the ITSDN. </p>

<p>i. Meet demands for interoperability requirements with North Atlantic
Treaty Organization (NATO) and our allies to support coalition warfare.
</p>

<p>j. Meet demands for connection of worldwide modeling and simulation,
and tele-training platforms that comprise warfighter decision support,
and distance learning training systems. </p>

<p>k. Meet operational demands for network availability, scalability, reliability,
ease of extension, restoral, faster provisioning, higher bandwidth, survivability,
and end-to-end global interoperability using commercial-off-the-shelf (COTS)
systems and components to maximum extent possible. </p>

<p>l. Operate in a diverse communications environment. </p>

<p>m. Meet projected C4I systems demands for responsive, and reliable command
and control, intelligence, and support information. </p>

<p>2.3 <U>Threat</U>. This worldwide DoD capability will support information
transport for C4l systems and facilities supporting DoD missions in all
operational environments. Each C4l element of this capability need not
be protected against all threats at every level of conflict. </p>

<p>2.3.1 Formal threat and vulnerability analyses were performed and documented
in the <i>NSA Threat Assessment for DISN-NT Security Architecture, dated
17 April, 1992, </i>and the <i>DISN-NT Security and Vulnerability Assessment,
dated 17 April 1992</i>, and prepared by NSA. A resulting <i>DISN-NT Risk
Assessment, dated 13 May, 1992 </i>was produced. The Assistant Secretary
of Defense for C3I approved the <i>DISN-NT Security Architecture </i>on
19 January, 1993. </p>

<p>2.3.2 The objective is to construct an appropriately integrated, balanced
and cost-effective network (including built-in redundancy and alternate
routing where required) that has as a minimum the core capability for voice
that includes data integrity, authentication, and availability; the core
capability for data that includes integrity and availability; and the core
capability for video that includes data integrity and confidentiality,
availability, and authentication. The respective capabilities are defined
in the <i>DISN-NT Security Architecture, </i>19 January 1993. </p>

<p>2.3.3 As with current C4I systems, individual network components must
provide varying degrees of threat mitigation as required for the supported
mission. In peacetime, the documented threats include counter-intelligence,
inadvertent jamming, intercept, OPSEC considerations, viruses, hackers,
and spoofing. During crisis or regional conflicts, the threats include
physical destruction, attacks by directed energy devices, and attacks through
electronic and information warfare techniques including jamming, deception,
and database penetration/corruption. Added threats in the unlikely event
of global war include nuclear blast, radiation, scintillation, High Altitude
Electromagnetic Pulse (HEMP), anti-satellite weapons and high-altitude
nuclear blasts. </p>

<p>2.4 <U>Priority/Timing</U>. The need for this integrated information
transfer capability generates and impacts a number of supporting Service
and Defense agency operational requirements, telecommunications systems,
and programs. This capability is mission essential to the warfighter, and
fiscal realities demand it. Consequently, supporting operational requirements
and telecommunications systems programs associated with this capability
are ranked high by Services and agencies. </p>

<p>2.4.1 As the C4I community continues to push the technology envelope,
it must ensure that appropriate interfaces for combined operations are
being considered and provided as an integral part of any future C4I capability.
Technology breakthroughs in C4I will be considered for insertion to the
integrated information transfer capability. </p>

<p>2.5 <U>General Capabilities</U>. The warfighter requires a service that
will provide transport and routing of voice, circuit switched data, packet
switched data, video, and point-to-point bandwith. In today's multiple
threat environment requiring quick response, the rapid establishment of
capable C4I connectivity is essential. Each segment of the capability must
incorporate appropriate safeguards commensurate with the existing or projected
threat, and as required by governing policies. </p>

<p>2.5.1 The integrated worldwide telecommunications capability will support
transmission of voice, data, imagery, and video at all security classification
levels. The network will support flexible and rapid provisioning, be easily
extended, and capable of easily accepting future technology insertions.
It will also provide seamless interfaces to commercial networks as required
to support increased traffic during surge, and contingency conditions.
The network will be capable of rapid restoral in order to minimize the
necessity for independent/stand-alone operations. </p>

<p>2.5.2 The network will support the requirement for the exponential increase
in bandwidth, especially in support of modeling, imagery, and video teleconferencing
requirements. </p>

<p>2.5.3 The network will integrate satellite, airborne and terrestrial
based (wire and wireless) transmission and switching systems (strategic
and tactical) and provide for end-to-end visibility to support integrated
management of the network and connected systems. To perform path assurance,
configuration management, and other global network and systems management
functions, network components will be integrated to provide seamless connectivity.
Additionally, it will have integrated network, systems and security management
and control capabilities at local, regional and theater levels. Integrated
network and systems management will employ state-of-the-art capabilities
to manage connected resources, and enhance responsiveness to surge contingencies
and outages. All management activities will be guided by common standards
and procedures for integrated management of network and system resources.
</p>

<p>2.5.4 The network will provide the transport mechanism for the Defense
Information Infrastructure (DII). </p>

<p>3. <U><b>Nonmateriel Alternatives</U></b>. Numerous policy and procedural
measures have been undertaken to improve the effectiveness and efficiency
of fielding and providing C4I connectivity capabilities for the warfighter.
Policies and procedures, to include DoD Directives, Defense Management
Review Decisions, and Chairman of the Joint Chiefs of Staff instructions,
that impose interoperability considerations on all new systems and mandate
the use of standards, have been published. However, progress toward the
required DoD-wide end-to-end integrated capability has been slow. Additionally,
the existence of legacy systems and the need for ClNCs, Services and Defense
agencies to retain the costly, dedicated transmission systems have resulted
in only marginal improvements in end-to-end C4l connectivity capability
and savings. Today's warfighters' missions, economic realities, and technology
breakthroughs demand faster improvements for C4l systems connectivity and
support. </p>

<p>4. <U><b>Potential Materiel Alternatives</U></b>. There are three material
alternatives for meeting this need: </p>

<p>a. Alternative 1 - Maintaining the &quot;status quo&quot; of loosely
coordinated development, implementation, and management of Service and
Defense agency systems (special purpose and common-user) that require special
interfaces to achieve the interoperability and integration to support joint
operations. Major common-user systems that require interfacing include,
the Defense Switched Network, Defense Data Network, AUTODIN, Defense Simulation
Network, Defense Red Switch Network, Defense Special Security Communications
System, and the national Secure Telephone System. Special purpose systems
with partial or dedicated transmission and network management that must
be interfaced include Service and agency router networks, dedicated intelligence
Community networks, and dedicated CINC C2 support networks. </p>

<p>b. Alternative 2 - Simultaneously upgrade all department switching/transmission
and network systems to standard state-of-the-art platforms. </p>

<p>c. Alternative 3 - Incrementally integrate all DoD network and transmission
systems into a secure, seamless, standards based, information transfer
infrastructure with responsible DoD activities at local, regional, theater
and global levels, and tactical interfaces, performing integrated management.
Stay in step with evolving commercial technology and standards via planned
technology insertion, thereby avoiding use of &quot;proprietary&quot; hardware
and software solutions and their related pitfalls. </p>

<p>5. <U><b>Constraints</U></b>. The requirements and capabilities prescribed
in this MNS must be satisfied through a logical and orderly transition
of information infrastructure assets (existing systems, networks, and associated
management systems) in order to minimize mission impact and implementation
cost. Management and replacement, or continuation of DoD information transfer
programs must be planned and phased to achieve the maximum benefit to DoD
and particularly to warfighting ClNCs and JTF commanders. </p>

<p>5.1 DISN will maintain C4I support in all operational environments.
</p>

<p>5.2 Services and agencies must perform assigned network and management
responsibilities within a centrally defined framework of architectures,
standards, and procedures. </p>

<p>5.3 The user will determine operations, maintenance, and logistics support
staffing required, with the goal of no increase in manpower requirements.
</p>

<p>5.4 NATO and other allied forces will continue to have the same or better
level of interoperability as experienced with the Defense Communications
System (DCS). </p>

<p>5.5 The network will employ appropriate security measures to address
known threats. Security measures will be implemented to protect mission
data and safeguard system resources and information to ensure system availability
in all conflict/threat scenarios. Using available information security
techniques and tools, the network will support information transfer at
all classification levels (Unclassified but Sensitive (UBS) to Top Secret
Special Compartmented Information (SCI)), in accordance with governing
security regulations. Multilevel security (MLS) capabilities will be incorporated
as they become available, assuming they are operationally feasible and
affordable. </p>

<p>5.6 The underlying implementation objective is that the DISN and other
connected telecommunication facilities and services, evolve with: technical
coherence, operational responsiveness, global interoperability and cost-effectiveness.
Maintaining the technological currency of the DISN will be accomplished
through the competitive and timely insertion of key technologies and services
via an acquisition approach that will include a leading edge technologies
activity. </p>

<p>6. <U><b>Joint Potential Designator</U></b>. Joint. </p>

<p>
<hr width="100%" ></p>

<p>Return to: </p>

<p><a href="../docs.html">DISN DOCUMENTS</a> </p>

<p><a href="../disnhome.html">DISN Home Page </a></p>

<p><a href="../../disahome.html">DISA Home Page</a> 
<hr></p>

<p><b><i>thomas2r@ncr.disa.mil - </i></b>Last Revision: 1 October 1996
</p>

</body>
</html>


