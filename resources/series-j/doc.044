www.cordis.lu/esprit/src/ecomint.htm
<HTML>

<HEAD>

<TITLE>Electronic Commerce - An Introduction</TITLE>

</HEAD>

<BODY BACKGROUND="../icons/logo4.gif">

<IMG border=0 hspace=20 SRC="../icons/logo.gif">
<CENTER>
<H1>Electronic Commerce - An Introduction</H1>
</CENTER>
<UL>
<A HREF="#INTRODUCTION"><H3>1 Introduction</H3></A HREF>
<A HREF="#WHAT"><H3>2 What is electronic commerce?</H3></A HREF>
<A HREF="#CATEGORIES"><H3>3 Categories of electronic commerce</H3></A HREF>
<A HREF="#IMPACT"><H3>4 Impact</H3></A HREF>
<A HREF="#SUPPLIER"><H3>5 Supplier opportunities and customer benefits</H3></A HREF>
<A HREF="#SCOPE"><H3>6 Scope of electronic commerce</H3></A HREF>
<A HREF="#EXAMPLES"><H3>7 Examples</H3></A HREF>
<A HREF="#LEVELS"><H3>8 Levels of electronic commerce</H3></A HREF>
<A HREF="#OPEN"><H3>9 Open issues</H3></A HREF>
<A HREF="#ACTORS"><H3>10 The actors and their roles</H3></A HREF>
<A HREF="#G7"><H3>11 The G-7 initiative</H3></A HREF>
<A HREF="#CONCLUSION"><H3>12 Conclusion</H3></A HREF>

</UL>
<HR>
<A HREF="/esprit/home.html" >Esprit homepage</A>
| <A HREF="/esprit/src/invencom.htm" >Inventory of Electronic Commerce Initiatives</A>
| <A HREF="/esprit/src/task7-10.htm" >Esprit task 7.10: Electronic Commerce Pilots</A>
| <A HREF="/esprit/src/wp96.htm" >Work-programme</A>
| <A HREF="mailto:paul.timmers@dg3.cec.be" >Contact Esprit's Electronic Commerce team</A>
<HR>
<P><P>

<A NAME="INTRODUCTION"><H1>1 Introduction</H1></A NAME>

<P>
This paper provides a brief introduction to electronic commerce.
It discusses the nature of electronic commerce, considers its
scope and impact, and outlines several examples. It then identifies
a number of open issues and the actors responsible for addressing
those issues. Finally, it gives a brief overview of the G-7 initiative
&quot;A Global Marketplace for SMEs&quot;.
<A NAME="WHAT"><H1>2 What is electronic commerce?</H1>

<P>
One possible definition of electronic commerce would be: &quot;any
form of business transaction in which the parties interact electronically
rather than by physical exchanges or direct physical contact&quot;.
However, while accurate, such a definition hardly captures the
spirit of electronic commerce, which in practice is far better
viewed as one of those rare cases where changing needs and new
technologies come together to revolutionise the way in which business
is conducted.
<P>
Modern business is characterised by ever-increasing supply capabilities,
ever-increasing global competition, and ever-increasing customer
expectations. In response, businesses throughout the world are
changing both their organisations and their operations. As illustrated
in figure 1, they are flattening old hierarchical structures and
eradicating the barriers between company divisions. They are lowering
the barriers between the company and its customers and suppliers.
Business processes are being re-designed so that they cross these
old boundaries. We now see many examples of processes that span
the entire company and even processes that are jointly owned and
operated by the company and its customers or suppliers.
<P>
<CENTER>
<IMG SRC="../icons/intro1.jpg" ALIGN="BOTTOM" >
<P>
Figure 1 Changing business organisations and processes
</CENTER>
<P>
Electronic commerce is a means of enabling and supporting such
changes <I>on a global scale</I>. It enables companies to be more
efficient and flexible in their internal operations, to work more
closely with their suppliers, and to be more responsive to the
needs and expectations of their customers. It allows companies
to select the best suppliers regardless of their geographical
location and to sell to a global market.
<P>
One special case of electronic commerce is <I>electronic trading</I>,
in which a supplier provides goods or services to a customer in
return for payment. A special case of electronic trading is <I>electronic
retailing</I>, where the customer is an ordinary consumer rather
than another company. However, while these special cases are of
considerable economic importance, they are just particular examples
of the more general case of any form of business operation or
transaction conducted via electronic media. Other equally valid
examples include internal transactions within a single company
or provision of information to an external organisation without
charge.
<P>
Electronic commerce is technology for change. Companies that choose
to regard it only as an &quot;add on&quot; to their existing ways
of doing business will gain only limited benefit. The major benefits
will accrue to those companies that are willing to change their
organisations and business processes to fully exploit the opportunities
offered by electronic commerce.
<A NAME="CATEGORIES"><H1>3 Categories of electronic commerce</H1>

<P>
As shown in figure 2, electronic commerce can be sub-divided into
four distinct categories:
<P>
<IMG SRC="../icons/bullet.gif">business-business
<P>
<IMG SRC="../icons/bullet.gif">business-consumer
<P>
<IMG SRC="../icons/bullet.gif">business-administration
<P>
<IMG SRC="../icons/bullet.gif">consumer-administration
<P>
<CENTER>
<IMG SRC="../icons/intro2.jpg" ALIGN="BOTTOM">
<P>
Figure 2 Categories of electronic commerce
</CENTER>
<P>
An example in the <I>business-business</I> category would be a
company that uses a network for ordering from its suppliers, receiving
invoices and making payments. This category of electronic commerce
has been well established for several years, particularly using
Electronic Data Interchange (EDI) over private or value-added
networks.
<P>
The <I>business-consumer</I> category largely equates to electronic
retailing. This category has expanded greatly with the advent
of the World Wide Web. There are now shopping malls all over the
Internet offering all manner of consumer goods, from cakes and
wine to computers and motor cars.
<P>
The <I>business-administration</I> category covers all transactions
between companies and government organisations. For example, in
the USA the details of forthcoming government procurements are
publicised over the Internet and companies can respond electronically.
Currently this category is in its infancy, but it could expand
quite rapidly as governments use their own operations to promote
awareness and growth of electronic commerce. In addition to public
procurement, administrations may also offer the option of electronic
interchange for such transactions as VAT returns and the payment
of corporate taxes.
<P>
The <I>consumer-administration</I> category has not yet emerged.
However, in the wake of a growth of both the <I>business-consumer</I>
and <I>business-administration</I> categories, governments may
extend electronic interaction to such areas as welfare payments
and self-assessed tax returns.
<A NAME="IMPACT"><H1>4 Impact</H1>

<P>
Electronic commerce is not some futuristic dream. It's happening
now, with many well-established success stories. It's happening
world-wide - while the USA, Japan and Europe are leading the way,
electronic commerce is essentially global in both concept and
realisation. It's happening fast. And, with the maturing of EDI
and the rapid growth of Internet and the World Wide Web, it's
accelerating.
<P>
The impact of electronic commerce will be pervasive, both on companies
and on society as a whole. For those companies that fully exploit
it's potential, electronic commerce offers the possibility of
<I>breakpoint</I> changes - changes that so radically alter customer
expectations that they re-define the market or create entirely
new markets. All other companies, including those that try to
ignore the new technologies, will then be impacted by these changes
in markets and customer expectations. Equally, individual members
of society will be presented with entirely new ways of purchasing
goods, accessing information and services, and interacting with
branches of government. Choice will be greatly extended, and restrictions
of geography and time eliminated. The overall impact on lifestyle
could well be comparable to, say, that of the growth in car ownership
or the spread of the telephone.
<A NAME="SUPPLIER"><H1>5 Supplier opportunities and customer benefits</H1>

<P>
As summarised in table 1, electronic commerce offers several opportunities
to suppliers and commensurate benefits to customers. These include:
<P>
global presence / global choice
<P>
The boundaries of electronic commerce are not defined by geography
or national borders, but rather by the coverage of computer networks.
Since the most important networks are global in scope, electronic
commerce enables even the smallest suppliers to achieve a global
presence and to conduct business world-wide.
<P>
The corresponding customer benefit is global choice - a customer
can select from all potential suppliers of a required product
or service, regardless of their geographical location. <HR>

Table 1   <B>Opportunities and benefits</B>                                                            
<P><P>
<CENTER>
<table border cellpadding=10 cellspacing=0>
	<TR>
		<TH><B>supplier opportunity</B></TH> <TH><B>customer benefit</B></TH>          

	</TR>

	<TR ALIGN=center>
		<TD>global presence</TD> <TD>global choice</TD>           

	</TR>

	<TR ALIGN=center>
		<TD>improved competitiveness</TD> <TD>quality of service</TD>
	</TR>

	<TR ALIGN=center>         
		<TD>mass customisation &amp; &quot;customerisation&quot;</TD> <TD>personalised products &amp; services</TD>  
	</TR>                                           

	<TR ALIGN=center>
		<TD>shorten or eradicate supply chains</TD> <TD>rapid response to needs</TD>      
	</TR>                                                             

	<TR ALIGN=center>
		<TD>substantial cost savings</TD> <TD>substantial price reductions</TD>    

	</TR>

	<TR ALIGN=center>
		<TD>novel business opportunities</TD> <TD>new products &amp; services</TD>      
	</TR>

</TABLE>
</CENTER>

<HR>

<P>
<B>improved competitiveness / quality of service</B>
<P>
Electronic commerce enables suppliers to improve competitiveness
by becoming &quot;closer to the customer&quot;. As a simple example,
many companies are employing electronic commerce technology to
offer improved levels of pre-and post-sales support, with increased
levels of product information, guidance on product use, and rapid
response to customer enquiries. The corresponding customer benefit
is improved quality of service.
<P>
<B>mass customisation / personalised products and services</B>
<P>
With electronic interaction, customers are able to gather detailed
information on the needs of each individual customer and automatically
tailor products and services to those individual needs. This results
in customised products comparable to those offered by specialised
suppliers <I>but at mass market prices</I>. One simple example
is an on-line magazine that is tailored for the individual reader
on each access to emphasise articles likely to be of interest
and exclude articles that have already been read.
<P>
<B>shorten or eradicate supply chains / rapid response to needs</B>
<P>
Electronic commerce often allows traditional supply chains to
be shortened dramatically. There are many established examples
where goods are shipped directly from the manufacturer to the
end consumer, by-passing the traditional staging posts of wholesaler's
warehouse, retailer's warehouse and retail outlet. (Typically
the contribution of electronic commerce is not in making such
direct distribution <I>feasible</I> - since it could also be achieved
using paper catalogues and telephone or postal ordering - but
rather in making it <I>practical</I> in terms of both cost and
time delays.)
<P>
The extreme example arises in the case of products and services
that can be delivered electronically, when the supply chain can
be eradicated entirely. This has massive implications for the
entertainment industries (film, video, music, magazines, newspapers),
for the information and &quot;edutainment&quot; industries (including
all forms of publishing), and for companies concerned with the
development and distribution of computer software.
<P>
The corresponding customer benefit is the ability to rapidly obtain
the precise product that is required, without being limited to
those currently in stock at local suppliers.
<P>
<B>substantial cost savings / substantial price reductions</B>
<P>
One of the major contributions of electronic commerce is a reduction
in transaction costs. While the cost of a business transaction
that entails human interaction might be measured in dollars, the
cost of conducting a similar transaction electronically might
be a few cents or less. Hence, any business process involving
&quot;routine&quot; interactions between people offers the potential
for substantial cost savings, which can in turn be translated
into substantial price reductions for customers.
<P>
<B>novel business opportunities / new products and services</B>
<P>
In addition to re-defining the markets for existing products and
services, electronic commerce also provides the opportunity for
entirely new products and services. Examples include network supply
and support services, directory services, contact services (i.e.
establishing initial contact between potential customers and potential
suppliers), and many kinds of on-line information services.
<P>
While these various opportunities and benefits are all distinct,
they are to some extent inter-related. For example, improvements
in competitiveness and quality of service may in part be derived
from mass customisation, while shortening of supply chains may
contribute to cost savings and price reductions.
<A NAME="SCOPE"><H1>6 Scope of electronic commerce</H1>

<P>
Rather than being a single uniform technology, electronic commerce
is characterised by diversity. As shown by the upper layer of
&quot;bubbles&quot; in figure 3, it can encompass a wide range
of business operations and transactions, including:
<P>
establishment of initial contact, for example between a potential
customer and a potential supplier
<P>
exchange of information
<P>
pre-and post-sales support (details of available products and
services, technical guidance on product use, responses to customer
questions, &#133;)
<P>
sales
<P>
electronic payment (using electronic funds transfer, credit cards,
electronic cheques, electronic cash)
<P>
distribution, including both distribution management and tracking
for physical products, and actual distribution of products that
can be delivered electronically
<P>
virtual enterprises - groups of independent companies that pool
their competences so that they can offer products or services
that would be beyond the capabilities of any of the individual
companies
<P>
shared business processes that are jointly owned and operated
by a company and its trading partners.
<P>
Equally, as shown by the lower layer of &quot;bubbles&quot; in
figure 3, electronic commerce encompasses a wide range of communication
technologies including e-mail, fax, electronic data interchange
(EDI) and electronic funds transfer (EFT). Any of the technologies
shown in the figure can be used to support electronic commerce,
with the choice between them being governed by the context - each
is highly appropriate for some contexts and inappropriate for
others.
<P>
Figure 3 also highlights the need for a well-defined legal and
regulatory framework that is conducive to electronic commerce,
facilitating electronic business transactions rather than imposing
barriers. Since the opportunity for global interaction is one
of the main pillars of electronic commerce, this legal and regulatory
framework must also have a global scope.
<P>
<CENTER>
<IMG SRC="../icons/intro3.jpg" ALIGN="BOTTOM">
<P>
Figure 3 Scope of electronic commerce
</CENTER><A NAME="EXAMPLES"><H1>7 Examples</H1>

<P>
There are many well-established examples of electronic commerce
in a wide range of industry sectors and a wide range of application
areas. A few of these will serve to illustrate the nature of current
activity.
<H2>Retail</H2>

<P>
<B>ibs </B><A HREF="http://www.bookshop.co.uk">(http://www.bookshop.co.uk)</A HREF>
<P>
The Internet Bookshop exists only as a site on the World Wide
Web - it has no physical outlets. The shop specialises in technical
books and currently offers more than 780000 titles. Customers
visiting iBS can browse, search using keywords, and obtain detailed
information on individual titles, including a descriptive text,
bibliographic information, contents list, reviews, and suggested
readership. They can order and pay for books, which are then delivered
through publishers' established international delivery channels.
<P>
<B>Virtual Vineyards </B><A HREF="http://www.virtualvin.com">(http://www.virtualvin.com)</A HREF>
<P>
Like iBS, Virtual Vineyards exists only as a site on the Web.
It offers wines and gourmet foods, providing an outlet for a number
of small Californian wine producers. There is detailed on-line
information on the various wines and foods, and also an on-line
query service (using e-mail). Customers can order and pay using
either credit cards or electronic cash.
<P>
Customer orders are transferred electronically from Virtual Vineyards'
San Jose office to their Napa Valley warehouse, along with instructions
from printing the shipping label and enclosures (such as tasting
notes). The goods are shipped by Federal Express. Customers can
track the progress of the delivery on-line by accessing the Federal
Express site.
<H2>Finance</H2>

<P>
<B>Barclays Bank </B><A HREF="http://www.barclays.co.uk">(http://www.barclays.co.uk)</A HREF>
<P>
Many banks have offered on-line querying of accounts for some
time. Following relaxation of controls on the export of security
technologies from the USA, Barclays has extended this to a large
scale trial offering customers full banking services from their
home computers.
<P>
<B>ESI </B><A HREF="http://www.esi.co.uk">(http://www.esi.co.uk)</A HREF>
<P>
Electronic Share Information Ltd offers an on-line share information
and trading facility. Customers can view London Stock Exchange
prices and the FTSE 100 index, buy and sell shares on-line via
ShareLink, use a range of technical analysis and research tools,
obtain company profiles and share tips, request automatic notification
of share price changes, and obtain real-time portfolio valuations.
Launched in September 1995, the service now has 15000 registered
users and attracts 1.25 million visits per month.
<H2>Distribution</H2>

<P>
<B>DIPA </B>
<P>
DIPA GmbH supplies high quality photographic images. Customers
can browse an extensive photographic library and order the required
images which are then delivered over satellite links.
<P>
<B>Oracle </B><A HREF="http://www.oracle.com">(http://www.oracle.com)</A HREF>
<P>
Potential customers can now access Oracle's Web site and browse
information on the company's products. They can then download
free trial versions of various products, or pay on-line and download
full versions. Because of potential legal and financial problems,
the on-line purchasing and delivery service is currently limited
to United States customers only.
<H2>Pre/post sales support</H2>

<P>
<B>Hewlett Packard </B><A HREF="http://www.hp.com">(http://www.hp.com)</A HREF>
<P>
Hewlett Packard's &quot;Access HP&quot; Web site provides thousands
of pages of information, including general company information,
news, world-wide contact points, new product announcements, and
details of HP's wide range of products and services.
<P>
<B>GE Plastics </B><A HREF="http://www.ge.com/gep">(http://www.ge.com/gep)</A HREF>
<P>
GE Plastics is an industry leader in the field of engineering
plastics. The company's Web site provides an overview of the company's
products, detailed profiles of the properties of each material,
and guidance and recommendations for designing applications using
the company's materials. There is also an on-line &quot;Technical
Tip Of The Week&quot; contest whereby any visitor can submit a
tip for working with GE materials. The company selects the best
tips for incorporation into its &quot;Past Technical Tips&quot;
pages.
<H2>Engineering design</H2>

<P>
Ford
<P>
Ford engineering teams world-wide collaborate in the design of
new car engines using Ford's private network. The design support
system is a combination of a real-time videoconferencing system
and a shared &quot;design whiteboard&quot;. Any participant in
a design conference can draw or write on the whiteboard, drag
objects onto the whiteboard, and edit objects on the whiteboard.
All changes to the whiteboard are immediately visible to all other
participants. The object types supported include CAD drawings,
text documents, and video clips.
<P>
<B>GEN </B><A HREF="http://www.gen.net">(hhtp://www.gen.net)</A HREF>
<P>
The Global Engineering network is co-ordinated by Siemens Nixdorf
and has participants from many European countries. GEN is a &quot;marketplace
for engineering knowledge&quot;, bringing together the suppliers
of components and sub-assemblies and those who might incorporate
those components or assemblies into their own new products. The
suppliers enter detailed technical information (perhaps including
3D CAD drawings) into the GEN network. Potential customers can
then search the supplier information looking for &quot;best fit&quot;
components or assemblies, and can experiment with incorporating
those components or assemblies into the early stages of their
own product designs.
<H2>Business support</H2>

<P>
<B>citiusNet </B>(e-mail: <a href="mailto:citius@mail.citius.fr">citius@mail.citius.fr)</A>
<P>
citiusNet is a well-established system for supporting business-to-business
electronic commerce. It currently has three major elements - altius,
citius and fortius. Altius is an electronic catalogue of industry
and office supplies. Citius is a system for handling trading transactions.
Fortius supports electronic payment by EDI, and is used not only
for payment of goods selected from altius and traded over citius,
but also for routine transactions such as pension and insurance
payments. citiusNet is a multi-language service that is offered
internationally. The systems were developed by DDP from Lyon,
France, with the co-operation of various partners from Spain,
Belgium, Germany and Italy.
<P>
DDP now plans to extend its services to offer general business
support (&quot;Intermediation&quot;). DPP will handle all routine
operations (banking, administration, pension funds, etc.) on behalf
of its subscribers, thus allowing those subscribers to concentrate
on their core businesses.
<H2>Publishing</H2>

<P>
<B>The Times </B><A HREF="http://www.the-times.co.uk">(http://www.the-times.co.uk)</A HREF>
<P>
The Times and the Sunday Times are now published on-line. The
complete content of the newspapers is available, and access is
free. Using the &quot;Interactive Times&quot; facilities, users
of the on-line service can tailor the newspaper to their own personal
interests and tastes or perform a search for past articles that
include specified keywords.
<H2>Professional services</H2>

<P>
<B>de Kreek </B><A HREF="http://www.dds.nl/dekreek">(http://www.dds.nl/dekreek)</A HREF>
<P>
Mr Jeroen de Kreek, a lawyer from Amsterdam, provides a legal
question answering service that is available 24 hours a day. Users
of this service are led through a hierarchy of menus that aids
them in ultimately formulating their question as a text message.
Mr de Kreek then responds to this question, normally within two
hours. The response to the first question is free, but subsequent
questions incur charges.
<H2>International contact</H2>

<P>
<B>Global Tradepoint Network </B><A HREF="http://www.unicc.org/untpdc">(http://www.unicc.org/untpdc)</A HREF>
<P>
The Global Tradepoint Network is a huge network of business information,
developed under the UN-supported Electronic Trade Efficiency Programme.
By interfacing to established national databases, the network
aims to supply key trading data for countries across the world.
Such data might cover, for example, market information, transportation
options and prices, insurance facilities, credit availability,
customs requirements, and import/export regulations. Further,
through its &quot;electronic trading opportunities&quot; system,
the network serves as a meeting place for buyers and sellers world-wide.
Potential matches between buyers and sellers are identified by
using both geographical details and information on products offered
or required, the latter being expressed using the Harmonised Customs
Tariff codes. Once a potential match has been identified, the
buyer and seller establish contact directly.
<H2>Shared business processes</H2>

<P>
Tesco
<P>
Tesco operates around 540 supermarkets in the UK. The company
has a &quot;sales based ordering&quot; system whereby information
on product sales at individual supermarkets, as collected by the
checkout scanners, is forwarded electronically to the computers
at the company's Store Control Centre. These computers determine
the goods needed to replenish the stock at each store, and send
this information electronically to the computers at the Tesco
depot serving that store. For many products Tesco itself holds
no stock, so orders are generated automatically and forwarded
to Tesco's suppliers using EDI. On delivery to the Tesco depot,
the replacement stock is immediately shipped on to the appropriate
stores. Within 24 hours of an item being sold by the supermarket
its replacement is back on the shelves. The re-stocking system
relies on electronic communication and on close co-operation between
Tesco and its suppliers, who in effect are partners in a shared
business process of replenishing products on the supermarket shelves.
<A NAME="LEVELS"><H1>8 Levels of electronic commerce</H1>

<P>
As the above examples help to illustrate, there are various &quot;levels&quot;
at which electronic commerce can be conducted, ranging from a
simple network presence to electronic support for processes that
are jointly owned and enacted by two or more companies.
<P>
Various levels of electronic commerce are shown in figure 4. Such
a figure highlights the distinction between national transactions
and international transactions. The sources of this distinction
are not technical - as already emphasised, electronic commerce
is essentially global in concept - but rather legislative. Electronic
commerce is more complex at the international level than the intra-national
level because of such factors as taxation, contract law, customs
payments, and differences in banking practices.
<P>
The lower levels of electronic commerce are concerned with a basic
network presence, company promotion, and pre- and post-sales support.
By using available &quot;off the shelf&quot; technologies, these
levels can be both cheap and straightforward to implement, as
thousands of small companies can already testify. By contrast,
the more advanced forms of electronic commerce pose complex problems
that are as much legal and cultural as technological. At these
levels there are no &quot;off the shelf&quot; solutions, so companies
are forced to develop their own custom systems. Thus at present
it tends to be only the larger and richer companies that are pioneering
these levels. However, over time the boundary of what is commonplace
will gradually move up to encompass the more complex levels of
electronic commerce, and further &quot;off the shelf&quot; technologies
will be established to support these higher levels, just as they
have been for the lower levels.
<P>
<CENTER>
<IMG SRC="../icons/intro4.jpg" ALIGN="BOTTOM">
<P>
Figure 4 Levels of electronic commerce
</CENTER>
<A NAME="OPEN"><H1>9 Open issues</H1>

<P>
While electronic commerce is growing rapidly, there are several
open issues that must be resolved if its full potential is to
be realised. These include:
<P>
<B>globalisation</B>
<P>
Potentially, global networks could make it as easy to do business
with a company on the other side of the world as with one on the
next street. However, the communication medium alone, while necessary,
is far from sufficient. How do companies in different continents
become aware of each others' existence, and the products and services
that are offered or required? How can a company gain an understanding
of the business traditions and conventions of some country on
the opposite side of the globe, particularly when those conventions
and traditions are often unwritten? And how can the linguistic
and cultural diversity of a global user community best be respected
and supported? These and related questions are all part of the
broad issue of <I>globalisation</I> - making truly global electronic
commerce a practical reality.
<P>
<B>contractual and financial issues</B>
<P>
Suppose that a company in Thailand browses the electronic catalogue
of a Russian company and places an electronic order for products
that will be delivered electronically and for which payment will
also be made electronically. This simple scenario raises several
fundamental questions that as yet are unresolved. At precisely
what point is a binding contract established between the companies?
What is the legal status of this contract? What body has legal
jurisdiction over the contract? Given differences in financial
regulations and practices, how is payment made and confirmed?
What taxes and customs charges apply to the products? How are
these taxes and charges &quot;policed&quot; and collected? Could
the charges and taxes be avoided by the simple expedient of maintaining
an electronic &quot;manufacturing&quot; facility in some third
country?
<P>
<B>ownership</B>
<P>
Particularly for goods that can be distributed electronically,
and hence can readily be copied, the issue of protecting copyright
and intellectual property rights represents a major challenge.
<P>
<B>privacy and security</B>
<P>
Electronic commerce over open networks demands effective and trusted
mechanisms for privacy and security. These mechanisms must provide
for confidentiality, authentication (i.e. enabling each party
in a transaction to ascertain with certainty the identity of the
other party), and non-repudiation (i.e. ensuring that the parties
to a transaction cannot subsequently deny their participation).
Since the recognised privacy and security mechanisms depend upon
certification by a trusted third party (such as a government body),
global electronic commerce will require the establishment of a
global certification system.
<P>
<B>interconnectivity and interoperability</B>
<P>
Realising the full potential of electronic commerce requires universal
access - every company and every consumer must be able to access
all organisations offering products or services, regardless of
geographical location or the specific networks to which those
organisations are connected. This in turn demands universal standards
for network interconnection and interoperation.
<P>
<B>deployment</B>
<P>
One factor that could limit the emergence of electronic commerce
is lack of awareness and skills. There is a danger that many companies
(particularly SMEs) could be left behind and placed at a disadvantage,
simply through being unaware of the possibilities and opportunities.
Hence, there is an urgent need to promote awareness, to publicise
examples of best practice, and to provide education and training.
<A NAME="ACTORS"><H1>10 The actors and their roles</H1>

<P>
Several of the open issues identified above must be resolved at
a global level. Hence, the actors with responsibility for resolving
the issues and promoting electronic commerce must include multi-national
bodies. Equally, there is a role for national governments in removing
national barriers and ensuring fair competition, and for sector
representatives in promoting awareness and best practice. Finally,
there are obvious roles for technology suppliers, user companies
and individual consumers in enabling, adopting and exploiting
electronic commerce. Hence, the actors include all those shown
in table 2.
<P>
Collectively, these actors must perform all the roles shown in
table 2. Each actor has some responsibility for several of the
roles and, conversely, each role is shared by several actors.
<HR>


Table 2   <B>Actors and roles</B>                                                         
<P><P>
<CENTER>
<TABLE BORDER CELLPADDING=10 CELLSPACING=0>

	<TR>
		<TH><B>actors</B></TH> <TH><B>roles</B></TH>                
	</TR>

	<TR ALIGN=center>
		<TD>multi-national bodies<BR><BR>national governments<BR><BR>
		    sector representatives<BR><BR>tecnology suppliers<BR><BR>
		    companies<BR><BR>consumers</TD>
		<TD>foster the Information Society<BR>remove global barriers<BR>             
		    ensure level playing field<BR>remove national barriers<BR>          
		    promote awareness &amp; adoption<BR>provide enabling technologies<BR>      
		    re-organise the business<BR>adopt the technologies<BR>             
		    grasp the opportunities</TD
	</TR>            


</TABLE>
</CENTER>
<HR>

<P>
<A NAME="G7"><H1>11 <A HREF="http://www.cordis.lu/esprit/src/smehome">The G-7 initiative</A HREF></H1>
<P>
Early in 1995 the G-7 nations launched a group of eleven initiatives
that collectively aim to demonstrate the potential of the information
society and stimulate its deployment. One of these initiatives,
&quot;A Global Marketplace for SMEs&quot;, has the overall objective
of facilitating increased competitiveness and participation in
global trade for SMEs by exploiting the opportunities offered
by the development of the global information society. Specific
objectives are to:
<P>
contribute to the development of a global electronic environment
for the open and non-discriminatory exchange of information (e.g.
on technologies, products, human resources), overcoming obstacles
of distance, time and country borders, for the benefit of SMEs
<P>
expand global electronic commerce in order to enable enterprises
to carry out their business operations and management more effectively
and more profitably.
<P>
The initiative, which is scheduled for completion at the end of
1988, has three themes, each with its own co-ordinator.
<P>
<B>theme 1 global information network for SMEs</B>
<P>
This theme will contribute to the development of an open non-discriminatory
environment enabling SMEs to access the information they need
and disseminate information on their products, technologies, and
so forth, using international information networks. The theme
co-ordinator is Japan.
<P>
<B>theme 2 SME requirements - legal, institutional and technical</B>
<P>
This theme aims to ensure that the systemic issues associated
with an open global marketplace for SMEs are addressed, and to
provide a framework based on the systemic issues that will ensure
that the project as a whole responds to the explicit needs of
SMEs. The systemic issues are those listed in section 9 above.
The theme co-ordinator is the European Commission.
<P>
<B>theme 3 international testbeds for electronic commerce</B>
<P>
This theme aims: (i) to promote awareness of the issues that must
be addressed to realise a &quot;global marketplace for SMEs&quot;
through global electronic commerce; (ii) to encourage the development
of testbeds, pilot projects and other co-operative ventures that
evaluate or demonstrate approaches to addressing the issues; (iii)
to publicise successful demonstrations of global electronic commerce
involving SMEs. The theme co-ordinator is the USA.
<P>
The initiative is open to participation by non-G-7 countries and
international organisations.
<A NAME="CONCLUSION"><H1>12 Conclusion</H1>

<P>
This introductory paper has emphasised the broad view of electronic
commerce as a vehicle for business revolution. It has stressed
the importance of taking a global perspective. And it has suggested
that the impact of electronic commerce will be pervasive, both
on companies and on society as a whole.
<P>
Despite a number of open issues that are yet to be resolved, electronic
commerce is happening today and happening fast. It is essentially
a &quot;bottom up&quot; revolution. Companies world-wide are establishing
a basic electronic presence on a global open network, learning
from the experience, and gradually becoming more sophisticated
in their use of the technologies. While the more advanced levels
of electronic commerce present substantial challenges, the more
basic levels are now well established and supported by &quot;off
the shelf&quot; solutions. The best way of gaining the mastery
of electronic commerce that will be vital in tomorrow's markets
is to try it today. 
<P><P><P><P><P><P>
<P><P><P><P><P><P>
<HR>

<B>The information in this article does not necessarily reflect the opinion
of the European Commission. Examples are included for illustrative purposes
only and are assumed to be correct. However, the Web sites mentioned or
linked to as well as products, unless specifically indicated, are not under
the control of the European Commission. For this reason, the Commission can
make no representation concerning such sites or products to you; nor does
the fact that the Commission has provided reference to these serve as an
endorsement by the Commission of any organisation or individual maintaining
or providing content for any of the sites or products listed. The Commission
has not tested any software found on the sites and is not in a position to make
any representation regarding the quality, safety or suitability of any software
found on them or retrievable from them.</B>
<HR><P><P><P><P><P><P><CENTER>
For more details, <A HREF="mailto:paul.timmers@dg3.cec.be" >contact Esprit's Electronic Commerce team.</A>
<P><P><P><P><P><P><HR>

<A HREF="/esprit/home.html" >Esprit homepage</A>
| <A HREF="/esprit/src/invencom.htm" >Inventory of Electronic Commerce Initiatives</A>
<BR><A HREF="/esprit/src/task7-10.htm" >Esprit task 7.10: Electronic Commerce Pilots</A>
<BR><A HREF="/esprit/src/wp96.htm" >Work-programme</A>
| <A HREF="mailto:paul.timmers@dg3.cec.be" >Contact Esprit's Electronic Commerce team</A>
<P>

<H5><I>The URL of this page is http://www.cordis.lu/esprit/src/ecomint.htm
<BR>
It was last updated on 8 May 1996, and is maintained by Paul
Timmers<BR><A HREF="mailto:paul.timmers@dg3.cec.be" >paul.timmers@dg3.cec.be</A></H5></I>
</CENTER>
</BODY>

</HTML>

