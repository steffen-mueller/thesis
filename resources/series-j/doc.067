www.omicronet.com/spin/frit9605.htm
<!DOCTYPE HTML PUBLIC "-//SQ//DTD HTML 2.0 HoTMetaL + extensions//EN">
<HTML>
<HEAD>
<TITLE>OMICRONet THE SPIN Magazine - MELTDOWN COUNTDOWN TO YEAR 2000</TITLE></HEAD>
<BODY BACKGROUND="/bkgrd/blu_strp.gif">
<P><A NAME="top">	</A></P>
<HR>
<TABLE WIDTH="100%">
<TR>
<TD VALIGN="TOP" ALIGN="LEFT"><IMG
SRC="/images/omnet-s.gif" ALIGN="TOP" BORDER="0"><BR><A
HREF="/i-maps/navbar.map"><IMG
SRC="/images/navbar.gif" ALIGN="BOTTOM" ISMAP="ISMAP" BORDER="0"></A></TD>
<TD WIDTH="50"></TD>
<TD VALIGN="BOTTOM" ALIGN="RIGHT"><IMG
SRC="/images/thespin3.gif" ALIGN="BOTTOM"><BR><IMG
SRC="/images/features.gif" ALIGN="BOTTOM"></TD></TR></TABLE>
<HR>
<TABLE>
<TR>
<TD>
<PRE>             </PRE></TD>
<TD COLSPAN="2"><FONT SIZE="2">Click here for
<A HREF="/atlanta/ats.htm">OMICRON - Atlanta Year 2000 Work Group</A> or to
contribute to the <A HREF="/yr2000/ei001.htm">Electronic Interchange On Year
2000</A></FONT></TD></TR></TABLE>
<HR>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR>
<TD VALIGN="TOP" ALIGN="LEFT" ROWSPAN="2"><FONT SIZE="2">By:<BR>Elicia
Fritsch</FONT>
<PRE>             </PRE></TD>
<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN="2" ROWSPAN="2">
<BLOCKQUOTE>
<P><FONT SIZE="5">Meltdown Countdown To Year 2000</FONT></P>
<UL>
<LI><A HREF="#Ready">ARE YOU READY</A></LI>
<LI><A HREF="#Problem">HOW DID THIS HAPPEN?
</A></LI>
<LI><A HREF="#Example">HOW BIG IS THE PROBLEM?</A></LI>
<LI SRC="#GOODNEWS"><A HREF="#GOODNEWS">WHAT'S THE GOOD NEWS?</A></LI>
<LI><A HREF="#Solution">WHAT IS THE SOLUTION?</A></LI>
<LI><A HREF="#Cost">HOW MUCH WILL IT COST?</A></LI>
<LI><A HREF="#1996">BUT IT'S ONLY 1996</A></LI>
<LI><A HREF="#(SIDEBAR)">(SIDE-BAR)</A></LI></UL>
<HR></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="LEFT" ROWSPAN="2" WIDTH="20"></TD></TR>
<TR></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="Ready"></A>
<HR>
<BLOCKQUOTE>
<P><FONT SIZE="7">A</FONT>re you ready for the year 2000? More importantly, is
your computer system? Before reading further, perform this simple test.</P>
<OL>
<LI>Set the date on your system to December 31, 1999
</LI>
<LI>Set the time to 23:58:00
</LI>
<LI>Turn your system off for five minutes
</LI>
<LI>Turn your system back on.</LI></OL>
<P>Check the date -- if it says January 4, 1980, your system is headed for a
meltdown and you share this problem with 90% of computer users in the world.</P>
<P>Predictions are that 98% of the applications being used today will fail on
January 1, 2000. From Wall Street to Main Street and across the oceans, all
businesses, and most computer systems, will be affected by century date change.
In reality, many of the systems are already failing.</P>
<P>Ramifications for business include failure of financial systems resulting in
inaccurate financial statements; failure of inventory systems to rollover to
acceptable year 2000 date format resulting in production shutdown; failure of
retail point-of-sale systems resulting in lost sales; failure of consumer
credit, resulting in economic collapse.</P>
<P>Let's say your birth year is 1955. Ask the computer how old you are and the
outcome will be determined by subtracting &quot;55&quot; from &quot;96&quot;
(for 1996). You are 41 years old. In the year 2000, the computer will subtract
your birth year, &quot;55&quot;, from &quot;00&quot; and determine your age to
be -55 years old.</P></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT" WIDTH="20"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="Problem"></A>
<HR><B>HOW DID THIS HAPPEN?
</B>
<P></P>
<BLOCKQUOTE>
<P> According to Peter de Jager, founder of Year 2000 organization, 
programmers of the 1950s and 1960s abbreviated code wherever possible because
programs need space and space was expensive. Thus, the MMDDYY date field,
assuming the first two numbers of the &quot;YY&quot; to be &quot;19&quot;.</P>
<P>&quot;If architects built buildings the way programmers designed systems, a
woodpecker could destroy civilization,&quot; says de Jager, paraphrasing another
computer guru. &quot;The year 2000 is our woodpecker and fewer than 20% of North
American business is doing anything.&quot;</P>
<P>The intention was to replace programs every five or six years, however, only
specific operations were updated. The source code remained unchanged. This &quot;little
date problem&quot; still resides in billions of lines of software source code,
alive and well inside your system. Do you know where the documentation for your
source code is?
</P></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT" WIDTH="20"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="Example"></A>
<HR><B>HOW BIG IS THE PROBLEM?</B>
<P></P>
<BLOCKQUOTE>
<P> Programmers have known about this problem for quite some time. The dilemma
has been that no one wanted to tell management that it needs to embark on a
multi-million dollar project just to keep the business running, a project that
has a zero return on investment, and no options.</P>
<P>If the problem is not addressed, then your company will go bankrupt. Can
your business do without the applications that depend on date information? An
inventory system? A benefits system? A payroll? Accounting? Electronic data
transfer? All these applications are at risk.</P>
<P>How can you get a good idea for the amount of work your system needs? Test
all your systems, according to de Jager. Change system clock as shown earlier,
post some entries -- general ledger, accounts parables and receivables,
invoicing, etc. Then pull end-of-year financial statements. Turn your system off
for a few minutes, go back into the system, post more entries, flow the
information into your financial system, and pull year-end statements for 1999,
2000 and 2001. Compare the reports with your 1996 test. Do they make sense?</P></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT" WIDTH="20"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="GOODNEWS"></A>
<HR><B>WHAT'S THE GOOD NEWS?
</B>
<P></P>
<BLOCKQUOTE>
<P> The solution is technologically simple, but extremely tedious and
potentially expensive. The good news is that there are programs and consultants
that can help with this problem.</P>
<P>One such program was designed by Software Eclectics in Alpharetta. Steve
Mills, President of the eight year old company, says his software, SE/ONE, can
find all date reference codes in Cobol programs, the primary language in
mainframe systems.</P>
<P>&quot;No human can scan thirty or forty thousand lines of code and find all
date references,&quot; said Mills. &quot;It's like trying to find every mention
of the word 'blue' in a novel. You may find most of them, but you can't find
them all. In software code, you must find them all. And then integrate the
changes with feeder applications.&quot;</P>
<P>An example of an integrated system is one where a company's accounting
system feeds information to its inventory system and its sales forecasting
system. Inventory is also tied to production, scheduling and purchasing.
Purchasing is tied to electronic data interchange which is tied in to vendor
systems. Each company must take responsibility for its internal and external
data exchanges. Software application vendors can offer their own solutions, but
none can assess a companywide situation.</P>
<P>Companies like Dun &amp; Bradstreet Software of Atlanta offer a whole-system
approach. According to Nora Laughton, program director for Year 2000 products,
DBS has products that replace existing applications in personnel, manufacturing
and materials management and accounting systems.</P>
<P>&quot;A company needs to perform an audit of their systems, both in-house
and in integrated applications,&quot; said Laughton. &quot;Then it needs to
determine the feasibility of rewriting their in-house programs, or replacing
them with a vendor solution.&quot;
</P></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT" WIDTH="20"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="Solution"></A>
<HR><B>WHAT IS THE SOLUTION?</B>
<P></P>
<BLOCKQUOTE>
<P>The first step is to create a task force in your organization composed of
key users, finance people and IS professionals. Identify the mission critical
applications - those your organization cannot run without. Then, locate all
date-dependent calculations inside those applications and determine if they are
Y2K compatible.</P>
<P>Tom Ash, CEO of SunTrust Service Corporation, says his company began
research last June and realized the problem was larger than anyone anticipated.</P>
<P>&quot;Almost every aspect of our business will potentially be impacted,&quot;
Ash said, &quot;including our telephone systems, ATMs, on-line transactions and
the software in our loan and operations departments. We have discovered over 30
million lines of code.&quot;
</P>
<P>SunTrust has a plan and a timeline to become Y2K compliant. For example, any
new software, hardware and telecommunications equipment acquisitions are
required to have Y2K provisions. The company expects applications software
vendors to give date of compliance deadline, and a commitment to make those
changes. They are also requesting operating systems software providers to give
assurance of compliance, and provide the changes. This is in addition to their
proprietary programs that a source specialist will have to modify.</P>
<P>Ash believes that his costs to implement the changes will be far below the
industry estimates.
</P></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="Cost"></A>
<HR><B>HOW MUCH WILL IT COST?
</B>
<P></P>
<BLOCKQUOTE>
<P>Current figures range from 40 cents to $1.10 to change a line of code. A
standard medium-size company will outlay up to $5 million to become Y2K
compliant. Rumor has it that the U.S. government will spend between $10 and $25
billion. And this is a worldwide predicament.</P>
<P>Every business has to make the decision whether to contract with an outside
source like Cap Gemini America that has established a Transmillenium division,
or Dun &amp; Bradstreet Software, or fix the problem itself. How much are you
willing to pay to ensure your accounting, production, sales and backup systems
are not going to fail? Or that your systems are compatible with your customers?

</P></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="1996"></A>
<HR><B>BUT IT'S ONLY 1996</B>
<P></P>
<BLOCKQUOTE>
<P>According to the experts, you are out of time. If you start today, it will
take about six months to answer the &quot;How big is it&quot; question, another
six months to plan your attack, one year to test all your applications and two
years of implementation and re-testing. This translates into <BLINK>thousands</BLINK>
of years of time, and there are less than four years left.</P>
<P>If you haven't yet experienced a system crash, let that be an ordeal someone
else endures. There is still time to avoid the meltdown, and it's easy to take
the first step.</P></BLOCKQUOTE>
<PRE>

</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR>
<TR>
<TD></TD>
<TD COLSPAN="2"><A NAME="(SIDEBAR)"></A>
<HR><B>(SIDE-BAR)</B>
<P></P>
<BLOCKQUOTE>
<P>Vendor solutions and consultants can be found on the Internet</P>
<UL>
<LI><A HREF="http://www.ansi.org">http://www.ansi.org</A>
<BR>ANSI online home page giving access to ANSI catalog and standards documents
</LI>
<LI><A HREF="http://www.software.ibm.com">http://www.software.ibm.com</A><BR>IBM
software page giving access to white paper entitled &quot;The Year 2000 and
2-Digit Dates:A Guide for Planning and Implementation&quot;</LI>
<LI><A HREF="http://www.y2k-info.com">http://www.y2k-info.com</A><BR>Information
about and resources for the Year 2000 solutions.</LI>
<LI><A HREF="http://www.year2000.com">http://www.year2000.com</A> (Y2K FAQ)<BR>
Year 2000 home page facilitated by Peter de Jager, including discussion group
and member forum</LI>
<LI><A HREF="http://www.chccorp.com">http://www.chccorp.com</A><BR>Home page
of Computer Horizons Corporation one of the leaders in Year 2000 solutions</LI></UL></BLOCKQUOTE>
<PRE>
</PRE></TD>
<TD VALIGN="TOP" ALIGN="RIGHT"><A HREF="#top"><IMG
SRC="/images/top.gif" ALIGN="TEXTTOP" BORDER="0" WIDTH="30">
</A></TD></TR></TABLE>
<HR>
<CENTER><FONT SIZE="4">Written By: Elicia Fritsch</FONT><BR><FONT SIZE="2">free-lance
writer and frequent contributor at OMICRON - Atlanta</FONT><BR><FONT SIZE="2">
 <A HREF="mailto:atlanta@omicronet.com">Email Elicia at: atlanta@omicronet.com</A>.
</FONT></CENTER></BODY></HTML>

