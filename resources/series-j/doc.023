www.mccutchen.com/ip/triadmai.htm
<HTML>
<HEAD>
<TITLE>McCutchen Intellectual Property and Technology Law Group: Triad and MAI: Providing Needed Protection For Software Copyrights</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">

<TABLE>
<TR VALIGN=CENTER>
<TD>
<A HREF="../mdbe_toc.htm"><IMG BORDER=0 ALT="McCutchen On-Line" WIDTH=300 HEIGHT=57 SRC="../gifs/mast16.gif"></A>
</TD>
<TD>
<A HREF="ip_030.htm"><IMG BORDER=0 ALT="Intellectual Property and Technology Law Group" WIDTH=322 HEIGHT=77 SRC="../gifs/ip_mast.gif"></A>
</TD>
</TR>
</TABLE>
<BR>


<H2><I>Triad</I> and <I>MAI</I>:  Providing Needed Protection For Software Copyrights</H2>

by Charlie Crompton and Daniel M. Wall
<P>
Recently a lot of criticism has been directed at a pair of Ninth Circuit decisions, <I>Triad Systems Corp. v. Southeastern Express Co.</I>,  and its predecessor, <I>MAI Systems Corp. v. Peak Computer, Inc.</I>   An article in this publication, for example, portrayed <I>Triad</I> and <I>MAI</I> as "unprecedented" and "troubling."   But to copyright holders -- and everyone else interested in ensuring that the courts protect creative works, in particular software -- there is nothing worrisome about these decisions.  Nor are they novel; they merely hold that parties making unauthorized copies of copyrighted works for financial gain are liable for infringement.  Under the facts of these cases, <I>Triad</I> in particular, no other outcome would make sense.
<P>
Critics of <I>Triad</I> and <I>MAI</I> nevertheless maintain that software copying is not significant enough to constitute infringement unless it entails proliferation of permanent, tangible copies of the software.   They also argue that infringement should be permitted so long as it enables the infringer to compete with the copyright holder, thereby providing consumers with more choices.   These positions are at odds with the interests of copyright holders, particularly now that millions can access, temporarily copy, and use programs and content via the Internet.  It is no surprise, then, that they are at odds with established principles of copyright law, as well.

<H3>The Copying at Issue</H3>

<I>Triad</I> involved a dispute between a computer manufacturer and service provider, Triad, and Southeastern, an independent service organization ("ISO") providing service to Triad computer owners.  Southeastern used Triad's copyrighted service software, repeatedly, to diagnose, locate, and efficiently fix problems with its customers' systems.  Southeastern's claimed ability to use Triad's software was featured prominently in its advertisements.  Southeastern admitted that it couldn't be in the service business without Triad's service software.
<P>
Running a software program on a Triad computer, as on most any other type of computer, resulted in a temporary copy of the program being made in the computer's random-access memory ("RAM").  In MAI, a previous case brought by another copyright-holding computer manufacturer (MAI) against another ISO (Peak) for using copyrighted software in the course of performing service, the Ninth Circuit held that RAM copies qualified as "copies" under the Copyright Act  and thus, unless authorized or excused, were infringements.
<P>
Southeastern never obtained Triad's permission to make RAM copies of Triad's software.  Triad sued Southeastern for infringing its software copyrights, ultimately relying on the Ninth Circuit's holding in <I>MAI</I>.  Like MAI before it, Triad won an infringement verdict, obtained monetary and injunctive relief, and won before the Ninth Circuit on appeal. The Supreme Court has let the ruling stand.

<H3>RAM Copies are Copies Too</H3>

In <I>MAI</I>, the court focused on the temporary copies of MAI's operating system software made automatically in RAM whenever Peak turned on one of its customers' computers.   Accordingly, <I>MAI</I>'s critics maintain that requiring ISOs to obtain copyright holders' permission to make these copies gives copyright holders too much power.  These critics claim that <I>MAI</I> will be used by copyright holders to prohibit unauthorized ISOs from turning on their customers' computers to perform even the most routine hardware maintenance.
<P>
Neither Peak nor Southeastern was merely performing hardware repairs, however.   In <I>Triad</I>, the dispute centered on Southeastern's constant, unauthorized use of Triad's service software, created for the benefit of Triad's service technicians, to save Southeastern time and money in its competing service business.  Triad did not even object in the Ninth Circuit about Southeastern's arguably inadvertent copying of Triad's copyrighted operating system software in the course of turning on computers.  Thus, the copying at issue was neither inadvertent, incidental, nor the result of some technical trap.
<P>
In both <I>Triad</I> and <I>MAI</I>, the ISO defendants argued that RAM copies of software were too temporary to be "fixed in a tangible medium of expression," as required under the Copyright Act's definition of a "copy."   However, this requirement means only that the copy must be more than transitory, not permanent.  Numerous cases and other authorities have found RAM copies to be potentially infringing copies.   The Ninth Circuit thus had little trouble finding that RAM copies to be sufficiently "fixed."
<P>
This makes sense.  A RAM copy of a program disappears once the program is no longer running, as when the computer is turned off, but this is offset by the user's power to control how long the copies will last and to make new copies at will.  After all, why make a copy that will last longer than immediately necessary when a new one can be made any time?

<H3>Software Licenses and the Copyright Act</H3>

Both MAI and Triad licensed, rather than sold, their software to their customers.   Their licensees acquired "a restricted right to use the software" but were prohibited from "allowing others [including unauthorized ISOs] to use it."   These are standard terms; similar ones can be found in most other licenses and numerous form books.  Indeed,  nearly all software today is licensed, rather than sold outright.
<P>
There are many reasons for this practice, but one paramount one is that the law distinguishes between the rights of a licensee of a copyrighted work and an owner.  For example, a "first sale" extinguishes the copyright holder's rights over a particular copy of a copyrighted work, but a license does not.   And, more importantly here, the Act gives "the owner of a copy of a computer program" the right to make certain types of copies that would infringe if made by licensees.   This includes RAM copies, as the Ninth Circuit held in both <I>Triad</I> and <I>MAI</I>.
<P>
Critics of these decisions argue that they are contrary to the expectations of licensees, who have paid for their software and thus presume they can permit anyone they want to use it.  However, no licensee of MAI's or Triad's who read the applicable license, much less the relevant case law, could reasonably believe that.  The argument that the software has already been paid for by the customer when ISOs use it ignores what was paid for:  a limited range of rights that excludes use by third parties, such as ISOs.  Thus, ISOs who make RAM copies in the course of servicing their customersí computers arenít using their customersí software, they are using the licensorís.  Customers who want broader rights can negotiate for them; so can their service providers, ISOs included.  But licensees should not be given more than they bargained for by the courts.  Copyright holders distribute their products with the understanding that their licenses and the rules underlying them will be enforced, and they set their prices accordingly.  Judicially re-writing these agreements would create a windfall for licensees and would deprive licensors of a revenue stream they intended to retain.  Rather than rewarding any silent, unwarranted expectations, the Ninth Circuit enforced Triad's and MAI's licenses as written.
<P>
The Ninth Circuit's inquiry in <I>MAI</I> effectively ended with the finding that RAM copying can lead to infringement.  In <I>Triad</I>, however, the court went on to consider two affirmative defenses raised by Southeastern, fair use and copyright misuse.

<H3>Fair Use</H3>

Fair use "'allows the holder of the privilege to use the copyrighted material in a reasonable manner without the consent of the copyright owner.'"   It allows the limited copying of a work incidental to the creation of another, or to advance particular public benefits, such as the development of art, science, or industry.  The defense is thus designed to prevent stifling "the very creativity . . . [the copyright] law is designed to foster.'"   Southeastern claimed that by giving Triad computer owners another service provider to choose from, it was providing the sort of public benefit the fair use doctrine was intended to protect.
<P>
Section 107 of the Copyright Act contains a non-exclusive list of four factors to be considered in determining whether a particular use is a fair use:
<P>
<OL>
<LI>the purpose and character of the use, including whether such use is of a commercial nature or is for nonprofit educational purposes;
<LI>the nature of the copyrighted work;
<LI>the amount and substantiality of the portion used in relation to the copyrighted work as a whole; and
<LI>the effect of the use upon the potential market for or value of the copyrighted work.
</OL>
<P>
The trial court found for Triad on all of these factors, as did the Ninth Circuit.  But a look at the leading fair use decisions reveals an even simpler, more direct answer to Southeasternís argument that increasing competition by itself is a fair use.
<P>
The Supreme Court recently held that an unauthorized copy which merely "serves as a market replacement for" a copyrighted work is not a fair use.   It follows that an unauthorized copy made by the copyright holder's competitor and offered as a direct economic substitute for the original work (or an authorized copy of it) is not a fair use.   And indeed, no court has ever found a fair use where an entire copyrighted work was copied in a non-transformative way to commercially compete with the copyright holder.
<P>
The Ninth Circuit recognized this, contrasting Southeastern's use of Triad's software with the defendant's use of copyrighted video-game software as an intermediate step in creating a new, competing game in <I>Sega v. Accolade</I>, 977 F.2d 1510 (9th Cir. 1992), a case relied on by Southeastern.
Southeastern's activities are wholly unlike the reverse-engineering in Sega.  Southeastern did not make a minimal use of Triad's programs solely to achieve compatibility with Triad's computers for Southeastern's own creative programs.  Rather, Southeastern has created nothing of its own  . . .  Southeastern is simply commandeering its customers' software and using it for the very purpose for which, and in precisely the manner in which, it was designed to be used.
<P>
The court further recognized that Southeastern's copying, if not stopped, would cause Triad to lose potential licensing revenues from the ISOs.  Thus, the court concluded, "we detect no appreciable public benefit arising from Southeastern's practice to justify this continuance under the fair use doctrine."

<H3>Copyright Misuse</H3>

Southeastern also argued that Triad's licensing practices and policies were a misuse of Triad's copyrights.  Once again, the Ninth Circuit disagreed.
<P>
Copyright misuse is a common law doctrine derived from the patent misuse defense and applied sparingly.  Apparently it has never been applied by the Supreme Court or any circuit court except the Fourth Circuit, where it originated.   The defense prevents copyright holders from curtailing legitimate creative activity by others, for example by making software licensees sign agreements forbidding them from independently creating competing works.
<P>
Triad's license policies and practices -- which, again, tracked industry norms -- "did not attempt to prevent Southeastern or any other ISO from developing its own service software to compete with Triad," the Ninth Circuit held.  Southeastern had no desire to develop its own software; it refused to consider doing so, arguing that it should be able to use Triadís software for free.  Accordingly, the court concluded there was no misuse.

<H3>Conclusion</H3>

The upshot of <I>Triad</I> and <I>MAI</I> is that ISOs cannot help themselves to free copies of computer manufacturers' software in order to compete with the manufacturers.  These cases might have been decided differently, or the opposite way, had the ISOs limited their copying, offered to pay a license fee, or added something creative to the copyrighted works.  Under the circumstances, however, any different result would be unfair to the copyright holders and undermine the copyright system.
<P>
As software programs become easier to obtain and copy without detection, vigilant copyright protection becomes increasingly important.  The value of a copyrighted program can now be obtained -- or lost forever -- with a few keystrokes.  Copyright law must keep pace.  <I>Triad</I> and <I>MAI</I> are steps in the right direction.<BR>
<P>
<CENTER>
<A HREF="http://www.mccutchen.com/tool_16a.map"><IMG BORDER=0 WIDTH=431 HEIGHT=41 SRC="../gifs/tool_16a.gif" ISMAP></A>
</CENTER>
<BR>


<!-- start of McCutchen Footer -->
<CENTER><FONT SIZE="-1">Copyright &copy; 1996, 1997 McCutchen, Doyle, Brown &amp; Enersen, LLP
<P>
<A HREF="/disclaim.htm">Disclaimer</A></FONT>
</CENTER>

</BODY>
</HTML>
