www-xdiv.lanl.gov/XCM/neural/xwindows/kenlee.html
<title>INDUSTRIAL  PARTNERSHIPS</title>
<h2>INDUSTRIAL  PARTNERSHIPS</h2>

<p>
<IMG SRC="neural_logo.gif">

<p>

This article appeared in the Winter, 1993 edition of X Windows, the X Division newsletter. <p>

Kenneth Lee's X-1 Teams Handle Many Projects<p>
  Kenneth Lee, Deputy Group Leader of X-1, is an idea man and team builder who has envisioned and pursued many CRADA and industrial partnership projects  for diversification in X-1.  According to his colleague Roger Jones, X-1, if an industry has a need, Ken is the one who fits together the jigsaw puzzle pieces of creative vision, theory, industrial needs, Laboratory personnel, and LANL technical and computational resources to get the Lab to produce a deliverable.<p>

  Once Ken, as point man, puts together a project, Roger takes over on the work, finds a project leader who will be responsible for follow-on, and turns the project over to him.  The two men, working in tandem, have thus inaugurated many innovative programs in X-1. <p>

  One of Ken's first projects, the three-year U. S. Navy  Patuxent River virtual reality project  for testing aircraft sensors, is nearing completion. Robert Webster, X-1, was project leader for this extremely complex and highly successful $7 million program. The prototype demonstration in September caused the Navy and Airforce to submit a joint proposal to DoD for FY-95 follow-on efforts.<p>

  A current project, working with the semiconductor industry, especially Sematech, Lam Research, Texas Instruments, and several universities, will apply neural networks to three computer chip sub-micron manufacturing processes in which plasma reactors etch circuits, deposit thin films, and implant ions. The neural networks will control the plasma reactors and detect and classify problems.  Kevin Buescher, X-1, director-funded Post-Doc., is project leader and Roger Jones, major technical support.  Other key participants are Michael Splichal, UNM/X-1 GRA, Mike Messina, X-1 GRA, Danny Sheviz, X-1 Post-Doc., and Vladimir Makhankov, CNLS.<p>

  A project in high computation for the oil and gas industry to model seismic shots in oil exploration has Ken Lee as project leader, working in collaboration with EES-4. Other key personnel include John Pearson, CNLS, Bob Webster, X-1, Frank Ortega, X-1, and Roger Jones.  To model a single seismic shot, requires a meaningful computation of 109 computational cells, 1010 bytes of memory, and 104 time steps.  There can be several hundred shots in a single seismic survey. This problem challenges our parallel processing supercomputers and demands development of efficient and accurate algorithms.<p>

  Ken is the offical point of contact for X Division with the ongoing oil and gas initiative. At the December 1-3 workshop conducted at LANL by Mobil, Chevron, and the University of Houston, Ken encouraged many X Division staff members to attend.  He sees potential opportunities for participation. Ken says,"It was clear from this successful workshop that our Division can participate in the many industry needs for multi-phase flow, mechanic fracturing, well completion, seismic measurements, visualization, database integration, and MPP code implementation and optimization."<p>

	Technical consultants: Roger Jones and Kenneth Lee, X-1, 667-8979<p>


Picture Caption: Leaders of Industrial Partnership projects in Group X-1; l. to r. Robert Webster, Kevin  Buescher, Kenneth Lee, and Roger Jones.<p>



<h2>Related X-Windows Articles </h2>
<ul>
<li> <a href="http://www-xdiv.lanl.gov/XCM/neural/xwindows/citibank1.html"> Industrial Partnerships</a>
<li> <a href="http://www-xdiv.lanl.gov/XCM/neural/xwindows/xwindows.html"> Other Articles</a>

</ul>
  
  


<h2>Other Neural Network Links </h2>
<ul>
<li> Click <a href="http://www.emsl.pnl.gov:2080/docs/cie/neural/neural.homepage.html#Sec8">here.</a>

<li> <a href="http://www-xdiv.lanl.gov/XCM"> Simulation and Adaptive Computation Effort</a>
<li> <a href="http://www-xdiv.lanl.gov/XCM/neural/neural_announcements.html"> Neural Network Announcements at Los Alamos</a>
<li> <a href="http://www-xdiv.lanl.gov/XCM/neural/neural_theory.html"> Neural Network Theory at Los Alamos</a>
<li> <a href="http://xyz.lanl.gov/adap-org/"> Adaptation and Organization at the Center for Nonlinear Studies</a>
<li> <a href="http://www.lanl.gov/subject/adaptive-processing"> Neural Network Hardware at Los Alamos</a>

<li> <a href="http://www.eeb.ele.tue.nl/neural/neural.html">Eindhoven University of Technology</a>
<li> <a href="http://www.emsl.pnl.gov:2080/docs/cie/neural/neural.homepage.html"> Pacific Northwest Laboratory</a>
<li> <a href="http://www.cs.cmu.edu:8001/afs/cs/project/cnbc/CNBC.html"> Carnegie Mellon</a>
</ul>

<h2>Other Related Links </h2>
<ul>
<li> <a href="http://www-xdiv.lanl.gov/XCM/"> X-1 Home Page</a>

<li> <a href="http://cnls-www.lanl.gov"> Center for Nonlinear Studies</a>

<li> <a href="http://xyz.lanl.gov"> Center for Nonlinear Studies (xyz)</a>

<li> <a href="http://www.acl.lanl.gov/Home.html"> Advanced Computing Laboratory</a>

<li> <a href="http://www.acl.lanl.gov/sunrise/sunrise.html"> Sunrise Project</a>

<li> <a href="http://www.c3.lanl.gov:1331/c3/projects/Medical/main.html"> Medical Data Analysis</a>

<li> <a href="http://www.lanl.gov:8000/tech"> Technology Transfer</a>



<li> <a href="http://bang.lanl.gov/video/history/lanl50th/Homepage.html"> 50th Anniversary Articles</a>

<li> <a href="http://www-xdiv.lanl.gov/XCM/neural/government.html"> Government Links</a>

</ul>



<h2>Rosters of Groups Involved in Adaptive Computation </h2>
<ul>
<li> <a href="http://<ul>
<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=X-1&ms=&room=">X-1</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=C-3&ms=&room=">C-3</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=ESA-9&ms=&room=">ESA-9</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=CNLS&ms=&room=">CNLS</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=T-13&ms=&room=">T-13</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=NIS-2&ms=&room=">NIS-2</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=NIS-4&ms=&room=">NIS-4</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=TSA-DO&ms=&room=">TSA-DO</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=AT-8&ms=&room=">AT-8</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=P-4&ms=&room=">P-4</a>

<li> <a href="http://www.lanl.gov:52271/?znumber=&first=&last=&phone=&fax=&group=ADP-5&ms=&room=">ADP-5</a>
</ul>







<a href="http://www.lanl.gov">Los Alamos home page</a><p>
Contact:<a href="http://www.lanl.gov:52271/?-l+090472"> Roger D. Jones (rdj@lanl.gov)</a><p>












