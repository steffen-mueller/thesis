www.nist.gov/public_affairs/results/results.html
<!doctype HTML public "-//W30//DTD W3 HTML 2.0//EN">
<HTML>
<HEAD>
<!-- HTML version of the report "DELIVERING RESULTS: A Progress Report from the National Institute of Standards
     and Technology", first issued 17 May 1995. HTML version edited by
     Michael Baum. Last revision: 05/26/95 04:45 pm

     Part 1 : Table of Contents, Foreword, Executive Summary,
              NIST: Producing Results That Work for Industry
 -->
<TITLE>DELIVERING RESULTS: A Progress Report from the National Institute of Standards and Technology</TITLE>
<LINK REV="OWNER" HREF="baum@micf.nist.gov">
</HEAD>

<BODY>
<IMG SRC ="nistlogo.gif" ALT = "[NIST Logo]">
<HR SIZE=5>
<A Name= CONTENTS><H1>DELIVERING RESULTS:<P>A Progress Report from the National Institute of Standards and Technology</H1></A>
<HR SIZE=5>

<DL>
<DT><A HREF= "#FOREWORD"><H2>FOREWORD</H2></A>

<DT><A HREF= "#EXECSUM"><H2>EXECUTIVE SUMMARY</H2></A>

<DT><A HREF= "#INTRO"><H2>NIST: PRODUCING RESULTS THAT WORK FOR INDUSTRY</H2></A>

<DT><A HREF= "res-atp.html"><H2>ADVANCED TECHNOLOGY PROGRAM</H2></A>
<DD>ATP -- A unique partnership between government and private industry
to accelerate the development of high-risk, enabling technologies
identified by industry as promising significant commercial payoffs and
broad-based economic benefits.

<DT><A HREF= "res-mep.html"><H2>MANUFACTURING EXTENSION PARTNERSHIP</H2></A>
<DD>MEP -- An integrated nationwide network of locally managed
manufacturing extension centers dedicated to helping smaller
manufacturers improve their competitiveness by adopting modern
technologies.

<DT><A HREF = "res-lab.html"><H2>NIST LABORATORIES</H2></A>
<DD>Laboratories -- Research and services providing the infrastructural
technologies, such as measurements, standards, evaluated data, and test
methods, that U.S. industry needs to improve products and processes and
to gain access to export markets.

<DT><A HREF = "res-qual.html"><H2>BALDRIGE NATIONAL QUALITY PROGRAM</H2></A>
<DD>Quality Program -- Manages the Malcolm Baldrige National Quality
Award, which was developed with the cooperation and financial support of
U.S. industry and now is the U.S. standard of quality achievement.

<DT><A HREF = "res-appa.html"><H2>APPENDIX A: PARTICIPANTS IN ATP PROJECTS</H2></A>

<DT><A HREF = "res-appb.html"><H2>APPENDIX B: MEP CENTERS</H2></A>

<DT><A HREF = "res-appc.html"><H2>APPENDIX C: NIST LABORATORIES' CRADA PARTNERS</H2></A>

<DT><A HREF = "res-appd.html"><H2>APPENDIX D: MALCOLM BALDRIGE NATIONAL QUALITY AWARD WINNERS</H2></A>
</DL>
<P><A HREF = "res-cred.html"><H3><I>Credits</I></H3></A>
<HR SIZE=5>

<A Name=FOREWORD><H2>FOREWORD</H2></A>

<P>A process that slashes the cost of advanced materials by a factor of 10
... A partnership that helps a high-tech start up to develop novel
technology with potential to increase the information-carrying capacity
of telecommunications networks ... Timely technical assistance that
enables an automotive parts supplier to reduce scrap and improve its
on-time delivery rate ... A quality management tool that a small
business credits with helping it increase sales by 35 percent in 1994.

<P>These examples of real benefits from the programs of the National
Institute of Standards and Technology (NIST) represent the kinds of
initial returns that federal taxpayers will earn on the dollars they are
investing in the agency this year. Allocating funds among its four
industry-driven programs, NIST aims to deliver the biggest economic bang
for the buck out of the entire $70 billion of the federal R&D budget.

<P>Ultimately, returns will be realized in new jobs and companies,
productivity growth, increases in sales, new markets, gains in market
share, and other economic benefits that, together, are the reasons for
NIST's existence. These are the kinds of impacts expected from an agency
that has the straightforward and unique mission of promoting economic
growth by providing part of the basic technical infrastructure needed by
U.S. industry.

<P>To satisfy taxpayers' expectations of economic benefits, NIST first must
deliver useful results to its primary customers: U.S. companies. After
all, U.S. industry, not government, drives economic growth by
transforming technology into the products and services that generate
jobs and profits -- returns that improve the nation's standard of
living. In tackling with industry key tasks that companies cannot
accomplish on their own, NIST provides timely, indispensable support
that companies themselves fashion into competitive advantages -- new or
more reliable processes, innovative products and services, new R&D
capabilities, shorter product-development cycles, and improvements in
quality.

<P>NIST and its parent organization, the Commerce Department's Technology
Administration, know that by being a strong partner to U.S. industry, we
can leverage the taxpayers' investment and make the most of NIST's one
percent share of federal R&D expenditures. To be sure, many factors
determine a nation's industrial competitiveness, from the business
climate and international trade practices to the skill levels of the
workforce. But of all these factors, technology is the most dynamic --
and volatile. Each new technology creates an opportunity to change the
terms of competition. By helping industry surmount technical risks and
obstacles as it pioneers, develops, and implements new technologies,
NIST improves the prospects that our nation will be both the source and
beneficiary of technological opportunities.

<P>This report describes how NIST delivers results that translate into
competitive advantages for individual companies and entire industrial
sectors -- and, ultimately, into benefits for the U.S. economy. It
relates representative examples of realized and anticipated returns on
activities in each NIST program. For our newer and longer-term efforts,
early-stage indicators of progress toward economic benefits are
presented. his document follows our earlier report that described how
U.S. industry's priorities drive the content of NIST's programs and how
the agency evaluates its efforts.

We welcome your ideas on how we can deliver even greater value to our
customers and even better returns to our investors.

<P><IMG SRC="prabhakar_sig.gif"><BR>
<B>Arati Prabhakar<BR>
Director, NIST</B>

<P><IMG SRC="good_sig.gif"><BR>
<B>Mary Good<BR>
Under Secretary for Technology, DOC</B>
<HR SIZE=5>

<A Name=EXECSUM><H2>EXECUTIVE SUMMARY</H2></A>
<BLOCKQUOTE><I>No more essential aid could be given to manufacturing,
commerce, the makers of scientific apparatus, the scientific work of
Government, of schools, colleges, and universities than by the
establishment of the institution proposed in this bill.
<P>H.R. 1452, 56th Congress, 1st Session, May 14, 1900</I></BLOCKQUOTE>

<P>Nearly a century after its founding, the National Institute of Standards
and Technology continues to fulfill the terms of its creation: Providing
science and technical support fundamental to the success of U.S.
industry and of the entire technical community. Over the last several
years, however, changing, ever more competitive global economic
conditions and the ceaseless advance of technology have added
significantly to the importance of the agency's activities, and NIST's
programs have evolved in response.

<P>The agency's unique core mission -- promoting U.S. economic growth by
working with industry to develop technology, measurements, and standards
-- marks a clear path for all of its programs. NIST focuses on jobs
vital to the technology infrastructure. With partners in industry,
government, and academia, it concentrates on developing the tools and
bedrock capabilities essential to the nation's scientific and
technological competitiveness -- a cornerstone of the country's economic
future. These are technology jobs that neither government nor industry
can accomplish separately. Companies do not undertake these tasks on
their own because they cannot capture the broad-based benefits that will
accrue to the larger ecnomy or because the time frame is too long and
the risks are too great. While industry's priorities guide NIST's work,
the agency makes certain that its efforts will yield results that
constitute necessary -- and otherwise unattainable -- contributions to
the nation's technology infrastructure. These efforts enable, extend, or
complement private-sector efforts. They do not replace or substitute for
work best done by industry.

<P>This report is an update on how NIST is carrying out its mission.
Examples are illustrative, intended to allow readers to assess the
roles, objectives, and progress of the agency's four major programs.
Striving to achieve the maximum return on taxpayer dollars, NIST
recognizes that there is always room -- and opportunity -- for
improvement and welcomes suggestions.

<H3>Advanced Technology Program</H3>

<P>A competitive, cost-shared program, the Advanced Technology Program
provides a mechanism for extending U.S. industry's technological reach
and pushing the envelope of what can be accomplished in today's fiercely
competitive global marketplace. For U.S. citizens, the real payoff --
and the young program's primary objective -- is the economic growth
fueled by the introduction of future products and industrial processes
based on ATP-sponsored research. The ATP invests only in pre-product
technology development, requiring companies to invest additional
resources and effort in refining and commercializing the technology.

<P>Because the program is only a few years old, full economic impacts will
not be realized for several years. Already, however, there is
encouraging evidence that the ATP is fostering research efforts with the
potential to deliver a sizable return on the federal investment. For
example, the ATP is:

<UL>
<LI><B>Fostering important technology developments.</B> Examples include new
processes leading to higher-quality, lower-cost optoelectronic devices;
a 50-percent reduction in the size of non-volatile memory chips; and a
prototype technology with the potential to protect the nation's supply
of blood and blood products from viruses.

<LI><B>Accelerating development of new technical capabilities.</B> Case studies of
three joint ventures indicate that ATP awards enabled research to be
performed more quickly and aggressively; a survey of 47 firms working on
ATP projects indicates significant reductions in time to market: Of
their 103 potential technology applications, 83 percent were estimated
to be more than a year ahead in their anticipated progression to market.

<LI><B>Promoting industrial alliances.</B> Among companies working on projects
awarded between 1990 and 1993, 24 firms (60 percent of those surveyed to
date) said they are collaborating with partners with whom they had never
done business before -- many with two or more new partners and one with 12.

<LI><B>Creating opportunities for companies of all sizes.</B> To date, half of
the ATP awards to single companies have gone to small firms, and small
firms are participating in 70 percent of the joint ventures receiving
ATP matching funds. Universities and non-profit, independent research
organizations also are participants in many ATP projects.

<LI><B>Opening the way to job creation.</B> Project-tracking data from 32 small
firms indicate that more than 90 percent expect to add new employees
within five years as a result of ATP technologies. Several ATP
participants already have grown as a result of their projects.
</UL>

<P>In 1994, the ATP initiated a new, focused-program strategy. Focused
programs concentrate on specific, industry-identified technical and
business goals achievable through interdependent R&D projects carried
out over several years. The approach creates a critical mass of
collaboration, encouraging industrial cooperation to overcome
early-stage technical obstacles common to prospective competitors in a
young, but commercially promising technology area. Eleven focused
programs have been established to date.

<P><A HREF="atpaward.jpg"><IMG SRC="atpaward.gif" alt="Click here for JPEG image of chart." ALIGN=TOP></A>
<B> ATP HAS A DIVERSE PORTFOLIO</B><BR>
177 ATP Awards by technology area as a percentage of the $556 million awarded

<P>The ATP portfolio is highly diversified. The 177 projects selected in
the first 10 competitions span a broad array of critical technologies.
More than 400 organizations, including companies, univer-sities,
independent non-profit research organizations, and government
laboratories, have participated directly in these projects. Several
hundred additional organizations have participated as subcontractors and
strategic partners. This effort is managed by an ATP staff of about 60,
who carry out a vigorous outreach program to make firms and economic
development organizations in states and localities across the country
more aware of the ATP, its potential, and its procedures.

<H3>Manufacturing Extension Partnership</H3>

<P>Begun in 1989 with the establishment of three extension centers, the MEP
is now making the transition from a modest pilot program to a nationwide
network leveraged to achieve substantial impact. By May 1995, 42 MEP
centers were operating (with one additional center preparing to open)
and sending engineers and other specialists with manufacturing or
business experience into the field to work with firms. When completed,
the 100-center network will put hard-to-find technical assistance within
reach of all of the nation's 381,000 small and medium-sized
manufacturing establishments.

<P>Many of these firms continue to use decades-old technologies and
manufacturing methods. Not surprisingly, the productivity gap between
smaller manufacturers and large U.S. producers has been widening. One
consequence is that the nation's large manufacturers look increasingly
to off-shore suppliers. Early evaluations suggest that by making
technical assistance accessible to smaller manufacturers in all regions
of the country, the MEP will deliver sizable benefits, reaped on scales
ranging from individual factories to the national economy. Results of
surveys of a subset of smaller manufacturers that received technical
assistance from MEP centers in 1994 are illustrative and consistent with
previous surveys:

<UL><LI><B>Benefits anticipated by the 610 firms responding to the MEP
centers' surveys totaled $167 million,</B> the cumulative result of sales
increases and cost savings attributed to actions undertaken with
technical assistance from MEP centers.
<LI><B>Anticipated benefits translate into a conservatively estimated
economic benefit of $8 on each $1</B> that the federal government invested
in the MEP.
</UL>

<P>Personnel from 28 MEP centers that operated during at least part of
1994 provided services or made initial visits to more than 30,000
com-panies during the year. The centers offered training and education
to employees and managers from more than 5,000 manufacturing firms. And
the MEP and its affiliated centers built partnerships with hundreds of
organizations, increasing the breadth and depth of capabilities of
individual centers and the entire network.

<UL><LI>The average MEP center has developed working relationships with
20 organizations; by mid-1994, 34 centers had links to nearly 700
partner organizations.

<LI>About half of the centers have ties to industry associations, and
about two-thirds have established relationships with private-sector
consultants.
</UL>

<P>Supported with federal dollars matched by states, all MEP centers are
chosen and funded through a rigorous, merit-based competition. The MEP
builds on the foundation of existing state and local industrial
extension services. It focuses on needed services that the private
sector either does not provide or cannot deliver economically to smaller
manufacturers. Emphasis is on grassroots delivery of service,
facilitated by a small NIST staff of fewer than 60 people.

<P><A HREF = "mepmap1.jpg"><IMG SRC ="mepmap1.gif" ALT = "Click here for JPEG image." ALIGN=BOTTOM></A>
<B>PROGRESS TOWARD BUILDING A NATIONWIDE MANUFACTURING EXTENSION NETWORK.</B>

<H3>NIST Laboratories</H3>

<P>The ultimate U.S. reference point for measurements with counterpart
organizations throughout the world, the NIST laboratori provide
companies, entire industries, and the whole science and technology
community with the equivalent of a common language needed in nearly
every stage of technical activity. In furthering the technical aims and
capabilities of U.S. industry, the NIST laboratory program serves as an
impartial source of expertise, developing highly leveraged measurement
capabilities and other infrastructural technologies that are:

<UL><LI><B>beyond the reach of individual companies</B>;

<LI><B>needed widely by industry</B>, as determined in assessments of
industrial priorities; and

<LI><B>likely to have high economic impact</B> if provided.
</UL>

<P>Studies of the economic impact of specific NIST laboratory services
and research projects indicate that significant benefits flow back to
U.S. society and the economy. In the eight economic impact studies
completed to date, the <B>median aggregate -- or "spillover" -- rate of
return ranges from 63 percent to 428 percent.</B> These rates compare
favorably with those reported in studies of returns on other public
investments in technology and on private-sector R&D investments.

<P><A HREF = "labecon.jpg"><IMG SRC ="labecon.gif" ALT = "Click here for JPEG Image of chart." ALIGN=BOTTOM></A>
<B>AGGREGATE ECONOMIC RATES OF RETURN FROM TECHNOLOGY INVESTMENT</B><BR>
NIST laboratories' R&D and services have yielded high returns.

<P>Large and small firms tap the laboratories' technical expertise in
many ways. For example, in 1994, the laboratories:

<UL><LI>Sold nearly 21,000 Standard Reference Materials to 2,348 U.S.
firms, many of which, in turn, provided high-accuracy, high-precision
services and products to other businesses and to consumers.

<LI>With its distributors, sold more than 6,000 Standard Reference Data
sets -- organized collec- tions of thoroughly evaluated data -- to
businesses and government and academic organizations.

<LI>Provided calibration services to nearly 750 U.S. businesses,
assuring that their measurement-intensive processes and products are
linked directly to national and international standards.

<LI>Accredited (or renewed the accreditation of) 850 private- and
public-sector testing and measurement laboratories as meeting the
requirements of national and international standards bodies.

<LI>Participated in and provided technical support to more than 800
national and inte rnational standards committees.

<LI>Hosted 197 guest researchers from companies and professional and
trade associations and an additional 717 researchers from universities
and other U.S. organizations.

<LI>Entered into 133 new Cooperative Research and Development Agreements
(CRADAs). Of NIST's more than 500 CRADA partners since 1988, 40 percent
are small businesses, 45 percent are large or mid-sized firms, and 15
percent are universities and other organizations.

<LI>Initiated three new NIST-sponsored industrial consortia, increasing
the NIST total to 13.
</UL>

<P>The NIST laboratories plan and carry out their research in
collaboration with industry. As a result, modest federal investments are
yielding critically needed measurement methods and other infrastructural
technologies that open the way to advances in research, improvements in
processes and products, efficiencies in the marketplace, and other
benefits reaped by companies and industries, and, through them, the
economy.

<H3>Baldrige National Quality Program</H3>
<P>A commitment to quality is no longer an option for American business.
It has become a necessity for doing business in today's competitive,
customer-oriented world market. Many in the manufacturing and service
sectors believe the Malcolm Baldrige National Quality Award has had a
tremendous influence in making quality part of America's corporate
culture because the award criteria capture better than anything else
what organizations need to do to improve the way they do business.

<P>The award's stature and industry's rapid adoption of the award
criteria as a guide to quality improvement attest to the strong success
of the team effort between industry and government. From 1988 to 1994,
the award program has received 546 applications from U.S. companies.
Twenty-two companies, including 11 large manufacturers, five service
companies, and six small businesses -- in a wide variety of industries
-- have won the award. Award-winning companies have taken seriously the
charge to be quality advocates, educating others on the benefits of
using the Baldrige framework and criteria. To date, the winners have
given over 15,000 presentations, reaching hundreds of thousands of
organizations.

<P><A HREF = "qual.jpg"><IMG SRC ="qual.gif" ALT = "Click here for JPEG image of chart." ALIGN=BOTTOM></A>
<B>QUALITY AWARENESS AIDED BY BROAD DISTRIBUTION OF QUALITY GUIDELINES</B>

<P>Other examples of the Baldrige Award's role in raising quality
awareness:

<UL><LI><B>Almost 1 million copies of the award criteria have been
distributed since 1988.</B> A comparable number are estimated to have been
reprinted and distributed.

<LI><B>Thousands of organizations use the criteria as a quality improvement
"road map."</B> For example, SEMATECH, a consortium of American
semiconductor manufacturers, launched a supplier partnership program
based on the award criteria, and the American Electronics Association
created a new quality steering committee and a three-year implementation
guide based on the Baldrige criteria.

<LI><B>The award is widely emulated, cultivating additional interest in
quality improvement.</B> Now, 42 state and local quality award programs are
in operation in 30 states, as compared with fewer than 10 states in
1991. Most of the new awards are modeled after the Baldrige Award. More
than 400 U.S. companies and organizations applied for these awards in
1994. Many companies plan to participate in these award programs before
applying for the national award.
</UL>

<P>Moreover, a recent report by The Conference Board, a global business
membership organization, says, "A majority of large U.S. firms have used
the criteria of the Malcolm Baldrige National Quality Award for
self-improvement, and the evidence suggests a long-term link between use
of the Baldrige criteria and improved business performance."

<P><A HREF= "#CONTENTS"><IMG SRC= "back.gif">Back to table of contents.</A>

<HR>
<A NAME= INTRO><H2>NIST: PRODUCING RESULTS THAT WORK FOR INDUSTRY</H2></A>

<P>The National Institute of Standards and Technology promotes economic
growth by working with industry to develop and apply technology,
measurements, and standards. Unique among federal agencies, NIST's
focused mission has its roots in the agency's 94-year history of working
with U.S. companies on measurements and standards. Since 1988, this
productive tradition of partnership between government and industry has
branched out into new domains. In all our efforts with industry, we
focus on areas beyond the capabilities of either partner alone but vital
to both.

<P>NIST concentrates on building the basic technical infrastructure
essential to U.S. industry and commerce. Working with U.S. companies, it
provides capabilities that industry needs to develop technology and
transform it into successful products, services, and processes that
ultimately drive national economic growth. By agency design and by the
very nature of infrastructural and leapfrog technologies, NIST's
programs generate benefits that "spill over" to many science and
technology pursuits and to many parts of industry and the economy.

<P>To make the most of the federal investment addressing civilian
technology needs, NIST:

<UL><LI>focuses on tasks that neither industry nor government can
accomplish separately but are vital to the nation's technical
infrastructure;

<LI>sets its priorities in consultation with its customers, ensuring
that the Institute's programs are aligned with U.S. industry's current
and anticipated needs;

<LI>applies rigorous, open, and competitive processes to steer all NIST
programs; and

<LI>regularly evaluates its programs to ensure that they yield high
returns on U.S. taxpayers' dollars.
</UL>

<P>NIST's direct connection to industry distinguishes its role in federal
research and development. Long focused on basic research and the
missions of national security, space, health, and energy, federal R&D
has yielded spin-off benefits to the economy. Steps toward realizing
those derivative benefits, however, often have been slow and
inefficient. Today's fierce competitive environment necessitates
entirely new ways of doing business -- more effective approaches to
capturing the economic benefits of new technology.

<P>With the growing complexity and economic significance of technology,
NIST's programs have become increasingly important ingredients in the
changing mix of resources, capabilities, and approaches that U.S.
industry needs to create and realize technological opportunities --
opportunities that lead to economic growth.

<H2>NIST's Industrial Customers</H2>

<P>NIST's clientele includes businesses and industries of nearly every
variety. Customers range from large companies to small ones and from
established firms to start-ups. Some partner firms come from the steel
industry and other sectors that arose during the last industrial
revolution. Others come from new manufacturing and service sectors
spawned by the revolutionary technologies emerging today. Services are
delivered to parts suppliers as well as to original equipment
manufacturers, to niche suppliers of specialty materials as well as to
commodity producers, and to makers of advanced production and
information technologies as well as to users trying to achieve the
tools' performance advantages.

<P>NIST's customized programs help companies accomplish technology and
quality objectives more efficiently -- and more rapidly -- than they
could on their own, if at all. Firms and entire industries can -- and do
-- benefit from an array of NIST activities and services. Among the top
50 U.S. firms in terms of R&D spending, for example, 90 percent
participated in or used the services of at least one NIST program or
activity in 1994. More than 75 percent used two or more, and more than
half used three or more.

<P>In addition to being the principal customers of NIST's Manufacturing
Extension Partnership, small businesses also make heavy use of the
Institute's other programs. Small and medium-sized businesses accounted
for nearly three-fourths of the 2,348 industrial customers who purchased
NIST Standard Reference Materials (SRMs) in 1994, and they were involved
in more than one-third of the Cooperative Research and Development
Agreements (CRADAs) with the Institute's laboratories. In the ATP, about
half of the awards have gone to small companies or to joint ventures led
by small companies.

<P>In 1994, about 400 organizations were pursuing leapfrog technologies
in cost-shared projects sponsored by the NIST Advanced Technology
Program; several hundred more were participating as contractors and
strategic partners. About 7,000 U.S. companies and trade and
professional organizations directly used the services of the NIST
laboratories or were engaged in collaborative work with agency
researchers. Some 30,000 companies tapped the technical services and
resources of the NIST Manufacturing Extension Partnership. In addition,
several thousand industry representatives attended more than 120
planning, priority-setting, or technical conferences and workshops
sponsored by NIST last year. With U.S. industry or on its behalf, NIST
specialists served on 800 national or international standards
committees, and they stepped up efforts to eliminate non-tariff barriers
to U.S. exports in several parts of the world.

<P>The breadth and strategic and practical importance of NIST's
assistance to U.S. industry are illustrated by examples from two of
the many industrial sectors --
<P><A HREF = "auto.html">automobiles<IMG SRC ="automobile.gif"></A>
<P>and <A HREF = "computer.html">electronics <IMG SRC ="computer.gif" ALIGN=TOP></A>
<P>(as represented by a personal computer). Planned and carried out with
the active involvement of these industries, including their diverse
subsectors, NIST's support helps companies work more efficiently and
productively as they develop, manufacture, market, and support their
products.

<H2>The NIST Technology Portfolio</H2>

<P>NIST's four major programs are designed to help U.S. companies
achieve their own success, each one providing appropriate assistance or
incentives to overcoming obstacles that can undermine industrial
competitiveness.

<P><B>Advanced Technology Program (ATP)</B> -- provides cost-shared
awards to companies and company-led consortia for competitively selected
projects to develop high-risk, enabling technologies during the
pre-product phases of research and development.

<P><B>Manufacturing Extension Partnership (MEP)</B> -- a growing
nationwide network of extension centers, co-funded by state and local
governments, that provides small and medium-sized manufacturers access
to technical assistance as they upgrade their operations to boost
performance and competitiveness.

<P><B>Laboratory Research and Services</B> -- develops and delivers
measurement techniques, test methods, standards, and other types of
infrastructural technologies and services that provide a common language
needed by industry in all stages of commerce -- research, development,
production, and marketing.

<P><B>Baldrige National Quality Program</B> -- with industry, manages
the Malcolm Baldrige National Quality Award, which may be awarded
annually to companies in the categories of small business,
manufacturing, and service and provides U.S. industry with comprehensive
-- and extensively used -- guides to quality improvement.

<H3>Tracking Industry and Taxpayer Benefits</H3>

<P>The federal investment in NIST yields multiple returns. Some can be
tabulated in terms of jobs created or saved, productivity growth,
increases in sales and market share, and other economic or "bottom line"
impacts. Ultimately these impacts are the reason for NIST's existence.
Indeed, all four programs track the economic benefits derived from their
work with U.S. industry.

<P>Commissioning its first economic impact study 15 years ago, NIST long
has focused on methodologies for assessing the broad economic benefits
attributable to technical activities. NIST investments pay off on
different timescales. Since the ATP funds research that precedes the
product-development stage, even the handful of projects that have been
completed require substantial additional privately funded work before
the technologies under development can deliver economic impacts. For
firms that have used the services of the Manufacturing Extension
Partnership and the Baldrige National Quality Program, returns are
quicker to materialize. Because the NIST laboratories respond both to
immediate and anticipated infrastructural technology needs, payback
horizons span from short to long term.

<P>Many NIST customers already are realizing tangible benefits and
competitive advantages. Customer-realized returns are translating into
benefits at several levels -- from the economies of client companies'
states and communities to these firms' customers and suppliers.

<P>NIST's evaluations and industry feedback reveal that the agency's
programs are generating a variety of important benefits. Ranging from
acceler-ated progress in company laboratories and less duplicative R&D
within industry, to improved product quality, to industry agreement on
standards, these impacts often are not readily quantifiable. Yet, such
benefits clearly contribute to the nation's industrial competitiveness,
a crucial component of its economic health and the welfare of U.S.
citizens.

<P>The following sections summarize some of the results and impacts
achieved by NIST's four programs over the last few years. They draw on
information gathered in formal economic impact studies, surveys,
progress reports, and anecdotal accounts from industry.

<P>This report describes the goals of each program, the types of results
and impacts they aim for, and the kinds of returns generated by the
taxpayers' investment in NIST.

<P><A HREF= "#CONTENTS"><IMG SRC= "back.gif">Back to table of contents.</A>

</BODY>
</HTML>


