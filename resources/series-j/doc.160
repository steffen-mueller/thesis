www.ent.ohiou.edu/~dkoonce/papers/cie.html
<html><head><!-- This document was created from RTF source by rtftohtml version
2.7.5 --><title>Information Model Level Integration for CIM
Systems:</title></head><p>
<BODY bgcolor=#ffffff text=#gggggg> <p>
<p>
<H2 ALIGN=center>Information Model Level Integration for CIM Systems:
A Unified Database Approach to Concurrent Engineering</H2>
<b></b><p>
David A. Koonce<p>
<p>
Department of Industrial and Systems Engineering<p>
Ohio University, Athens, OH 45701<p>
<p>
<p>
<p>
<i>	In traditional manufacturing systems engineering, systems are designed
around the transformation process of raw materials into usable components or
materials.  These systems are value added processes, such as materials
processing, or support systems such as scheduling.  In the last thirty years,
information has become an important resource to manage in the manufacturing
environment.  Unfortunately, information is poorly managed in many
manufacturing systems.  Highly automated computer systems that are not well
interfaced are often referred to as "islands of automation."  They increase the
efficiency in their departments without aiding activities outside of their
areas.  This lack of interfacing can often be traced to a lack of understanding
of the mappings between data in the individual systems.  A new methodology for
expressing these mappings will aid designers in developing the interfaces
between these island systems.</i><p>
<i></i><p>
<b>Introduction</b><p>
<b></b><p>
	Computer Integrated Manufacturing (CIM) is an ideal state in which computer
based manufacturing applications communicate information to coordinate design,
planning and manufacturing processes.  Traditional approaches to integration
focus on either developing translations between two systems or a single data
file that acts as a database for all integrated tools.  What is needed,
however, is an integration architecture which supports each application's local
data requirements.  To understand each system's data requirements, we need to
model them using an information model.<p>
	Many methods exist for modeling data in relational and object-oriented
computer systems.  These methods use graphics to represent the things
(entities) in the database and how these things are related (relationships).
Researchers have proposed extensions to relational information modeling methods
for manufacturing (IDEF1.X and CIM-OSA).   These, however, are top-down models
that address the information in a facility as a whole.  Data in a CIM
application is an abstraction of the application's reality, i.e., it represents
the world as an application sees it.  By developing a top-down analysis of the
entire CIM system, we neglect the individual application's view of the
environment.  What is needed is an approach to modeling the CIM system which
requires a bottom-up analysis of the individual applications.  This paper
presents a new approach for integrating entities in individual applications.<p>
<p>
<b>Background</b><p>
<b></b><p>
There are several methods for integrating information systems.  One approach is
to develop translators that coerce data from one format into another.  Mappings
between the two representation schemes allow entities in one data file to be
transformed into entities for the receiving data file.  The drawback to this
method is its serial nature.  Once the data is translated, the initial system
cannot access the data in the second system's model.  Another approach is to
develop a neutral storage format.  All data used in the integrated system must
reside in the common data storage structure.  While this allows multiple
systems to access the data necessary for concurrent engineering, the common
format must be robust and allow for data necessary for all tools.  Also, issues
of concurrent access must be addressed if multiple users are to access the
data.<p>
	Between the two approaches, there exists a compromise that allows individual
tools to manage their data locally, yet allows these tools to share their data
with other tools in the system.  In this integrated data model approach, a
mapping between the local tools' data models and an enterprise data model
allows tools to access data from other tools.  This new enterprise data model
must capture the entire range of information available in all of the
applications in the system.  To develop this model, a comprehensive study of
the information tools and the organization must take place.
<h2>
<a name="RTFToC1">Information Modeling
</a></h2>
<p>
	In developing an information model, we seek to represent the types of
information that will be represented in the database of our information system.
The model itself attempts to represent the real-

world
information state.  The information in the model is classified as one of two
types: entities or the relationships between these entities.<p>
	Entities are the things about which we wish to record data.  There are six
primary classes of entities: people, things (objects), places, organizations,
events and concepts (Fertuck 1995).  It is these things about which we will
record information in our tables.  In a traditional entity relationship
diagram, we use blocks to represent entities.<p>
	Entities are not enough to express our database.  Relationships are the links
between entities.  We draw lines in our diagrams to represent these links.
Where these lines intersect the boxes (entities), we express attributes of the
relationship.  Depending on the notation scheme, we use some designator to
signify if the cardinality is 0, 1 or unlimited.  Figure 1 shows a simple entity-relationship
(ER) diagram in which an unspecified number of components are required for a
final assembly.  In this diagram, we express that we will have a table for
components, a table for finished assemblies and a table that lists which
components are in each assembly.<p>
	These standard boxes and links have been extended to include generalization
and aggregation.  Generalization allows us to define different types of
finished assemblies that have different attributes that describe them.
Aggregation allows us to decompose entities.  For example, a finished assembly
may have a product, a shipping container and a manual.  We can also use
advanced notation schemes to represent ternary and n-

ary
relationships<p>
<p>
<CENTER><IMG SRC="cie1.gif"><p>
Figure 1.  Simple ER diagram<p></CENTER>
<p>
	The limitations of the ER modeling scheme in modeling data for object-

oriented
data systems led to the extension of ER techniques for object-

oriented
data modeling (Rumbaugh, 1992 and 1994).  With this approach, entities are
objects that belong to classes.  They inherit both attributes and operations.<p>
	An ER extension for manufacturing information is IDEF1.X, a methodology
developed by the U.S. Air Force's Integrated Computer Aided Manufacturing
program.  IDEF1.X proposes to support integration in CIM environments through
development of a single semantic definition of the data in a system (FIPS 184,
1993).  This model is expressed using extensions to the ER model.  The IDEF1.X
approach requires a formal development process that eliminates N:M
cardinalities and n-ary relationships.  This produces a data model that can be
transformed into a normalized set of relations that an integrated information
system can utilize.  Figure 2 shows how the ER diagram in Figure 1 would be
represented in IDEF1.X.<p>
<p>
<CENTER><IMG SRC="cie2.gif"><p>
Figure 2.  Simple IDEF1.X diagram<p></CENTER>
<p>
	IDEF1.X is not the only IDEF extension of information modeling for CIM.  Wang
et al. (1993) use an extension of IDEF0 function modeling to model the
relational information flows in a CIM environment.  However, many information
systems in a CIM environment will not use relational databases.  IDEF4 is an
object-oriented design (OOD) methodology that allows for objects, their
structures (relations) and their behaviors (Mayer et al., 1992).  IDEF4
however, is a modeling scheme for implementing full object oriented information
systems.  What is needed is a modeling approach that accounts for non-relational
structures and can represent the relationships between these entities and
entities in other relational or non-relational
systems.<p>
<p>
<b>CIM Information Modeling</b><p>
<b></b><p>
	Information in a CIM environment exists in disparate formats and may represent
system views that are not compatible in a single ER diagram.  As noted above,
IDEF1.X constrains the information model to ER modeling constructs.  Other
efforts however, have focused on the diversity of information in a CIM
environment.  CIM-OSA, CIM Open Systems Architecture (Jorysz and Vernadat,
1990a and 1990b) and (Klittich 1990) is an approach to CIM modeling and
integration from four views: functional, information, resource and
organization.  Information modeling views in CIM-OSA use an Object-Entity
Relationship Attribute (OERA) approach (Jorysz and Verndat 1990b).  In OERA, an
object-oriented high level model, focusing on semantic unification of the model, drives the
decomposition to a more traditional ER model.  The ER model is extended to
handle static and dynamic properties of the data, integrity constraints and
database transactions (logical units of work) for consistency.  Sriram et al.
(1992) discuss a transaction method for integrated CIM databases.<p>
	Individual ad hoc integration between relational and non-relational
systems can be implemented, such as Koonce and Parks' (1994) semantic network
for integrating PERT and relational resource data.  Full relational integration
has also been presented.  Rangan and Fulton (1991) address the multitude of
information in a CIM environment by developing an Engineering and Manufacturing
Technical Relational Information System (EMTRIS).  In EMTRIS, they develop a
complete ER model for a facility.  This model then drives the integration of
the relational data systems.<p>
<p>
<b>CIM Information Systems</b><p>
<p>
	What makes a CIM application different from traditional database or
transaction processing applications?  As in a traditional database environment,
a CIM environment involves many tools which may be modeling the same entity.
What differs is in the viewpoints of these models.  Parks <i>et al.</i> (1994)
have proposed three criteria that can be used to classify a CIM data model or
tool: life cycle, domain and level of abstraction.  Within these categories,
data integration or translation may be well-defined. But, when represented entities 
cross more than one category, translation may
not be easily defined.  Figure 3 shows the independence of these criteria.  The
following sections detail what is meant by each of these categories.<p>
<p>
<CENTER><IMG SRC="cie3.gif"><p>
Figure 3. Axes of categorization<p></CENTER>
<p>
<b>Life Cycle</b><p>
<b></b>	In the development of a product, marketing specifications may form the
initial data model for the product.  As the specifications are refined, CAD
design files for components are developed.  Entities in these design files
represent features that satisfy marketing specifications.  These features also
constrain planning models which must also coordinate with marketing demands.
MRPII utilizes production plans and process plans, both affected by the
marketing model and CAD model.  Maintenance and engineering changes all factor
into the product's data model during the life cycle of the product.  These life
cycles are all part of the overall facility's data model.<p>
<p>
<b>Domain</b><p>
	The operation of complex manufacturing systems involves the cooperation of
designers and engineers from many disciplines.  Specialists in each of these
disciplines must cooperate on the total system.  Information about
requirements, costs, materials, schedules and constraints must flow freely
between the different engineers and managers.  However, each of these
cooperating disciplines has developed different methods and modeling techniques
to help them analyze the system from their own perspective.  From the previous
example, the relational data from marketing will differ greatly from the 3-D
part models developed in design.  The NC tool paths from the mechanical
engineers will be unusable to the service staff conducting field maintenance.
Representing these entities extends beyond the aliases defined in ER modeling.
The representation of the instances of an entity may be fundamentally different
between databases.<p>
<p>
<b>Abstraction</b><p>
<b></b>	Within or across domains, an entity in one model may be an abstraction
of several entities in another model.  A detailed production plan from an MRPII
system represents the aggregated production in a master production schedule.
ER modeling handles the direct level of abstraction well.  However, when other
categories are crossed, the level of abstraction can add complexity to the
modeling task.<p>
<p>
<b>Methodology</b><p>
<b></b><p>
	To integrate models from different domains, levels of abstraction and points
in the life cycle, we need to discuss how data can be represented in a
manufacturing information system.  In the ANSI SPARC architecture for database
systems, there are three levels of abstraction at which data models exist.
Closest to the user is the external level, which represents the data
relationships in a format which is most usable to the application tool.  Below
that is the conceptual schema which seeks to represent the real world
relationships and entities that the data itself represents.  And lastly, the
internal schema represents the data in a format that best suits the internal
storage structures utilized by the tool's file manager.  Figure 4 displays the
traditional three-level
architecture for a given tool.  Between each layer are the mappings used by the
data server to translate data for successive layers.<p>
	If all tools in the CIM system can be mapped into this three-level
architecture, the individual conceptual schema will represent the tool's view
of the CIM system.  However to integrate these tools, an enterprise level model
of the system is necessary.  These conceptual schemata exist as views of the
enterprise that fit along the axes of Figure 3.  A method of representing the
relationships between entities along and across axes is at the heart of this
problem.<p>
<p>
<CENTER><IMG SRC="cie4.gif"><p>
Figure 4. The ANSI SPARC architecture<p></CENTER>
<p>
	Along the life-cycle axis, entities should map one-to-one.
 That is, an entity of a part design should map directly to the part
classification in the maintenance database.  Along the abstraction axis,
aggregation and generalization decomposition in extended ER modeling should
represent most of these relationships.  And along the domain axis, aliases
could represent identical entities in some cases.  It is the trans-axis
linkages that will need more sophisticated modeling methods.  Some
relationships may have to be expressed as procedural translations in which data
must be transformed before it can be used by subsequent systems.<p>
	With all entities and relationships established, an enterprise-wide
conceptual schema can be developed.  Figure 6 displays how all of the
individual conceptual schema are mapped into an enterprise model.<p>
	The ANSI SPARC architecture allows for multiple external schema.  The multiple
schema support the data needs of different tools and users (Date, 1995).
However, the ANSI SPARC architecture does not support multiple internal schema.
Many integration methods (e.g., IGES and PDES STEP) eliminate this by
specifying a common data structure.  By utilizing techniques of distributed
databases and adhering to their autonomy rules, the enterprise-wide
conceptual schema can serve as the core of a distributed CIM information
system.<p>
<p>
<CENTER><IMG SRC="cie5.gif"><p>
Figure 5. Enterprise level conceptual schema<p></CENTER>
<p>
<b>Conclusion</b><p>
<b></b><p>
	Currently, manufacturing information systems either exist as islands of
automation or exchange information through standardized files.  Those that are
integrated require the use of a centralized database that constrains the local
applications.  What is needed is an architecture that supports independent CIM
applications as a distributed database system supports individual databases.
But, before we can develop this ambitious architecture, we must develop a
conceptual schema of the system these applications model.  Developing this
model is consequently the first step in providing a fully integrated CIM
system.<p>
<p>
<b>Bibliography</b><p>
<b></b>Date, C.J., <u>An Introduction to Database Systems, Sixth edition</u>,
Addision Wesley Publishing Co., 1995.<p>
Fertuck, L., <u>Systems Analysis and Design With Modern Tools, Second
edition</u>, Wm. C. Brown Communications, Inc., 1995.<p>
FIPS 184, <u>Integrated Definition for Information Modeling (IDEF1.X)</u>,
Federal Information Processing Standards Publication 184, Department of
Commerce, 1993.<p>
Jorysz, H.R. and Vernadat, F.B., CIM-OSA Part 1: total enterprise modeling and
functional view, <u>International Journal of Computer Integrated
Manufacturing</u>, Vol. 3, Nos. 3-4, 1990a.<p>
Jorysz, H.R. and Vernadat, F.B., CIM-OSA Part 2: information view,
<u>International Journal of Computer Integrated Manufacturing</u>, Vol. 3, Nos.
3-4, 1990b.<p>
Klittich, M., CIM-OSA Part 3: CIM-OSA integrating infrastructure - the
operational basis for integrated manufacturing systems, <u>International
Journal of Computer Integrated Manufacturing</u>, Vol. 3, Nos. 3-4, 1990.<p>
Koonce, D.A. and Parks, C.M., Low Volume Manufacturing Schedule Monitoring and
Control Using a Symbolic Programming Approach, <u>Computers and Industrial
Engineering</u>, Vol 21., Nos. 1-4, 1991.<p>
Mayer, R.J., Keen, A.A. and Wells, M.S., <u>IDEF4 Object Oriented Design Method
Manual</u>, U.S. Air Force, 1992.<p>
Parks C.M., Koonce, D.A., Rabelo L.C., Judd R.P. and Sauter, J.A., Model-Based
Manufacturing Integration: a Paradigm for Virtual Manufacturing Systems
Engineering, <u>Computers and Industrial Engineering</u>, (to appear).<p>
Rangan, R.M. and Fulton, R.E., A Data Management Strategy to Control Design and
Manufacturing Information, <u>Engineering with Computers</u>, Vol 7, 1991.<p>
Rumbaugh, James, An object or not an object, <u>Journal of Object Oriented
Programming</u>, June, 1992.<p>
Rumbaugh, James, Virtual worlds: Modeling at different levels of abstraction,
<u>Journal of Object Oriented Programming</u>, January, 1994.<p>
Sriram, D., Ahmed, S. and Logcher, R., Transaction Management Framework for
Collaborative Engineering, <u>Engineering with Computers</u>, Vol. 8, 1992.<p>
Wang, W, Popplewell, K. and Bell, R., An integrated multi-

view
system description approach to approximate factory modeling, , <u>International
Journal of Computer Integrated Manufacturing</u>, Vol. 6, Nos. 3,
1993.<b></b><p>
<b></b>
</body></html>

