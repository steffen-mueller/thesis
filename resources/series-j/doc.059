www.is.ilstu.edu/Notes/IS_web_guidelines.html
<HTML>
<HEAD>
<TITLE>Guidelines for Information Systems' Departmental WWW Pages</TITLE>
</HEAD>
<!--Inforamtion Systems' web group  -->
<!--chairperson Ray Smock, Mike Gallagher, Ron Willert, Bill Cummins -->
<!--Connie Barling, Robin Gould, Steve Blair, Mark Troester, Eric Pinge --> 
<!--Created: Thursday - Feburary 29, 1996 Time: 16:59 -->
<BODY background=../Images/bg_draft.gif>
<!-- BODY bgcolor="#ffffff" -->
<table border=0>
<tr>
<td><a href=http://www.ilstu.edu/Welcome.html>
<img border=0 align=left 
src=../Images/isuemblem.gif
width=100 height=100
alt="Illinois State University"></a>
<a href=http://www.infosys.ilstu.edu/Welcome.html>
<img border=0 
src=../Images/tis.gif
width=372 height=050
alt="Information Systems"></a><br><img border=0
src=../Images/filler.gif
width=372 height=3
alt="filler "><p>
<font size=+2>
<b>Information Systems Web Guidelines <br>* D R A F T *</b>
<br clear=left>
</font>
</td>
</tr>
</table>

<a name="top">
<IMG SRC="../Images/div.gif"
width=472 height=004
ALT="-------------------------------------"></a>
<br clear=left>
Bill Gorrell, director of Information Systems (IS), asked Steve Bragg, Director of Planning, Policy Studies, and Information Systems (PPSIS) and Steve Bell, Director of Administrative Computing, to coordinate an Information Systems web development group to create guidelines for common practices for the IS area. The initial meeting was held on <a href=Dec8_meeting.html>December 8, 1995</a>, <a name=back1>where</a> an <a href=IS_web_gl_footnotes.html#ISfoot1>IS web team</a> was established,  that would have weekly meetings to work out the guidelines. <a name=back2>This</a> team would report to the <a href=IS_web_gl_footnotes.html#ISdir>IS directors</a> with the web team's proposal. Ray Smock convened the team at the start of Spring semester 1996.<p>

In the spirit of cooperation that is the basis of the Internet, this IS guide to web style is for creating better web pages. The team is a small group of people who have designed web pages and have put together a best effort guideline to direct others in designing their own web pages. These are our observations, we welcome your <a href=mailto:infosys@ilstu.edu>comments</a>, for or against. The following is a working document and such will change as the needs of the departments change. The growth and versatility of the World Wide Web (WWW) continues to expand almost exponentially.
<p>
<b>The Policy:</b><br>
It is the intent of IS to host and promote the administrative informational uses of computing resources in a professional and responsible fashion. <a name=back3>Therefore,</a> <a href=IS_web_gl_footnotes.html#ISweb>IS web space</a> will be constrained to highly minimize, if not exclude, <a href=IS_web_gl_footnotes.html#rs6k>personal</a> information. While we value the right of personal expression, it has no place in this environment. The following practice  will be used for compliancy to the IS policy:

<ul>
<li><b>Administration.</b> Each collection will be maintained by administrative units in a hierarchical fashion, with ultimate approval for content eminating from the IS directors. Collections will be reviewed on a regular cycle not to exceed one year in duration.
<li><b>Approval.</b> The University  <a href=http://138.87.1.2/images/ISUlogos.html>signature</a> and common IS page design will be used to confer approval on a collection. Presence of the IS page design will be required on all IS web documents to at least the third "level" from the IS home page, and will confer approval of design and content of the collection.
<li><b>Non-approval.</b> Collections, pages or page elements that are deemed inappropriate by the IS directors will be removed from the IS web space.
<li><b>Content.</b> It is our goal to use the web server as a means to keep up-to-date documents online. This refers to all documents and documentation. Yes, we are asking people to make a major mind shift from the way they are use to doing business; i.e. create the document for a web browser. If the user needs a printed copy, let the user print a  hardcopy from the online version. There are many text to html software program, review the guideline <a href=IS_web_gl_footnotes.html#ISref>references</a> "Edit your HTML:". The web has a clear advantage as an intranet, interoffice communication, of "electronic paperwork", because web browser software is available on most hardware & operating systems for little or no additional cost to higher education users.

<ul>
<li>Content will follow University Communications' "University Web Guidelines" as closely as the media will allow. Until there is a link to "University Web Guidelines", the following are listed in this document.

<ul>
<li>Proper use of Illinois State signature. 
<li>Always use Illinois State University, Illinois State, or University in referring to Illinois State University.
<li><i>The use of copyright �, all rights reserved and an equal opportunity employer.</i><-A discussion point for the IS directors... 
</ul>

</ul>
<li><b>Copyright.</b> Creators of web pages must adher to all copyright laws, obtaining permission to use photos, documents, other copyrighted media.
<li><b>Security and Confidentiality.</b> In accordance with the Family Educational Rights and Privacy Act of 1974 as amended, Illinois State University has established the following policy to ensure the <a href=http://www.infosys.ilstu.edu/depts/AdminComp/Security/security_data.html>security and confidentiality of information.</a>



</ul>
<b>The Basics:</b><br>

<ul>
<li>Information will be maintained in a timely & up-to-date manner. 
<li>A page design and length that will load in less than 10 seconds using a 14.4 baud connection.
<li>Specific items, like spelling and grammar will be verified. 
<li>No links (hot spot) to "under construction" document or just as aggravating - unimplemented links, 404 errors.
<li><a name=back4>Design web pages</a> for a graphical browser that will handle, at a minimum, tables and forms; but also attempt to make the pages readable by a <a href=IS_web_gl_footnotes.html#ISlynx>text only browser</a> like lynx. Suggest the use of Netscape ver 1.1 or newer version for optimal viewing. If additional software is required, then those requirements will be mention on the web page; examples: JAVA applets, frames or Acrobat.
</ul>
<b>The Page design:</b><br>

<ul>
<li>Optimize around pages for a VGA (640x480, 256 color) monitor, with the most important information on the top half of the document.
<li>A white background color and no change of the default colors of the web browser. Use of the * D R A F T * in the heading and footing, plus a "Draft" background (Images/bg_draft.gif) when the page is a working dodcument not ready to be advertised to the world.
<li><a href=../Images/IS_banners.html>Common IS banners</a> 
and <a href=../Images/IS_banner2.html>more IS banners</a> through three levels of the IS area. 
Limit banner size to a  height less than 101, width less than 475 pixels. The banner will consist of three objects, two of which will have links. ISU signature, 100x100, will be common to all header banners with a link to the official Illinois State home page. The second link will be to the next higher menu.
<li>IS departmental pages will include:

<ul>
<li>IS departmental banner
<li>Mission statement
<li>Information decided on by department director
<li>Standard footer 
<ul>
<li>Common branching bar to University Home, Search University, Help and Feedback
<li>link to top of document, last reviewed date, time frame of the data, who to contact, page http address. 
</ul>
</ul>
<li>Common navigation image buttons with words for back, forward a page or level and home. 
<li>No "orphan page", pages that have no branching to other web documents, other than the buttons incorportated with the web browser. 
<li>Links at bottom to include previous page, department and IS home page.
<li>Use of &ltfont size=&gt and &ltb&gt &lt/b&gt instead of &ltHn&gt &lt/Hn&gt when it is necessary to conserve white space.
</ul>
<b>The Images:</b><br>

<ul>
<li>Will include width, height and alt parameters, to speed up document loading and readability.
<li>Banner images will be in a central location: <a href=../Images/IS_banners.html>www.infosys.ilstu.edu/Images</a>.
<li>Images will be drafted in Information Systems. Illinois State's Graphics Department will put the professional edge on the images.
</ul>

<IMG SRC="../Images/div.gif"
hspace=000 vspace=000
width=472 height=004
ALT="-------------------------------------">
<br clear=left>

<table border=0 width=473>
<tr align=middle>
<td align=left   width=25%>
<A HREF=http://www.ilstu.edu/Welcome.html>
University Home</a>
</td>
<td              width=25%>
<a href=Harvest/brokers/ilstu/query.html>
Search University</a>
</td>
<td              width=25%>
<b>Help</b>
</td>
<td align=right width=25%>
<b>Feedback</b>
</td>
</tr>
</table>

<IMG SRC="../Images/div.gif"
hspace=000 vspace=000
width=472 height=004
ALT="-------------------------------------">
<br clear=left>
* D R A F T *
<font size=-1>
Go to the <a href="#top">top</a> of document. Go to IS web guidelines <a href=IS_web_gl_footnotes.html#ISfoot1> footnotes</a> and <a href=IS_web_gl_footnotes.html#ISref>references</a>.<br>
Mail comments to: <a href="mailto:infosys@ilstu.edu">
infosys webperson</a>
 (infosys@ilstu.edu)<p>
</font>
<table border=0 width=472>
<tr>
<td align=left>
<font size=-1>
http://www.infosys.ilstu.edu/Notes/IS_web_guidelines.html</td>
</font>
<td align=right>
<font size=-1>
Reviewed for 1995-96 on April 16, 1996</td>
</font>
</td>
</tr>
</table>
</BODY>
</HTML>

