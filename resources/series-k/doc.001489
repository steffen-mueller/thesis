http://www.yahoo.com/text/headlines/970912/entertainment/stories/music_grant_1.html
<html>
<head>
<title>Yahoo! - FEATURE: Amy Grant Goes For Depth On 'Behind The Eyes'</title>
</head>
<body>
<!-- ANPA File: working/0874093288-0000030287 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970912/entertainment/stories/television_spain_1.html><b>Competition Keen For Spain's Untested Digital TV</b></a>
<br>
Next Story: <a href=/text/headlines/970912/entertainment/stories/television_fight_2.html><b>CORRECTION: Radio Station Fires DJ After Punch-Up</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874093260 -->
Friday September 12  3:41 PM EDT
</strong>
<h2>FEATURE: Amy Grant Goes For Depth On 'Behind The Eyes'</h2>
<!-- TextStart -->
<p>
	    By Gary Graff
<p>
	    DETROIT (Reuter) - In one of the songs on her new album,
&quot;Behind the Eyes,&quot; Amy Grant offers some advice to a friend:
<p>
	    &quot;So why don't we just up and leave it all behind
<p>
	    Maybe a change would ease your mind
<p>
	    For a time, leave it all behind&quot;
<p>
	    She may well have been talking to herself, too.
<p>
	    &quot;Behind the Eyes&quot; is another dramatic change for Grant,
who was considered the first lady of contemporary Christian
music before embracing secular pop in 1991 with the hit &quot;Baby
Baby&quot; and a couple of multimillion-selling albums.
<p>
	    But Grant found that even at that level, mainstream success
left her craving something more substantial.
<p>
	    &quot;I was starting to perceive myself more as a hood ornament
than as an artist,&quot; says Grant, 36. &quot;I think that's what I
really had to come to grips with. I'm not proud of that, but I
think it happened.
<p>
	    &quot;I was starting to say, 'Who was I before all this pop
stuff?&quot;'
<p>
	    Grant wasn't the only one who felt that way. David Anderle,
a senior vice-president at A&amp;M Records, her label, sat down with
her in 1995 and gave her instructions, such as &quot;scare me&quot;
and &quot;dig deep,&quot; she recalls.
<p>
	    &quot;He said, 'What I'd really like for you to do is not
re-create what you've done the last two records ... I want a
concentrated dose of you, right now,&quot;' Grant remembers. &quot;He
said, 'Just give me enough of you that I can either love you or
hate you. But just give me enough to have an opinion, please.&quot;'
<p>
	    The challenge left Grant &quot;exhilarated&quot; but also feeling
overwhelmed. She began writing in earnest, retreating at one
point to the Caribou Ranch and studios in Colorado, where she
recorded some of her earliest music.
<p>
	    She also got Anderle to allow her to use longtime
collaborator Wayne Kirkpatrick as the album's primary producer,
despite his modest track record.
<p>
	    Then, in the middle of the production of &quot;Behind the
Eyes,&quot; Kirkpatrick won a Grammy Award as co-writer of the Eric
Clapton hit &quot;Change the World.&quot;
<p>
	    &quot;So he was kind of having a track record created while we
were going,&quot; Grant says.
<p>
	    She acknowledges it took awhile to get her creative
grounding as she wrote songs for the album. At one point, she
even sent James Taylor a song that she wanted to sing with him.
He received it enthusiastically, but then Grant didn't hear from
him.
<p>
	    &quot;Finally I called him and said, 'I don't know if that tape
got lost in the mail or something had come up,&quot;' she recalls.
&quot;He said, 'Amy, this song is just awful.' I laughed then; it
still makes me laugh now.
<p>
	    &quot;He said, 'Oh, gosh, you must've written it.' I said I did,
but that's OK. I didn't think it was awful, but evidently it's
awful. He was right; it's just not a good song.&quot;
<p>
	    But with more than 30 songs recorded for &quot;Behind the
Eyes,&quot; Grant had little trouble finding songs that did hit the
mark, from the Sheryl Crow-style rocker &quot;Takes a Little Time,&quot;
to the positive-thinking &quot;Turn This World Around,&quot; to the
three-hanky special &quot;Cry a River,&quot; which features a duet with
Kirkpatrick.
<p>
	    What unifies them all are Grant's lyrics, which take quiet
stock of her life from the vantage point of a woman in her
mid-30s, with three young children.
<p>
	    &quot;I'm just searching to re-connect with the woman that I was
before having children,&quot; says Grant, who's married to Christian
singer-songwriter Gary Chapman.
<p>
	    &quot;I'm so glad I have them, but that's a labor-physical
intensive time of life and a whole heck of a lot of sleep
deprivation.
<p>
	    &quot;I felt like I had lost my footing somehow, and I think
that's very common for anybody in their 30s. Suddenly you're
going, 'This is going to end,' and I'm not talking career. I'm
talking life. There's a difference in looking at your life from
somewhere around your half-way point.&quot;
<p>
	    The difference on &quot;Behind the Eyes,&quot; Grant says, is that
she feels like she's singing from her heart again rather than
simply singing the emotionally distant lyrics of some of her
early '90s pop songs.
<p>
	    &quot;I think it's possible for us to just be traveling along
and .. our feelings about one thing or another will change, and
we're just so busy we don't even know what we're saying is not
genuine anymore,&quot; she explains.
<p>
	    &quot;I know that this process for me was just so
life-affirming. That's what I really hope happened on this
record; I hoped somebody would hear this record and it would
make them want to just risk being in touch with what's going on
in themselves.&quot;
<p>
	    That includes fans of Grant's spiritual music, who were
caught off guard by her move towards mainstream pop. &quot;Behind
the Eyes&quot; is hardly a return to her Christian roots, but the
album's serious tone and thematic depth is far more familiar
than &quot;Baby Baby&quot; and her other pop hits.
<p>
	    &quot;I think anybody that's really been a supporter, they never
had a problem with it,&quot; Grant says. &quot;I think why I caught heat
was that a lot of us who had our roots in gospel music, we
wanted to broaden what fit in gospel music.
<p>
	    &quot;I think it says a lot that the gospel music song of the
year was Bob Carlisle's 'Butterfly Kisses,' which is not
evangelical in any way. That song has just taken the world by
storm ... and the whole world goes, 'God, this is fabulous. You
mean there's a whole genre of people making this kind of music?'
If that's what we tell people, then it's a good thing.&quot;
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	    (Gary Graff is a nationally syndicated journalist who covers
the music scene from Detroit. He also is the supervising editor
of the award-winning &quot;MusicHound&quot; album guide series.)
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970912/entertainment/stories/television_spain_1.html><b>Competition Keen For Spain's Untested Digital TV</b></a>
<br>
Next Story: <a href=/text/headlines/970912/entertainment/stories/television_fight_2.html><b>CORRECTION: Radio Station Fires DJ After Punch-Up</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

