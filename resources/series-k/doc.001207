http://www.yahoo.com/text/headlines/970918/entertainment/stories/culture_wolfe_1.html
<html>
<head>
<title>Yahoo! - FEATURE: Latest Tom Wolfe Novella Is For Ears Only</title>
</head>
<body>
<!-- ANPA File: working/0874618261-0000041425 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970918/entertainment/stories/culture_royals_1.html><b>Scandal-Filled Book On British Royals Hits U.S. Shops</b></a>
<br>
Next Story: <a href=/text/headlines/970919/entertainment/stories/media_peru_1.html><b>Peruvian Brothers Take Control Of Disputed TV Channel</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874618200 -->
Thursday September 18  5:30 PM EDT
</strong>
<h2>FEATURE: Latest Tom Wolfe Novella Is For Ears Only</h2>
<!-- TextStart -->
<p>
	    By Patrick Connole
<p>
	    NEW YORK (Reuter) - Tom Wolfe says he got a great compliment
the other day when a friend told him how he drove around the
block repeatedly so he could finish listening to a crucial
section of the best-selling author's latest work, a novella
called &quot;Ambush at Fort Bragg.&quot;
<p>
	    Turning off the car stereo would have ended his friend's
literary experience because Wolfe's latest &quot;page turner&quot; is
available only on tape, a three-hour publication sans print.
<p>
	    Publisher Bantam Doubleday Dell (BDD), which bought the
rights to the novella after it was serialized in two 1996
editions of Rolling Stone magazine, said this was the first time
a major author had skipped a book and gone the ears-only route.
<p>
	    &quot;In 1968 I became the first author to have two books
published on the same day, so I thought this might be good,&quot;
Wolfe told Reuters, referring to &quot;The Pump House Gang&quot; and
&quot;The Electric Kool Aid Acid Test.&quot;
<p>
	    Being first on the block appeals to Wolfe, but the &quot;Bonfire
of the Vanities&quot; author also is tapping into a $1.4 billion
slice of the book publishing industry, according to 1994 Los
Angeles-based Audio Publishers Association figures.
<p>
	    &quot;There are 30 percent increases per year from that time,
making audio the fastest-growing segement of the industry,&quot; the
APA's Jan Nathan said.
<p>
	   
<p>
	    THROWBACK TO 1930s RADIO PLAYS
<p>
	    Besides, Wolfe likes doing his reading by listening in the
car, which he sees as a sort of throwback to the radio plays of
the 1930s, brought up to speed for 1990s freeways.
<p>
	    &quot;I have listened to a number of books in the car. 'Dracula'
by Bram Stoker, which was spookier on tape than when I read it,
'Huckleberry Finn' and a very abridged version of 'Moby Dick'
and some Bible stories,&quot; Wolfe said.
<p>
	    His &quot;Ambush&quot; played into the audio format because many of
the characters speak in Southern dialects more comfortable to
hear than to read.
<p>
	    &quot;It was very tricky to do for the reader, Edward Norton
(Oscar-nominated actor for the movie 'Primal Fear'). In print,
the Southern accents, the redneck accents, look strange when you
have to decode them; it is a particular accent from areas in
Florida to south Georgia,&quot; Wolfe said.
<p>
	    The main characters, a including a pair of U.S. Marines
faced off against an aggressive television news show &quot;sting&quot;
investigation for killing a gay soldier, are all played by
Norton, who changes voices often and swiftly.
<p>
	    &quot;It really is a phonetics thing. In the written version,
'was a madder wit chew' is actually, 'what is the matter with
you,&quot;' Wolfe said.
<p>
	    The other hows and whys for Wolfe choosing audio revolve
around some trickier marketing aspects of publishing.
<p>
	   
<p>
	    SAVING THE 'SONIC BOOM' FOR 'BONFIRE' FOLLOWUP
<p>
	    Wolfe, who also wrote &quot;The Right Stuff&quot; about the U.S.
space program in 1975, has not released a hardcover novel since
the immensely popular &quot;Bonfire&quot; in 1987. But he is crafting
the last bits of his long-anticipated follow-up, a yet-unnamed
long-form novel for release late next year.
<p>
	    So going audio on &quot;Ambush&quot; allowed him to save the sonic
boom marketing ploys for the grand unveiling in 1998, according
to BBD Audio Publishing head Jenny Frost.
<p>
	    &quot;Tom's novella was exactly what we were looking for. ... It
dovetailed nicely with what we wanted and with the fact he did
not want a print publication before the new novel,&quot; said Frost,
who along with Wolfe's agent, Eric Siminoff, thought of the
audio-only format over lunch some 18 months ago.
<p>
	    &quot;This is really an opportunity for the public to experience
the Wolfe novella in a new way, gets people familiar with the
audio format who otherwise would not have,
and is a wonderful harbinger for the new novel,&quot; Frost said.
<p>
	    Wolfe said &quot;Ambush&quot; started life as part of the upcoming
novel, but it grew and grew until he ditched that plan and
separated the story for the novella, or short-novel, format.
<p>
	    Frost said the first &quot;print&quot; run for &quot;Ambush&quot; was a
healthy 80,000 units, with more tapes ($21.95) and CDs ($24.95)
on the horizon. The price does not include extra fuel for
driving the car around the block.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970918/entertainment/stories/culture_royals_1.html><b>Scandal-Filled Book On British Royals Hits U.S. Shops</b></a>
<br>
Next Story: <a href=/text/headlines/970919/entertainment/stories/media_peru_1.html><b>Peruvian Brothers Take Control Of Disputed TV Channel</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

