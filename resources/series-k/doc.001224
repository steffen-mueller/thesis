http://www.yahoo.com/text/headlines/970918/entertainment/stories/review_fairy_1.html
<html>
<head>
<title>Yahoo! - REVIEW/FILM: 'Fairy Tale' Harmless Fluff</title>
</head>
<body>
<!-- ANPA File: working/0874585847-0000040762 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970918/entertainment/stories/review_passion_1.html><b>REVIEW/FILM: Man, Leopard Make Out In 'Passion'</b></a>
<br>
Next Story: <a href=/text/headlines/970918/entertainment/stories/industry_dreamworks_1.html><b>FEATURE: DreamWorks Ready To Roll Out First Pictures</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874585800 -->
Thursday September 18  8:30 AM EDT
</strong>
<h2>REVIEW/FILM: 'Fairy Tale' Harmless Fluff</h2>
<!-- TextStart -->
<p>
	    Fairy Tale: A True Story (Period family drama, color, no
rating, 1:39)
<p>
	    By Derek Elley
<p>
	    TORONTO (Variety) - An uneasy mixture of social essay and
period kiddy-cute, &quot;Fairy Tale: A True Story&quot; is as light --
and centerless -- as a marshmallow.
<p>
	    Based on the true-life event known as the Cottingley
Fairies, in which two young English girls claimed to have
photographed sprites in 1917, pic tries to appeal to both adult
audiences and children but is more likely to prove too
insubstantial for the former and often plain confusing for the
latter. Though there's charm aplenty on display here, this Oct.
24 Paramount release should have a longer afterlife on home
video than in theaters.
<p>
	    Basic material is identical to the British film
&quot;Photographing Fairies,&quot; helmed by Nick Willing from the Steve
Szilagyi novel, which recently premiered at the Edinburgh
festival and goes out in the U.K. this month. Though flawed by
script and performance problems, &quot;Photographing Fairies&quot; is in
many ways a more impressive construct, directly targeting an
adult audience with its dramatic envelope of a grand romantic
passion, and at least getting halfway to first base in its theme
of post-WWI loss of faith and the need to believe. And with its
central character a professional photographer, the twin poles of
scientific inquiry and unscientific spiritualism are held more
securely in focus.
<p>
	    &quot;Fairy Tale&quot; takes more than a while to show its hand,
with the opening reels confusingly structured and likely to put
off younger audiences. Pic crosscuts between London, where
illusionist Harry Houdini (Harvey Keitel) is the toast of the
capital and good friend of Sherlock Holmes creator Arthur Conan
Doyle (Peter O'Toole), and the small Yorkshire town of
Cottingley, where the Wright family lives.
<p>
	    The father, Arthur Wright (Paul McGann), is a down-to-earth
engineer who has just designed the electrification of Cottingley
spinning mill. His wife, Polly (Phoebe Nicholls), is a devoted
mom, still marked by the loss of their young son. Also in their
rural abode is 12-year-old daughter Elsie (Florence Hoath), and
her 10-year-old cousin, Frances (Elizabeth Earl), a war orphan
from Africa, where her father is presumed missing or dead.
<p>
	    One day, in a nearby copse, the girls spot some flying
fairies. Though Elsie's mom says there's no such thing, some
drawings left behind by Elsie's dead brother convince the kids
they're right. Pics they take galvanize interest from both the
press and London literary figures including Conan Doyle, who's
becoming more and more engrossed in spiritualism, to the
interested amusement of his friend Houdini.
<p>
	    Movie's chief problem is in getting a handle on its main
focus, given the various ideas flitting around in the script.
O'Toole's linguistically rotund Conan Doyle and Keitel's more
streetwise showman Houdini make an odd couple, and seem to be
running a debating sideshow, far removed from the tale of
childhood wonderment taking place up in rural Yorkshire. And
though, like &quot;Photographing Fairies,&quot; the pic touches on the
rise of spiritual cranksterism in war-shocked Britain, that too
is never deeply woven into the separate story far north.
<p>
	    What's left is a movie that yo-yos between various points
and finally decides to go for the family-kid angle, with an f-x
tour de force at the end in which the fairies -- expressing
simple childhood joy and optimism -- bind together the Wright
family in an aerial ballet throughout their home. Zbigniew
Preisner's string-oriented score, till then largely built on
pregnant motifs, delivers the goods for a warm, magical finale.
<p>
	    As the two girls, Hoath and Earl are neatly contrasted, with
the latter making a sparkier companion to the former's more
wistful, baby-faced Elsie. McGann has a smallish role as the
father, and Nicholls (actress wife of pic's director, Charles
Sturridge) is effective enough without making much impression.
Mel Gibson, representing Icon Prods.' involvement, makes a
lightning cameo at the very end as Frances' returning father.
<p>
	    Production values are tasty, with Sturridge marshaling a
strong team of past collaborators (several from NBC's Gulliver's
Travels) and period specialists. Shirley Russell's costume
design is typically detailed, and production design by Michael
Howells seamlessly incorporates work at the U.K.'s Shepperton
Studios with locations in London and Yorkshire. Flying sequences
involving the fairies by visual f-x supervisor Tim Webber are
ultra-smooth, though the gadfly-like creatures are a less
startling creation than the sexy, R-rated ones in Photographing
Fairies.
<p>
	   
<p>
	    Elsie Wright ............. Florence Hoath
<p>
	    Frances Griffiths ........ Elizabeth Earl
<p>
	    Arthur Wright ............ Paul McGann
<p>
	    Polly Wright ............. Phoebe Nicholls
<p>
	    Sir Arthur Conan Doyle ... Peter O'Toole
<p>
	    Harry Houdini ............ Harvey Keitel
<p>
	    E.L. Gardner ............. Bill Nighy
<p>
	    Harry Briggs ............. Bob Peck
<p>
	    John Ferret .............. Tim McInnerny
<p>
	    Frances' Father .......... Mel Gibson
<p>
	   
<p>
	    A Paramount release of an Icon Prods.-Wendy Finerman Prods.
production. Produced by Wendy Finerman, Bruce Davey. Executive
producer, Paul Tucker. Co-producers, Selwyn Roberts, Albert Ash,
Tom McLoughlin.
<p>
	    Directed by Charles Sturridge. Screenplay, Ernie Contreras;
story, Albert Ash, Tom McLoughlin, Contreras. Camera
(Metrocolor-Deluxe prints), Michael Coulter; editor, Peter
Coulson; music, Zbigniew Preisner; production design, Michael
Howells; costume design, Shirley Russell; sound (Dolby), John
Midgley; visual effects supervisor, Tim Webber; associate
producer, Margaret French Isaac; assistant director, Micky
Finch; second unit director, Selwyn Roberts; casting, Mary
Selway, Trevor Hoath. Reviewed at Toronto Film Festival (Gala),
Sept. 11, 1997.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970918/entertainment/stories/review_passion_1.html><b>REVIEW/FILM: Man, Leopard Make Out In 'Passion'</b></a>
<br>
Next Story: <a href=/text/headlines/970918/entertainment/stories/industry_dreamworks_1.html><b>FEATURE: DreamWorks Ready To Roll Out First Pictures</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

