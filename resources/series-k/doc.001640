http://www.yahoo.com/text/headlines/970909/entertainment/stories/review_in_1.html
<html>
<head>
<title>Yahoo! - REVIEW/FILM: 'In &amp; Out' An Engaging Comedy</title>
</head>
<body>
<!-- ANPA File: working/0873807211-0000025025 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970909/entertainment/stories/review_thousand_1.html><b>REVIEW/FILM: 'Thousand Acres' An Awkward Failure</b></a>
<br>
Next Story: <a href=/text/headlines/970909/entertainment/stories/people_archerd_4.html><b>ARCHERD: Gregory Peck's World Travels</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873807180 -->
Tuesday September  9  8:13 AM EDT
</strong>
<h2>REVIEW/FILM: 'In &amp; Out' An Engaging Comedy</h2>
<!-- TextStart -->
<p>
	    In &amp; Out (Comedy, color, PG-13, 1:30)
<p>
	    By Todd McCarthy, Daily Variety Chief Film Critic
<p>
	    HOLLYWOOD (Variety) - A very broad mainstream comedy, &quot;In &amp;
Out&quot; has more trouble than it should stretching a high-concept
premise into 90 minutes of mirth.
<p>
	    Basically a one-joke farce that plays around with a
once-delicate subject that by now is a mainstay even on TV,
this &quot;Is he or isn't he?&quot; situation comedy about a prim
Midwestern schoolteacher whose sexuality is placed uncomfortably
in the national spotlight should ride a number of good laughs, a
feel-good demeanor and a fine lead performance by Kevin Kline to
solid box office in wide early fall release.
<p>
	    Penned by Paul Rudnick, best known for the gay-themed Off
Broadway hit &quot;Jeffrey,&quot; this well-cast picture was triggered
by Tom Hanks' Oscar acceptance speech in which he thanked his
high school drama teacher. Here, however, the ultra-cool young
star Cameron Drake (Matt Dillon), who wins the Academy Award for
a gay-themed war drama, goes a step further by outing his mentor
to millions of viewers on the telecast, calling him a &quot;great
gay teacher.&quot;
<p>
	    The problem is, Howard Brackett (Kline) is not only not
&quot;out,&quot; he is not, he claims, even gay. True, he wears a trim
little bow tie, has a possibly inordinate fondness for Barbra
Streisand and has passed the age of 40 without ever marrying,
but the latter situation, at least, is about to change: after a
three-year (and unconsummated) courtship, Howard is about to
marry fellow school teacher Emily Montgomery (Joan Cusack).
<p>
	    Although Howard's friends and family are aghast and confused
by Cameron's startling proclamation, they are inclined to give
the benefit of the doubt to one of their own. But the frenzied
media smell a hot story and descend upon cozy Greenleaf, Ind.,
to nose out the truth, which puts Howard on the defensive with
his uptight principal (Bob Newhart), who can scarcely bring
himself to utter the word &quot;gay,&quot; his fiancee and his
long-suffering mother (Debbie Reynolds), who is determined to
see her son married whether he's gay.
<p>
	    The most persistent and suspicious reporter to track Howard
is slick Peter Malloy (Tom Selleck), a hack tabloid-style
broadcaster who disarmingly describes himself as &quot;show business
garbage&quot; and freely admits that he's gay himself. Lurking
around town and delivering regular reports of small-town gossip
to his large TV audience, Peter finally finds a way, in a very
funny scene almost exactly halfway through the movie, to
determine the truth about Howard; remainder of the running time
is spent dealing with the fallout in the most public and
comically contrived manner imaginable.
<p>
	    One of the most genuinely amusing aspects of the second half
concerns the arrival in town of Cameron and his spacey
supermodel girlfriend (Shalom Harlow). With his dyed blond hair,
inarticulate manner and hipper-than-thou 'tude, Cameron, in
Dillon's witty performance, constitutes a delightful Brad Pitt
knockoff, a small-town boy who's hit it big virtually overnight
and returns to set matters straight, so to speak, on a number of
fronts.
<p>
	    Beginning with mock excerpts from Cameron's Oscar-winning
vehicle, &quot;To Serve and Protect,&quot; a sendup of both &quot;Platoon&quot;
and &quot;A Few Good Men,&quot; in which a dogface finally admits his
true feelings about a fellow soldier whose life he saves, there
are a number of big laughs in &quot;In &amp; Out,&quot; undoubtedly enough
to make general audiences feel they've gotten their money's
worth. But instead of five or six out-and-out guffaws, the
material seems ripe enough to have provoked two or three times
that many, leaving the impression, by the end, of a comedy
that's been decent fun but is somewhat undernourished.
<p>
	    Rudnick and director Frank Oz graft old-school stereotypes
of gayness -- a taste for musical comedy, fussy neatness,
&quot;un-masculine&quot; mannerisms and interests -- onto a more
politically up-to-date stance about the desirability of honesty
and a non-judgmental mindset in a manner that all but the most
conservative audiences should have no trouble swallowing. Nearly
every gag is pitched and underlined in the most obvious way,
with punchlines played to the upper balcony.
<p>
	    Still, pic is given a gratifying measure of grace by Kline's
effortlessly light and dextrous performance. A fine dramatic
actor, Kline nonetheless always seems most in his element when
he's acting up a bit in comic roles, which is amply illustrated
by his work here. As the increasingly discomfited Howard, he
underplays some of the easiest laughs, all the better to provoke
hilarity with some unexpected outbursts later on. The actor and
role fit each other beautifully.
<p>
	    Aside from Dillon, who brightens every scene he's in, the
delightful surprise here is Selleck, who brings wonderfully
mischievous, energizing and self-deprecating qualities to the
role of the dirt-digging but ultimately on-the-level
broadcaster. Cusack also has her moments to shine as the
previously overweight bride-to-be who finds herself at the
center of unwanted controversy, while the slinky Harlow
generates terrific laughs of recognition as the self-centered
model who freaks out when confronted with a rotary phone at a
small-town motel. Reynolds and Wilford Brimley are somewhat
under-used as Howard's mother and father.
<p>
	    Shot largely on Long Island, ably standing in for Indiana,
pic boasts spiffy production values.
<p>
	   
<p>
	    Howard Brackett ........... Kevin Kline
<p>
	    Emily Montgomery .......... Joan Cusack
<p>
	    Cameron Drake ............. Matt Dillon
<p>
	    Berniece Brackett ......... Debbie Reynolds
<p>
	    Frank Brackett ............ Wilford Brimley
<p>
	    Tom Halliwell ............. Bob Newhart
<p>
	    Peter Malloy .............. Tom Selleck
<p>
	    Ava Blazer ................ Deborah Rush
<p>
	    Ed Kenrow ................. Lewis J. Stadlen
<p>
	    Walter Brackett ........... Gregory Jbara
<p>
	    Sonya ..................... Shalom Harlow
<p>
	    Trina Paxton .............. J. Smith-Cameron
<p>
	    Aunt Becky ................ Kate McGregor-Stewart
<p>
	    Jack ...................... Shawn Hatosy
<p>
	    Mike ...................... Zak Orth
<p>
	    Vicky ..................... Lauren Ambrose
<p>
	    Meredith .................. Alexandra Holden
<p>
	   
<p>
	    A Paramount release presented in association with Spelling
Films of a Scott Rudin production. Produced by Rudin. Executive
producer, Adam Schroeder. Co-producer, G. Mac Brown.
<p>
	    Directed by Frank Oz. Screenplay, Paul Rudnick. Camera
(Duart color, Deluxe prints), Rob Hahn; editors, Dan Hanley,
John Jympson; music, Marc Shaiman; production design, Ken Adam;
art direction, Charles V. Beal; set decoration, Leslie A. Pope;
costume design, Ann Roth; sound (Dolby), Danny Michael;
associate producer, Suzanne Santry; assistant director, Michael
E. Steele; casting, Margery Simkin. Reviewed at Harmony Gold
screening room, L.A., Aug. 27, 1997. (In Toronto Film
Festival.)
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970909/entertainment/stories/review_thousand_1.html><b>REVIEW/FILM: 'Thousand Acres' An Awkward Failure</b></a>
<br>
Next Story: <a href=/text/headlines/970909/entertainment/stories/people_archerd_4.html><b>ARCHERD: Gregory Peck's World Travels</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

