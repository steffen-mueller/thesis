http://www.yahoo.com/text/headlines/970930/entertainment/stories/review_rent_1.html
<html>

<head>
<title>Yahoo! - REVIEW/STAGE: 'Rent' Collects Praise In Los Angeles</title>
</head>
<body>
<!-- ANPA File: working/0875622146-0000062091 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970930/entertainment/stories/review_arabian_1.html><b>REVIEW/STAGE: 'Arabian Nights' Offers A Myriad Delights</b></a>
<br>
Next Story: <a href=/text/headlines/971001/entertainment/stories/film_baldwin_1.html><b>Baldwin Home In 'Providence'</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 875622120 -->
Tuesday September 30  8:22 AM EDT
</strong>
<h2>REVIEW/STAGE: 'Rent' Collects Praise In Los Angeles</h2>
<!-- TextStart -->
<p>
	    Rent (Ahmanson Theatre; 2,033 seats; $70 top; 2:55)
<p>
	    By Charles Isherwood
<p>
	    HOLLYWOOD (Variety) - Inside their Julian Schnabel arch,
under the Noguchi moon, Jonathan Larson's art-or-bust bohemians
have taken up residence in the swank confines of the Music
Center's Ahmanson Theatre, as &quot;Rent&quot; settles in for a
four-month Los Angeles run.
<p>
	    Two months after bowing at the La Jolla Playhouse, this
third company of the instantly legendary musical remains a fresh
and vibrant ensemble. The infectious joy they take in both each
other's company and in performing now more cleanly smoothes over
some remaining troubles. They seem to feel the deep bonds among
the makeshift family of New York twentysomethings that Larson's
book sometimes only sketchily describes.
<p>
	    Neil Patrick Harris' charismatic turn as the narrator Mark
no longer dominates the show. Perhaps it's the more somber edge
he's bringing to the role, particularly in the opening number,
or perhaps it's that we've recovered from the
Doogie-Howser-singing-and-making-lewd-gestures thing. He's still
terrific, but rather than seeming to host the party, he's joined
it.
<p>
	    Wilson Cruz, a fellow escapee from primetime TV (My
So-Called Life), has honed his performance as the doomed drag
queen Angel to a fine polish. His nuances mine laughs from even
the most throwaway bits, and he's infinitely more secure in his
patent-leather platforms. (Though why costume designer Angela
Wendt gave Angel two of the most hideous ensembles known to man,
woman or drag queen remains anyone's guess; and for that matter,
with money rolling in, why does each company have to sport the
exact same costumes -- wouldn't it be more in keeping with the
show's mean-streets ethos to have each bunch shop at a different
thrift store?)
<p>
	    A strong rapport has grown between Kenna Ramsey and Leigh
Hetherington as the fractious but loving lesbian couple.
Hetherington's performance-art number is more richly satiric in
tone -- making it harder to take seriously, perhaps, the group's
collective artistic pretensions, but winning nonetheless. And
Ramsey's both a vocal powerhouse and an actress who can shape a
cohesive character out of minimal dialogue.
<p>
	    Likewise, D'Monroe as the turncoat capitalist among this
grunge bunch has brought more focus to his role and made it
surprisingly sympathetic. But as the central couple, songwriter
and ex-heroin addict Roger and exotic dancer and current heroin
addict Mimi, Christian Mena and Julia Santana aren't entirely
satisfactory. Santana acts the role well, but her vocal
resources are clearly being strained -- and no wonder, with her
big number, &quot;Out Tonight,&quot; pitched at a constant shout. Her
voice frequently sounded frayed on opening night. Mena,
meanwhile, is a strong vocalist, but he's playing -- or rather
singing -- his role at the same level of brooding intensity
throughout, resulting in monotony.
<p>
	    As for the show's intrinsic merits, they remain as ever
Larson's unmistakable and refreshingly various melodic gift,
encompassing everything from rock anthems to ballads to a
tango-flavored duet; and the quixotic courage to bring the
traditional musical form to bear on subjects that seem designed
to defy its neat exigencies: the AIDS epidemic, the problem of
urban poverty, a world being reshaped by a generation with new
sexual and social mores.
<p>
	    With such ambitions, it's hardly surprising that the show
doesn't always manage to make its points with sufficient depth
or subtlety (the homeless angle seems particularly perfunctory
-- and dated). Clarity is also a problem. Michael Greif's
direction puts almost every number across musically (with the
exception of the still-dire sex-talk number Contact), but the
book's narrative line comes in and out of focus; after more than
one viewing, you might still get a little lost in the second
act. But from its continuing rapturous reception, audiences
appear more than willing to get lost.
<p>
	   
<p>
	    Cast: Christian Mena (Roger Davis), Neil Patrick Harris
(Mark Cohen), Mark Leroy Jackson (Tom Collins), D'Monroe
(Benjamin Coffin III), Kenna Ramsey (Joanne Jefferson), Wilson
Cruz (Angel Schunard), Julia Santana (Mimi Marquez), Leigh
Hetherington (Maureen Johnson), Carla Bianco, Kevyn Brackett,
Sharon Brown, Curt Skinner, Andy Senor, Brent Davin Vance, Sala
Iwamatsu, Owen Johnston II, Monique Daniels.
<p>
	   
<p>
	    Center Theatre Group-Ahmanson Theatre, Jeffrey Seller, Kevin
McCollum, Allan S. Gordon and New York Theatre Workshop present
a musical in two acts with book, music and lyrics by Jonathan
Larson. Directed by Michael Greif. Musical direction by Robert
Sprayberry; musical arrangements by Steve Skinner. Choreography
by Marlies Yearby. Original concept, additional lyrics by Billy
Aronson. Set, Paul Clay; costumes, Angela Wendt; lighting, Blake
Burba; sound, Steve Canyon Kennedy; casting, Bernard Telsey
Casting; production supervisor, Tom Bartlett. Opened, reviewed
Sept. 28, 1997; runs through Jan. 18.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970930/entertainment/stories/review_arabian_1.html><b>REVIEW/STAGE: 'Arabian Nights' Offers A Myriad Delights</b></a>
<br>
Next Story: <a href=/text/headlines/971001/entertainment/stories/film_baldwin_1.html><b>Baldwin Home In 'Providence'</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

