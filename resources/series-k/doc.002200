http://www.yahoo.com/text/headlines/971008/business/stories/greenspan_4.html
<html>

<head>
<title>Yahoo! - Greenspan: Economy on 'Unsustainable Track'</title>
</head>
<body>
<!-- ANPA File: working/0876349493-0000075412 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<center>[
<strong>Business</strong>
| <a href="http://biz.yahoo.com/">Financial</a>
| <a href="http://biz.yahoo.com/news/">Company</a>
| <a href="http://biz.yahoo.com/industry/">Industry</a>
| <a href="http://biz.yahoo.com/prnews/">PR News</a>
| <a href="http://biz.yahoo.com/bw/">Biz Wire</a>
| <a href="http://quote.yahoo.com/">Quotes</a>
]</center>
<p>
<!-- StartLinks -->
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 876349440 -->
Wednesday October  8  6:24 PM EDT
</strong>
<h2>Greenspan: Economy on 'Unsustainable Track'</h2>
<!-- TextStart -->
<p>
	    By Knut Engelmann
<p>
	    WASHINGTON (Reuter) - Federal Reserve Chairman Alan
Greenspan warned on Wednesday that the U.S. economic expansion
and stock market boom may be unsustainable, sending global
markets into a tailspin on fears of higher interest rates.
<p>
	    In his unexpectedly candid remarks to a congressional budget
committee, the central bank chairman used his strongest language
on the stock market since he spoke of Wall Street's &quot;irrational
exuberance&quot; last December.
<p>
	    &quot;The performance of the labor markets this year suggests
that the economy has been on an unsustainable track,&quot; Greenspan
said in prepared testimony.
<p>
	    It was not the first time Greenspan touched on this subject.
He used almost exactly the same line on labor market tightness
in another congressional testimony in July, but did so in what
appeared to be a more optimistic context.
<p>
	    The central bank chief on Wednesday said the world's top
economy had performed far better than expected, with fewer signs
of inflation than in past expansions, but he cautioned that
recent stock market gains may not continue.
<p>
	    &quot;It clearly would be unrealistic to look for a continuation
of stock market gains of anything like the magnitude of those
recorded in the past couple of years,&quot; he said.
<p>
	    His remarks knocked more than 100 points from the Dow Jones
industrial average of key U.S. stocks and prompted selloffs
across the world. The Dow later recovered slightly to close down
83.25 points at 8,095.06.
<p>
	    &quot;Alan Greenspan speaks and Wall Street listens, and down it
went,&quot; said Hildegard Zagorski, an analyst at Prudential
Securities in New York.
<p>
	    The benchmark 30-year Treasury bond fell by almost two
points in heavy trading. Analysts said the market, expecting a
speech concentrating on U.S. budget issues, was caught off-guard
by Greenspan's stern inflation warnings.
<p>
	    &quot;If he thought the tone of his remarks was inappropriate
for the bond market, he would have toned them down,&quot; said Lou
Crandall of HR Wrightson &amp; Associates Inc. in New York.
<p>
	    The Fed has left interest rates untouched since March 25,
when it nudged up the federal funds rate -- which determines
borrowing costs throughout the economy and, by extension, much
of the rest of the world -- by a quarter point to 5.5 percent.
<p>
	    Its next policy-setting meeting is Nov. 12. Prior to
Greenspan's testimony, few analysts had expected a rate rise
then and most said Wednesday's comments may not have raised the
odds for a tightening move next month.
<p>
	    &quot;I do not believe it is quite clear yet the Fed is going to
raise rates in November,&quot; said Alan Ackerman, executive vice
president and market strategist at Fahenstock &amp; Co. in New
York. &quot;It was a shot across the bows, a word of warning to try
to cool things down.&quot;
<p>
	    Twenty of 29 economists surveyed by Reuters this week said
the Fed would keep short-term rates steady until next year. Most
of them confirmed their forecast on Wednesday, saying the
central bank was unlikely to raise the cost of credit before it
has clear evidence of rising prices.
<p>
	    Greenspan said the Fed was carefully watching for the first
signs of inflationary pressures, particularly as a result of the
country's tightly stretched labor markets.
<p>
	    &quot;There would seem to be emerging constraints on potential
labor input. If the recent two million plus annual pace of job
creation were to continue, the pressures on wages ... could
escalate more rapidly,&quot; he said.
<p>
	    Greenspan acknowledged that wage rises had been kept in
check so far, citing the dollar's strength and heightened worker
insecurity as contributing factors. &quot;To be sure there is little
evidence of wage acceleration,&quot; he said.
<p>
	    &quot;A reemergence of inflation is, without question, the
greatest threat to sustaining what has been a balanced economic
expansion virtually without parallel in recent decades,&quot; he
added.
<p>
	    While Greenspan spoke on Capitol Hill, a Senate Banking
Committee voted unanimously to approve President Clinton's
nominees for the two vacant seats on the Federal Reserve board,
Roger Ferguson and Edward Gramlich.
<p>
	    Aides said Sen. Tom Harkin, the Iowa Democrat who held up
Greenspan's renomination last year, would seek a Senate floor
debate on the confirmation. They said the debate could delay
voting on the Fed nominees.
<!-- TextEnd -->
<!-- StartRelated -->
<hr>
<strong>Earlier Related Stories</strong>
<ul>
<li><a href=/text/headlines/971008/business/stories/greenspan_3.html>Greenspan: US Economy May Be ``Unsustainable''</a> - <small>Wed Oct  8  1:56 pm</small></li>
<li><a href=/text/headlines/971008/business/stories/greenspan_2.html>Greenspan: US Economy on 'Unsustainable Track'</a> - <small>Wed Oct  8 11:49 am</small></li>
</ul>
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<strong>Biz</strong> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

