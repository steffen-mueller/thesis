http://www.yahoo.com/text/headlines/970929/entertainment/stories/people_devito_1.html
<html>
<head>
<title>Yahoo! - FEATURE: L.A. Confidential? Danny DeVito Disses Tabs</title>
</head>
<body>
<!-- ANPA File: working/0875558245-0000061083 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970929/entertainment/stories/people_diana_5.html><b>Probe Of Diana Crash Points To Speed, Drink, Not Paparazzi</b></a>
<br>
Next Story: <a href=/text/headlines/970930/entertainment/stories/music_soundbites_1.html><b>SOUND BITES: Album Review Package</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 875558220 -->
Monday September 29  2:37 PM EDT
</strong>
<h2>FEATURE: L.A. Confidential? Danny DeVito Disses Tabs</h2>
<!-- TextStart -->
<p>
	    By Steve James
<p>
	    LOS ANGELES (Reuter) - Like many Hollywood stars, Danny
DeVito hates the &quot;in-your-face&quot; breed of paparazzi -- the type
of creature he plays in his latest movie.
<p>
	    &quot;My character, Sid Hudgens, is in the vanguard of tabloid
journalism in a way; he was the grandfather of all these TV
shows and newspapers that are in the supermarkets now. He goes
to work every day wondering how he can compromise people's
lives,&quot; DeVito said with a sly grin.
<p>
	    But when the conversation turns from his part in &quot;L.A.
Confidential,&quot; a film noir about 1950s Los Angeles, to the
real-life tabloids, the screen funnyman turns deadly serious.
<p>
	    &quot;(It's) ballistic guerrilla warfare these people are waging
against celebrities and public figures. There's got to be a way
to fix that,&quot; he told Reuters in an interview.
<p>
	    DeVito's hatred of the tabloids is no different from the 
stream of bile vented by Hollywood stars ever since the death of
Princess Diana Aug. 31 in a car crash many have blamed on
pursuing photographers.
<p>
	    The whole issue of where a celebrity's public persona ends
and his private life begins has been a festering sore for years
in the movie capital of the world.
<p>
	    &quot;It's a tragedy how people are treated. I've been chased by
people on the street. It happens to everybody who's in the
limelight,&quot; said DeVito, 52, who became famous as the obnoxious
dispatcher Louie on the TV comedy series &quot;Taxi.&quot;
<p>
	    But DeVito, also a successful director and producer, knows
the importance of publicity to an actor's success.
<p>
	   
<p>
	    'PEOPLE HUNT YOU DOWN LIKE AN ANIMAL'
<p>
	    &quot;First of all, you want to be in the newspapers, you know
people want to see (you), but there's no need for people to hunt
you down like an animal. I usually just smile, (let them) take
the picture and go on my merry way, and the people who are
chasing me, I don't look at them,&quot; he said.
<p>
	    &quot;(But) if it's so belligerent, so in-your-face, and you
have no defense against it, you probably act like a frightened
animal and run away. Anything can happen when you're in that
situation, and unfortunately sometimes the consequences are very
grave. In this very tragic thing, I was moved and saddened by
Princess Diana being killed and Dodi (Fayed) ... that's uncalled
for.&quot;
<p>
	    But DeVito relishes &quot;L.A. Confidential,&quot; where his sleazy
newsman colludes with corrupt police to set up minor celebrities
in drug or sex scandals.
<p>
	    Was it a stretch for a man who has found fame playing
lowlifes with a human side in such films as &quot;One Flew Over the
Cuckoo's Nest,&quot; &quot;Ruthless People&quot; and &quot;The War of the
Roses&quot; (which he also directed)?
<p>
	    &quot;It's not based on any particular incident. It's just a
coincidence the movie is coming out as all this stuff is
happening,&quot; he said of his latest film.
<p>
	    But unlike the 1950s, when DeVito was spending his teen-age
years in New Jersey, nowadays the public's hunger for such
tabloid fodder is insatiable.
<p>
	    &quot;There's more of everything now,&quot; he said. &quot;Satellites,
TV and radio. You can't go anywhere without seeing it. It's all
over you, you're saturated with it, the public wants it.&quot;
<p>
	   
<p>
	    ONLY IN BARBERSHOPS IN THE OLD DAYS
<p>
	    Young DeVito never saw such stuff in Asbury Park, N.J. &quot;The
magazines, you'd find them in barbershops. This was the only
place I was exposed to them. They were a kind of a novelty --
you know, 'Confidential Magazine,' how many times has this guy
been married? Who's been sleeping with whose wife?&quot;
<p>
	    Now DeVito, who is married to &quot;Cheers&quot; star Rhea Perlman
and has three children, protects his own private life and
supports moves to legislate against photographers who snap
pictures of stars in the street.
<p>
	    &quot;People can chase you down and harass you, call out your
name and stick a camera in your kid's face and follow you from
store to store while you're shopping in the name of freedom of
the press? That's baloney,&quot; he said.
<p>
	    DeVito believes ultimately the readers and the viewers will
put the tabloid magazines and TV shows out of business.
<p>
	    &quot;If you're an editor, say, and you look at those films
people send them and they see a shot of somebody water-skiing,
you got lucky, you wound up catching (Luciano) Pavarotti on
water-skis. Who's not going to be interested?
<p>
	    &quot;But if you see 19 shots of the guy, and they're in his
face, and they're in a boat and following him, trying to find
out where he put his skis on and where he took his bathing suit
off, then they shouldn't buy the damn thing,&quot; he said.
<p>
	    &quot;If you see somebody sneaking around somebody's bedroom,
taking pictures of the guy and his wife in bed, or naked
pictures, don't buy the damn magazine, that's all.&quot;
<p>
	    Reuters/Variety
<p>
	   
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970929/entertainment/stories/people_diana_5.html><b>Probe Of Diana Crash Points To Speed, Drink, Not Paparazzi</b></a>
<br>
Next Story: <a href=/text/headlines/970930/entertainment/stories/music_soundbites_1.html><b>SOUND BITES: Album Review Package</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

