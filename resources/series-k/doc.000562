http://www.yahoo.com/text/headlines/971008/entertainment/stories/film_indies_1.html
<html>

<head>
<title>Yahoo! - FEATURE: Odds For Indie Pic Distribution Get Longer</title>
</head>
<body>
<!-- ANPA File: working/0876312320-0000074887 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971008/entertainment/stories/television_cbs_1.html><b>CBS Demotes Executive Over Diana Fiasco</b></a>
<br>
Next Story: <a href=/text/headlines/971008/entertainment/stories/film_melting_1.html><b>Trio Finish Stirring 'Melting Pot'</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 876312300 -->
Wednesday October  8  8:05 AM EDT
</strong>
<h2>FEATURE: Odds For Indie Pic Distribution Get Longer</h2>
<!-- TextStart -->
<p>
	    By Leonard Klady
<p>
	    HOLLYWOOD (Variety) - Despite outward signs of a vibrant
independent feature film scene, the truth is that the odds of a
young filmmaker getting his/her film picked up theatrically have
gotten worse.
<p>
	    The reasons run the gamut from considerably more films being
produced and distributed annually to the skyrocketing costs of
promoting and distributing movies and, some say, a resulting
caution in what's considered for acquisition. Savvy filmmakers
know they have to work every possible angle just to get seen.
<p>
	    &quot;There's no question that fewer completed films are being
picked up theatrically,&quot; said Sony Classics' Michael Barker.
&quot;But part of the reason is that everyone is getting involved
earlier in the process. Something that has heat because of
content or talent is going to get investment prior to
filmmaking. We have three upcoming films that we committed to at
the script stage -- something we rarely did even a few years
ago. Pictures like 'Shine,' which arrived at Sundance without a
distributor, or 'The Brothers McMullen,' which popped up at the
Independent Feature Film Market, are now a rarity.&quot;
<p>
	    True, not all of the news is bad. In fact, the fall festival
season has experienced an extraordinary amount of acquisition
activity after almost a year-long drought. And it wasn't just
American indies with obvious commercial hooks, like Robert
Duvall's &quot;The Apostle,&quot; for which October paid a hefty amount,
or Sony Classics' grabbing David Mamet's &quot;The Spanish
Prisoner&quot; starring Campbell Scott and Steve Martin.
<p>
	    Rather, it was the purchase of such unsung movies as the
black comic &quot;Life During Wartime,&quot; the spoof &quot;Orgazmo&quot; and
the African-American romantic comedy &quot;Hav Plenty.&quot;
<p>
	    Chris Cherot, who wrote and directed &quot;Hav Plenty,&quot; put his
picture together by getting his grandmother to take out a
mortgage on her home and loan him $65,000 for the movie. He
hoped to sell it at the independent market. However, the picture
got the notice of executives from Edmonds Entertainment --
producer/singer Kenneth &quot;Babyface&quot; Edmonds' film company --
who placed it at the Acapulco and Toronto festivals. In Canada,
his sales represents were able to negotiate a deal that will
bring back more than 10 times his initial costs.
<p>
	    The picture is a happy exception. The numbers bear out that
American indies are having a tougher time getting into the
marketplace. In the past three years, there have been fewer
films bought, and their performance has been steadily eroding.
<p>
	    Through the first nine months of 1997, U.S. indie
acquisitions accounted for just 1.3% of the domestic box office,
down 50% from each of the prior three years. To date, only
&quot;Sling Blade&quot; among them has grossed more than $10 million,
whereas six films passed that level last year, and five did it
in 1995.
<p>
	    &quot;I'm not convinced that there are fewer American films
being picked up,&quot; October Films' Bingham Ray said. &quot;There were
actually quite a few Sundance movies that were bought this year.
But ... there haven't been those one or two films that seemed to
pop up every year which had heat at screenings and translated
into specialized box office success.&quot;
<p>
	    However, for a filmmaker who had limited resources to make a
movie and didn't make the cut for major fest exposure, the IFFM
holds out the promise of an opportunity to be discovered. It is
the ultimate clearing house for idiosyncratic, maverick and
orphan films. Most have only a glimmer of a commercial life --
it is after all a market, not a festival.
<p>
	    William Harrity is just one extreme among the people
representing 403 films and scripts at this years Indie Film
Market. A successful Pasadena anesthesiologist, he always dreamt
of making movies, and finally realized his dream last year when
he produced and starred in &quot;The F Zone,&quot; a tale of a
businessman who seeks revenge after being ruined by the tax man.
Though he's yet to make a sale, Harrity is planning another
picture, pondering setting up a distribution company and putting
up more money for a suite and screenings at the American Film
Market and Cannes.
<p>
	    Former singer Kirsten Clarkson, says drive and naivete
spurred her to make &quot;Horsey,&quot; a somewhat autobiographical tale
of a bisexual artist involved with a singer-drug addict. She
raised her initial money from benefit rock concerts and, on a
Vancouver talk show, blithely asked investors to call in. That
resulted in a major investment from a dentist, and Clarkson was
able to secure government money and a sales agent. When the
market concluded, a couple of indie distributors had made
requests to screen her picture, and the Hollywood Film Festival
selected the film as a finalist in its competition section.
<p>
	    &quot;We received more than 450 fiction feature submissions last
year,&quot; said Sundance Film Festival artistic director Geoff
Gilmore. &quot;Maybe 10% were resubmits from the year before. But
that's an awful lot of films, and you start to wonder where
these pictures come from and realize that a lot of people have
figured out how to make a film for $30,000 that, if it's good
enough, will be able to secure finishing costs.&quot;
<p>
	    Certainly that's been the case in the past for such
no-budget ventures as &quot;Go Fish,&quot; &quot;Clerks&quot; and &quot;El
Mariachi.&quot; And despite concern that the indies are gambling
less and less, it spurs all kinds of indies to pick up a camera
and shoot for the best.
<p>
	    After all, what other choice is there?
<p>
	    Reuters/Variety
	
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971008/entertainment/stories/television_cbs_1.html><b>CBS Demotes Executive Over Diana Fiasco</b></a>
<br>
Next Story: <a href=/text/headlines/971008/entertainment/stories/film_melting_1.html><b>Trio Finish Stirring 'Melting Pot'</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

