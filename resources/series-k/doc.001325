http://www.yahoo.com/text/headlines/970916/entertainment/stories/music_talkshow_1.html
<html>
<head>
<title>Yahoo! - FEATURE: Talk Show Continues STP's Legacy</title>
</head>
<body>
<!-- ANPA File: working/0874424381-0000037589 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970916/entertainment/stories/music_lozenges_1.html><b>BBC Takes The Cough Out Of Concerts</b></a>
<br>
Next Story: <a href=/text/headlines/970916/entertainment/stories/review_chinese_1.html><b>REVIEW/FILM: 'Chinese Box' Best Left Unopened</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874424340 -->
Tuesday September 16 11:39 AM EDT
</strong>
<h2>FEATURE: Talk Show Continues STP's Legacy</h2>
<!-- TextStart -->
<p>
	    By Dean Goodman
<p>
	    LOS ANGELES (Reuter) - In its brief existence, Stone Temple
Pilots provided one of the best soap operas in the world of rock
'n' roll.
<p>
	    The Los Angeles quartet overcame critical hatred to sell 10
million copies of its three albums but was consistently hindered
by the well-publicized drug problems of its magnetic lead
singer.
<p>
	    The story is almost a cliche in the annals of pop music, but
the group's members hope to become more than just a footnote.
For now, STP is on the backburner, essentially defunct, while
its personnel pursue projects that seem likely to prove a band's
whole is greater than the sum of its parts.
<p>
	    The vocalist in question, Scott Weiland, is recording a solo
album, and his three lower-profile colleagues have hired a
singer and formed a group called Talk Show. Its self-titled
debut album debuted at a meager 131st place on the Billboard
album charts last week.
<p>
	    Headed by bass player Rob DeLeo, Talk Show consists of his
elder brother Dean on guitar, drummer Eric Kretz and singer Dave
Coutts, who was formerly with a local band called Ten Inch Men.
<p>
	    Inevitably, questions about Talk Show relate directly to
Stone Temple Pilots. What's STP's future? Is Talk Show a side
project or a serious effort to establish a new band?
<p>
	    The intense interest in STP is somewhat ironic since most
critics just wished the band would go away. Now that it has gone
away, everyone wants to know if it will get back together again.
<p>
	    In an interview with Reuters, the DeLeo brothers were hardly
bullish about the prospects of an STP resurrection, and Rob
especially displayed antipathy toward Weiland. By their account,
all the DeLeos ever wanted to do was make music, Weiland was
getting in the way, and Talk Show is their new music-making
vehicle.
<p>
	    &quot;We just wanna make records, man,&quot; Dean DeLeo said.
&quot;Three records in six years? I wanna do a record a year, two a
year.&quot;
<p>
	    &quot;We were in an unproductive situation,&quot; Rob adds, with
some understatement.
<p>
	    After forming in the late 1980s, STP hit it big with its
1992 debut album, &quot;Core.&quot; Although critics accused the group
of being Seattle wannabes, fans didn't mind: they bought 4.2
million copies of the album thanks to heavy radio play of such
songs as &quot;Plush&quot; and &quot;Sex Type Thing.&quot;
<p>
	    The 1994 follow-up, &quot;Purple,&quot; overcame the so-called
&quot;sophomore jinx,&quot; by selling 3.9 million copies.
<p>
	    Just as the band seemed on the verge of conquering the
world, Weiland got addicted to heroin, got busted several times
and entered rehab several times.
<p>
	    The 1996 album &quot;Tiny Music ... Songs from the Vatican Gift
Shop&quot; received decent notices but sold 1.4 million copies,
hampered by the band's inability to tour because of Weiland's
problems.
<p>
	    Talk Show started coming together in May 1995, several
months after the first attempt to record &quot;Tiny Music&quot; ended in
failure. Rob DeLeo called Coutts out of curiosity, having seen
him play locally about eight years earlier.
<p>
	    It turned out that Coutts lived nearby, so Rob invited him
to record a demo with the Weiland-less STP. Several months
later, when STP kick-started the &quot;Tiny Music&quot; project, the
DeLeos set aside a bunch of song ideas for Talk Show.
<p>
	    The album was recorded in the space of four weeks in July
and August of 1996, several months after &quot;Tiny Music&quot; was
released, and represents more of a collaborative effort than
STP. In that band, Weiland wrote all the lyrics, while the
brothers dominated the music side. In Talk Show, the credits are
shared relatively evenly.
<p>
	    The DeLeos said they had no problem slotting in a different
singer, and the healthy recording environment was a pleasant
change.
<p>
	    &quot;It was kind of a questioning (environment), working with
someone you've never worked with before,&quot; Rob DeLeo said. &quot;But
I think we fed off of that curiosity of what was to become of
the record. I think a lot of that purity and a lot of that
optimism shows on the record. There's some hurt ... We just
threw it all in the kettle.&quot;
<p>
	    The album has the inescapable sound of an STP record, and
the lyrics -- written mostly by Coutts with help from drummer
Kretz -- are just as opaque. Looking for any references to
Weiland is a fairly pointless exercise.
<p>
	    &quot;I hate explaining lyrics, man,&quot; Dean DeLeo said. &quot;Just
let people get out of it what they want. Maybe one day in 27
years from now, we'll tell 'em.&quot;
<p>
	    But for those who can't wait that long, what is their
relationship with Weiland?
<p>
	    &quot;I have learned that you don't necessarily need to be
friends to make music together,&quot; Rob DeLeo said. &quot;We don't
hang out. I love the guy deeply on a level that probably some
people wouldn't understand. It's an understanding as a musical
bond.&quot;
<p>
	    Although Dean DeLeo considers Weiland a friend, he does not
socialize with him. Still, he said Weiland called to say he
liked the Talk Show album.
<p>
	    &quot;And I was like, 'Really? You like it?' And he said,
'Yeah.' And I said, 'Well that really means a lot to me.&quot;'
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970916/entertainment/stories/music_lozenges_1.html><b>BBC Takes The Cough Out Of Concerts</b></a>
<br>
Next Story: <a href=/text/headlines/970916/entertainment/stories/review_chinese_1.html><b>REVIEW/FILM: 'Chinese Box' Best Left Unopened</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

