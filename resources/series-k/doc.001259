http://www.yahoo.com/text/headlines/970917/entertainment/stories/film_lange_1.html
<html>
<head>
<title>Yahoo! - FEATURE: Actress Jessica Lange Loves Quiet Life, Volatile Roles</title>
</head>
<body>
<!-- ANPA File: working/0874511940-0000039328 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970917/entertainment/stories/people_fugitive_1.html><b>Body Of Dr. Sheppard Dug Up For DNA Testing In Ohio</b></a>
<br>
Next Story: <a href=/text/headlines/970917/entertainment/stories/music_hendrix_1.html><b>Hendrix Guitar Fails To Sell At British Auction</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874511940 -->
Wednesday September 17 11:59 AM EDT
</strong>
<h2>FEATURE: Actress Jessica Lange Loves Quiet Life, Volatile Roles</h2>
<!-- TextStart -->
<p>
	    By Sue Zeidler
<p>
	    LOS ANGELES (Reuter) - Jessica Lange is glad the wild days
of her youth are over, but the Academy Award-winning actress
gets a kick out of playing characters who are crazed.
<p>
	    &quot;A character who is very normal is less interesting to me
because it doesn't allow you the flights of the imagination that
playing someone on the verge of madness does,&quot; said Lange, who
at 48 exudes the glamorous allure that first earned her fame
more than 20 years ago.
<p>
	    &quot;I like to do those parts. It's fun to indulge in those
types of actions on screen. You don't have to suffer the
repercussions you would in life because they're selfish and
destructive,&quot; the leggy, blonde mother of three told Reuters in
an interview.
<p>
	    Indeed, Lange has made a career out of playing dark,
unsettled characters, as in her Oscar-winning 1995 portrayal of
a sex-obsessed Army wife in &quot;Blue Sky,&quot; her searing
performance in &quot;Frances&quot; about the tragic life of actress
Frances Farmer and her stage rendition of the emotionally
fragile Blanche Dubois in &quot;A Streetcar Named Desire.&quot;
<p>
	    Her latest film, &quot;A Thousand Acres,&quot; opens in the United
States this week. In the film, based on the Pulitzer Prize novel
by Jane Smiley and also starring Michelle Pfeiffer, Lange plays
Ginny, the eldest of three sisters who struggles with repressed
desires as her family collapses around her.
<p>
	    While Lange's character is not as volatile as Pfeiffer's  or
as many of her past roles, she was intrigued because Ginny
evolves in the midst of her family's unfolding madness.
<p>
	   
<p>
	    SCENES FROM LIFE
<p>
	    Lange, who was born in rural Minnesota, also saw some
parallels between her own life and the film about a family on a
farm headed by an indomitable patriarch played by Jason Robards.
She recalled &quot;growing up in the shadow of some kind of
all-powerful father, someone who is tremendously charismatic and
well-thought-of and yet has all these dark currents running
through.&quot;
<p>
	    Lange's father was a teacher and traveling salesman, and the
family moved 12 times before she graduated from high school. She
has described him as extreme and volatile.        Lange
positively beams about her current family life with her husband,
playwright Sam Shepard, and three children. The family moved
back to Minnesota in 1995.
<p>
	    Nevertheless, Lange recalls fondly a more reckless time in
her life. While in her 20s, shortly after her first marriage to
Spanish experimental filmmaker and photographer Paco Grande in
1970, Lange left her husband to study mime in Paris and wound up
staying for two years, living a bohemian lifestyle.
<p>
	    &quot;I'm really glad that I lived those years the way I did.
They were wild and really out there on the edge. I don't feel
like I've missed anything. I did everything that was presented
to me and pretty much to its fullest degree,&quot; she said.
<p>
	    &quot;But I'm glad I'm not living at that kind of velocity
anymore. I would never be able to sustain that kind of energy
output, although now the energy is channeled toward my family
and raising children,&quot; she said with a smile.
<p>
	    Lange returned from Paris in the early 1970s for a brief
reconciliation with Grande before they went their separate ways.
They were not formally divorced until 1982, the year she fell in
love with Shepard.
<p>
	   
<p>
	    BIG BREAK WITH 'KING KONG'
<p>
	    During the 1970s, Lange pursued various romances as well as
an acting career in New York. She got her big break in 1976 with
a lead role in the big-budget remake of &quot;King Kong,&quot; which was
panned by critics.
<p>
	    She went on to star in various films after that but was
perhaps better known during the 1970s for her love life,
including a seven-year romance with ballet star Mikhail
Baryshnikov, whom she met in 1976.
<p>
	    While the two never married, that romance produced endless
tabloid gossip as well a daughter named Alexandra, who lives
with Lange and Shepard and their two children.
<p>
	    The role that finally earned Lange critical recognition
was &quot;Frances&quot; in 1982. A year later, she earned her first
Oscar, for best supporting actress in the comedy &quot;Tootsie,&quot;
and she said she would like to work on another comedy.
<p>
	    She describes herself as an extremely emotional person who
in the past has been predisposed to depression and loneliness.
<p>
	    &quot;I'm a very emotional person. I wish I wasn't quite so
emotional ... I think it really gets in the way,&quot; she said.
<p>
	    Lange recently completed another film, &quot;Bloodline,&quot; which
will be released in 1998.
<p>
	    &quot;It's a film about a mother's absolutely obsessive love of
her son,&quot; she said. &quot;The character is certifiably nuts. She's
really psychotic and quite malevolent. She's probably the
maddest of anyone I've ever played. Actually, it was quite
fun.&quot;
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970917/entertainment/stories/people_fugitive_1.html><b>Body Of Dr. Sheppard Dug Up For DNA Testing In Ohio</b></a>
<br>
Next Story: <a href=/text/headlines/970917/entertainment/stories/music_hendrix_1.html><b>Hendrix Guitar Fails To Sell At British Auction</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

