http://www.yahoo.com/text/headlines/970912/entertainment/stories/review_dalloway_1.html
<html>
<head>
<title>Yahoo! - REVIEW/FILM: Redgrave Shines In 'Mrs. Dalloway'</title>
</head>
<body>
<!-- ANPA File: working/0874066960-0000029831 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970912/entertainment/stories/review_bowie_2.html><b>REVIEW/PERFORMANCE: Bowie Thrills At Club Gig</b></a>
<br>
Next Story: <a href=/text/headlines/970912/entertainment/stories/variety_insided_3.html><b>Inside Daily Variety: September 12</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874066920 -->
Friday September 12  8:22 AM EDT
</strong>
<h2>REVIEW/FILM: Redgrave Shines In 'Mrs. Dalloway'</h2>
<!-- TextStart -->
<p>
	    Mrs. Dalloway (Period romantic drama, color, PG-13, 1:37)
<p>
	    By Emanuel Levy
<p>
	    TORONTO (Variety) - Whatever reservations literary and film
critics may have about Marleen Gorris' screen adaptation of
&quot;Mrs. Dalloway,&quot; arguably Virginia Woolf's masterpiece, one
thing is beyond doubt: Vanessa Redgrave's towering performance
in the title role.
<p>
	    Perfectly cast, Redgrave portrays a middle-aged society
lady, a perfect &quot;hostess&quot; who, thrown into a crisis, reflects
upon her life. A highly romantic, deeply melancholy drama, the
film offers psychological and existential insights about the
inevitable effects -- and price -- of life choices. Well
executed, and engaging for the most part, pic should appeal to
supporters of literary adaptations and costumers, with the bonus
of the growing circle of Woolf's fans.
<p>
	    In the end credits, Gorris thanks Ismail Merchant and James
Ivory; indeed, the film visually approximates the smooth,
cautious and genteel style used by the veteran team in their
literary cinema, and lacks the roughness and audacity of
Orlando, Sally Potter's version of another Woolf novel.
Nonetheless, feminist director Gorris imbues the story with a
modernist interpretation, bringing to the surface issues of
sexual politics and sexual identity that were more latent in the
book. In this respect, Mrs. Dalloway is directly linked to
Gorris' hard-core feminist expose, A Question of Silence, and to
her Oscar-winning Antonia's Line. All three films deal with the
emergence of the modern woman.
<p>
	    The first reel is rather weak and less involving than the
rest, a combined result of the introduction of dozens of
characters and the constant shifts through flashbacks from
present to past and back again. While the time transitions are
gracefully engineered, the numerous cuts interfere with the flow
of the story.
<p>
	    Set during the course of one fateful day in June 1923, tale
begins with the Mrs. Dalloway's extensive preparations for the
&quot;perfect&quot; party she is hosting that night. Walking down Bond
Street, she reflects upon her life, sharing with the audience
her innermost thoughts and feelings. She is particularly
troubled by the decision she made 30 years ago to engage in a
safe, comfortable marriage to a successful, bourgeois
politician.
<p>
	    Introspective shots reveal a young, vibrant Clarissa
Dalloway (played by the exquisitely photogenic Natascha
McElhone) romantically involved with Peter (Alan Cox), who wants
to marry her, and intimately bonded with the dynamic, outspoken
Sally (Lena Headey). Gorris accentuates the physical intimacy
between Clarissa and Sally in a disturbing scene that leaves the
former bewildered.
<p>
	    Mrs. Dalloway's story, past and present, is intercut with
that of Septimus (Rupert Graves, in a touching performance), a
shellshocked vet of the Great War who's diagnosed as
&quot;unstable&quot; by the medical establishment. Married to an adoring
Italian wife, who tries but can't save him from his demons, he
takes his life when his unfeeling doctor suggests putting him in
an asylum. Gorris repeats a haunting image, one in which
Septimus observes the death of a mate in combat, and is
extremely perceptive in dealing with the primitive state of
&quot;scientific&quot; knowledge concerning madness.
<p>
	    Indeed, what makes &quot;Mrs. Dalloway&quot; especially poignant is
the detailed attention scenarist Eileen Atkins pays to the
values, mores, biases and hypocrisies of urban British society
in the 1920s. Observations on the ruthless class system,
snobbery of the wealthy, discrimination against foreigners and
minorities, and religious fanaticism are evident in almost each
of the film's priceless vignettes.
<p>
	    The saga builds toward the big party, which occupies the
last reel and is nothing short of stunning. It's here that
Gorris' staging excels and Redgrave shines, as Mrs. Dalloway
greets her guests and engages in secret commentary about them.
There is inevitable sadness in observing the older Sally, now
Lady Rosseter (Sarah Badel), a tempestuous woman who vowed to
lead a bohemian life but instead ended up an upper-middle-class
housewife with five children.
<p>
	    The piece de resistance is the long, heartbreaking monologue
that Redgrave delivers upon learning of the suicide of Septimus,
whom she never met but with whom she empathizes. As in
&quot;Orlando,&quot; Woolf put a lot of her own personality into a male
character, here in the unstable Septimus. Woolf suggests that
Mrs. Dalloway is seeing the &quot;truth&quot; whereas Septimus is seeing
the &quot;insane truth.&quot; In the end, it is Mrs. Dalloway's
humanistic vision and reaffirmation of life that dominates and
hovers over her doubts.
<p>
	    The versatile, luminously beautiful Redgrave renders the
kind of performance that is often described as delicious in
theater circles. With her richly musical and resonant voice, she
conveys brilliantly Mrs. Dalloway's changing emotions: from
exultation to introspection, from deep confusion and irritation
to emphatic reconciliation. Remainder of the ensemble, which
includes veterans of the British stage and television, is
universally impressive.
<p>
	   
<p>
	    Mrs. Dalloway ................. Vanessa Redgrave
<p>
	    Clarissa Dalloway ............. Natascha McElhone
<p>
	    Septimus Warren Smith ......... Rupert Graves
<p>
	    Peter Walsh ................... Michael Kitchen
<p>
	    Richard Dalloway .............. John Standing
<p>
	    Young Peter ................... Alan Cox
<p>
	    Young Sally ................... Lena Headey
<p>
	    Lady Bruton ................... Margaret Tyzack
<p>
	    Lady Rosseter (Sally Seton) ... Sarah Badel
<p>
	   
<p>
	    A First Look Pictures release. Produced by Stephen Bayly,
Lisa Katselas Pare. Co-producers, Chris J. Ball, William Tyrer,
Simon Curtis, Bill Shepherd.
<p>
	    Directed by Marleen Gorris. Screenplay, Eileen Atkins, based
on the novel by Virginia Woolf. Camera (color), Sue Gibson;
editor, Michiel Reichwein; music, Ilona Sekacz; production
design, David Richens; art direction, Alison Wratten, Nik
Callan; set decoration, Carlotta Barrow, Jeanne Vertigan;
costume design, Judy Pepperdine; sound (Dolby), Peter Glossop,
Brian Simmons; associate producer, Paul Frift; assistant
director, Richard Whelan; casting, Celestia Fox. Reviewed at
Toronto Film Festival, Sept. 9, 1997.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970912/entertainment/stories/review_bowie_2.html><b>REVIEW/PERFORMANCE: Bowie Thrills At Club Gig</b></a>
<br>
Next Story: <a href=/text/headlines/970912/entertainment/stories/variety_insided_3.html><b>Inside Daily Variety: September 12</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

