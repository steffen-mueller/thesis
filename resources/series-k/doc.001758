http://www.yahoo.com/text/headlines/970905/entertainment/stories/review_game_1.html
<html>
<head>
<title>Yahoo! - REVIEW/FILM: 'The Game' Plays With Taut Suspense</title>
</head>
<body>
<!-- ANPA File: working/0873461708-0000016699 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970905/entertainment/stories/review_felon_1.html><b>REVIEW/CABLE: 'First-Time Felon' Reaches For The Heart</b></a>
<br>
Next Story: <a href=/text/headlines/970905/entertainment/stories/review_fire_1.html><b>REVIEW/FILM: 'Fire Down Below' Generates More Smoke Than Heat</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873461700 -->
Friday September  5  8:15 AM EDT
</strong>
<h2>REVIEW/FILM: 'The Game' Plays With Taut Suspense</h2>
<!-- TextStart -->
<p>
	    The Game (Mystery thriller, color, R, 2:08)
<p>
	    By Todd McCarthy
<p>
	    HOLLYWOOD (Variety) - An unsettling, paranoia-inducing
suspenser about an extreme form of invasion of privacy, &quot;The
Game&quot; is a high-toned mind-game of a movie.
<p>
	    Crafted with a commanding, aloof precision by David Fincher
in his first outing since hitting the jackpot with &quot;Seven,&quot;
this unusual dive into the ambiguous world of an undefined
pastime without apparent rules generates a chilly intellectual
intrigue that will arouse buffs, trendies and techies more than
it will mainstream audiences.
<p>
	    This first release by Polygram's newly formed domestic
distribution arm is likely to stir initial box office interest
in urban and more upscale markets, but doesn't look to be more
than a middle-distance runner in general situations.
<p>
	    Despite its profound darkness and gruesomeness, &quot;Seven&quot;
overcame these seeming commercial liabilities due to its genuine
distinction and the star turns of Brad Pitt and Morgan Freeman,
the latter of whom contributed some all-important warmth.
<p>
	    &quot;The Game&quot; projects the same sense of suffocating
enclosure and mounting despair in a style that will inevitably
be compared to that of Stanley Kubrick in its steely technical
mastery and remote, disenchanted world view, all in the service
of a story that resembles a highbrow puzzle as much as it does
an involving narrative.
<p>
	    With a nod to his Oscar-winning role of Gordon Gekko in
&quot;Wall Street,&quot; Michael Douglas here plays Nicholas Van Orton,
a fabulously wealthy San Francisco investment banker whose
forbidding coldness sets the tone for the picture.
<p>
	    Ruthless in his business dealings and curt, even cutting in
his personal interactions, the divorced, childless man is so
antisocial that he prefers to dine alone in his opulent mansion
while watching financial reports on CNN, even on his birthday.
<p>
	    This year he has reached the same age, 48, his father was
when the man jumped to his death before young Nicholas' eyes.
Nicholas' younger brother, Conrad (Sean Penn), has gone the
opposite route into disreputable addictions of all kinds.
<p>
	    But when the jumpy fellow turns up after a long absence and
offers Nicholas a card giving him entree to unusual
entertainment courtesy of something called Consumer Recreation
Services, the behaviorally rigid businessman eventually succumbs
to curiosity and checks it out.
<p>
	    The setup provided by John Brancato and Michael Ferris in
their provocative script draws in the viewer just as inevitably
as it does Nicholas.
<p>
	    Interviewing with CRS rep Jim Feingold (James Rebhorn) in
the company's plush offices, Nicholas submits to extensive tests
to determine just what sort of vacation he will experience.
<p>
	    We provide -- whatever is lacking, Feingold teasingly
explains, and a colleague who has already been through it
further tantalizes the skeptical Nicholas by quoting
scripture: &quot;Whereas once I was blind, now I can see.&quot;
<p>
	    Thus primed for an unusual trip, Nicholas, who is accustomed
to having everything exactly his own way, is stunned to be
informed that CRS has rejected him.
<p>
	    That night, however, in a darkly funny sequence, a Daniel
Schorr newscast becomes oddly split between routine announcing
and direct address to Nicholas; it is merely the first of many
increasingly disorienting and ultimately threatening events that
turn Nicholas' life upside down and inside out.
<p>
	    Early on, it becomes clear that one of the film's main
themes is the cracking of complacency, the disruption of the
meticulous control its string-pulling protagonist exercises over
virtually aspect of his life.
<p>
	    Nicholas continues to go about his business, engineering the
ouster of a distinguished old publisher (Armin Mueller-Stahl)
who was a close friend of his father, and persists in callous
treatment of other people. But untoward events begin besetting
him -- waiters spill things on him, a CRS pen stains his shirt,
his briefcase won't open -- to the point that he decides that
the Game is in force, and that everything that happens to him is
a prank.
<p>
	    Nicholas makes a human connection of sorts when an offending
waitress, Christine (Deborah Kara Unger), admits that she was
paid to spill drinks on him. Matters take more sinister turns
when CRS' offices appear to have vanished, compromising
photographs of Nicholas and a woman very like Christine are
found in a hotel room Nicholas never occupied, a private
investigator is caught following him, and Nicholas' home is
disfigured by graffiti and black light.
<p>
	    From here, Nicholas is sucked into a vortex of complete
paranoia and uncertainty. After being fired upon by apparent CRS
hit men and drugged, he eventually wakes up somewhere in Latin
America as a penniless nobody.
<p>
	    The sense of entitlement and assurance to which he was
always accustomed is gone, and the final half-hour is devoted to
his desperate attempt to get to the bottom of what has befallen
him, to determine what is real and what is not, and to know who
is responsible for his weird trip; as he says, &quot;I want to meet
the wizard.&quot;
<p>
	    The Alice in Wonderland aspect to this odyssey without road
map is explicitly suggested by the use of the Jefferson
Airplane's White Rabbitas the film's nominal theme song, and the
viewer is adroitly kept in the dark as long as Nicholas is when
it comes to knowing where, if anywhere, the limits to the Game
exist.
<p>
	    The film itself is limited by the material's nature as a
brainy exercise and by its narrow focus; individual response
will depend upon how tantalized one is by puzzles and games, as
well as upon how off-putting one finds the central character,
who is center-stage throughout.
<p>
	    But the film is more than a technical exercise, as the
overriding notion of control vs. chaos is actively engaged
throughout, with the specter of the total loss of privacy always
lurking in the background.
<p>
	    Regardless of how far one chooses to buy into The Game-- and
the ending ambiguously suggests that it could go on and on --
there is no doubt as to Fincher's staggering expertise as a
director and his almost clinical sense of precision.
<p>
	    Every effect seems utterly planned and predetermined, as in
the Game itself, with nothing left to chance. Nicholas'
pampered, cloistered lifestyle could not be more fully
expressed, and the small increments by which his existence is
eroded are unnervingly conveyed. It's a film one can easily
admire without exactly embracing it.
<p>
	    In addition to Wall Street,Douglas' performance here reminds
at times of Falling Downin its function as a prism reflecting
society's pressures and extremes.
<p>
	    The actor not only has no problem projecting Nicholas'
hardness and authority, but he deftly notes the man's discomfort
at having to deal with any human frailty or vulnerable emotion.
He carries the picture well.
<p>
	    Penn pops up only intermittently, his quicksilver
personality used to contrast the ne'er-do-well brother with his
control-freak sibling. As the initially affable CRS functionary,
Rebhorn stands out amongst the rest of the well-chosen cast
members.
<p>
	    Technically, the film is immaculate, with Harris Savides'
sleek lensing, Jeffrey Beecroft's rich production design, James
Haygood's intricate editing, Howard Shore's brooding score and
the dense sound work all making important contributions to a
work carefully calculated to the tiniest detail.
<p>
	   
<p>
	    Nicholas Van Orton..........  Michael Douglas
<p>
	    Conrad......................  Sean Penn
<p>
	    Christine.................... Deborah Kara Unger
<p>
	    Jim Feingold................  James Rebhorn
<p>
	    Samuel Sutherland...........  Peter Donat
<p>
	    Ilsa......................... Carroll Baker
<p>
	    Elizabeth...................  Anna Katarina
<p>
	    Anson Baer..................  Armin Mueller-Stahl
<p>
	   
<p>
	    A Polygram Films release of a Polygram Filmed Entertainment
presentation of a Propaganda Films production. Produced by Steve
Golin, Cean Chaffin. Executive producer, Jonathan Mostow.
Co-producers, John Brancato, Michael Ferris.
<p>
	    Directed by David Fincher. Screenplay, John Brancato,
Michael Ferris. Camera (Technicolor, Panavision widescreen),
Harris Savides; editor, James Haygood; music, Howard Shore;
production design, Jeffrey Beecroft; supervising art director,
Jim Murakami; art direction, Steven Saklad; set design, Alan
Kaye; set decoration, Jackie Carr; costume design, Michael
Kaplan; sound (DTS-Dolby SR-SDDS), Willie Burton; sound design,
Ren Klyce; visual effects supervisor, Kevin Haug; assistant
director, Yudi Bennett; second unit camera, Jeff Cronenweth;
casting, Don Phillips.
<p>
	    (Reviewed at the Polygram screening room, Beverly Hills,
Sept. 2.)
<p>
	    Reuters/Variety
	
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970905/entertainment/stories/review_felon_1.html><b>REVIEW/CABLE: 'First-Time Felon' Reaches For The Heart</b></a>
<br>
Next Story: <a href=/text/headlines/970905/entertainment/stories/review_fire_1.html><b>REVIEW/FILM: 'Fire Down Below' Generates More Smoke Than Heat</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

