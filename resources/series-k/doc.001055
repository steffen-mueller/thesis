http://www.yahoo.com/text/headlines/970923/entertainment/stories/review_good_1.html
<html>
<head>
<title>Yahoo! - REVIEW/STAGE: 'Good As New' Covers Old Ground</title>
</head>
<body>
<!-- ANPA File: working/0875017902-0000049095 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970923/entertainment/stories/review_danza_1.html><b>REVIEW/TELEVISION: 'Tony Danza' Utterly Banal</b></a>
<br>
Next Story: <a href=/text/headlines/970924/entertainment/stories/film_zeitgeist_1.html><b>Zeitgeist Picks Up Palme d'Or 'Cherries'</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 875017860 -->
Tuesday September 23  8:31 AM EDT
</strong>
<h2>REVIEW/STAGE: 'Good As New' Covers Old Ground</h2>
<!-- TextStart -->
<p>
	    Good as New (South Coast Repertory Second Stage; 161 seats;
$41 top; 2:00)
<p>
	    By Charles Isherwood
<p>
	    HOLLYWOOD (Variety) - A wry, somber comedy about some small
explosions rocking the remains of a nuclear family, Peter
Hedges' &quot;Good as New&quot; tartly observes how a few home truths
can turn youthful idealism to disillusion.
<p>
	    Yet, it's a subject that's hardly new and the execution
isn't quite as good as its initial promise.
<p>
	    The first act is a trio of tete-a-tetes in the front seat of
a car on 16-year-old Maggie's (Robin Mary Florence) first day at
the wheel. Driving her father (she calls him Dennis) to the
airport, the spunky, self-possessed young woman tries to inspire
her laconic dad with some enthusiasm for the speech he's about
to give. With her bright eyes just as often turned adoringly on
him as on the road, she recalls the galvanizing effect his
accomplished record as a civil-rights lawyer had on her
classmates when he gave a similar speech at her school.
<p>
	    There are delicately placed glints of foreboding in her
patter. She remembers the answer he advised the kids to give
when asked about future plans -- &quot;I expect to be surprised&quot; --
and interlaces some golden rules of driving into her spiel, one
of which -- &quot;It's when you feel safest that you're least safe&quot;
-- she thinks she may have invented.
<p>
	    Later, as Maggie drives her mom Jan (Linda Gehringer) home
from that morning's facelift, she goes on the offensive,
accusing Jan of betraying the ideals she once held in succumbing
to the temptation to go under the knife. With the merciless
self-righteousness of youth, Maggie calls her mother's facelift
a form of lying, and says proudly, &quot;I want age to happen to
me!&quot; Jan's dry response: &quot;And guess what? You'll have that
choice.&quot; Worn out by Maggie's onslaught, Jan lets slip that her
decision may have been motivated by her belief that Dennis is
having an affair. &quot;You wanted truth,she bitterly adds.&quot;
<p>
	    In the third car conversation, Maggie confronts her father,
who is instantly angered that Jan would involve Maggie --
thereby indirectly confirming Jan's suspicions. As Maggie fights
back tears of disbelief, Dennis adds that Jan, too, had once
strayed.
<p>
	    But the carefully structured first act, which builds
smoothly to its emotional climax, gives way to a second act that
takes far too long to get back to the heart of the story. A
funny and marvelously delivered monologue from Jan about
cosmetic surgery (Frank Gifford's face is like a pie crust with
eye holes) winds on aimlessly, as the play dances around its
emotional center for what seems like an hour before ending with
a series of short, hysterical bursts of anguished truth-telling
and recrimination that are distinguished from other
divorce-drama theatrics only by a slightly lurid excess.
<p>
	    That girl in the driver's seat is problematic, as well.
Florence is patently unconvincing as a girl of 16 (the actress
is a college graduate, her bio informs), and her game efforts to
impersonate the brash perkiness of a dangerously inquisitive
teenager serve to point up the rather too-poised and polished
dialogue of this girl, whip-smart though she may be. An actress
closer to the character's age might have been able to work
against this problem in the writing with a more natural juvenile
mien; Florence is too much a trained actress. The strain shows,
and the character never takes on the jagged contours of a real
teenager.
<p>
	    Stephen Rowe has the least to do as Dennis, but he makes the
most of his moments, revealing with precise comic timing that
his mistress is &quot;Almost 30 -- 26 -- next month.&quot; It's
Gehringer's performance that carries the play. With her
red-ringed eyes peering warily out from a face wrapped in
bandages, she makes casually but achingly understandable how a
woman of intelligence and spirit brings herself to go under the
knife. Neither desperate nor delusional, she's clear-eyed and
resigned about the world she lives in, and Ehringer movingly
conveys the pathos of her simple need to have people continue to
look at her in the way a young woman is looked at.
<p>
	    Martin Benson's direction is smooth and effective, though
that central piece of miscasting must be laid at his door. Tony
Fanning's act one set, a jumble of colorful Chicago-area road
signs, is delightful.
<p>
	   
<p>
	    Cast: Robin Mary Florence (Maggie), Stephen Rowe (Dennis),
Linda Gehringer (Jan).
<p>
	   
<p>
	    South Coast Repertory presents a play in two acts by Peter
Hedges. Directed by Martin Benson. Set, Tony Fanning; costumes,
Susan Denison Geller; lighting, York Kennedy; sound, B.C.
Keller; music, David Van Tieghem; production manager, Michael
Mora; stage manager, Randall K. Lum. Opened, reviewed Sept. 20,
1997; runs through Oct. 19.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970923/entertainment/stories/review_danza_1.html><b>REVIEW/TELEVISION: 'Tony Danza' Utterly Banal</b></a>
<br>
Next Story: <a href=/text/headlines/970924/entertainment/stories/film_zeitgeist_1.html><b>Zeitgeist Picks Up Palme d'Or 'Cherries'</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

