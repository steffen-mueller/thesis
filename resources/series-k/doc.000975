http://www.yahoo.com/text/headlines/970924/entertainment/stories/online_livewire_1.html
<html>
<head>
<title>Yahoo! - LIVEWIRE: Encryption Primer -- What It Is, And Why Care</title>
</head>
<body>
<!-- ANPA File: working/0875152939-0000051940 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970924/entertainment/stories/multimedia_programs_1.html><b>PROGRAMS: Barbie And British Trivia</b></a>
<br>
Next Story: <a href=/text/headlines/970924/entertainment/stories/media_peru_2.html><b>Peruvian Media Owners Create Body To Defend Rights</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 875152920 -->
Wednesday September 24 10:02 PM EDT
</strong>
<h2>LIVEWIRE: Encryption Primer -- What It Is, And Why Care</h2>
<!-- TextStart -->
<p>
	    By Michelle V. Rafter
<p>
	    LOS ANGELES (Reuter) - The long-running debate over
encryption of Internet communications is heating up again as a
bill that would expand government control of the technology
winds through Congress.
<p>
	    But what does controlling encryption matter to the average
Joe?
<p>
	    The answer is plenty, whether you're for it or against it,
and whether you use the Internet or not.
<p>
	    Law enforcement authorities led by the FBI advocate tighter
domestic and export controls, including a back door into present
encryption schemes to covertly track and thwart criminals and
terrorists.
<p>
	    But civil liberties and cyber-rights groups believe proposed
controls are unworkable, unnecessary, unconstitutional and a
threat to the future of the Internet. They argue that strong
encryption is more important than ever for Internet users, and
non-users.
<p>
	    &quot;Even people who don't use the Internet will be impacted as
more information goes online,&quot; said Jonah Seiger,
communications director for the Center for Democracy and
Technology (<a href="http://www.cdt.org">http://www.cdt.org</a>), the Washington D.C.
public-interest group opposing to the proposed legislation. &quot;If
your medical records are being transmitted online and they're
not encrypted, you might find them posted to a news group
somewhere.&quot;
<p>
	    Encryption scrambles digital information, making it
unreadable without a password or software &quot;key.&quot; The
public-key encryption on the Internet uses a mathematical code
as a sort of combination lock and a pair of keys to lock and
unlock coded information. An Internet user can put his public
key in a public place -- an electronic message board for example
-- and anyone can use it to encode and send a message that can
be decoded only with the user's private key.
<p>
	    Present U.S. law does not restrict encryption's use or sale
inside the country, but does limit export of encryption products
of more than 56 bits -- the length of the key -- that don't have
a key recovery system in place. Key recovery is a technological
back door that gives the police or FBI immediate real-time
access to an encrypted message without the knowledge of the
sender or receiver.
<p>
	    A recent amendment to Security and Freedom through
Encryption (SAFE) Act introduced earlier this year in the House
reverses the one-time pro-encryption bill to add mandatory key
recovery and other new controls.
<p>
	    The amendment, written by Republican Mike Oxley of Ohio and
Democrat Thomas Manton of New York, requires all encryption
software used in the United States to incorporate key recovery
by Jan. 1, 1999.
<p>
	    Manton, a former New York City policeman, was concerned the
original SAFE bill didn't adequately address the possibility
that criminals could use encryption to commit crimes and cover
their tracks, said Cinnamon Rogers, a Manton staffer.
<p>
	    &quot;There are cases now where technology has caused problems
for law enforcement,&quot; she said.
<p>
	    At least four other House committees with jurisdiction over
encryption regulation have already weighed in on the issue, with
two opting for the status quo and two for more stringent
controls. Once the Commerce committee votes, the various
versions of the bill will go before the House Rules committee,
which will settle on a single bill that could come before the
entire House in the next few months or linger until next year.
<p>
	    Meanwhile, civil liberties and cyber-rights groups are
putting up a fight. A coalition of 63 information industry and
citizens' rights groups banded together last week to oppose the
Oxley-Manton Amendment. On its own, the Center for Democracy and
Technology (CDT) said it mustered thousands of people to call
their Congressional representatives about the bill.
<p>
	    The groups maintain the controls the government seeks are
physically impossible, unconstitutional and could possibly shut
down the Internet as a medium for electronic commerce and
secured communications. They also argue that while criminals are
using encryption technology, the problem isn't as widespread as
law enforcement authorities would have people believe, and that
crooks are being caught regardless of encryption back doors.
<p>
	    &quot;You may pick up a few more bad guys through a mandatory,
ubiquitous trap door system, but you've opened up such huge
windows for government abuse, mistakes, fraud and hackers that
you've created problems far huger and more widespread than the
problem you've solved,&quot; said James X. Dempsey, a CDT senior
staff counsel.
<p>
	    If you think the FBI reading your e-mail is trouble, imagine
the IRS demanding back doors so it can secretly monitor whether
you're paying your taxes, said Jim Lucier, spokesman for the
conservative Americans for Tax Reform (<a href="http://www.atr.org">http://www.atr.org</a>).
<p>
	    &quot;That's alarming to us,&quot; Lucier said. &quot;The correct answer
is to preserve privacy rather than let the government audit your
use of your money. Steve Forbes said it best, if the government
can see and hear everything on the net, they'll want to tax and
regulate it too.&quot;
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	    (Michelle V. Rafter writes about cyberspace and technology
from Los Angeles. Reach her at mvrafter(at)deltanet.com.
Opinions expressed in this column are her own.)
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970924/entertainment/stories/multimedia_programs_1.html><b>PROGRAMS: Barbie And British Trivia</b></a>
<br>
Next Story: <a href=/text/headlines/970924/entertainment/stories/media_peru_2.html><b>Peruvian Media Owners Create Body To Defend Rights</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

