http://www.yahoo.com/text/headlines/970908/entertainment/stories/industry_bart_1.html
<html>
<head>
<title>Yahoo! - BART: Empathy May Be Counterfeit, Emotion Is Real</title>
</head>
<body>
<!-- ANPA File: working/0873721176-0000023073 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970908/entertainment/stories/review_edge_1.html><b>REVIEW/FILM: 'The Edge' A Sharp Thriller</b></a>
<br>
Next Story: <a href=/text/headlines/970908/entertainment/stories/people_murdoch_1.html><b>ABOVE THE LINE: Lachlan Murdoch, Mogul In Waiting</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873721140 -->
Monday September  8  8:19 AM EDT
</strong>
<h2>BART: Empathy May Be Counterfeit, Emotion Is Real</h2>
<!-- TextStart -->
<p>
	    By Peter Bart, Daily Variety Editor-in-Chief
<p>
	    HOLLYWOOD (Variety) -
<p>
	    SEPT. 6, 1998: It's been exactly one year since the
astonishing spectacle of Princess Diana's funeral, and, during
this period, a great deal of revisionist thinking has occurred
about that unforgettable chain of events.
<p>
	    Lesson No. 1: it was instructive to observe how quickly the
Di-Dodi media blitz ultimately turned on itself. The relentless
overcoverage of the event itself was quickly followed by the
dire warnings about what that overcoverage portended.
<p>
	    To hear the pundits tell it, the real heavies ultimately
were neither the paparazzi nor the swackered limo driver, but
rather something amorphous called the celebrity culture.
<p>
	    Intoned Maureen Dowd, columnist for the New York Times:
&quot;The celebrity culture has become a mass psychosis that has
broken out of its former confines in the entertainment industry
and has overrun institutions of authority.&quot;
<p>
	    Translation: Hollywood is messing with our heads.
<p>
	    She concluded: &quot;All that the celebrity culture teaches is a
counterfeit empathy which mistakes prurience for interest and
voyeurism for genuine human identification.&quot;
<p>
	    After this initial blast of punditry, the following
developments unfolded in the aftermath of the funeral:
<p>
	    - Dowd publicly apologized for her column, admitting that
having lived in Washington too long, she had become immune to
outpourings of genuine emotion. She also acknowledged that she
was shopping a spec screenplay titled &quot;Counterfeit Empathy,&quot;
dealing with Prince William's first romantic entanglement, with
the daughter of a Hollywood producer.
<p>
	    - Following the lead of its star columnist, the New York
Times pledged a moratorium on editorials denouncing the
&quot;celebrity culture.&quot; The newspaper emphasized its new
understanding of the need for superstars in all segments of our
star-driven society -- superstar designers, superstar chefs and,
befitting a truly egalitarian newspaper, superstar panhandlers.
<p>
	    - Acknowledging its boneheaded ignorance of the media, the
Royal Family shortly after the funeral hired the Hollywood PR
firm of PMK to help it come to terms with counterfeit empathy.
After a highly successful initial campaign, however, PMK was
dismissed when Prince William walked out in the middle of a
Barbara Walters TV interview: Walters, having failed to elicit
meaningful opinions on any topic, unctuously asked the
16-year-old to share his feelings about nocturnal emissions.
<p>
	    - Having removed themselves from the celebrity photo
business following the funeral, the Gamma, Sygma and Sipa photo
agencies were all forced into bankruptcy during the subsequent
months, only to be purchased by Rupert Murdoch.
<p>
	    - Bored with the ineffectiveness of his anti-tabloid
campaign, George Clooney acquired the National Enquirer,
converting it into an upscale journal of political opinion. By
year's end, it was, in turn, acquired by Murdoch.
<p>
	    - Los Angeles both passed and then rescinded pioneering
rules requiring a 12-foot safety zonesurrounding every
celebrity. The legislation was revoked after an ugly incident at
the Oscar ceremony when Arnold Schwarzenegger, pursued by a
flying wedge of cameras, tried to shake hands with Sly Stallone,
encased in his own wedge, resulting in the trampling of four
photographers and Army Archerd.
<p>
	    - Star-driven consumer magazines, led by People, announced a
boycott of superstar interviews to protest the interpretation of
newly invoked &quot;privacy laws.&quot; As enforced by PMK, no questions
could be posed involving love affairs or any other topics that
might elicit counterfeit empathy. Similarly, photo shoots were
banned at any star's residence. Devoid of both Princess Di and
superstar covers, People magazine's circulation tumbled, and,
late in the year, it was acquired by Murdoch.
<p>
	    - Troubled by the fact that his handsome son, Lachlan, heir
to the Murdoch empire, was being increasingly smothered by
paparazzi as a result of his romance with one of the Spice
Girls, Rupert himself announced the sale of his photo agencies
and star-driven magazines. The purchaser: Mohamed Al Fayed.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970908/entertainment/stories/review_edge_1.html><b>REVIEW/FILM: 'The Edge' A Sharp Thriller</b></a>
<br>
Next Story: <a href=/text/headlines/970908/entertainment/stories/people_murdoch_1.html><b>ABOVE THE LINE: Lachlan Murdoch, Mogul In Waiting</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

