http://www.yahoo.com/text/headlines/970910/entertainment/stories/review_apostle_1.html
<html>
<head>
<title>Yahoo! - REVIEW/FILM: Duval's 'Apostle' A Triumph</title>
</head>
<body>
<!-- ANPA File: working/0873894761-0000026612 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970910/entertainment/stories/review_marley_1.html><b>REVIEW/PERFORMANCE: Ziggy Marley & Siblings Induce Chills</b></a>
<br>
Next Story: <a href=/text/headlines/970910/entertainment/stories/review_clover_1.html><b>REVIEW/CABLE: McGovern Poignant In 'Clover'</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873894720 -->
Wednesday September 10  8:32 AM EDT
</strong>
<h2>REVIEW/FILM: Duval's 'Apostle' A Triumph</h2>
<!-- TextStart -->
<p>
	    The Apostle (Drama, color, no rating, 2:28)
<p>
	    By Emanuel Levy
<p>
	    TORONTO (Variety) - A labor of love coming to fruition after
13 years, &quot;The Apostle,&quot; Robert Duvall's third -- and best --
directorial effort, is a sharply observed exploration of a
middle-aged preacher who embarks on a redemption odyssey after
committing a crime.
<p>
	    Financed, exec produced, written and acted by Duvall, in
what is surely one of his most riveting performances, pic is a
triumph on every level but one: its excessive running time. With
the right handling and savvy marketing, this October pickup can
reach the kinds of discriminating viewers who supported such
Southern gothic tales as &quot;Sling Blade,&quot; even if its serious
treatment of religious issues makes it even more demanding than
Billy Bob Thornton's movie.
<p>
	    A devout Pentecostal preacher from New Boston, Texas,
Eulis &quot;Sonny&quot; Dewey (Duvall) lives a seemingly happy and
fulfilling life with his beautiful wife, Jessie (Farrah
Fawcett), and two children. In an early sequence, driving around
with his mother, he stops his car at a road accident and manages
to convert a badly wounded driver with his intense preaching
just minutes before the latter expires.
<p>
	    However, forced to face a series of unanticipated
adversities, Sonny's stable world crumbles. Jessie is cheating
on him with a younger minister, Horace (Todd Allen), and by
manipulating the by-laws, she succeeds in wresting control of
the church from him. Losing his beloved family and congregation,
Sonny descends into an uncontrollable rage and strikes Horace
with a bat at a softball game, where his children are playing.
When Horace falls into a coma, he flees town; in a wonderful
image that conveys deep confusion, he circles his car at a
crossroad, until deciding to get on a bus for Louisiana.
Shedding all traces of his past, Sonny chooses a new name, E.F.,
and baptizes himself as &quot;The Apostle&quot; to God, his new
identity.
<p>
	    Inevitable comparisons will be made between &quot;The
Apostle's&quot; central premise and that of &quot;Witness,&quot; in which a
city cop begins a new life in a rural Amish community. But the
similarities lie only on the surface, for, ultimately, the
psychological-moral journey taken by Duvall's character is
deeper, more disturbing and less romantic than the one in the
earlier film.
<p>
	    Landing in the predominantly black town of Bayou Boutte,
La., E.F. befriends the Rev. Blackwell (John Beasley), a former
preacher who retired because of a bad heart. With zealous
passion that often borders self-righteousness and obsession, he
persuades Blackwell to help him start up a new church. He agrees
to work as a garage mechanic for the local radio station owner,
Elmo (Rick Dial), in exchange for free air time to preach. Soon
the Apostle marshals the community to help him renovate a
run-down pastoral church.
<p>
	    Burying the frustration of not being around when his mom
dies and aggravated by the sorrow of learning that Horace had
also died, E.F. commits himself wholeheartedly to his calling:
he preaches on the radio, takes to the streets, gathers
supporters on a revamped school bus. Seeking his own salvation,
the Apostle conquers his inner demons by fervently organizing a
grass-roots church, until his estranged wife discovers his
whereabouts and informs the police.
<p>
	    Beautifully detailed and deftly structured, every scene in
&quot;The Apostle&quot; logically leads to the next one, each
elaborating on the central theme of religious redemption. As a
writer, Duvall never allows viewers to think that they know
everything there is to know about E.F. Perhaps even more
remarkably, he doesn't violate the character by summing him up:
almost every scene discloses another dimension of the preacher's
complex personality.
<p>
	    Duvall also reveals a masterly touch as director, achieving
the kind of fluid storytelling that even more experienced
filmmakers often lack. The movie contains many poignant scenes,
but at least two stand out. There is electric tension and the
imminent threat of violence when Sonny confronts his wife and
begs her not to leave him, but contrary to expectations, he
walks out quietly. In a later scene, when a racist troublemaker
(Billy Bob Thornton) arrives on a bulldozer to destroy the
church, the Apostle places his Bible on the ground and embraces
the hoodlum with his sermon, eventually dissuading him from his
spiteful intent.
<p>
	    It's hard to imagine anyone but Duvall in the title role,
which bears slight resemblance to his Oscar-winning turn in
&quot;Tender Mercies,&quot; a film that dealt with a country singer's
redemption. But here, Duvall renders an even more superlative
and modulated performance, one that allows the audience to feel
an immediate, emphatic connection with his character, even when
his motives or conduct are dubious.
<p>
	    While the central role dominates the proceedings, the other
thesps do well in the same emotionally truthful vein, including
Farrah Fawcett as the wife who deserts him, Miranda Richardson
as a gentle neighbor who's attracted to E.F., Thornton as the
bigot, Billy Joe Shaver as old friend Joe, and June Carter Cash
as his mom.
<p>
	    While tech credits are good across the board, running time
presents a problem; a streamlining of perhaps 20 minutes seems
possible without causing any damage to the film's integrity.
<p>
	   
<p>
	    The Apostle E.F. ....... Robert Duvall
<p>
	    Jessie Dewey ........... Farrah Fawcett
<p>
	    Toosie ................. Miranda Richardson
<p>
	    Horace ................. Todd Allen
<p>
	    Brother Blackwell ...... John Beasley
<p>
	    Mrs. Dewey Sr. ......... June Carter Cash
<p>
	    Sam .................... Walton Goggins
<p>
	    Joe .................... Billy Joe Shaver
<p>
	    Troublemaker ........... Billy Bob Thornton
<p>
	    Elmo ................... Rick Dial
<p>
	   
<p>
	    An October Films release of Butchers Run Films production.
Produced by Rob Carliner. Executive producer, Robert Duvall.
Co-producer, Steven Brown.
<p>
	    Directed, written by Robert Duvall. Camera (CFI color),
Barry Markowitz; editor, Steve Mack; music, David Mansfield;
production design, Linda Burton; art direction, Irfan Akdag; set
decoration, Lori Johnson, Dea Jensen; costume design, Douglas
Hall; sound (Dolby), Steve C. Aaron; associate producer, Ed
Johnston; assistant director, Louis Shaw Milito; casting, Renee
Rousselot, Ed Johnston. Reviewed at Toronto Film Festival, Sept.
8, 1997.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970910/entertainment/stories/review_marley_1.html><b>REVIEW/PERFORMANCE: Ziggy Marley & Siblings Induce Chills</b></a>
<br>
Next Story: <a href=/text/headlines/970910/entertainment/stories/review_clover_1.html><b>REVIEW/CABLE: McGovern Poignant In 'Clover'</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

