http://www.yahoo.com/text/headlines/971009/entertainment/stories/film_almodovar_1.html
<html>

<head>
<title>Yahoo! - FEATURE: Sex, Guns And Glitz: Almodovar Is Back!</title>
</head>
<body>
<!-- ANPA File: working/0876422394-0000076861 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971009/entertainment/stories/stage_poe_1.html><b>FEATURE: Did Poe Show At Chicago Seance?</b></a>
<br>
Next Story: <a href=/text/headlines/971009/entertainment/stories/television_bbc_1.html><b>FEATURE: BBC Plans For Future On 75th Birthday</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 876422340 -->
Thursday October  9  2:39 PM EDT
</strong>
<h2>FEATURE: Sex, Guns And Glitz: Almodovar Is Back!</h2>
<!-- TextStart -->
<p>
	    By Merissa Marr
<p>
	    MADRID, Spain (Reuter) - Eroticism, guilt, fluorescent green
platform shoes, drug abuse -- the latest film from Spain's king
of kitsch, Pedro Almodovar, has all the ingredients his faithful
fans have come to love.
<p>
	    After two years in hiding, the 48-year-old filmmaker has
come out with &quot;Live Flesh,&quot; a thriller based on a Ruth Rendell
novel that explores a triangle of passion involving a paraplegic
policeman, his wife and an ex-con.
<p>
	    &quot;This is the most disturbing film I have made, and it is
the one that has disturbed me the most,&quot; Almodovar said at a
preview. &quot;'Live Flesh' is an intense drama, baroque and
sensual, that has the quality of a thriller and a classic
tragedy. ... It's a very sexy film.&quot;
<p>
	    The movie traces the fortunes of Victor, the son of a
prostitute, who was born on a Madrid bus in 1970. After his
wrongful imprisonment 26 years later in the shooting of a
policeman, he sets out to take revenge.
<p>
	    Disturbing it may be, but the subject matter is not as
dubious as some of Almodovar's previous films. In &quot;Kika,&quot; he
caused a storm of controversy by including a 12-minute rape
scene and portraying the victim as having enjoyed it.
<p>
	    He also outraged feminists with &quot;Tie Me Up, Tie Me Down,&quot;
which depicts a bound-and-gagged woman who falls in love with
her captor.
<p>
	    But such controversy has done the village boy from Castille
no harm, and he has become Spain's most commercially successful
filmmaker.
<p>
	    His comedy &quot;Women On the Verge of a Nervous Breakdown,&quot;
which shot him to fame abroad, was the most successful foreign
film of 1989 and earned an Oscar nomination. Hollywood bought
the rights, and Jane Fonda, Whoopie Goldberg and Sally Field
fought for the lead role.
<p>
	   
<p>
	    HAS FILMMAKER MATURED?
<p>
	    &quot;The Flower of My Secret,&quot; his most recent production
before &quot;Live Flesh,&quot; departed from the usual mix of kitsch and
sex that characterized his earlier films -- perhaps a sign he
had matured, critics said. Although &quot;Live Flesh&quot; also has been
heralded as mature, it revives some of the melodrama and
hedonism of his earlier films.
<p>
	    Almodovar pays homage to two icons of Spanish society in the
production: filmmaker Luis Bunuel and democracy.
<p>
	    &quot;I have made an homage to Bunuel not deliberately but
unconsciously, which is better,&quot; he said. Almodovar has often
been compared to Bunuel, the distinguished director who produced
films with strong Spanish roots while in exile in France and
Mexico.
<p>
	    Almodovar's film plays on Bunuel's fascination with women's
feet and includes clips from one of his films.
<p>
	    For the first time, Almodovar also confronts political
issues in his film -- specifically the democratization of Spain
following nearly 40 years of dictatorship under Francisco
Franco.
<p>
	    In previous films, the director shied away from being
overtly political, arguing that Franco, who died in 1975, was a
relic of the past. But &quot;Live Flesh&quot; is a departure,
contrasting life under Franco's repressive rule with modern-day
freedom.
<p>
	    &quot;I needed to remember that we have changed and that this
country has transformed into something very different, radically
better, invulnerable to forces that may try to drag us down,&quot;
he said. &quot;We are not so far from that, but if there is
something from this past that has gone, it's fear, and once you
lose that, it's once and for all.&quot;
<p>
	   
<p>
	    DEMOCRACY IS CLOSE TO HIS HEART
<p>
	    Democracy is a subject close to Almodovar's heart. His film
career started as the new era of democracy in Spain was born.
After Franco's death, he was a leading force in &quot;La Movida,&quot;
the era of artistic exploration, and he quickly gained the
status of a cult figure in Madrid.
<p>
	    His newfound fame was a far cry from life in the tiny
village of Calzada de Calatrava, where he developed a love for
films to escape boredom.
<p>
	    He fled to Madrid as a 16-year-old and took a job in the
national telephone company. On the side, he started writing for
underground magazines and comic strips, joined an avant garde
theater group and sang in a pop band.
<p>
	    Soon he was making Super 8 short films and in 1980 he made
his first feature film, &quot;Pepi, Luci, Bom and Other Girls on the
Heap,&quot; an outrageous production that appealed to the newly
liberated Spain. What followed was a string of daring films that
often depicted strong-willed women.
<p>
	    Although Antonio Banderas is the biggest international star
to emerge from the Almodovar stable, it has always been the
female characters who have had the most intriguing, complex
roles in his films. &quot;I'm more interested in the female universe
than the male one,&quot; Almodovar once said.
<p>
	    But an important development in his latest film is the shift
toward the male characters.
<p>
	    &quot;One of the changes in this film from my previous work is
(that) the moral independence, the cruelty, the determination,
which until now have endowed my female characters, are
characteristics of the men in 'Live Flesh',&quot; he said.
<p>
	    His 13th feature film will be released in Spain Oct. 10 and
will close the New York film festival Oct. 12.
<p>
	    What is next for the outrageous director? &quot;I am going to
make sure that the next film is a comedy,&quot; he said, &quot;because I
want to make people laugh and laugh myself a bit.&quot;
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971009/entertainment/stories/stage_poe_1.html><b>FEATURE: Did Poe Show At Chicago Seance?</b></a>
<br>
Next Story: <a href=/text/headlines/971009/entertainment/stories/television_bbc_1.html><b>FEATURE: BBC Plans For Future On 75th Birthday</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

