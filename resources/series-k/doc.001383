http://www.yahoo.com/text/headlines/970915/entertainment/stories/industry_bart_1.html
<html>
<head>
<title>Yahoo! - BART: Scribes Drop Pens, Pick Up Swords</title>
</head>
<body>
<!-- ANPA File: working/0874328541-0000035497 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970915/entertainment/stories/review_hope_1.html><b>REVIEW/TELEVISION: '413 Hope St' Premiere Formulaic</b></a>
<br>
Next Story: <a href=/text/headlines/970915/entertainment/stories/review_accident_1.html><b>REVIEW/TELEVISION: 'The Accident' A Poignant Melodrama</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874328520 -->
Monday September 15  9:02 AM EDT
</strong>
<h2>BART: Scribes Drop Pens, Pick Up Swords</h2>
<!-- TextStart -->
<p>
	    By Peter Bart, Daily Variety Editor-in-Chief
<p>
	    HOLLYWOOD (Variety) - Everyone has a few youthful
indiscretions to account for.
<p>
	    Among mine was a brief foray into screenplay writing -- an
adventure I quickly came to regret. For one thing, I discovered
that screenplays represented the most intensely disciplined form
of writing -- they're all about structure and economy and other
dreadful things writers disdain. Moreover, once you start
writing and selling screenplays, that exercise plunges you into
the demimonde of a creative fraternity that goes under the name
of the Writers Guild of America.
<p>
	    Ostensibly, the Guild is an organization set up to protect
and advance the cause of writers. Well, maybe.
<p>
	    My thoughts turned to the Writers Guild again this week
because it's caught up in one of its typically disputatious
votes over a proposed contract extension. Fiercely worded
diatribes appear in the newspapers arguing the pros and cons of
the contract, and enormous stacks of mail fill my mailbox at
home -- sure signs that the WGA is in turmoil yet again.
<p>
	    Most professional guilds around the world have found a way
to work out internal disputes in an atmosphere of mutual
respect. With the WGA, every debate is akin to reliving the war
in Vietnam. There are charges of &quot;treachery&quot; and &quot;sell-out,&quot;
implications of malfeasance.
<p>
	    But then, membership in the Writers Guild has always proved
to be an exercise in combat-readiness. Almost every member I
know has his own war stories to tell -- battles with the Guild
about health benefits, residuals, punishments for evading
picket-line duties, etc.
<p>
	    As one long-term Guild member puts it, &quot;Belonging to the
Guild is more often a case of them fighting you than them
fighting for you.&quot; Hence, the present Great Debate in the Guild
is being carried on in the customary spirit of acrimony. In an
effort to avoid the nightmare strikes of the '80s, the WGA West
board, led by president Brad Radnitz, has tried to sidestep the
traditional negotiating gridlock. They've hammered out a deal in
a sort of pre-negotiation and feel they've come away with a
sound accommodation.
<p>
	    Once the contract extension is ratified, further
negotiations will then commence over thorny issues involving
residuals and what is euphemistically called the Professional
Status of Writers. A strike would thus be avoided while these
discussions get under way. As Dan Petrie Jr., who is running for
WGA president, puts it, &quot;This contract is as good as the
contract we would be left with at the end of a strike.&quot;
<p>
	    This position is ferociously rebutted by such superstar
writers as Robert Towne, Nora Ephron, Joe Eszterhas, Tom
Schulman, Joan Didion and Frank Pierson, who argue that these
nontraditional bargaining techniques have brought about &quot;nine
years of treading water while the industry is revolutionizing
itself.&quot; The so-called &quot;new deal,&quot; they charge, contains
rollbacks on medical contributions, which Petrie denies, and
demonstrates a lack of attention to such emerging issues as
foreign residuals and pay TV.
<p>
	    Putting aside economic questions, there's an important
subtext to the fusillade from the superstar writers: they
basically feel they get no respect and that the WGA doesn't give
a damn.
<p>
	    The respect issue takes many forms -- resentment over late
payments, vanity credits, demand for free rewrites and the fact
that few writers are given the courtesy of seeing a first cut.
But most nettling is that in the movie business, a film director
can plaster his name on the credits as &quot;A film by...&quot; Writers
understandably demand to know how a director could take words
written by a writer, put those words on the screen and then
apply a credit implying that he is the sole auteur.
<p>
	    They have a point there. The so-called &quot;possessory credit&quot;
was born under shadowy circumstances several years ago, and
until something is done to mitigate it, anger will continue to
gnaw at screenwriters -- and thus exacerbate the tensions
surrounding every contract negotiation.
<p>
	    But &quot;shadowy&quot; describes not just the credits issue but the
Guild as a whole. Many members feel the WGA has become an
impenetrable, self-sustaining bureaucracy with a mind of its own
-- one which forges ahead with or without the assent of its
members. Lynn Roth, a candidate for the WGAW presidency, says,
&quot;Sometimes you get a warmer reception when you call the gas
company than when you call the Writers Guild.&quot;
<p>
	    When I first became editor of this newspaper, I made a point
of introducing myself to Brian Walton, executive director of the
WGA, and to other top officers, pointing out that I was surely
the first Guild member ever to occupy such a position and hence
would like to create a healthier relationship between Daily
Variety and the Guild. Our reporters generally found the WGA a
hard organization to cover, I pointed out: on occasion,
officials tended to suppress the news rather than make it
intelligible to our reporters.
<p>
	    My efforts were singularly unsuccessful. I even invited
Walton to meetings or to lunch on several occasions, but each
time the date was canceled, supposedly because one negotiation
or another was about to take place.
<p>
	    My inevitable conclusion: the Guild, for whatever reason,
prefers to surround itself in an aura of secrecy and doesn't
welcome inquiries from outsiders -- especially an editor who
happens to be a member. In doing so, the Guild has created an
atmosphere that invites suspicion and encourages the sort of
fierce debates that are raging this week.
<p>
	    But then writers are curious people: maybe they'd be
downright bored if it were any other way.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970915/entertainment/stories/review_hope_1.html><b>REVIEW/TELEVISION: '413 Hope St' Premiere Formulaic</b></a>
<br>
Next Story: <a href=/text/headlines/970915/entertainment/stories/review_accident_1.html><b>REVIEW/TELEVISION: 'The Accident' A Poignant Melodrama</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

