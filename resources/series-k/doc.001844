http://www.yahoo.com/text/headlines/970903/entertainment/stories/film_heat_1.html
<html>
<head>
<title>Yahoo! - HEAT: The 'In &amp; Out' Kiss That Launched a Thousand Trepidations</title>
</head>
<body>
<!-- ANPA File: working/0873305787-0000013406 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970903/entertainment/stories/television_directv_1.html><b>DirecTV Reportedly Faces State Inquiry</b></a>
<br>
Next Story: <a href=/text/headlines/970903/entertainment/stories/music_oasis_1.html><b>Oasis Album Grabs Second Spot On U.S. Charts</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873305700 -->
Wednesday September  3 12:55 PM EDT
</strong>
<h2>HEAT: The 'In &amp; Out' Kiss That Launched a Thousand Trepidations</h2>
<!-- TextStart -->
<p>
	    PLUS ...
<p>
	    + Tom Selleck's Self-Imposed Semiretirement
<p>
	    + Jodie Foster Wanted To Be Michael Douglas's Daughter
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	    By Robert Hofler
<p>
	    HOLLYWOOD (Reuter) - Paul Rudnick puts it best when he
describes his hilarious new Paramount film, &quot;In &amp; Out.&quot;
<p>
	    &quot;It's like one of those great screwball comedies, only with
Cary Grant and Randolph Scott,&quot; the screenwriter says.
<p>
	    Of course, real-life lovers Grant and Scott never kissed
on-screen, which is one reason it's such a surprise when, six
decades later, Kevin Kline and Tom Selleck do just that in &quot;In
&amp; Out.&quot;
<p>
	    No doubt about it: This is the longest gay kiss in a
major-studio release, ever.
<p>
	    Selleck plays a TV reporter who coaxes a closeted
high-school teacher (Kline) to admit the truth about himself,
and in the process takes him by physical force.
<p>
	    &quot;As a writer I know I could never get away with that kiss
today with a man and a woman,&quot; Rudnick says.
<p>
	    In the kissing scene, Selleck remarks, &quot;You know what you
need?&quot; And without asking, he plants one on his co-star.
<p>
	    Performed with a man and a woman, &quot;that would be a
lawsuit!&quot; Rudnick says. &quot;And this would be a movie about
harassment. But if they're both guys and tall, it's OK.&quot;
<p>
	    Kline says the Longest Kiss took a day and a half to film.
<p>
	    &quot;But not because we kept missing or anything,&quot; he
explains. &quot;There was a lot of coverage.&quot;
<p>
	    For Selleck, his gay role in the comedy brings to mind a
major lawsuit the actor won a few years ago when The Globe
tabloid falsely reported that he was homosexual.
<p>
	    &quot;Saying you're not gay is not anti-gay,&quot; Selleck now says
a tad defensively. &quot;The tabloid was implying that I was leading
a dishonest life. I'd made a commitment to my wife, and I had a
daughter and a son.&quot;
<p>
	    Throughout the &quot;In &amp; Out&quot; press junket in New York City,
Selleck kept reminding every reporter that he was married with
children. Insecurities do linger, popping up in the oddest
places.
<p>
	    For instance, in the &quot;In &amp; Out&quot; production notes, none of
the other actors' profiles -- including those of Joan Cusack,
Matt Dillon, Bob Newhart, Wilford Brimley, Debbie Reynolds or
Shalom Harlow -- mention one word about their personal lives or
respective mates.
<p>
	    But then, they're all playing confirmed heterosexuals in the
film.
<p>
	    Kline and Selleck's profiles are a little different; they
each have a coda, which informs us that they are married to,
respectively, Phoebe Cates and Jillie Mack.
<p>
	    There hasn't been this much spousal publicity since the
original playbill of &quot;The Boys in the Band.&quot;
<p>
	   
<p>
	    LOYALTY WILL GET YOU NOWHERE
<p>
	    Inquiring minds may want to know why they've seen so little
of Tom Selleck on the big screen these days.
<p>
	    We all remember him in &quot;Mr. Baseball,&quot; &quot;Pholks,&quot; and
&quot;Quigley Down Under.&quot; Then again, maybe we don't.
<p>
	    As Selleck himself tells it, if you thought those movies
were bad ... &quot;I had a four-picture deal with Disney, for about
four and a half million dollars per picture,&quot; Selleck
explains. &quot;They kept sending me scripts that were just
abominable.&quot;
<p>
	    Television's Magnum, P.I., says he put his money where his
mouth is -- or was.
<p>
	    &quot;Whenever I said no to three scripts in a row, four a half
million dollars went bye-bye. I felt an ethical obligation to
Disney to be a team player. I don't anymore.
<p>
	    &quot;I don't think they were fair. Their development was so
bad, and it shows.&quot;
<p>
	    Could the movies he rejected have been worse than the
aforementioned three that Selleck did crank out? Says he: &quot;It's
not like any of the pictures I turned down became big
successes.&quot;
<p>
	    Fortunately for him, &quot;In &amp; Out&quot; looks to be huge.
<p>
	   
<p>
	    THE GAMES MOVIE STARS PLAY
<p>
	    Sean Penn plays Michael Douglas's kid brother in &quot;The
Game,&quot; the first release from Polygram, due out Sept. 12.
<p>
	    For a while there, when Jodie Foster expressed interest in
the David Fincher-directed thriller, it looked like Douglas was
going to have a kid sister instead of a brother. Deals were
supposedly made.
<p>
	    Then the 52-year-old Douglas learned that the 35-year-old
Foster wanted the role to be changed to that of his daughter,
which took the less-than-geriatric actor by surprise.
<p>
	    &quot;I know how old Jodie is!&quot; Douglas exclaims. &quot;Granted, we
could all hedge our ages a little bit, but I said I didn't want
to do that. I go look in the mirror a couple of times and say,
'What is her problem, man?' I don't know. Maybe Jodie needs to
look in the mirror too!&quot;
<p>
	    According to Douglas, &quot;it really became a big issue.&quot;
<p>
	    Who put Foster in &quot;The Game&quot; without a clear understanding
of her role in the film?
<p>
	    &quot;Genius, right here,&quot; says the ultramagnanimous producer,
Steve Golin. &quot;It wasn't made clear enough. It just got out of
hand. It was a misunderstanding.&quot;
<p>
	    Golin says he doesn't know whether vanity was behind
Foster's attempt to play Douglas's daughter instead of his kid
sister, but the issue was resolved amicably.
<p>
	    &quot;It's been settled. We're friends.&quot;
<p>
	    Asked if the settlement involved money or the offer of
another movie, Golin responds: &quot;I'm contractually obliged not
to talk about it. Even if I wanted to. Which I don't.&quot;
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	    (Robert Hofler is managing editor of Buzz magazine. Opinions
expressed in this column are his own.)
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970903/entertainment/stories/television_directv_1.html><b>DirecTV Reportedly Faces State Inquiry</b></a>
<br>
Next Story: <a href=/text/headlines/970903/entertainment/stories/music_oasis_1.html><b>Oasis Album Grabs Second Spot On U.S. Charts</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

