http://www.yahoo.com/text/headlines/970930/entertainment/stories/review_six_1.html
<html>

<head>
<title>Yahoo! - REVIEW/FILM: 'Six Ways To Sunday' Defiantly Oddball</title>
</head>
<body>
<!-- ANPA File: working/0875621497-0000062081 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970930/entertainment/stories/review_rent_1.html><b>REVIEW/STAGE: 'Rent' Collects Praise In Los Angeles</b></a>
<br>
Next Story: <a href=/text/headlines/970930/entertainment/stories/film_boxofficeexclusives_1.html><b>'Storm' Blows Into Exclusives Box Office At No. 1</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 875621460 -->
Tuesday September 30  8:11 AM EDT
</strong>
<h2>REVIEW/FILM: 'Six Ways To Sunday' Defiantly Oddball</h2>
<!-- TextStart -->
<p>
	    Six Ways to Sunday (Black comedy, color, no rating, 1:37)
<p>
	    By Lisa Nesselson
<p>
	    DEAUVILLE, France (Variety) - &quot;Spanking the Monkey&quot; meets
semi-organized crime in &quot;Six Ways to Sunday,&quot; in which a
virginal young gentile with a possessive mother discovers his
capacity for violence and finds himself recruited as a wisegoy
by the Jewish mob.
<p>
	    A black comedy awash in fine, quirky performances, queasy
subtext and a defiantly oddball tone, the picture will probably
be consigned to the commercial margins, but has much of what it
takes to please trendy urban hipsters. Midnight shows will be
followed by plenty of video rentals by fashion-conscious guys
and gals nursing a crush on relative newcomer Norman Reedus,
whose Prada ads and supremely of-the-moment demeanor lend him
priceless appeal for a certain kind of audience. And the boy can
act.
<p>
	    In his helming follow-up to the commercially flat &quot;It's
Pat,&quot; rock-video vet Adam Bernstein is perfectly comfortable
with the gleefully discomfiting material, adorning a creepy
premise with visual and musical asides that render the
unconventional proceedings palatable and frequently funny.
<p>
	    Eighteen-year-old Harry (Ree-dus) is living with his
extremely overprotective mother, Kate (Deborah Harry), and
flipping burgers when he tags along with his best friend, Arnie
Finkelstein (Adrien Brody), on a mission to collect shakedown
money in a sleazy strip joint. The sight of bumping and grinding
strippers triggers a sort of psychic break, and mild-mannered
Harry inexplicably starts beating out the brains of the
recalcitrant owner with fists of fury.
<p>
	    Harry is worried that Arnie's employers will be angry, but
they couldn't be more pleased. Although Harry is not Jewish,
they're convinced he has what it takes to be an enforcer and hit
man for their little circle of racketeers.
<p>
	    Harry's new mentor is Abie (Peter Appel), a lower-echelon
career mobster. Abie's boss, Varga (Jerry Adler), lives in fine
style in a house kept up by disabled but fetching maid Iris
(Elina Lowensohn). Harry, whose libido has been seriously warped
by Mom's pampering, is taken with Iris, but has trouble
consummating the relationship. The scene in which he brings Iris
home to Mom is a little gem of awkward one-upwomanship.
<p>
	    Harry has a tough-guy alter ego, a virile sidekick who is
introduced in a manner that will leave some viewers baffled, but
eventually becomes clear. While this device adds interesting
insight into Harry's Janus-like personality, it is also the most
problematic aspect of a scenario that demands a flawless mix of
gritty reality and borderline absurdity to remain aloft.
<p>
	    Reedus is to Harry what Bud Cort was to Harold in &quot;Harold
and Maude,&quot; perfectly embodying the blend of gawky teenhood and
incipient macho bravado needed to put the character across. The
lanky thesp -- who looks like a cross between Leonardo DiCaprio
and Jared Harris -- deftly juggles the two sides of Harry's
personality and renders the character sympathetic even in
unbridled killer mode.
<p>
	    Deborah Harry is soothingly peculiar as the suffocating
parent who couldn't be nicer -- or more inappropriate in her
approach. Lowensohn is aces, as is the rogues' gallery of
lowlifes. Isaac Hayes does a nifty turn as a cop with a secret.
<p>
	    Rundown contemporary urban industrial setting of Youngstown,
Ohio, is a perfect fit for tale, updated from source novel's
Brooklyn of the '30s, when Jewish gangsters held sway. Pic takes
fine advantage of the compositional possibilities afforded by
wide lenses. Snappy score of jazzy riffs and mostly outmoded
popular tunes is also a nice fit.
<p>
	   
<p>
	    Harry Odum ............ Norman Reedus
<p>
	    Kate Odum ............. Deborah Harry
<p>
	    Iris .................. Elina Lowensohn
<p>
	    Arnie ................. Adrien Brody
<p>
	    Bennet ................ Isaac Hayes
<p>
	    Varga ................. Jerry Adler
<p>
	    With: Peter Appel, Clark Gregg, Anna Thomson, Paul Lazar,
Holter Graham.
<p>
	   
<p>
	    A Prosperity Electric production. (International sales: John
Sloss, N.Y.) Produced by Adam Bernstein, David Collins, Michael
Naughton. Executive producer, Charles Johnson. Co-producers,
Marc Gerald, Michael Williams.
<p>
	    Directed by Adam Bernstein. Screenplay, Marc Gerald,
Bernstein, based on the novel Portrait of a Young Man Drowning
by Charles Perry; camera (color, widescreen), John Inwood;
editor, Doug Abel; music, Theodore Shapiro; production design,
Teresa Mastropierro; set design, Christine Manca; costume
design, Edi Giguere; sound (Dolby), Antonio L. Arroyo; assistant
director, Alan Breton; casting, Billy Hopkins, Suzanne Smith,
Kerry Barden. Reviewed at Deauville Festival of American Cinema
(competing), Sept. 10, 1997.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970930/entertainment/stories/review_rent_1.html><b>REVIEW/STAGE: 'Rent' Collects Praise In Los Angeles</b></a>
<br>
Next Story: <a href=/text/headlines/970930/entertainment/stories/film_boxofficeexclusives_1.html><b>'Storm' Blows Into Exclusives Box Office At No. 1</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

