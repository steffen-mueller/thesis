http://www.yahoo.com/text/headlines/971007/entertainment/stories/culture_flamingo_1.html
<html>

<head>
<title>Yahoo! - FEATURE: American Drive-In Rituals Stir 'The Flamingo Rising'</title>
</head>
<body>
<!-- ANPA File: working/0876267545-0000074012 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971007/entertainment/stories/art_theft_2.html><b>Indictment Returned In $10 Million Art Theft</b></a>
<br>
Next Story: <a href=/text/headlines/971007/entertainment/stories/music_pirates_1.html><b>FEATURE: Pirates Plunder Latin American Music</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 876267540 -->
Tuesday October  7  7:39 PM EDT
</strong>
<h2>FEATURE: American Drive-In Rituals Stir 'The Flamingo Rising'</h2>
<!-- TextStart -->
<p>
	    By Patrick Connole
<p>
	    NEW YORK (Reuter) - For a time, in the middle of the 20th
century, America was home to thousands of drive-in movie
theaters, mostly in rural areas where giant white screens
stretched above the cars lined up neatly in unpaved parking
lots.
<p>
	    Larry Baker has written a novel about that time, starting in
the 1950s and ending when the era of Richard Nixon begins. His
story is actually about a family that lives inside the world's
largest drive-in movie screen, which towers next to a funeral
home run by an atheist.
<p>
	    The Flamingo theater also sits beside the Florida shore,
enabling erstwhile shrimpers anchor for a night of third-rate
horror flicks or John Wayne's latest shoot-out. Meanwhile, the
funeral director rests in whatever peace he can manage, waiting
for daylight, suffering in the neon glare and other peculiar
sights and sounds known only at an American drive-in.
<p>
	    Baker's theater in his first novel, &quot;The Flamingo Rising&quot;
(Knopf), seems a strange place for housing a tale, but the
author may at first seem an odd person to do the telling. By
day, the middle-aged Baker is an Iowa City, Iowa, councilman who
works a couple of jobs teaching and selling books.
<p>
	    Before his attempt to merge literature and politics, the
really bizarre bits of his life took place at Oklahoma drive-in
theaters he ran more than a few decades ago.
<p>
	   
<p>
	    FOUND A DEAD WOMAN IN THE TOILETS
<p>
	    &quot;During my drive-in career I was robbed four times, shot at
once, stabbed once, arrested for collusion to transport
pornographic materials across state lines, beat up by a
motorcycle gang in Tulsa, found a dead woman in the toilet,&quot;
Baker told Reuters during a book tour in New York, sparing
details of the time he caused a 13-car pile-up after showering a
highway with windblown fireworks.
<p>
	    &quot;It was a great job -- sleep all day and stay up all
night,&quot; Baker said, adding that the business is a sociologist's
dream laboratory for studying &quot;weird human behavior.&quot;
<p>
	    By the time he sold his last movie theater in the late
1970s, Baker had moved on to graduate studies in English. After
a stint in North Carolina, he took his family to Iowa City and
in 1983 won election to the Zoning Commission. After a detour to
Florida, he returned to Iowa, winning his council seat in 1993.
<p>
	    Even though Iowa City is considered the most educated town
in the United States by the U.S. Census Bureau -- and home to
the vaunted Writer's Workshop -- Baker says his attempt at
combining writing and a political life may perplex voters.
<p>
	    &quot;It will be an acid test of whether irony and politics can
survive together,&quot; he said.
<p>
	    Concurrently, his novel may test the winds for mixing a 
coming-of-age story, Moby Dick symbolism -- read the great white
screen as the great white whale -- and marketing efforts to sell
a curse-free novel to Christian readers as well as to the wider
market.
<p>
	    &quot;I go to Mass every Sunday with people who take an hour out
of their week, searching for something. This book is about
faith, and I wrote it first for this very complex group of
people,&quot; Baker said.
<p>
	   
<p>
	    STORY IS ABOUT AMERICAN FLAMBOYANCE
<p>
	    Turning a drive-in theater into the backdrop for faith is
tricky, but the story also is about American flamboyance. The
narrator, Abraham Isaac Lee, is adopted from Korea and brought
into the world of his crazed adoptive father.
<p>
	    Over the course of the book, young Lee falls in love with
the daughter of his father's nemesis, funeral director Turner
West, while the freakish show of running a drive-in whirls
behind the scenes.
<p>
	    The plot turns on the outrageous acts of the older Lee and
an assortment of incidents, including a fictionalized version of
the dead woman in the toilet, that eventually transform the
giant screen from a symbol of life to death and back again.
<p>
	    &quot;People have compared it to Romeo and Juliet, the Hatfield
and McCoys and Moby Dick,&quot; Baker notes.
<p>
	    The drive-in, once a pillar of a unique American style of
freedom, is ever-present and may provoke a nostalgic reaction
for readers. It is a place where kids decked out in pajamas sit
with parents in the front row of cars, stuffing themselves with
popcorn and soda as they drift off to sleep.
<p>
	    In the back rows and the dark recesses, restless youth
pursued their own scores in their parked cars, static-filled
microphones awkwardly resting in the crack of the window, under
a screen wide enough to land a small plane.
<p>
	    &quot;The book makes people feel good,&quot; Baker said.
<p>
	    Reuters/Variety
<p>
	    
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971007/entertainment/stories/art_theft_2.html><b>Indictment Returned In $10 Million Art Theft</b></a>
<br>
Next Story: <a href=/text/headlines/971007/entertainment/stories/music_pirates_1.html><b>FEATURE: Pirates Plunder Latin American Music</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

