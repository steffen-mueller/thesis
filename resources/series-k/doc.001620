http://www.yahoo.com/text/headlines/970908/entertainment/stories/music_r_1.html
<html>
<head>
<title>Yahoo! - COLUMN/JAZZ: Babyface's 'Soul Food' Leads New R&amp;B Soundtracks</title>
</head>
<body>
<!-- ANPA File: working/0873754806-0000024059 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873754800 -->
Monday September  8  5:40 PM EDT
</strong>
<h2>COLUMN/JAZZ: Babyface's 'Soul Food' Leads New R&amp;B Soundtracks</h2>
<!-- TextStart -->
<p>
	    By Franklin Paul
<p>
	    NEW YORK (Reuter) - Back in 1995, Kenny &quot;Babyface&quot; Edmonds
did something most songwriters and producers only dream about.
<p>
	    He wrote and produced every song on the hit soundtrack for
&quot;Waiting to Exhale&quot; and brought in top-notch singers such as
Aretha Franklin, Whitney Houston and Brandy.
<p>
	    Now, Babyface is back with &quot;Soul Food,&quot; the album
companion to the upcoming film of the same name. But he says
don't expect another &quot;Exhale&quot; -- this project is different.
<p>
	    &quot;I don't really look at them as the same kind of records or
even the same kind of movie, so I don't think of comparing them
in that sense,&quot; he said recently from his California home.
<p>
	    He notes that immediate similarities in the two albums are
limited to his executive production of both, and the fact that
they are attached to films about and predominantly cast with
African-Americans.
<p>
	    &quot;I think that it's two totally different kinds of
projects,&quot; he said. &quot;The last project was certainly more
conceptual, there was a certain theme that ran throughout. In
this one, the only thematic song is from Boyz II Men, a song
called 'A Song for Mama.'
<p>
	    &quot;The more I felt I tried to make it all become a concept,
the more I felt I was forcing it,&quot; Babyface explained. &quot;So I
backed up on that idea and just tried to put a decent record
together.&quot;
<p>
	    Unlike with &quot;Exhale,&quot; &quot;Soul&quot; finds Babyface at the helm
of only about half the tunes, leaving the others in the capable
hands of fellow hit makers such as Teddy Riley, Missy Elliott
and Timbaland and Sean &quot;Puffy&quot; Combs.
<p>
	    Still, there is no doubt this is Edmonds' baby. Released on
his LaFace label, &quot;Soul Food&quot; the album is borne of &quot;Soul
Food&quot; the film, produced by Edmonds Entertainment, which he
runs with his wife, Tracey.
<p>
	    Babyface would have done the score to the film too, but
instead he and his wife brought aboard Wendy Melvoin and Lisa
Coleman, known to most as members of Prince's band the
Revolution (despite their recent success as a production team).
<p>
	    &quot;He didn't have the time to do the score,&quot; Tracey Edmonds
said. &quot;We wanted to reach out to someone who could really voice
the feeling and the emotion that was in the film, and we thought
of Wendy and Lisa.&quot;
<p>
	    Even if he wasn't as omnipresent here as with &quot;Exhale,&quot;
Babyface's spirit is still felt throughout on &quot;Soul Food.&quot;
<p>
	    If there is a thematic thread running through the record,
it's the strength of vocal group performances -- either a duet
or group performs every tune. Performances include songs by
Total, Dru Hill, Blackstreet, and Milestone -- a quintet made up
of Babyface and his two brothers (from the group After 7) plus
brothers K-Ci and JoJo Haley (of Jodeci).
<p>
	    Babyface said one tune, &quot;Slow Jam,&quot; was a particular
treat. The tune, performed here by single-named teen singers
Usher and Monica, was originally recorded some 15 years ago by
the group Midnight Star, when Babyface was just Kenny Edmonds, a
young unknown songwriter in Cincinnati.
<p>
	    &quot;I loved the first version, but I wasn't a producer yet. I
was just learning,&quot; he said. &quot;It was probably the first song
as a writer that I got attention. I mean (legendary R&amp;B group)
the Whispers came to me because I wrote the song 'Slow Jam.'
<p>
	    &quot;This time it was actually me getting a chance to produce
closer to how I originally wanted it to be.&quot;
<p>
	    &quot;Soul Food&quot; is only one of a handful of soundtracks
released recently that have predominantly R&amp;B themes. Here's a
quick glance at some of them.
<p>
	   
<p>
	    &quot;Hoodlum&quot;
<p>
	    This impressive collection of tunes bends the barriers
between rap, soul and jazz. Not content to include songs of
confined to these three genres, the producers make such crafty
moves as presenting L.V. -- previously seen behind Warren G and
other rappers -- belting out a stunning Sam Cooke-style
rendition of &quot;Basin Street Blues,&quot; backed by the
Clayton-Hamilton Jazz Orchestra.
<p>
	    Standout tunes on this well-done collection include a remake
of the Crusaders' &quot;Street Life&quot; by Rahsaan Patterson, hip-hop
soul from Davina featuring Chef Raekwon on &quot;So Good,&quot; and &quot;I
Can't Believe,&quot; a sensitive '90s R&amp;B ballad from 112 and Faith
Evans, produced by Combs.
<p>
	   
<p>
	    &quot;Money Talks&quot;
<p>
	    Though more hip-hop flavored than &quot;Hoodlum,&quot; this
collection also sparkles when its R&amp;B acts are front and center.
Outstanding efforts from Barry White and Faith Evans (&quot;My
Everything&quot;), Me'Shell Ndegeochello (&quot;The Teaching&quot;) and
D'Angelo-soundalike Angie Stone (&quot;Everyday&quot;) highlight the
album. And though Bunny Debarge's &quot;A Dream&quot; has been sampled
and covered ad nauseam, Mary J. Blige, full of grit and
yearning, delivers a delightful take of the song.
<p>
	   
<p>
	    &quot;Steel&quot;
<p>
	    Hidden within the soundtrack for this fast-disappearing
Shaquille O'Neal vehicle are a few R&amp;B gems, from Tevin
Campbell's aggressive &quot;No More Fighting,&quot; to &quot;Coming Home to
You,&quot; by Blackstreet. Most impressive are string-laden &quot;Free
To Be Me,&quot; performed by Gina Breedlove and written/produced by
Andre Betts, and &quot;Anything for Your Love,&quot; by Jon B.
<p>
	   
<p>
	    Honorable mentions go to the soundtracks to: &quot;Good
Burger,&quot; which includes pleasant songs from Mint Condition,
Tracie Spencer and 702; and &quot;Sprung,&quot; thanks to a new song
from Monifah, who once again teams with writers Heavy D and Big
Bub and producer Tony Dofat.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	    (Franklin Paul writes a bi-weekly column about R&amp;B for
Reuters. Opinions expressed are his own. He can be reached at
Franklin.Paul(at)Reuters.Com)
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

