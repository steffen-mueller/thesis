http://www.yahoo.com/text/headlines/971006/entertainment/stories/industry_bart_1.html
<html>

<head>
<title>Yahoo! - BART: Requiem For An Anchorman</title>
</head>
<body>
<!-- ANPA File: working/0876141540-0000071788 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971006/entertainment/stories/film_upcoming_1.html><b>Upcoming Film Releases: Weeks of Sept. 29 & Oct. 6</b></a>
<br>
Next Story: <a href=/text/headlines/971006/entertainment/stories/review_ali_1.html><b>REVIEW/STAGE: 'Ali' Is Flattered On Stage</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 876141540 -->
Monday October  6  8:39 AM EDT
</strong>
<h2>BART: Requiem For An Anchorman</h2>
<!-- TextStart -->
<p>
	    By Peter Bart, Daily Variety Editor-in-Chief
<p>
	    HOLLYWOOD (Variety) - It was a defining moment for John
Schubeck. At the end of his 11 p.m. news show, a story popped up
on his &quot;prompter&quot; that he had already read 10 minutes
earlier.
<p>
	    With more than two decades of experience as an anchorman,
Schubeck had to decide whether to read it a second time or ad
lib the remaining two minutes. He opted for a third alternative:
fuming over this screw-up, he simply glared at the camera for
two minutes and remained utterly silent -- perhaps one of the
longest interludes of silence in recent TV history.
<p>
	    John Schubeck died last week, penniless at age 61. In his
prime, he was a $1 million a year anchorman in New York, Chicago
and Los Angeles. He was also one of the most brilliant, yet
downright goofy, denizens of the TV news fraternity -- a man
who, despite his considerable success, was absolutely convinced
that what he did for a living was absurd and vaguely demeaning.
<p>
	    There were times he put his attitude on display in dopey
ways. A big burly man who enjoyed his alcohol, Schubeck kept a
coffee mug full of rum just off camera, but viewers now and then
could see him take a swig during what he thought was a break.
Once, upon finishing his 5 p.m. segment, he started introducing
the anchor for the next hour, but couldn't remember his name (it
was Warren Olney, who looked on, astonished). Sometimes he would
run stories together, so that an explosion in Montebello
resulted in four casualties in Pacoima -- viewers were left to
their own devices to figure out the surreal melodrama.
<p>
	    Nonetheless, Schubeck was a serious and highly intelligent
man who read the New York Times and the Economist in the
newsroom and, while anchoring at night, completed law school by
day. He also wrote two fascinating screenplays that vividly
depicted the innate absurdity of his calling as he saw it. In
his scripts, TV newsmen planted evidence to embellish stories,
rearranged crime scenes to create better camera angles and
carefully rehearsed witnesses to describe scenes they hadn't
observed.
<p>
	    Now and then a news readerin Schubeck's scripts would escape
from his desk to break an important news story and expose
bureaucratic hypocrisy, but these incidents clearly represented
Schubeck's own fantasy life. He thought the format and content
of local news shows were downright ridiculous -- the twin
anchors of predictably diverse ethnicity glued to their desk,
rattling off inane items about local muggings and gang wars.
<p>
	    &quot;Why does everyone have to do things in lockstep?&quot;
Schubeck would rage when we met for an occasional dinner or
drink. &quot;Why does everything have to be exactly alike?&quot;
<p>
	    On several occasions, Schubeck presented to management
daring schemes to break the pattern -- ideas for free-flowing
news shows in which the anchors were in the field covering
important news stories, not freeway chases. The ideas were
rejected. So were his screenplays. They were funny but chaotic,
like their author -- filled with vivid incident, but structured
in a stream-of-consciousness manner. The late Paddy Chayefsky
could have cobbled them into memorable movies, but alas he
wasn't around for a rewrite.
<p>
	    Filled with indignation over the state of his profession,
Schubeck ricocheted from station to station, at one time or
another working for all three of the network affiliates in Los
Angeles.
<p>
	    &quot;They can't fire you in this job once you've achieved a
certain level,&quot; he once told me. &quot;They don't have enough
imagination.&quot;
<p>
	    But they did fire him. By the time he was in his late 50s,
he was rattling around independent stations in places like Palm
Desert, and it had become quite obvious, even in those sleepy
places, that Schubeck was drinking too much.
<p>
	    He was also fighting all sorts of other demons. He had a
habit of marrying with great frequency. When he got bored, he
would announce, &quot;I'm going to a celebrity golf tournament in
Spain,&quot; but in fact no one ever knew where he actually went or
why.
<p>
	    I find myself thinking of Schubeck this week, now that the
lockstep of local TV news has spread to that of network
newsmagazine shows. The new TV season is awash in Datelines and
20/20s and Inside Editions, all of them dishing out their
stories about Princess Diana or &quot;road rage&quot; or other
supposedly sure-thing topics. The proliferation has been so
great that it's no longer possible to differentiate the
supposedly more serious shows like 48 Hours from the
hard-breathing Hard Copy or Extra. They've all become a blur.
<p>
	    Bryant Gumbel, for one, finds all this baffling. In starting
his new show, Public Eye,last week, he insisted it was possible
to start a newsmagazine without &quot;chasing down the latest
celebrity who picked up a transvestite.&quot; He started out with a
vivid glimpse at famine in North Korea plus an interview with
the Army sergeant major accused of sexual misconduct. It was a
promising start greeted by unpromising ratings.
<p>
	    In surfing through these news shows, however, I got the
feeling that all of them could have used a bit of the
nihilistic, let's break-the-mold spirit of John Schubeck, that
brilliant and bizarre man who loved making a million bucks a
year, but hated the price he had to pay.
<p>
	    Reuters/Variety
	
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971006/entertainment/stories/film_upcoming_1.html><b>Upcoming Film Releases: Weeks of Sept. 29 & Oct. 6</b></a>
<br>
Next Story: <a href=/text/headlines/971006/entertainment/stories/review_ali_1.html><b>REVIEW/STAGE: 'Ali' Is Flattered On Stage</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

