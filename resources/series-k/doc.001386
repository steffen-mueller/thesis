http://www.yahoo.com/text/headlines/970915/entertainment/stories/review_seven_1.html
<html>
<head>
<title>Yahoo! - REVIEW/FILM: Brad Pitt Can't Save 'Seven Years In Tibet'</title>
</head>
<body>
<!-- ANPA File: working/0874328464-0000035488 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970915/entertainment/stories/review_boogie_1.html><b>REVIEW/FILM: 'Boogie Nights' A Potent Parable Of Partying Times</b></a>
<br>
Next Story: <a href=/text/headlines/970915/entertainment/stories/television_ellen_1.html><b>The Ins And Outs Of 'Ellen'</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874328460 -->
Monday September 15  9:01 AM EDT
</strong>
<h2>REVIEW/FILM: Brad Pitt Can't Save 'Seven Years In Tibet'</h2>
<!-- TextStart -->
<p>
	    Seven Years in Tibet (Biographical drama, color, PG-13,
2:19)
<p>
	    By Derek Elley
<p>
	    TORONTO (Variety) - Brad Pitt climbs lots of mountains and
meets the young Dalai Lama, but doesn't carry the audience with
him for much of the odyssey in &quot;Seven Years in Tibet.&quot;
<p>
	    Despite some magnificent widescreen lensing, faultless
ethnographic detail and a timely sympathy for the plight of the
Tibetan people, director Jean-Jacques Annaud's true-life tale
about a self-obsessed Austrian mountaineer who learns
selflessness in the Himalayas too rarely delivers at a simple
emotional level.
<p>
	    Pitt's name and the exotic, bigscale nature of the yarn
should ensure initial box office interest, but the picture looks
to scale only midrange peaks domestically, with international
picking up some of the slack. &quot;Seven Years in Tibet&quot; will also
prove an interesting test case for audiences' interest in such
subject matter, as Martin Scorsese's &quot;Kundun,&quot; centered
specifically on the Dalai Lama, readies for Christmas release.
<p>
	    Annaud's previous pics have often shown a tendency to get
bogged down in local or historical detail at the expense of pure
emotional sweep (Quest for Fire, The Lover, The Bear, The Name
of the Rose). In &quot;Tibet,&quot; which starts with the hurdle of
asking audiences to identify with a ruthlessly self-absorbed
member of the Nazi Party, the script by Becky Johnston (The
Prince of Tides) rarely hits the heights of eloquence or poetry
needed to engage viewers in the protag's interior struggle or
underpin the visual sweep of the picture. With the first half of
the narrative skipping from dateline to dateline as we follow
his progress to Tibet, and a good chunk of the dialogue devoted
to cultural backgrounding and historical foot-noting, Pitt's
character remains a somewhat cold, one-dimensional cipher prior
to finally meeting the young Dalai Lama. It's only then -- well
over an hour into the movie -- that the picture starts to tread
solid dramatic ground.
<p>
	    The blond, Aryan-looking Heinrich Harrer (Pitt) is
introduced in Austria, 1939, as he sets out with buddy Peter
Aufschnaiter (David Thewlis) on a four-month trek to the
Himalayas to conquer Nanga Parbat peak. Several previous teams
have already failed, and scaling the mountain has now become a
matter of Teutonic pride. Harrer's farewell to his heavily
pregnant wife (Ingeborga Dapkunaite) at the railroad station is
cruelly unemotional.
<p>
	    Despite a bloody injury, Harrer saves the life of
Aufschnaiter on the mountain face. However, the team fails in
its mission and, as WWII has officially started by the time of
its descent, the group is interned by the British in a North
Indian POW camp.
<p>
	    Harrer, ever the loner, tries several times to escape, but
ironically only succeeds when he joins a team led by
Aufschnaiter in fall 1942. Striking off on his own to the north,
he's finally rejoined by his friend when almost close to death,
and the pair eventually reach the closed kingdom of Tibet, where
they're first rebuffed at the border but then allowed to enter.
Smuggling themselves into the capital, Lhasa, they are later
given the singular honor (for foreigners) of being allowed to
stay. Aufschnaiter falls for a female tailor (Lhakpa Tsamchoe),
whom he marries.
<p>
	    By this point, at which the movie has been running for
around an hour, the audience has been treated to a wealth of
incident (mountain climbing, a POW escape, trekking through
hostile landscape) and some stunning widescreen vistas. Yet
Harrer has remained essentially the same buttoned-up character
as at the start, and the long-limbed yarn only just cleared its
throat prior to the meat of the picture.
<p>
	    Though the movie crosscuts during the first half between
Harrer's exploits and Tibet, the main character drama is
basically between Harrer and Aufschnaiter, whose bonds of
friendship are, however, more stated than felt. With Pitt and
English thesp Thewlis both hampered by doing various versions of
a German accent, there are already barriers to making their
relationship (edgy and remote to start with) come alive on
screen. Though both actors are detailed technicians, there's
minimal chemistry between them; only in a later scene where Pitt
visits the married Thewlis after a long period of being apart
does their friendship really come alive.
<p>
	    Pic's emotional clout is largely thanks to the scenes
between Pitt and Kundun, the boy Dalai Lama (Bhutanese actor
Jamyang Jam-tsho Wangchuk), which have zest and some welcome
humor, as well as real onscreen bonding. Even here, however,
Johnston's fragmented script doesn't really rise to the
challenge: Harrer's building of a movie theater at Kundun's
request (almost a subject for a film in itself) and Pitt's
gradual acceptance of Tibetan values are treated as just two of
several strands in an over-busy, didactic script that takes in
cultural info, historical events and even Pitt's distant
relationship with a son he's never seen back home.
<p>
	    Pitt turns in a game, focused performance but is saddled
with a role that is essentially a bystander to history rather
than a proactive shaper of events. Thewlis, equally
perfectionist, tends to come in and out of focus rather than
truly partner Pitt throughout the movie. Aside from the sparky
Wangchuk, who's excellent as Kundun, several smaller
performances make one wish the actors had more screen time to
develop their roles, especially Tsamchoe as Aufschnaiter's
strong Tibetan wife, and B.D. Wong as a government secretary
whose loyalties to the Tibetan cause remain suspect.
<p>
	    Production values are tip-top, with all of the reported $70
million budget up on the screen: from At Hoang's clever use of
Argentine locales and the foothills of the Andes for Tibet, to
Enrico Sabbatini's lived-in costumes, both for Tibetans and
Westerners. John Williams' score, though thematically
unmemorable, is effective when allowed to bloom -- which, apart
from the end title, is unfortunately all too rare.
<p>
	    And that's the basic Achilles' heel of Seven Years in Tibet:
For a story with all the potential of a sweeping emotional
drama, sited in great locations, too often you just long for the
picture to cut loose from the ethnography and correct attitudes,
and go with the drama in old Hollywood style.
<p>
	   
<p>
	    Heinrich Harrer ........ Brad Pitt
<p>
	    Peter Aufschnaiter ..... David Thewlis
<p>
	    Ngawang Jigme .......... B.D. Wong
<p>
	    Kungo Tsarong .......... Mako
<p>
	    Regent ................. Danny Denzongpa
<p>
	    Chinese Amban .......... Victor Wong
<p>
	    Ingrid Harrer .......... Ingeborga Dapkunaite
<p>
	    Dalai Lama, aged 14 .... Jamyang Jamtsho Wangchuk
<p>
	    Pema Lhaki ............. Lhakpa Tsamchoe
<p>
	    Great Mother ........... Jetsun Pema
<p>
	    With: Ama Ashe Dongtse, Sonam Wangchuk, Dorjee Tsering, Ric
Young, Ngawang Chojor, Duncan Fraser, Benedick Blythe.
<p>
	   
<p>
	    A Sony Pictures Entertainment release from TriStar Pictures
of a Mandalay Entertainment presentation of a Reperage and
Vanguard Films-Applecross production. Produced by Jean-Jacques
Annaud, John H. Williams, Iain Smith. Executive producers,
Richard Goodwin, Michael Besman, David Nichols.
<p>
	    Directed by Jean-Jacques Annaud. Screenplay, Becky Johnston,
based on the book by Heinrich Harrer. Camera (Technicolor,
Panavision-UK widescreen), Robert Fraisse; editor, Noelle
Boisson; music, John Williams (cello solos, Yo-Yo Ma);
production design, At Hoang; set decoration, Jim Erickson;
costume design, Enrico Sabbatini; sound (Dolby-SDDS), Ken
Weston, Dean Humphreys, Tim Cavagin, Mark Lafbery; second unit
director &amp; camera (Argentina and Canada), Allen Smith; Himalayan
unit director, Eric Valli; High Himalayan unit director &amp;
camera, David Breashears; stunt co-ordinator, Nick Gillard;
Tibetan adviser, Tenzin Tethong; special effects co-ordinator,
Dean Lockwood; associate producer, Alisa Tager; assistant
director-second unit director, Mark Eggerton; casting, Priscilla
John, Francine Maisler. Reviewed at Toronto Film Festival
(Gala), Sept. 12, 1997.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970915/entertainment/stories/review_boogie_1.html><b>REVIEW/FILM: 'Boogie Nights' A Potent Parable Of Partying Times</b></a>
<br>
Next Story: <a href=/text/headlines/970915/entertainment/stories/television_ellen_1.html><b>The Ins And Outs Of 'Ellen'</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

