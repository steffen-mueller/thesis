http://www.yahoo.com/text/headlines/970908/entertainment/stories/film_buzz_1.html
<html>
<head>
<title>Yahoo! - BUZZ: Crime Columnist Cracks Hollywood</title>
</head>
<body>
<!-- ANPA File: working/0873721057-0000023065 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970908/entertainment/stories/stage_strands_1.html><b>STRANDS: News From The London Theater World</b></a>
<br>
Next Story: <a href=/text/headlines/970908/entertainment/stories/television_lunden_1.html><b>'Good Morning America' Bids Lunden Adieu</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873721020 -->
Monday September  8  8:17 AM EDT
</strong>
<h2>BUZZ: Crime Columnist Cracks Hollywood</h2>
<!-- TextStart -->
<p>
	    By Michael Fleming
<p>
	    NEW YORK (Variety) - Who says crime doesn't pay?
<p>
	    Certainly not Mike McAlary, who after spending a decade
mining crime and police corruption for newspaper columns, is
finding more interest from producers and studios hungry for the
crime tales he uncovers.
<p>
	    Warner Bros. just bought his Esquire piece &quot;Mark of a
Murderer,&quot; a feature in the September issue, about three
generations of a family on different sides of the law. WB paid a
low-six-figure sum for the article and life rights to Vincent
LaMarca, a cop with a distinguished police record, but a lousy
genetic one. He's the son of a man executed for kidnapping and
killing a month-old infant for $2,000 ransom. Vincent LaMarca's
son, Joey, now faces the death penalty, charged with brutally
murdering an accomplice in an attempted petty theft. McAlary
uncovered the hard-to-believe story the same way he always does.
<p>
	    &quot;I got a call from a cop,&quot; says the New York Daily News
columnist. &quot;To find a cop in the middle of that story is
astounding. Here is a guy raised and trained by the guys who
executed his father, and he, in turn, helped train some of the
guys who caught his son.&quot;
<p>
	    The deal was brokered by Brillstein-Grey co-president
Matthew Baer, with company principal Brad Grey and New York
publicist Dan Klores attached to produce and McAlary consulting.
<p>
	    McAlary will also turn the generational story into a book,
with Miramax Books said to be interested. Not surprising, after
McAlary wrote the novelization of Miramax's &quot;Cop Land&quot; and
helped plug a most gaping plothole. The picture had New York
City cops living in New Jersey -- until, that is, McAlary
alerted the filmmakers that it's as impossible for city cops to
live in New Jersey as it is for them to live on Mars.
<p>
	    &quot;I've worked for publishers who don't read what I write,
but Harvey Weinstein is a reader, and he changed the movie,&quot;
McAlary says.
<p>
	    &quot;They added a (Robert) De Niro voiceover about how the town
came to be, with the guys being transit cops. They could live in
New Jersey. Most don't, because their gun carry permit is only
good in New York.&quot;
<p>
	    While a somewhat implausible solution, it was a better
alternative than being busted for it by police brass who labeled
the premise of crooked cops ridiculous.
<p>
	    McAlary recalls, &quot;Police commissioner Howard Safir sat
behind me in the screening, and said, 'I don't know cops like
that.' He was in denial, and two weeks later, we met a cop who
was worse.&quot;
<p>
	    That would be Justin Volpe, a white cop in Brooklyn's 70th
Precinct accused of sodomizing Haitian immigrant Abner Louima
with a toilet plunger. McAlary broke that story, interviewing
Louima in his hospital bed, and the scandal ruled the Gotham
papers until the Princess Diana tragedy. Four years after he
nearly died in a car accident that took him out of action,
McAlary once again led the pack on a juicy cop scandal.
<p>
	    &quot;Funny thing is, 10 years after being a columnist in New
York, I was trying to get away from the cop stuff,&quot; he says.
&quot;I was supposed to be doing 'Cop Land' stuff when I got the
call, and I moved from fantasy corruption to real corruption.&quot;
<p>
	    He'll likely do the Louima case as a book.
<p>
	    McAlary has yet more projects percolating. Universal is
about to get a new draft of a screen adaptation by Ken Nolin of
another McAlary book, &quot;Good Cop, Bad Cop,&quot; which the studio
bought for Sylvester Stallone. The thesp will play Joe Trimboli,
the Internal Affairs officer who nailed Michael Dowd, a dirty
cop in the 75th Precinct who acted as frontman and enforcer for
Dominican Republican drug dealers, in one of the biggest cop
scandals in Gotham history. McAlary was all over that story, as
he was with the drug scandal in the 77th, where a cop named
Brian O'Regan met McAlary for an interview, and afterward blew
his brains out with his gun. That story became McAlary's
bestselling book, &quot;Buddy Boys.&quot;
<p>
	    McAlary's written his first novel, &quot;Sore Losers,&quot; out next
year from William Morrow, but he won't give up the column. And
why not, since there's an inexhaustible supply of corruption and
crime stories?
<p>
	    &quot;Corruption changed when crack put drugs and cash on every
street corner,&quot; McAlary says. &quot;In the 'Serpico' days, you had
to be in a special detail to even have the chance to be corrupt.
Now, cops coming out of the academy have to make that decision,
because the next car they stop, there could be three years
salary in the trunk and the driver will say, 'keep it.&quot;'
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970908/entertainment/stories/stage_strands_1.html><b>STRANDS: News From The London Theater World</b></a>
<br>
Next Story: <a href=/text/headlines/970908/entertainment/stories/television_lunden_1.html><b>'Good Morning America' Bids Lunden Adieu</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

