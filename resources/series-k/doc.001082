http://www.yahoo.com/text/headlines/970922/entertainment/stories/music_soundbites_1.html
<html>
<head>
<title>Yahoo! - SOUNDBITES: Album Review Package</title>
</head>
<body>
<!-- ANPA File: working/0874963721-0000048107 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970922/entertainment/stories/art_hirst_1.html><b>FEATURE: Sculptor Damien Hirst, Man Or Man-Made Myth?</b></a>
<br>
Next Story: <a href=/text/headlines/970922/entertainment/stories/people_calendarfuture_1.html><b>CALENDAR: Upcoming Arts, Culture & ShowBiz Events (Nov.-Jan.)</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 874963680 -->
Monday September 22  5:28 PM EDT
</strong>
<h2>SOUNDBITES: Album Review Package</h2>
<!-- TextStart -->
<p>
	    By Gary Graff
<p>
	    DETROIT (Reuter) - A quick look at some of what's new in the
record racks. Albums are rated according to the following scale:
<p>
	    5 stars....Buy it now
<p>
	    4 stars....Buy it at your leisure
<p>
	    3 stars....Borrow a friend's copy first
<p>
	    2 stars....Only for devotees
<p>
	    1 star.....Barely worth a first listen
<p>
	    No stars...Read a book instead
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	   
<p>
	    Elton John
<p>
	    &quot;The Big Picture&quot;
<p>
	    (Rocket)
<p>
	    2.5 stars
<p>
	    In danger of being eclipsed by his &quot;Candle in the Wind
'97&quot; tribute to Princess Diana, Elton John's latest album
continues this piano man's move away from rock into a lush,
heavily orchestrated and mellow brand of pop. The 11 songs here
have a majestic sweep, which is not new terrain for John and
lyricist Bernie Taupin. But they lack the dramatic edge of the
duo's earlier creations, such as &quot;Levon&quot; or &quot;Sixty Years
On.&quot; Only the big, brassy &quot;Wicked Dreams&quot; raises any ruckus,
while even the strongest cuts -- &quot;Something About the Way You
Look Tonight,&quot; &quot;If the River Can Bend&quot; and &quot;Recover Your
Soul&quot; -- would be merely mellow respites at an earlier juncture
in John's career.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	   
<p>
	    Bjork
<p>
	    &quot;Homogenic&quot;
<p>
	    (Elektra)
<p>
	    4 stars
<p>
	    Add this Icelandic songstress to the growing ranks of rock
musicians exploring the realm of classical music. The
ever-adventurous Bjork, however, does it without foresaking the
other hallmarks of her sound and, working with &quot;Homogenic&quot;
co-producer Mark Bell, crafts a seamless fusion of
sensibilities. In fact, because she's working with techno rather
than standard guitar rock, Bjork may have found two forms that
fit more naturally together, and the results -- on songs such
as &quot;5 Years&quot; and &quot;Bachelorette&quot; -- are revelatory.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	   
<p>
	    Cream
<p>
	    &quot;Those Were the Days&quot;
<p>
	    (Polydor/Chronicles)
<p>
	    4.5 stars
<p>
	    A welcome retrospective, this four-volume set is noteworthy
more for its comprehensiveness than for mining the British
threesome's vaults for unreleased gems -- though there are a few
of those. Mostly, &quot;Those Were the Days&quot; presents the collected
works of Cream, a stunning catalog that defined the rock 'n'
roll power trio, re-defined the way rockers embraced blues and
confirmed Eric Clapton's status as the pre-eminent guitar hero
of a generation.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	   
<p>
	    Kacy Crowley
<p>
	    &quot;Anchorless&quot;
<p>
	    (Atlantic)
<p>
	    4 stars
<p>
	    Taking her independent album, remixing it and adding some
songs for its major-label release, Kacy Crowley has just made a
good thing better. The singer-ongwriter from Austin, Texas, uses
her powerhouse voice to deliver sharp narratives and evocative
images, deftly exploring emotional terrain that's not always
comfortable. With songs such as &quot;Hand to Mouthville,&quot;
&quot;Melancholy Bridge,&quot; &quot;Vertigo,&quot; &quot;Follow Me Outside&quot; and
&quot;Scars,&quot; &quot;Anchorless&quot; is one of the year's most compelling
debuts and should earn Crowley a rash of well-deserved
attention.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	   
<p>
	    Various Artists
<p>
	    &quot;One Step Up/Two Steps Back: The Songs of Bruce      
Springsteen&quot;
<p>
	    (The Right Stuff)
<p>
	    4 stars
<p>
	    The tribute album field has become a cesspool of indulgence,
but don't tell that to the organizers of this mammoth salute to
Springsteen. With 28 songs on the two-disc CD, plus eight more
on two singles, this is an ambitious and sweeping homage that is
without precedent. It's also quite good, particularly when the
participants choose more obscure material, such as Sonny
Burgess' version of &quot;Tiger Rose&quot; and the Rocking Chairs'
rendition of &quot;Restless Nights.&quot; Marshall Crenshaw, John Wesley
Harding, Martin Zellar and BoDeans' Kurt Neumann also turn in
aces amidst a winning batch of covers.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	   
<p>
	    Matthew Ryan
<p>
	    &quot;Mayday&quot;
<p>
	    (A&amp;M)
<p>
	    2 stars
<p>
	    On his debut album, Matthew Ryan establishes himself as a
Dylan/Springsteen-style troubadour with promise but also plenty
of room to grow. His greatest strengths are melodies and
dynamics, but his mostly melancholy lyrics are scattershot, and
he hasn't yet developed a distinctive style to separate himself
within the crowded singer-songwriter field.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	    (Gary Graff is a nationally syndicated journalist who covers
the music scene from Detroit. He also is the supervising editor
of the award-winning &quot;MusicHound&quot; album guide series. Opinions
expressed here are his own.)
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970922/entertainment/stories/art_hirst_1.html><b>FEATURE: Sculptor Damien Hirst, Man Or Man-Made Myth?</b></a>
<br>
Next Story: <a href=/text/headlines/970922/entertainment/stories/people_calendarfuture_1.html><b>CALENDAR: Upcoming Arts, Culture & ShowBiz Events (Nov.-Jan.)</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

