http://www.yahoo.com/text/headlines/970930/entertainment/stories/film_dish_2.html
<html>

<head>
<title>Yahoo! - DISH: Celebs See Red After Mags Go To Bed</title>
</head>
<body>
<!-- ANPA File: working/0875621375-0000062076 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 875621340 -->
Tuesday September 30  8:09 AM EDT
</strong>
<h2>DISH: Celebs See Red After Mags Go To Bed</h2>
<!-- TextStart -->
<p>
	    By Michael Fleming
<p>
	    NEW YORK (Variety) - After gracing the cover of Esquire in a
feature by Tom Junod, Kevin Spacey might feel like Chazz
Palminteri at the close of &quot;The Usual Suspects&quot; -- hoodwinked.
<p>
	    Junod's decision to structure the story on rumor Spacey
might be gay -- a potentially career-altering designation made
not by reputable sources, but by Junod's 80-year-old mother --
left other magazine editors drop-jawed and publicists looking to
hide women, children and clients from the newly revamped
Esquire.
<p>
	    It also left many feeling that a spate of celeb-scorching
features indicates that the leverage once held by publicists is
switching back to magazines and their star byliners.
<p>
	    After extensive access, Chris Farley was the subject of a
lengthy &quot;Special Report&quot; in Us magazine headlined &quot;On the
Edge of Disaster,&quot; which characterized the large comic as a
ticking time bomb courting a premature demise like that of his
idol, John Belushi.
<p>
	    Mira Sorvino, GQ's August cover, hated her profile, with her
father and others close to her complaining that writer Andrew
Corsello, a college acquaintance of hers, had gotten even for
some past campus snub.
<p>
	    And let's not forget the highest-profile cover story
dissection of the year, Lynn Hirschberg's feature on ABC exec
Jamie Tarses in the New York Times Magazine, in which the only
thing about Tarses that seemed smart was her outfit for the
cover photo.
<p>
	    In each case, the celebs were left feeling not only like
they'd been robbed, but that they'd let the robbers in and
helped them carry out the goods.
<p>
	    After a weeklong barrage from Spacey's publicist, agent and
media over the article and cover headline &quot;Kevin Spacey Has a
Secret,&quot; new Esquire editor David Granger claims that &quot;it's
the best profile written on Kevin Spacey,&quot; and that the gay
assertion is a red herring to the real story: Spacey is a movie
star.
<p>
	    An &quot;Esky&quot; column in the same issue asserting celebs
shouldn't have private lives has been taken as a mission
statement by celeb publicists, though Granger called it a humor
piece.
<p>
	    Spacey and his publicist, Staci Wolfe Newman, are not
laughing, left to deny what they felt was the magazine's
implication that Spacey somehow acknowledged he's gay. He has
not.
<p>
	    They say Spacey was duped by promises that Granger cared
about his acting, not his sexuality. In a statement, Wolfe
Newman said, &quot;Esquire has made it abundantly clear that they
have now joined the ranks of distasteful journalism, and this
mean-spirited, homophobic, offensive article proves that the
legacy of Joseph McCarthy is alive and well.&quot;
<p>
	    Granger said the cover piece stemmed from his love of &quot;L.A.
Confidential,&quot; and he denied the mag tried to out Spacey.
<p>
	    &quot;If you read to the end of Tom's piece, you can see he
rejects the idea that Kevin's sexuality is an issue, that it
just doesn't matter.&quot;
<p>
	    Despite those assertions, several editors at national
magazines used the word &quot;appalling&quot; in reference to the
article, and publicists made it clear its memory would linger
long after the next issue hits the stands.
<p>
	    &quot;That first piece by the new Esquire team certainly put me
on guard,&quot; said PR high priestess Pat Kingsley at PMK. &quot;I'll
take a wait-and-see attitude before our clients take part. No
one wants to be party to leading the Christians to the lions.&quot;
<p>
	    Editors have long decried publicists' insistence that they
scrutinize writers as though they were choosing plastic
surgeons. But Kingsley felt the Spacey piece and certain others
underscore why it has become mandatory.
<p>
	    &quot;If you're not prepared to give (us) a writer the client is
confident about, we skip the piece, with no hard feelings,&quot; she
said. &quot;It's not like the major client needs it; in fact, we
usually have to convince them.&quot;
<p>
	    Eddie Michaels, a partner of  Spacey's rep Wolfe Newman,
felt the cover element should merit some goodwill: &quot;Given the
financial benefits of access to stars, it seems only courteous
and right that magazines be upfront about their motivations.&quot;
<p>
	    To GQ editor Art Cooper, the mag only has the obligation to
be responsible. Cooper, who employed Granger until he bolted in
a widely publicized defection, would not comment on the Spacey
feature, other than to say he would not have published it. But
he also rebelled against the notion of kowtowing to publicists
who feel his mag's an extension of their hype machine.
<p>
	    &quot;I don't look at it as part of the marketing process,&quot; he
said. &quot;They want the GQ cover, what I want is access, and I
don't mean a lead that says Mira Sorvino took a forkful of
salmon and talked about blah blah blah at some lunch place. I'm
interested in character, and a person reveals and defines
character by what they do.
<p>
	    &quot;No writer knows what he's going to find. Several years
ago, we decided to put Steven Seagal on the cover and Alan
Richman, who was a big fan, did the story. He went out to
Hollywood and what he found was a big jerk. He came back and
wrote about it. He didn't set out to do it, and Andrew Corsello
had no agenda.&quot;
<p>
	    Of the Sorvino piece, Cooper said: &quot;Some might say it's a
really nasty piece, but I just got back from L.A., meeting with
agents and publicists, and they brought that piece up as an
example of a piece that was honest and accurate.&quot;
<p>
	    Farley was devastated by the Us article, as was his family.
He felt positives were downplayed and that he sealed his fate
with reporter Erik Hedegaard by spontaneously flying to Hawaii
with two women he'd just met, leaving the scribe hanging.
Farley's plan to sit for magazine profiles effectively began and
ended with that article, say those close to him.
<p>
	    But Us editorial director Sid Holt said the mag's primary
duty is to report accurately, which it did. Another mag editor
said it's up to celebs and their handlers to decide whether they
can withstand scrutiny from reporters.
<p>
	    Are celebs powerless when they feel they're done wrong by a
magazine? Not entirely. When writer-producer David E. Kelley
invited an Entertainment Weekly reporter on the &quot;Ally McBeal&quot;
set and felt a story didn't match the effusive onset demeanor of
the scribe, Kelley used his pen to square things, said sources.
In an episode, McBeal -- a lawyer whose inner thoughts can be
heard onscreen -- ended a jury summation wondering if she got
through to the jury. &quot;People are idiots,&quot; she said, &quot;They do
read Entertainment Weekly.&quot;
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

