http://www.yahoo.com/text/headlines/971006/entertainment/stories/review_dance_1.html
<html>

<head>
<title>Yahoo! - REVIEW/STAGE: Polanski Sinks Teeth Into Musical</title>
</head>
<body>
<!-- ANPA File: working/0876140857-0000071727 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971006/entertainment/stories/people_love_1.html><b>Courtney Love Changes Talent Agencies</b></a>
<br>
Next Story: <a href=/text/headlines/971006/entertainment/stories/variety_insided_2.html><b>Inside Daily Variety: October 6</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 876140820 -->
Monday October  6  8:27 AM EDT
</strong>
<h2>REVIEW/STAGE: Polanski Sinks Teeth Into Musical</h2>
<!-- TextStart -->
<p>
	    Dance of the Vampires (Raimund Theater, Vienna; 1,180 seats;
$105 top; 3:00)
<p>
	    By Cathy Meils
<p>
	    VIENNA (Variety) - Heavily hyped and budgeted ($6
million-plus), the eagerly awaited stage version of Roman
Polanski's cult film &quot;The Fearless Vampire Killers&quot; marks the
director's debut as a musical comedy helmer. The film's simple
and colorful story translates smoothly to the stage in &quot;Dance
of the Vampires.&quot;
<p>
	    In fact, lyricist/book writer Michael Kunze's largely
faithful adaptation even improves on it. The addition of a love
triangle between the three leads (a worldly vampire, a
wet-behind-the-ears student and a beguiling village beauty) may
be a stock musical tradition, but it works. It adds an element
of emotional conflict absent in the film.
<p>
	    The musical is given a handsome production, especially with
William Dudley's overripe sets.
<p>
	    Regrettably, the deft touches of subversive humor that kept
the film percolating are reduced to the occasional bubble.
All-too-serious performances smother the film's sense of parody.
That could be easily and happily restored. More of a problem
facing the possible transfer to demanding Broadway or West End
audiences is composer Jim Steinman's score. Too often thin and
uninspired, the musical high point turns out to be a recycled
hit from the '80s. What it needs are equally tantalizing new
tunes. Polanski's decision to occasionally bring the action out
into the auditorium is questionable, but it helps lighten a
production that veers toward dourness.
<p>
	    While the film focused on the quest of a vampire expert
accompanied by his student assistant, here the expert, Professor
Abronsius (Gernot Kranner), takes a back seat to his protege,
Alfred (Aris Sas). In search of vampires, the pair arrive in the
snowbound wilds of Transylvania, where an abundance of garlic
strung around the local inn hints they're on the right trail.
<p>
	    Bedding down at the inn, Alfred spots the innkeeper's nubile
daughter Sarah (Cornelia Zenz). Seeing that Sarah is attracted
to the love-smitten student, papa Chagal (James Sbano) does his
best to keep the two apart, hypocritically pursuing his own
illicit affair with the servant girl Magda (Eva Maria Marold),
while his suspicious wife Rebecca (Anne Welte) fumes.
<p>
	    Meanwhile, local vampire Count Von Krolock (Steve Barton)
also sets his sights on Sarah, and eventually lures her to his
castle, where the show culminates in the dance of the title (a
disappointment mired in robotic choreography) and a final chase
scene.
<p>
	    Dudley's fantasy set design, fashioned with a foam gun run
amok, is a wonderfully decadent creation, ripe to the point of
rotting. The act one inn has the dark charm of a cottage
straight out of a Mitteleuropa fairy tale, a twisted and gnarled
three-storied structure. He maintains his style while opening up
the stage to the creepy grandeur of the act two castle, complete
with a marvelous portrait gallery that cries out for more
lighting. The same applies to Sue Blane's costumes, which only
get a really good showing when the vampire chorus parades
through the aisles.
<p>
	    The pedestrian dance numbers don't rise out of the routine.
The exception is a homoerotic duet between Alfred and Von
Krolock's son Herbert that sizzles, but leaves the forgotten
Sarah a voyeuristic wallflower. Unfortunately, the choreography
emphasizes the percussion-heavy score that is frequently
predictable. Falling back on a 1983 hit, &quot;Total Eclipse of the
Heart,&quot; for the Von Krolock-Sarah love theme seems an easy out
when there are too few other numbers that come near matching its
pop-rock lushness. Given the promise of the overture and the
composer's past credits (including Meat Loaf's Bat Out of Hell
and Celine Dion's recent hit It's All Coming Back to Me Now),
the disappointment of some of the numbers is more pronounced
than it would be with another debuting musical composer.
<p>
	    Polanski's bow as a stage musical director is strong and
assured, but not on a par with his best film-helming talents.
Surprisingly, he proves more conventional in his stagecraft than
his Central European theater roots would seem to indicate.
<p>
	    Performances are solid and well-sung across the board.
Kranner has the hard task of living up to Jack MacGowan's film
performance as the Professor, but manages to acquit himself
admirably in a more limited role. He carries the responsibility
for most of the musical's humor, and gives a tour-de-force
race-tempo rendering of two musical numbers. Zenz as Sarah and
Marold as Magda have fine, strong voices suitable for Broadway.
So, too, does Barton as Count Von Krolock, although his
deep-freeze demeanor is a far cry from Frank Langella's Broadway
vampire, and Sarah's attraction to this stiff stretches
credulity.
<p>
	    The discovery of the production is Sas as Alfred, the role
originated by Polanski in the film. His second-act solo, &quot;For
Sarah,&quot; soars with the pain of love and longing. It's a true
spine-tingler. The 21-year-old at first relies on easy mugging
that gradually gives way to more genuine expression, and with
his catharsis at the performance's end, he at last captures the
production for himself.
<p>
	   
<p>
	    Cast: Steve Barton (Count Von Krolock), Aris Sas (Alfred),
Cornelia Zenz (Sarah), Gernot Kranner (Professor Abronsius),
James Sbano (Chagal), Anne Welte (Rebecca), Nik (Herbert), Eva
Maria Marold (Magda), Torsten Flach (Koukol); Suzan Carey,
Elisabeth Ebner, Shana Mahoney, Brigitte Oelke, Lenneke
Willemsen, Ute Ziemer, Mate Kamaras, Frank-Heiko Lohmann, Alex
Melcher, Charles Moulton, Jerome Phaff, Vincent Pirillo, Mario
van Dulmen, Yara Blumel, Tina Decker, Marion Hagele, Christa
Helige, Ramona Ludwig, Tanja-Maria Meier, Friedrich Buhrer,
Brian Carmack, Axel Olzinger, Otto Pichler, Philip Ranson,
Christian Tschinder.
<p>
	   
<p>
	    A Vereinigte Buhnen Wien production in cooperation with
Roman Polanski and Andrew Braunsberg of a musical in two acts
with music by Jim Steinman, book and lyrics by Michael Kunze.
Based on the film &quot;The Fearless Vampire Killers or Pardon Me
But Your Teeth Are in My Neck.&quot; Directed by Roman Polanski;
choreography, Dennis Callahan; sets, William Dudley; costumes
and makeup, Sue Blaine; lighting, Hugh Vanstone; sound, Richard
Ryan; musical supervision, vocal and dance arrangements, Michael
Reed; orchestrations, Steve Margoshes. Opened, reviewed Oct. 4,
1997.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971006/entertainment/stories/people_love_1.html><b>Courtney Love Changes Talent Agencies</b></a>
<br>
Next Story: <a href=/text/headlines/971006/entertainment/stories/variety_insided_2.html><b>Inside Daily Variety: October 6</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

