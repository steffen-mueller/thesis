http://www.yahoo.com/text/headlines/970929/entertainment/stories/television_teletubbies_2.html
<html>
<head>
<title>Yahoo! - FEATURE: BBC's Teletubbies: The Kiddie Cult That Ravers Love</title>
</head>
<body>
<!-- ANPA File: working/0875552871-0000060982 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970929/entertainment/stories/people_devito_1.html><b>FEATURE: L.A. Confidential? Danny DeVito Disses Tabs</b></a>
<br>
Next Story: <a href=/text/headlines/970929/entertainment/stories/culture_litpicks_1.html><b>LITPICKS: News From The Literary World</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 875552820 -->
Monday September 29  1:07 PM EDT
</strong>
<h2>FEATURE: BBC's Teletubbies: The Kiddie Cult That Ravers Love</h2>
<!-- TextStart -->
<p>
	    By Paul Majendie
<p>
	    LONDON (Reuter) - Over the hills and far away, Teletubbies
come out to play.
<p>
	    And much to the fury of perplexed parents, Britain's tiny
tots are transfixed by the television antics of Tinky Winky,
Dipsy, Po and Laa-Laa.
<p>
	    Children clamor every morning for their daily dose of the
Teletubbies, who gobble up toast and custard when not out
frolicking on a surreal hillside covered in garish flowers and
gambolling rabbits.
<p>
	    A baby-faced sun shines down on the carefree antics that 2-
to 4-year-olds lap up with glee.
<p>
	    The Teletubbies watch real children playing on television
screens embedded in their tummies and never have to worry about
housework -- Noo-Noo the vacuum cleaner tidies up after them in
their Tubbytronic Superdome.
<p>
	    In just six months, the BBC television show has achieved
cult status. It attracts 2 million pre-school viewers and even
appeals to the &quot;rave generation&quot; of teenagers eager to wind
down after a night of clubbing on amphetamines.
<p>
	    &quot;The Teletubbies phenomenon -- bigger than Shakespeare&quot;
proclaimed one magazine headline.
<p>
	    But parents, linguists and educators are up in arms about
what they see as the &quot;dumbing down&quot; of children's television
with much burbling baby chat and endless simplistic repetition.
<p>
	    Take Dipsy. He loves to eat his Tubby Toast, and his special
song is &quot;Bptum bptum, bptum bptum.&quot;
<p>
	    It could be responsible for turning tots as young as 18
months into &quot;cot potatoes,&quot; complained one newspaper.
<p>
	   
<p>
	    CREATOR SCOFFS AT CRITICS
<p>
	    Anne Wood, creator of the cuddly purple, red, yellow and
green creatures, scoffs at her critics. She says the carefully
researched show opens friendly windows onto a hi-tech world.
<p>
	    &quot;I believe television and video are the most
under-estimated force for good in educating our children in the
technological age in which we live,&quot; said the former teacher.
<p>
	    But she confessed: &quot;I was surprised by the vehemence of the
criticism. I hadn't expected to be national news.&quot;
<p>
	    Wood, who brought in speech therapist Andrew Davenport to
develop the show, defends the repetition of the real-life child
play inserts in the Teletubbies' tummies.
<p>
	    &quot;That's why children go back to favorite tapes and books
time and time again,&quot; she said.
<p>
	    She tries out each insert -- of children painting, riding
ponies or helping mummy wash up -- on playgroups and nurseries.
If the inserts get the thumbs-down, out they go.
<p>
	    The satirical magazine Private Eye wondered if the program
makers weren't on drugs because they had created a psychedelic
hallucination complete with voice trumpets and talking flowers.
<p>
	    But Wood is unperturbed by attacks on the program, which
will run for 260 episodes in Britain and has already been sold
to the United States, South Africa and Portugal.
<p>
	    &quot;Everything in Teletubbies has been done with love and a
sense of fun. Children need to see their experiences reflected
back to them as part of discovering who they are,&quot; she said.
<p>
	    The motto of her production company, Ragdoll, is lifted from
Carl Jung: &quot;Without play, without fantasy, no creative work has
ever come to birth.&quot;
<p>
	    But one German program buyer did appear baffled by the show
and turned it down. &quot;These are like spacemen. I think it will
frighten our children,&quot; he said.
<p>
	    At least one of the characters was initially deemed by the
show's producers to be giving the wrong impression.
<p>
	    Dave Thompson, who originally played Tinky Winky, was fired
by the producers because they didn't like his interpretation of
the character. His flamboyant handbag-waving was said to have
made him into a gay icon.
<p>
	    He has found work elsewhere, taking on the role of hybrid
dolphin Fin Fin. &quot;Being Fin Fin is like playing Hamlet compared
to being a Teletubby,&quot; he says.
<p>
	    The success of &quot;Teletubbies&quot; has astounded the BBC.
<p>
	    &quot;The demand is unprecedented, the interest is phenomenal,&quot;
said a spokeswoman for the public service broadcaster's
marketing department.
<p>
	    The first two videos from the show sold 100,000 copies each
in the first two weeks in release to become the BBC's
fastest-ever sellers.
<p>
	    Cuddly toys, tapes and books are rolling off the production
line in a franchise business which, it is estimated, will push
sales to at least 30 million pounds ($48 million) by the end of
the year.
<p>
	    And the bandwagon just keeps on rolling. The Teletubbies
plan to record a single that could be No. 1 in the pop charts
for Christmas.
<p>
	    As Wood admits almost wearily: &quot;We are only doing it
because we have been advised that if we do not do it, someone
else will.&quot;
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<hr>
<strong>Earlier Related Stories</strong>
<ul>
<li><a href=/text/headlines/970929/entertainment/stories/television_teletubbies_1.html>Britain's 'Teletubbies' Head For U.S.</a> - <small>Mon Sep 29  8:20 am</small></li>
</ul>
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970929/entertainment/stories/people_devito_1.html><b>FEATURE: L.A. Confidential? Danny DeVito Disses Tabs</b></a>
<br>
Next Story: <a href=/text/headlines/970929/entertainment/stories/culture_litpicks_1.html><b>LITPICKS: News From The Literary World</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

