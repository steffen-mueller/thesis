http://www.yahoo.com/text/headlines/971006/entertainment/stories/review_jealousy_1.html
<html>

<head>
<title>Yahoo! - REVIEW/FILM: 'Mr Jealousy' A Real Charmer</title>
</head>
<body>
<!-- ANPA File: working/0876141476-0000071784 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<center>

</center>
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971006/entertainment/stories/television_allen_1.html><b>Allen Staying 'Home' For $1.25 Million Per Episode</b></a>
<br>
Next Story: <a href=/text/headlines/971006/entertainment/stories/review_gang_1.html><b>REVIEW/FILM: 'Gang Related' Marks Shakur's Last Stand</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 876141420 -->
Monday October  6  8:37 AM EDT
</strong>
<h2>REVIEW/FILM: 'Mr Jealousy' A Real Charmer</h2>
<!-- TextStart -->
<p>
	    Mr. Jealousy (Romantic comedy, color, no rating, 1:45)
<p>
	    By Derek Elley
<p>
	    TORONTO (Variety) - With its right-on cast, movie references
and self-analytical Manhattan dialogue, it would be easy to
dismiss &quot;Mr. Jealousy&quot; as Woody Lite.
<p>
	    But the truth is that, on its own level, the picture is a
real charmer for those prepared to check their preconceptions at
the door. This warmhearted romantic comedy about an emotional
klutz consumed by jealousy over his girlfriend's ex-bpyfriend
has enough laughs and likable characters to make it easy to
spend an hour and three-quarters in their company. Pic should do
moderate business in upscale urban sites before strolling on to
a longer life on homevideo.
<p>
	    Though a smoother and more technically accomplished ride
than Noah Baumbach's first feature, &quot;Kicking and Screaming,&quot;
there's the same film-buff tone throughout -- from the spoken
opening credits (a la Welles, Bergman), through the use of
Georges Delerue's music for Truffaut's &quot;Jules and Jim,&quot; to a
voiceover narrator who starts, &quot;This is the story of Lester and
Ramona&quot; (pick your own French New Wave movie) and a lead
character whose first date with a girl was for a screening of
Renoir's &quot;Rules of the Game.&quot; Viewers who get a rash just
thinking about this kind of knowingly referential picture,
populated by people with too much spare time on their hands,
should stay away.
<p>
	    Lester (Eric Stoltz, who also exec produces) is a former CNN
producer and occasional substitute teacher who's drifted through
life as a professional wannabe and just can't seem to get the
tumblers to fall into place with girls. His next big decision is
whether he accepts an offer from Iowa U. for its graduate
writing program.
<p>
	    On the babe front, things are looking up for Lester: through
his friends Vince (Carlos Jacott) and Lucretia (Britisher
Marianne Jean-Baptiste, from Secrets &amp; Lies), he's met the
upfront but slightly wacky Ramona (Annabella Sciorra). The only
blip on the horizon is Lester's uncontrollable jealousy -- not
so much about Ramona's 26 previous boyfriends as over her most
recent, arrogant bestselling author Dashiell (Chris Eigeman).
Lester is consumed by suspicions, and under an alias joins
Dashiell's therapy group to discover whether the oily scribe
still carries a torch for her.
<p>
	    Writer-director Baumbach's development of this plot fluff
into something that can hold the attention over feature length
is generally clever. He elicits enough fondness for the
characters in the early going to carry the viewer through some
of the later duller patches -- mostly the scenes with Dashiell's
shrink (played with professorial glee by Peter Bogdanovich),
which aren't quite as funny as they need to be, nor as clear,
given the plot complexities at that point. Pic also doesn't know
when to quit, with three endings and even a further scene after
the final roller.
<p>
	    Still, the performances are the thing, and both Sciorra,
who's excellent as the slightly out-of-reach Ramona, and Stoltz,
who's very assured as Lester, exactly catch the '90s screwball
tone in Baumbach's script. Eigeman is fine as Dashiell, and
Jean-Baptiste, making a reasonable stab at an American accent,
OK. Bridget Fonda contribs a neat, surprisingly touching cameo
as Dashiell's stuttering girlfriend.
<p>
	   
<p>
	    Lester Grimm ............ Eric Stoltz
<p>
	    Ramona Ray .............. Annabella Sciorra
<p>
	    Dashiell Frank .......... Chris Eigeman
<p>
	    Vince ................... Carlos Jacott
<p>
	    Lucretia ................ Marianne Jean-Baptiste
<p>
	    Stephen ................. Brian Kerwin
<p>
	    Dr. Poke ................ Peter Bogdanovich
<p>
	    Irene ................... Bridget Fonda
<p>
	    Lint .................... John Lehr
<p>
	   
<p>
	    A Joel Castleberg Prods. production. (International sales:
Kathy Morgan Intl., L.A.) Produced by Joel Castleberg. Executive
producer, Eric Stoltz.
<p>
	    Directed, written by Noah Baumbach. Camera (Deluxe color),
Steven Bernstein; editor, J. Kathleen Gibson; music, Luna,
Robert Een; production design, Anne Stuhler; costume design,
Katherine Jane Bryant; sound (Dolby), Jeff Pullman, Ken S. Polk;
associate producer, Chris Eigeman; casting, Todd Thaler.
Reviewed at Toronto Film Festival (Special Presentations), Sept.
12, 1997.
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/971006/entertainment/stories/television_allen_1.html><b>Allen Staying 'Home' For $1.25 Million Per Episode</b></a>
<br>
Next Story: <a href=/text/headlines/971006/entertainment/stories/review_gang_1.html><b>REVIEW/FILM: 'Gang Related' Marks Shakur's Last Stand</b></a>
<!-- EndLinks -->
<center>

</center>
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

