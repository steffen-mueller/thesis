http://www.yahoo.com/text/headlines/970904/entertainment/stories/online_livewire_1.html
<html>
<head>
<title>Yahoo! - LIVEWIRE: Forget Term-Paper Blues; Reference Works Abound Online</title>
</head>
<body>
<!-- ANPA File: working/0873414793-0000015596 -->

<map name="reuters">
<area shape="rect" coords="0,0,210,57" href="http://www.yahoo.com/headlines/">
<area shape="rect" coords="210,0,447,57" href="http://www.yahoo.com/reutersonline/?http://www.online.reuters.com/">
</map>
<center><img usemap="#reuters" width=447 height=57 border=0 alt=" Reuters New Media" src=http://www.yahoo.com/images/features/news/reuters/reuters3.gif></center><br>
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<center><b>[
<a href=http://www.yahoo.com/>Yahoo</a> |
<a href=/text/suggest.html>Write Us</a> |
<a href=/text/search.html>Search</a> |
<a href=/text/docs/info/>Info</a> ]</b>
</center>
<br>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<a href=/text/headlines/entertainment/>Entertain</a>
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>
<p>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970904/entertainment/stories/multimedia_programs_1.html><b>PROGRAMS: Encyclopedia Is Nightmare On A Disk</b></a>
<br>
Next Story: <a href=/text/headlines/970904/entertainment/stories/people_calendarfuture_1.html><b>CALENDAR: Upcoming Arts, Culture & ShowBiz Events (Nov.-Jan.)</b></a>
<!-- EndLinks -->
<hr>
<strong>
<!-- Yahoo TimeStamp: 873414720 -->
Thursday September  4  7:12 PM EDT
</strong>
<h2>LIVEWIRE: Forget Term-Paper Blues; Reference Works Abound Online</h2>
<!-- TextStart -->
<p>
	    By Michelle V. Rafter
<p>
	    LOS ANGELES (Reuter) - Next time your teacher assigns you a
hairy term paper or project to report, don't sweat it -- net it.
<p>
	    Online aid abounds for researching and writing reports,
whether you're 8 or 18. Help runs the gamut from encyclopedias,
fact books and university-level research for gathering
information, to dictionaries and grammar guides to make sure you
write it right.
<p>
	    If you can't find what you're looking for, real-life
reference librarians and tutors are only an e-mail away.
<p>
	    The best part: many resources are free, though some of the
best charge subscriptions that range from $9.95 to $12.50 a
month.
<p>
	    Whether you're studying the solar system or the American
Revolutionary War, a good place to start is one of the
multimedia encyclopedias now online. Britannica Online
(<a href="http://eb.com">http://eb.com</a>) is the Internet incarnation of the venerable
Encyclopedia Britannica. A monthly subscription to the 32-volume
reference is $12.50, or $150 for the year; use a seven-day free
trial to try before you buy.
<p>
	    Free online encyclopedias include the Knowledge Adventure
Encyclopedia (<a href="http://www.adventure.com/encyclopedia/">http://www.adventure.com/encyclopedia/</a>), and
Microsoft's Encarta Concise Encyclopedia
(<a href="http://www.encarta.com">http://www.encarta.com</a>), an abbreviated version of the software
giant's popular CD-ROM reference book that contains 16,000
articles and 2,200 pictures.
<p>
	    If the topic you're tackling involves current events, search
Infonautics Corp.'s Electric Library (<a href="http://www.elibrary.com">http://www.elibrary.com</a>).
The four-year-old service offers articles from hundreds of
newspapers and magazines including the Los Angeles Times,
Christian Science Monitor, Time, and BusinessWeek, plus books,
maps, pictures and other reference works. After a 30-day free
trial, a monthly subscription is $9.95.
<p>
	    Infonautics recently spun off the portion of Electric
Library devoted to school reports into a free Web site called
Researchpaper.com (<a href="http://www.researchpaper.com">http://www.researchpaper.com</a>). Use it to
search for term-paper ideas by keyword, or browse through
categories such as history, science and society. In addition,
Researchpaper.com offers a writing center, public message boards
for exchanging ideas, and soon, live chat. Register at the site
to have updates on new features and special offers e-mailed to
you.
<p>
	    In time for the fall semester, the University of Michigan's
Internet Public Library (<a href="http://www.ipl.org">http://www.ipl.org</a>) is offering the A+
Research &amp; Writing resource for high-school and college
students. The guide -- which also can be downloaded -- walks
students through the paper-writing process, and offers guides to
search tools for finding information online as well as links to
other resources.
<p>
	    Volunteer librarians also staff the Internet Public
Library's &quot;Ask a Question&quot; e-mail reference service. This reference was last updated in
1996, however, so information on countries with volatile
political situations may be out of date.
<p>
	    After a map? MapQuest (<a href="http://www.mapquest.com">http://www.mapquest.com</a>) is a
year-old site that lets you create and print out maps for any of
three million locations worldwide.
<p>
	    If you're stuck for a subject, or stumble onto a problem you
-- or your parents -- can't solve, plug into a tutor. America
Online subscribers can use the &quot;Ask a Teacher&quot; feature
(America Online, Keyword: Homework Help) to e-mail a question,
and one of several hundred volunteer teachers will reply within
48 hours. Or go to one of the tutoring chat rooms for live help;
be sure to check the schedule first to find out when subject
experts are online.
<p>
	    Once you've gathered your materials, it's time to write. For
help with the basics, consult Strunk and White's &quot;The Elements
of Style,&quot;  (<a href="http://www.columbia.edu/acis/bartleby/strunk">http://www.columbia.edu/acis/bartleby/strunk</a>), a
classic treatise on proper sentence structure, punctuation,
composition and common word mistakes.
<p>
	    Speaking of words, use Merriam-Webster Online
(<a href="http://www.m-w.com">http://www.m-w.com</a>), the online edition of the Merriam-Webster
Dictionary to look up word spellings or search for bon mots. Try
Roget's Thesaurus (<a href="http://www2.theasurus.com">http://www2.theasurus.com</a>) for synonyms and
related words.
<p>
	     Need a grabby quote to start things off? Try Bartlett's
Familiar Quotations
(<a href="http://www.columbia.edu/acis/bartleby/bartlett">http://www.columbia.edu/acis/bartleby/bartlett</a>), the famous
list of famous phrases that's been transported to cyberspace by
the electronic scriveners at Columbia University's Bartleby
Library, which also put Strunk and White online.
<p>
	    Now that it's becoming more common to cite information
obtained on the Internet, reference librarians are working out
the proper format for listing such citations in a term-paper
bibliography.
<p>
	    One recommendation (using this capitalization and
punctuation): Author's Last Name, Author's First Name. &quot;Title
of Document.&quot; Title of Complete Work (if applicable). Document
date or date or last revision. Protocol and address (Web, email,
etc.), date of access.
<p>
	    Using this format, an example would be: Magistad, Mary
Kay. &quot;China's Religious Persecution.&quot; National Public Radio
broadcast. July 23, 1997. Electric Library, www.elibrary.com,
Sept. 2, 1997.
<p>
	    -----    -----     -----     -----     -----     -----
<p>
	    (Michelle V. Rafter writes about cyberspace and technology
from Los Angeles. Reach her at mvrafter(at)deltanet.com.
Opinions expressed in this column are her own.)
<p>
	    Reuters/Variety
<!-- TextEnd -->
<!-- StartRelated -->
<!-- EndRelated -->

<center>
<form method=get action="http://search.main.yahoo.com/search/news">
<hr>
<input size=24 name=p> <input type=submit value="Search News">
<input type=hidden name=n value=10>
<a href="http://www.yahoo.com/docs/info/news_search_help.html">
<small>Help</small></a><br>
</form>
</center>
<!-- StartLinks -->
<hr>
Previous Story: <a href=/text/headlines/970904/entertainment/stories/multimedia_programs_1.html><b>PROGRAMS: Encyclopedia Is Nightmare On A Disk</b></a>
<br>
Next Story: <a href=/text/headlines/970904/entertainment/stories/people_calendarfuture_1.html><b>CALENDAR: Upcoming Arts, Culture & ShowBiz Events (Nov.-Jan.)</b></a>
<!-- EndLinks -->
<!-- AdSpace -->
<!-- AdParam	2716295	 -->
<center>

</center>

<!-- /AdSpace -->
<hr>
<center><strong>[
<a href=/text/headlines/>Index</a> |
<a href=/text/headlines/news/>News</a> |
<a href=/text/headlines/international/>World</a> |
<a href=/text/headlines/business/>Biz</a> |
<a href=/text/headlines/tech/>Tech</a> |
<a href=/text/headlines/politics/>Politic</a> |
<a href=/text/headlines/sports/>Sport</a> |
<a href=http://sports.yahoo.com/>Scoreboard</a> |
<strong>Entertain</strong> 
 |
<a href=/text/headlines/health/>Health</a>
]</strong>
</center>

<hr>
<small>
<center><strong>Reuters Limited</strong></center><br>
<br>
<center>
<address>
<a href="http://www.yahoo.com/docs/info/news_faq.html">Questions or Comments</a>
</address>
</center>
</small>
</body>
</html>

