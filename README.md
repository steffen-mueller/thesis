# README #

Dies ist die Implementierung zur Diplomarbeit von Steffen Müller an der TU Darmstadt.

### Inhalt ###

* Algorithmus zur Textklassifikation anhand eines Schlüsselwort-Graphen
* Implementiert mit Java
* Integriert eine Embedded Neo4J Datenbank
* Resources usw. für Testfälle enthalten, außerdem Ergebnisprotokolle.

### Setup ###

* Alles inklusive: das Repo enthält ein IntelliJ IDEA Projekt, das sich direkt öffnen lassen sollte
* config.txt im Working Dir - Standardeinstellungen werden mitgeliefert
* Dependencies: Neo4J Embedded (enthalten), JSoup (auch enthalten), Porter Stemming (enthalten) und RAKE liegt noch im Repo, wird aber nicht mehr verwendet.
* Das Artefakt "Linked JAR" erstellen und einfach mit 'java -jar Thesis.jar' ausführen

### Contribution ###

* Keine weiteren Arbeiten geplant. Macht mit dem Quelltext was ihr wollt.

### Who do I talk to? ###

* Steffen Müller, den Repo Owner.