package me.steffenmueller.thesis;

import me.steffenmueller.thesis.controller.Controller;
import me.steffenmueller.thesis.controller.DependencyContainer;
import me.steffenmueller.thesis.extractor.KeywordExtractor;
import me.steffenmueller.thesis.graphbuilder.IntermediateKeywordRelationshipStore;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        System.out.println("Keyword Graph System starting!");

        // Instantiate the dependency container and fill it with options and config file content
        DependencyContainer con = new DependencyContainer();
        parseParameters(args, con);

        // Create the controller, based on the config file and parameters stored in the depcon
        Controller ctr = processConfigFileAndCreateController(con);

        // Output empty line to have space between option status and execution.
        System.out.println("");

        // Find command and execute it
        String command = "";
        boolean commandFound = false;

        // The command is to test a whole document set using the current configuration.
        command = getParameter(args, "testset");
        if (command.length() > 0) {
            commandFound = true;
            System.out.println("Running 'testset': going to test all documents in '"+command+"' by one-off method.");

            File setp = new File(command);
            String reportPath = con.properties.getProperty("tmpPath", "tmp")+"/report-"+(new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()))+"-"+setp.getName().toLowerCase()+".txt";

            ctr.testDocumentSet(command, reportPath);
        }

        // The command is to test a single document from a set
        command = getParameter(args, "testfile");
        if (command.length() > 0) {
            commandFound = true;
            File setp = new File(command);
            System.out.println("Running 'testfile': going to test the single document '"+setp.getName()+"' against the document set in '"+setp.getParent()+"'.");

            ctr.testSingleDocument(setp.getParent(), setp.getName());
        }

        // No command, sir!
        if (!commandFound) {
            System.out.println("No command specified. Please specify one of the following commands:");
            System.out.println("    testset [path-to-folder]");
            System.out.println("    testfile [path-to-document]");
        }


        // Processes a very simple document set to get a small database that can be analyzed by hand
        // ctr.processDocumentSet("resources/series-sam");

        // Classify a single document from a document set
        //ctr.testSingleDocument("resources/series-f", "doc.06");

        // Test a whole set and create a whole output.
        //

        ctr.shutdown();
    }

    protected static Controller processConfigFileAndCreateController (DependencyContainer con) {
        String dbPath = con.properties.getProperty("tmpPath", "tmp")+"/db-"+(new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()))+".graphdb";
        Controller ctr = new Controller(con, dbPath, true);
        ctr.initialize();
        System.out.println("... using database path "+dbPath);

        // Set the stopwords
        try {
            String swf = con.properties.getProperty("stopwordFile", "resources/stopwords-en.txt");
            ctr.setStopWordFilePath(swf);
            System.out.println("... using stopword file "+swf);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }

        // Set the weighting method
        String wm = con.properties.getProperty("relationWeightingMethod", "chi");
        if (wm.equalsIgnoreCase("chi")) {
            con.relationWeightingMethod = IntermediateKeywordRelationshipStore.WeightingMethod.Chi;
        }
        else if (wm.equalsIgnoreCase("weightedchi") || wm.equalsIgnoreCase("weighted-chi")) {
            con.relationWeightingMethod = IntermediateKeywordRelationshipStore.WeightingMethod.WeightedChi;
        }
        else if (wm.equalsIgnoreCase("naive")) {
            con.relationWeightingMethod = IntermediateKeywordRelationshipStore.WeightingMethod.Naive;
        }
        else {
            System.out.println("Aborting: the unknown weighting method '"+wm+"' was specified.");
            System.exit(1);
        }
        System.out.println("... using relation weighting method "+wm);

        // Set the cutoff method
        String cm = con.properties.getProperty("relationCutoffMethod", "none");
        if (cm.equalsIgnoreCase("none")) {
            con.relationCutoffMethod = IntermediateKeywordRelationshipStore.CutoffMethod.None;
        }
        else if (cm.equalsIgnoreCase("pareto")) {
            con.relationCutoffMethod = IntermediateKeywordRelationshipStore.CutoffMethod.Pareto;
        }
        else if (cm.equalsIgnoreCase("mean")) {
            con.relationCutoffMethod = IntermediateKeywordRelationshipStore.CutoffMethod.Mean;
        }
        else {
            System.out.println("Aborting: the unknown cutoff method '"+cm+"' was specified.");
            System.exit(1);
        }
        System.out.println("... using relation cutoff method "+cm);

        // Set the keyword weighting method
        String kwm = con.properties.getProperty("keywordWeightingMethod", "tf-idf");
        if (kwm.equalsIgnoreCase("tf-idf") || kwm.equalsIgnoreCase("tfidf")) {
            con.keywordWeightingMethod = KeywordExtractor.WeightingMethod.TfIdf;
        }
        else {
            System.out.println("Aborting: the unknown keyword weighting method '"+kwm+"' was specified.");
            System.exit(1);
        }
        System.out.println("... using keyword weighting method "+kwm);

        con.keywordThresholdFactor = ((float)Integer.parseInt(con.properties.getProperty("keywordThresholdFactorPercent", "20")))/100;
        con.keywordHardLimit = Integer.parseInt(con.properties.getProperty("keywordHardLimit", "128"));
        System.out.println("... using keyword threshold factor of "+(int)(con.keywordThresholdFactor*100)+"% with a hard limit of "+con.keywordHardLimit+" keywords per document.");

        return ctr;
    }

    protected static void parseParameters (String[] args, DependencyContainer con) {
        // Find the config file
        String cfgPath = getParameter(args, "-cfg");
        if (cfgPath.length() == 0) {
            System.out.println("... no config file parameter found, using default 'config.txt' in workdir. Use '-cfg pathToFile.txt' to change.");
            cfgPath = "config.txt";
        }
        con.configFilePath = cfgPath;
        System.out.println("... using config file '"+con.configFilePath+"'.");

        // Load the config in the file
        try {
            Properties properties = new Properties();
            BufferedInputStream stream = new BufferedInputStream(new FileInputStream(cfgPath));
            properties.load(stream);
            stream.close();
            con.properties = properties;
        }
        catch (Exception ex) {
            System.out.println("Aborting. Could not load properties file: "+ex.getLocalizedMessage());
            System.exit(1);
        }

        // Find debug flag
        boolean debug = existsParameter(args, "-debug");
        if (debug) {
            System.out.println("... enabling debug mode.");
            con.enableDebug();
        }
        else
            System.out.println("... using production mode. Switch to debug mode with the '-debug' flag.");
    }

    protected static String getParameter (String[] args, String key) {
        boolean foundParam = false;
        String result = "";
        for (String a : args) {
            if (a.equalsIgnoreCase(key)) {
                foundParam = true;
                continue;
            }
            if (foundParam) {
                result = a;
                break;
            }
        }

        return result;
    }

    protected static boolean existsParameter (String[] args, String key) {
        for (String a : args) {
            if (a.equalsIgnoreCase(key)) {
                return true;
            }
        }

        return false;
    }
}
