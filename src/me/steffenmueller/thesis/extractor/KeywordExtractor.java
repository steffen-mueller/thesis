/*******************************************************************************
 *  Based on the RAKE keyword extractor that is copyright 2010 Stephen O'Rourke
 *  (stephen.orourke@sydney.edu.au).
 *
 *  This modified version only extracts single words, not multiple words as
 *  keywords. This fits the graph relation approach better. The original was mainly
 *  extended to have a proven approach for word frequency etc.
 *
 *  As the original code, this is licensed under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at 
 *
 *  	http://www.apache.org/licenses/LICENSE-2.0 
 *
 *  Unless required by applicable law or agreed to in writing, software 
 *  distributed under the License is distributed on an "AS IS" BASIS, 
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *  See the License for the specific language governing permissions and 
 *  limitations under the License.
 *******************************************************************************/
package me.steffenmueller.thesis.extractor;

import me.steffenmueller.thesis.controller.DependencyContainer;

import java.util.*;

/**
 * This class extracts keywords from a document contained in a corpus. It analyzes the corpus beforehand.
 * The algorithm used is TF-IDF.
 *
 * @author Steffen Müller
 *
 */
public class KeywordExtractor {

    public enum WeightingMethod {
        TfIdf
    }

    KeywordCorpusAnalyser corpusAnalyser;
    DependencyContainer depCon;

    public KeywordExtractor (DependencyContainer con, KeywordCorpusAnalyser analyser) {
        corpusAnalyser = analyser;
        depCon = con;
    }

    public List<Result> execute (String text, Locale locale, Set<String> stopwords) {
        // extract keywords
        TextWordExtractor wordExtractor = new TextWordExtractor(stopwords);
        List<String> words = wordExtractor.extractStemmedWords(text);

        // Calculate word frequencies
        Map<String, Integer> wordFrequencies = new HashMap<String, Integer>();
        for (String word : words) {
            if (wordFrequencies.containsKey(word)) {
                wordFrequencies.put(word, wordFrequencies.get(word) + 1);
            } else {
                wordFrequencies.put(word, 1);
            }
        }

        // Find max frequency
        int max = 0;
        for (int freq : wordFrequencies.values()) {
            if (freq > max)
                max = freq;
        }

        // calculate keyword weighting results
        double maxWeight = 0;
        ArrayList<Result> results = new ArrayList<Result>();
        for (String word : new HashSet<String>(words)) {
            double weight = 0;

            if (depCon.keywordWeightingMethod == WeightingMethod.TfIdf)
                weight = ((double) wordFrequencies.get(word) / max) * corpusAnalyser.getIdfForWord(word);

            if (weight > maxWeight)
                maxWeight = weight;

            results.add(new Result(word, weight));
        }

        // After-Filter: remove all words with a weight below threshold.
        double minWeight = maxWeight * depCon.keywordThresholdFactor;
        for (Iterator<Result> iterator = results.iterator(); iterator.hasNext();) {
            Result res = iterator.next();

            if (res.getWeight() < minWeight)
                iterator.remove();
        }

        // sort results by keyword weighting
        Collections.sort(results, Collections.reverseOrder());

        // Hard limit - not nice, I know.
        int maxc = depCon.keywordHardLimit;
        if (results.size() < maxc) maxc = results.size();

        return results.subList(0, maxc);
    }

    public class Result implements Comparable<Result> {
        private String keyword;
        private Double weight;

        public Result (String kw, double w) {
            keyword = kw;
            weight = w;
        }

        public String getKeyword() {
            return keyword;
        }

        public Double getWeight() {
            return weight;
        }

        @Override
        public int compareTo(Result result) {
            return this.weight.compareTo(result.weight);
        }
    }

}
