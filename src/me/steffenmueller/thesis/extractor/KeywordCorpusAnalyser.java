package me.steffenmueller.thesis.extractor;

import me.steffenmueller.thesis.controller.DependencyContainer;
import me.steffenmueller.thesis.utils.StopwordStore;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Set;

/**
 * This class analyses a document corpus regarding the contained keywords as a preparation for TF-IDF.
 * @author Steffen Müller
 */
public class KeywordCorpusAnalyser {

    protected DependencyContainer depCon;
    protected StopwordStore stopWordStore;
    protected TextWordExtractor wordExtractor;
    protected Charset encoding = Charset.defaultCharset();

    protected int totalCount;
    protected HashMap<String, Integer> wordMap;

    public KeywordCorpusAnalyser (DependencyContainer depcon, StopwordStore store) {
        depCon = depcon;
        stopWordStore = store;
        wordExtractor = new TextWordExtractor(stopWordStore.getStopWordSet());

        totalCount = 0;
        wordMap = new HashMap<String, Integer>();
    }

    public void addDocument (File path) throws IOException {
        totalCount++;

        // Get all UNIQUE words in the document
        byte[] encoded = Files.readAllBytes(path.toPath());
        Set<String> words = wordExtractor.extractUniqueWords(new String(encoded, encoding));

        // Each unique word gets a wordMap entry.
        for (String word : words) {
            if (wordMap.containsKey(word))
                wordMap.replace(word, wordMap.get(word)+1);
            else
                wordMap.put(word, 1);
        }
    }

    public double getIdfForWord (String word) {
        if (totalCount < 1) return 1;

        if (!wordMap.containsKey(word))
            return 0.0;

        return Math.log((double) totalCount / wordMap.get(word));
    }
}
