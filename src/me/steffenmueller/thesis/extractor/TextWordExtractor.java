package me.steffenmueller.thesis.extractor;

import org.tartarus.porter.PorterStemmer;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Steffen on 21.03.2015.
 */
public class TextWordExtractor {
    Set<String> stopwords;
    protected int minWordLength = 3;

    public TextWordExtractor (Set<String> stopwords) {
        this.stopwords = stopwords;
    }

    public List<String> extractStemmedWords(String text) {

        List<String> words = new LinkedList<String>();

        // Prepare the porter stemmer
        PorterStemmer stemmer = new PorterStemmer();

        // Extract each and every word
        String[] tokenizedTerms = text.replaceAll("[\\W&&[^\\s]]", "").split("\\W+");
        for (String rawWord : tokenizedTerms) {
            String word = cleanWord(rawWord);

            if (stopwords.contains(word) || word.matches("\\W+") || word.matches(".*\\d.*"))
                continue;

            if (word.length() < minWordLength)
                continue;

            stemmer.add(word.toCharArray(), word.length());
            stemmer.stem();
            words.add(stemmer.toString());
        }

        return words;
    }

    public Set<String> extractUniqueWords (String text) {
        List<String> allWords = extractStemmedWords(text);
        return new HashSet<String>(allWords);
    }

    private String cleanWord(String word) {
        return word.trim().toLowerCase();
    }
}
