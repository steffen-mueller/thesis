package me.steffenmueller.thesis.graphdb;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseBuilder;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.helpers.collection.IteratorUtil;
import org.neo4j.kernel.impl.util.StringLogger;

import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * Created by Steffen on 28.12.2014.
 */
public class GraphDatabase {

    protected GraphDatabaseService neoDb;
    protected ExecutionEngine neoEngine;

    public void initialize (String dbPath, String configPath) {
        GraphDatabaseBuilder factory = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(dbPath);
        if (configPath.length() > 0)
            factory.loadPropertiesFromFile(configPath);

        neoDb = factory.newGraphDatabase();
        neoEngine = new ExecutionEngine(neoDb, StringLogger.SYSTEM);

        createNeoShutdownHook();
    }

    public void initialize (String dbPath) {
        initialize(dbPath, "");
    }

    public Transaction beginTx () {
        return this.neoDb.beginTx();
    }

    public ExecutionResult executeQuery (String q) {
        ExecutionResult result = neoEngine.execute(q);
        return result;
    }

    public Node findNode (String labelString, String propkey, String value) {
        ExecutionResult res = this.executeQuery("MATCH (node:" + labelString + " {" + propkey + ": \"" + value.replace("\"", "\\\"")+"\"}) RETURN node");

        Iterator<Node> n_column = res.columnAs("node");
        for ( Node node : IteratorUtil.asIterable(n_column) )
        {
            return node;
        }

        return null;
    }

    public Node createNode (String labelString, Map<String,String> properties) {
        final Node myNode = neoDb.createNode();

        // Add a label
        Label myLabel = DynamicLabel.label(labelString);
        myNode.addLabel(myLabel);

        // Set all properties
        properties.forEach(new BiConsumer<String, String>() {
            @Override
            public void accept(String key, String val) {
                myNode.setProperty(key, val);
            }
        });
        return myNode;
    }

    public Relationship findRelationship (Node n1, Node n2, String type) {
        for (Relationship r : n1.getRelationships(DynamicRelationshipType.withName(type), Direction.BOTH)) {
            if (r.getOtherNode(n1).getId() == n2.getId())
                return r;
        }
        return null;
    }

    public Relationship createRelationship (Node n1, Node n2, String type) {
        return n1.createRelationshipTo(n2, DynamicRelationshipType.withName(type));
    }

    public void shutdown () {
        if (neoDb != null)
            neoDb.shutdown();
    }


    /**
     * Registers a shutdown hook for the Neo4j instance so that it shuts down nicely when the VM exits
     * (even if you "Ctrl-C" the running application).
     */
    protected void createNeoShutdownHook () {
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                neoDb.shutdown();
            }
        } );
    }

}
