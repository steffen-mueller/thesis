package me.steffenmueller.thesis.controller;

import me.steffenmueller.thesis.analyzer.DocumentAnalyzer;
import me.steffenmueller.thesis.analyzer.DocumentAnalyzerResult;
import me.steffenmueller.thesis.extractor.KeywordCorpusAnalyser;
import me.steffenmueller.thesis.graphdb.GraphDatabase;
import me.steffenmueller.thesis.graphbuilder.GraphBuilder;
import me.steffenmueller.thesis.utils.FileUtils;
import me.steffenmueller.thesis.utils.HtmlDocument;
import me.steffenmueller.thesis.utils.StopwordStore;

import java.io.*;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Created by Steffen on 28.12.2014.
 */
public class Controller {
    protected GraphDatabase db;
    protected DependencyContainer depCon;
    protected StopwordStore stopWordStore;
    protected String dbStorePath;
    protected boolean doWipeDb;

    public Controller (DependencyContainer container, String databaseStorePath, boolean wipeDb) {
        depCon = container;
        dbStorePath = databaseStorePath;
        db = null;
        doWipeDb = wipeDb;
    }

    public void initialize () {
        stopWordStore = new StopwordStore(depCon);
    }

    protected void lazyInitializeDatabase () throws IOException {
        if (db != null) return;

        File path = new File(dbStorePath);
        if (path.exists()) {
            FileUtils.removeRecursive(path.toPath());
        }

        db = new GraphDatabase();
        db.initialize(dbStorePath);
    }

    protected void closeDatabase () {
        if (db == null) return;
        db.shutdown();
        db = null;

        try {
            Thread.sleep(5000);
        }
        catch (InterruptedException ex) {}
    }

    public void setStopWordFilePath (String path) throws IOException {
        stopWordStore.loadStopWordsFromFile(path);
    }

    public String processDocumentSet (String path) {
        try {
            lazyInitializeDatabase();
            GraphBuilder builder = new GraphBuilder(depCon, db, stopWordStore);
            return builder.processDocumentSet(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    public void processSingleDocument (String path) {
        try {
            lazyInitializeDatabase();

            KeywordCorpusAnalyser analyser = new KeywordCorpusAnalyser(depCon, stopWordStore);
            analyser.addDocument(new File(path));

            GraphBuilder builder = new GraphBuilder(depCon, db, stopWordStore);
            builder.processFile(analyser, new File(path), new String[]{"std"});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void analyzeSingleDocument (String path, String graphLabel) {
        try {
            lazyInitializeDatabase();
            DocumentAnalyzer an = new DocumentAnalyzer(depCon, db, graphLabel, stopWordStore);
            an.analyzeFile(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void testDocumentSet (String setPath, String logPath) {

        PrintWriter logger = null;
        try {

            File logFile = new File(logPath);
            if (logFile.exists())
                logFile.delete();

            logger = new PrintWriter(new FileWriter(logPath, true));
            logger.println("Graph-based text classification test protocol - "+(new Date()).toString());
            logger.println("============================================================================");
            logger.println("");
            logger.println("Testing all documents in '"+setPath+"'");
            logger.flush();

            File metaFile = new File(setPath+File.separator+"meta.txt");
            Map<String,String[]> classMap = FileUtils.parseMetaFile(metaFile);

            File folder = new File(setPath);

            int success = 0;
            int successGroup = 0;
            int fail = 0;
            int total = 0;

            for (final File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) continue;
                if (fileEntry.getName().equals("meta.txt")) continue;
                if (!classMap.containsKey(fileEntry.getName())) continue;

                total++;

                String bn = fileEntry.getName();

                closeDatabase();
                DocumentAnalyzerResult res = this.testSingleDocument(setPath, bn);

                boolean passedExact = false;
                boolean passedGroup = false;
                String expected = "";
                for (String s : classMap.get(bn)) {
                    expected += s+"-";
                    if (s.equals(res.getClassification())) {
                        passedExact = true;
                        break;
                    }
                    else if (s.substring(0,1).equals(res.getClassification().substring(0,1))) {
                        passedGroup = true;
                    }
                }

                expected = expected.substring(0, expected.length()-1);

                if (passedExact) {
                    logger.println("- "+bn+": correctly classified as "+res.getClassification()+" with "+res.getConfidencePercent()+"% confidence.");
                    success++;
                    successGroup++;
                } else if (passedGroup) {
                    logger.println("- "+bn+": correctly group-classified as "+res.getClassification().substring(0,1)+" but "+res.getClassification()+" was no direct hit in "+expected+".");
                    successGroup++;
                }
                else {
                    logger.println("- "+bn+": FAILED. Expected "+expected+" but got this:");
                    logger.println(res.toString());
                    logger.println("");
                    fail++;
                }

                logger.println("Intermediate result: " + (int)( ((float)success / total) * 100) + "% exact / " + (int)(((float)successGroup / total) * 100)+"% group success rate.");
                logger.flush();
            }

            logger.println("Done. "+success+" exact matches, "+successGroup+" group matches, "+fail+" humiliating fails in "+total+" total documents.");
            logger.println("Final result: " + (int)( ((float)success / total) * 100) + "% epic / " + (int)(((float)successGroup / total) * 100)+"% group success rate.");
            logger.close();

        } catch (IOException ex) {
            ex.printStackTrace();
            if (logger != null)
                logger.close();
        }
    }

    public DocumentAnalyzerResult testSingleDocument (String setPath, String testFileName) {

        try {
            lazyInitializeDatabase();

            GraphBuilder builder = new GraphBuilder(depCon, db, stopWordStore);
            builder.setExcludedBasename(testFileName);
            String gl = builder.processDocumentSet(setPath);
            if (!builder.wasExcludedBasenameUsed())
                throw new IOException("The file '"+testFileName+"' to test was not excluded from the parsed document set?");

            String fullTestFile = setPath+File.separator+testFileName;
            DocumentAnalyzer an = new DocumentAnalyzer(depCon, db, gl, stopWordStore);
            DocumentAnalyzerResult result = an.analyzeFile(new File(fullTestFile), builder.getLastUsedCorpusAnalyser());

            depCon.out.info("== Classification for "+testFileName+" ==");
            depCon.out.info(result.toString());

            return result;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void extractKeywordsFromDocument (String filePath) {
        try {
            KeywordCorpusAnalyser analyser = new KeywordCorpusAnalyser(depCon, stopWordStore);
            analyser.addDocument(new File(filePath));

            // Parse the keywords from the document
            HtmlDocument doc = new HtmlDocument(depCon, analyser);
            doc.setStopWords(stopWordStore.getStopWordSet());
            doc.loadFromFile(new File(filePath));
            Collection<HtmlDocument.KeywordResult> result = doc.parseKeywords();

            for (HtmlDocument.KeywordResult res : result) {
                depCon.out.info(res.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void shutdown () {
        closeDatabase();
    }
}
