package me.steffenmueller.thesis.controller;

import me.steffenmueller.thesis.extractor.KeywordExtractor;
import me.steffenmueller.thesis.graphbuilder.IntermediateKeywordRelationshipStore;
import org.neo4j.kernel.impl.util.StringLogger;

import java.util.Properties;

/**
 * Created by Steffen on 29.12.2014.
 */
public class DependencyContainer {
    /**
     * The logger of choice. Direct any output in the whole project to this logger. The user can set different log levels
     * etc.
     */
    public StringLogger out;

    /**
     * Path to the config file that populates the @properties.
     */
    public String configFilePath = "";

    /**
     * Contains all properties from the config file that was passed at program start (usually config.txt in the workdir).
     */
    public Properties properties;

    /**
     * The weighting method to calculate the relation weights between keywords, as defined in the config file.
     */
    public IntermediateKeywordRelationshipStore.WeightingMethod relationWeightingMethod;

    /**
     * The cutoff method to reduce the number of relations that are saved - only a certain amount is allowed.
     */
    public IntermediateKeywordRelationshipStore.CutoffMethod relationCutoffMethod;

    /**
     * The weighting method to calculate weights of individual keywords in individual documents, as defined in config.
     */
    public KeywordExtractor.WeightingMethod keywordWeightingMethod;

    /**
     * Defines a threshold weight in percent of the maximum weight found. Keywords below this weight are cut off.
     * Range is 0 - 1, in the config it is specified between 0 and 100.
     */
    public float keywordThresholdFactor;

    /**
     * Maximum number of keywords used per document.
     */
    public int keywordHardLimit;

    public DependencyContainer () {
        out = StringLogger.SYSTEM;
    }

    public void enableDebug () {
        out = StringLogger.SYSTEM_DEBUG;
    }
}
