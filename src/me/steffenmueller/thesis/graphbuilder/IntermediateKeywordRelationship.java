package me.steffenmueller.thesis.graphbuilder;

import java.util.ArrayList;

/**
 * This class encapsules ONE relationship of a keyword with another keyword. It is more of a struct than a big and
 * important class. It is managed in the IntermediateKeywordRelationshipStore and used in the GraphBuilder.
 * Created by Steffen on 07.03.2015.
 */
public class IntermediateKeywordRelationship implements Comparable<IntermediateKeywordRelationship> {

    protected String kw1;
    protected String kw2;
    protected String category;

    protected ArrayList<Double> weights;
    protected double finalWeight;

    public IntermediateKeywordRelationship (String k1, String k2, String cat) {
        kw1 = k1;
        kw2 = k2;
        category = cat;
        weights = null;
        finalWeight = -1;
    }

    public String getKw1() {
        return kw1;
    }

    public String getKw2() {
        return kw2;
    }

    public String getCategory() {
        return category;
    }

    public int getCount () {
        if (weights == null)
            return 0;
        else
            return weights.size();
    }

    public ArrayList<Double> getWeights () {
        return weights;
    }

    public void addEntry (double w) {
        if (weights == null)
            weights = new ArrayList<Double>();

        weights.add(w);
    }

    public void setFinalWeight (double w) {
        weights = null;
        finalWeight = w;
    }

    public double getFinalWeight () {
        return finalWeight;
    }

    @Override
    public int compareTo(IntermediateKeywordRelationship o) {
        if (finalWeight == o.finalWeight) return 0;
        return (finalWeight < o.finalWeight) ? -1 : 1;
    }

    @Override
    public String toString() {
        String s = "'"+kw1+"' -["+category+"]-> '"+kw2+"'";
        if (finalWeight == -1)
            s += " (no final weight)";
        else
            s += " ("+finalWeight+")";
        return s;
    }
}
