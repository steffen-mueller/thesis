package me.steffenmueller.thesis.graphbuilder;

import me.steffenmueller.thesis.controller.DependencyContainer;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Stores the relationships created by the graph builder in structures that
 * allow finding them again.
 * Created by Steffen on 07.03.2015.
 */
public class IntermediateKeywordRelationshipStore {

    public enum WeightingMethod {
        Naive,
        Chi,
        WeightedChi
    }

    public enum CutoffMethod {
        None,
        Pareto,
        Mean
    }

    protected DependencyContainer depCon;

    /**
     * This is a huge map with 3 layers:
     * 1. Keyword 1
     * 2. Keyword 2
     * 3. Category
     * 4. The concrete IntermediateKeywordRelationship
     * So, it maps (kw1, kw2, category) to a IntermediateKeywordRelationship. Remember that the graph is not directed,
     * what will be kw1 and kw2 is random and depends on the order of insertion (first come, first index).
     */
    protected HashMap<String, HashMap<String,HashMap<String,IntermediateKeywordRelationship>>> store;

    /**
     * Saves the total number of existing relations.
     */
    protected int size;

    /**
     * Saves the total number of existing relations.
     */
    protected HashMap<String, Integer> categoryCounts;


    /**
     * Saves the number of documents processed in total.
     */
    protected int documentCount;

    /**
     * Weighting statistics: this is the total weight summed up over all relations.
     */
    protected double totalWeight;
    /**
     * Weighting statistics: this is the maximum weight found in the heaviest relation.
     */
    protected double maxWeight;

    public IntermediateKeywordRelationshipStore (DependencyContainer container) {
        depCon = container;
        store = new HashMap<String, HashMap<String,HashMap<String,IntermediateKeywordRelationship>>>();
        categoryCounts = new HashMap<String, Integer>();
        size = 0;
        documentCount = 0;
    }

    public int size () {
        return size;
    }

    public int getDocumentCount () {
        return documentCount;
    }

    public void startNewDocument (String[] categories) {
        documentCount++;

        // Save all the categories of this document
        for (int i = 0; i < categories.length; i++) {
            Integer c = categoryCounts.getOrDefault(categories[i], 0);
            if (categoryCounts.containsKey(categories[i]))
                categoryCounts.replace(categories[i], c + 1);
            else
                categoryCounts.put(categories[i], c + 1);
        }
    }

    public double getTotalWeight () {
        return totalWeight;
    }

    public double getMaxWeight () {
        return maxWeight;
    }


    /**
     * Finds an IntermediateKeywordRelationship in the mighty store. Traverses all layers and creates missing
     * elements along the way. Remember, the graph is not directed, kw1 and kw2 can be interchanged.
     * @param kw1 One of the keywords
     * @param kw2 The other keyword
     * @param category The category
     * @return
     */
    IntermediateKeywordRelationship findOrCreateRelationship (String kw1, String kw2, String category, double weight) {

        // Try to find both keywords in the store. Mind you, the graph is not directed, we can start with both!
        HashMap<String,HashMap<String,IntermediateKeywordRelationship>> set = null;
        set = store.get(kw1);
        if (set == null) {
            set = store.get(kw2);
            if (set != null) {
                String tmp = kw1;
                kw1 = kw2;
                kw2 = tmp;
            }
        }

        // Both keywords are not yet known. Create an entry for one of them.
        if (set == null) {
            set = new HashMap<String,HashMap<String,IntermediateKeywordRelationship>>();
            store.put(kw1, set);
        }

        // If we are here, we have a set and kw1 is the index in the store. Now, find the second keyword.
        HashMap<String,IntermediateKeywordRelationship> set2 = null;
        set2 = set.get(category);
        if (set2 == null) {
            set2 = new HashMap<String,IntermediateKeywordRelationship>();
            set.put(kw2, set2);
        }

        // We have kw1, kw2 and the category. Now, find or create the relationship
        IntermediateKeywordRelationship rel = null;
        rel = set2.get(category);
        if (rel == null) {
            rel = new IntermediateKeywordRelationship(kw1, kw2, category);
            set2.put(category, rel);

            size++;
        }

        // We have the relationship. Give it an entry.
        rel.addEntry(weight);

        return rel;
    }

    public List<IntermediateKeywordRelationship> weightAndReturnRelationships (WeightingMethod wmethod, CutoffMethod cmethod) {
        final ArrayList<IntermediateKeywordRelationship> out = new ArrayList<IntermediateKeywordRelationship>(size);

        // Initialise the statistic keepers
        totalWeight = 0;
        maxWeight = 0;

        // Loop over all relations and calculate their weights
        store.forEach(new BiConsumer<String, HashMap<String, HashMap<String, IntermediateKeywordRelationship>>>() {
            @Override
            public void accept(String kw1, HashMap<String, HashMap<String, IntermediateKeywordRelationship>> kw1Map) {
                kw1Map.forEach(new BiConsumer<String, HashMap<String, IntermediateKeywordRelationship>>() {
                    @Override
                    public void accept(String kw2, HashMap<String, IntermediateKeywordRelationship> kw2Map) {
                        kw2Map.forEach(new BiConsumer<String, IntermediateKeywordRelationship>() {
                            @Override
                            public void accept(String cat, IntermediateKeywordRelationship rel) {
                                switch (wmethod) {
                                    case Naive:
                                        rel.setFinalWeight(calculateWeightNaive(rel));
                                        break;
                                    case Chi:
                                        rel.setFinalWeight(calculateWeightChi(rel, kw2Map));
                                        break;
                                    case WeightedChi:
                                        rel.setFinalWeight(calculateWeightChi(rel, kw2Map)*calculateWeightNaive(rel));
                                        break;
                                }

                                totalWeight += rel.getFinalWeight();
                                if (rel.getFinalWeight() > maxWeight)
                                    maxWeight = rel.getFinalWeight();

                                out.add(rel);
                            }
                        });
                    }
                });
            }
        });

        if (cmethod == CutoffMethod.Pareto)
            return applyParetoCutoff (out);
        if (cmethod == CutoffMethod.Mean)
            return applyMeanCutoff(out);

        // No cutoff: just return.
        return out;
    }

    protected double calculateWeightNaive (IntermediateKeywordRelationship rel) {
        ArrayList<Double> weights = rel.getWeights();

        double sum = 0;
        for (int i = 0; i < weights.size(); i++)
            sum += weights.get(i);

        return (sum/weights.size());
    }

    protected double calculateWeightChi (IntermediateKeywordRelationship rel, HashMap<String, IntermediateKeywordRelationship> kw2Map) {

        double A,B,C,D,N;

        // A: t enthalten und als c kategorisiert. Dies entspricht genau der Anzahl der Fundstellen dieser Kante.
        A = rel.getCount();

        // B: t enthalten und NICHT als c kategorisiert. Das entspricht der Anzahl Fundstellen dieser Kante in allen anderen Kategorien
        B = 0;
        for (String key : kw2Map.keySet()) {
            if (key.compareTo(rel.getCategory()) != 0)
                B += kw2Map.get(key).getCount();
        }

        // C: t NICHT enthalten und als c kategorisiert. Entspricht Gesamtzahl an c-Kategorisierungen minus der enthaltenen.
        C = categoryCounts.getOrDefault(rel.getCategory(), 0) - A;

        // D: t NICHT enthalten und NICHT als c kategorisiert. Entspricht Gesamtzahl Dokumente - Kategorisiert als c - B
        D = documentCount - categoryCounts.getOrDefault(rel.getCategory(), 0) - B;

        // N: einfach die Anzahl verschiedener Dokumente.
        N = documentCount;

        double res = (N * Math.pow(A*D - C*B, 2)) / ((A+C)*(B+D)*(A+B)*(C+D));

        return res;
    }

    protected List<IntermediateKeywordRelationship> applyParetoCutoff (List<IntermediateKeywordRelationship> out) {

        // Reset total weight as we will kick out some.
        totalWeight = 0;

        // Sort the array.
        Collections.sort(out);

        int paretoCount = (int) Math.floor(out.size()*0.2);
        out = out.subList(out.size()-paretoCount-1, out.size()-1);

        for (int i = 0; i < out.size(); i++) {
            depCon.out.debug("+ "+out.get(i));
            totalWeight += out.get(i).getFinalWeight();
        }

        return out;
    }

    protected List<IntermediateKeywordRelationship> applyMeanCutoff (List<IntermediateKeywordRelationship> out) {

        ArrayList<IntermediateKeywordRelationship> res = new ArrayList<IntermediateKeywordRelationship>(out.size());
        double mean = totalWeight / out.size();

        // Reset total weight as we will kick out some.
        totalWeight = 0;

        for (int i = 0; i < out.size(); i++) {
            if (out.get(i).getFinalWeight() < mean)
                continue;

            res.add(out.get(i));
            totalWeight += out.get(i).getFinalWeight();
        }

        return res;
    }
}
