package me.steffenmueller.thesis.graphbuilder;

import me.steffenmueller.thesis.controller.DependencyContainer;
import me.steffenmueller.thesis.extractor.KeywordCorpusAnalyser;
import me.steffenmueller.thesis.graphdb.GraphDatabase;
import me.steffenmueller.thesis.utils.FileUtils;
import me.steffenmueller.thesis.utils.HtmlDocument;
import me.steffenmueller.thesis.utils.StopwordStore;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;

/**
 * Created by Steffen on 28.12.2014.
 */
public class GraphBuilder {

    protected StopwordStore stopWordStore;
    protected DependencyContainer depCon;
    protected GraphDatabase db;
    protected String excludedBaseName = "";
    protected boolean usedExcludedBaseName = false;

    protected KeywordCorpusAnalyser lastUsedCorpusAnalyser;

    protected IntermediateKeywordRelationshipStore relStore;

    public GraphBuilder(DependencyContainer container, GraphDatabase gdb, StopwordStore stopstore) {
        depCon = container;
        db = gdb;
        relStore = null;
        stopWordStore = stopstore;
        lastUsedCorpusAnalyser = null;
    }

    public KeywordCorpusAnalyser getLastUsedCorpusAnalyser () {
        return lastUsedCorpusAnalyser;
    }

    public void processDocumentSet(String path, String graphLabel) throws IOException {
        relStore = new IntermediateKeywordRelationshipStore(depCon);

        File folder = new File(path);

        File metaFile = new File(folder.getAbsolutePath() + File.separator + "meta.txt");
        if (!metaFile.exists())
            throw new FileNotFoundException("Meta file for document set not found (expected '"+metaFile.getAbsolutePath()+"' to exist)");
        Map<String,String[]> classMap = FileUtils.parseMetaFile(metaFile);

        KeywordCorpusAnalyser analyser = new KeywordCorpusAnalyser(depCon, stopWordStore);
        lastUsedCorpusAnalyser = analyser;

        // Build a list of the files we REALLY want to process
        List<File> files = new ArrayList<File>((int) folder.length());
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) continue;
            if (fileEntry.getName().equals("meta.txt")) continue;
            if (fileEntry.getName().equals(excludedBaseName)) {
                usedExcludedBaseName = true;
                continue;
            }

            if (!classMap.containsKey(fileEntry.getName()))
                throw new IOException("The meta file '"+metaFile.getAbsolutePath()+"' does not contain a classification for source file '"+fileEntry.getName()+"'");

            files.add(fileEntry);
        }

        // Let the analyser run on the file
        for (File fileEntry: files) {
            analyser.addDocument(fileEntry);
        }

        // Analyse each file
        for (File fileEntry: files) {
            this.processFile(analyser, fileEntry, classMap.get(fileEntry.getName()));
        }

        // Calculate real edge weights and receive relations for that.
        List<IntermediateKeywordRelationship> relations = calculateEdgeWeights();

        depCon.out.info("Commiting those relations to the database.");
        commitRelationsToDb(relations, graphLabel);
        depCon.out.info("Done committing. The db is filled.");
    }

    public void setExcludedBasename (String bn) {
        excludedBaseName = bn;
        usedExcludedBaseName = false;
    }

    public boolean wasExcludedBasenameUsed () {
        return usedExcludedBaseName;
    }

    public String processDocumentSet(String path) throws IOException {

        File folder = new File(path);
        String graphLabel = folder.getName().replaceAll("[^A-Za-z]", "").toLowerCase();

        processDocumentSet(path, graphLabel);

        return graphLabel;
    }

    protected List<IntermediateKeywordRelationship> calculateEdgeWeights () throws IOException {
        if (relStore == null)
            throw new IOException("Tried to calculate edge weights without intermediate keyword relationship store.");

        depCon.out.info("Starting to weight "+relStore.size()+" edges from "+relStore.getDocumentCount()+" documents.");
        List<IntermediateKeywordRelationship> relations = relStore.weightAndReturnRelationships(depCon.relationWeightingMethod, depCon.relationCutoffMethod);
        depCon.out.info("Created a list of "+relations.size()+" relations with a total weight of "+relStore.getTotalWeight()+" and a max weight of "+relStore.getMaxWeight()+".");

        return relations;
    }

    protected void commitRelationsToDb (List<IntermediateKeywordRelationship> list, String graphLabel) {

        // Create all the nodes and the relationships between them
        final Map<String, Node> nodeMap = new HashMap<String, Node>();

        int written = 0;
        while (written < list.size()) {
            try (Transaction tx = db.beginTx();) {

                int ctr = 10000;
                if (list.size()-written < ctr) ctr = list.size()-written;

                depCon.out.info("... writing batch from #"+written+" to #"+(written+ctr));

                for (int i = 0; i < ctr; i++) {
                    IntermediateKeywordRelationship rel = list.get(i);

                    if (!nodeMap.containsKey(rel.getKw1()))
                        nodeMap.put(rel.getKw1(), createNode(rel.getKw1(), graphLabel));
                    if (!nodeMap.containsKey(rel.getKw2()))
                        nodeMap.put(rel.getKw2(), createNode(rel.getKw2(), graphLabel));

                    Relationship r = db.createRelationship(nodeMap.get(rel.getKw1()), nodeMap.get(rel.getKw2()), rel.getCategory());
                    r.setProperty("weight", rel.getFinalWeight());
                    written++;
                }

                tx.success();
            }
        }
    }

    protected Node createNode (String kw, String graphLabel) {
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("keyword", kw);
        return db.createNode(graphLabel, properties);
    }

    public void processFile(KeywordCorpusAnalyser analyser, File path, String[] classifications) throws IOException {

        if (relStore == null)
            relStore = new IntermediateKeywordRelationshipStore(depCon);

        relStore.startNewDocument(classifications);

        // Create beautiful debug output. Crazy. By the way, check for at least one classification.
        String info = "Starting to process file '" + path.getName() + "' in categor";
        if (classifications.length == 1)
            info += "y '"+classifications[0]+"'.";
        else if (classifications.length > 1) {
            info += "ies ";
            for (int i = 0; i < classifications.length; i++) {
                info += "'"+classifications[i]+"'";
                if (i < classifications.length-1) info += ", ";
            }
            info += ".";
        }
        else
            throw new IOException("Exception in metadata for file '"+path.getName()+"': at least one classification has to be given!");
        depCon.out.info(info);

        // Parse the keywords from the document
        HtmlDocument doc = new HtmlDocument(depCon, analyser);
        doc.setStopWords(stopWordStore.getStopWordSet());
        doc.loadFromFile(path);
        final Collection<HtmlDocument.KeywordResult> result = doc.parseKeywords();

        // Create the edges - one for each category.
        final ArrayList<HtmlDocument.KeywordResult> visitedList = new ArrayList<HtmlDocument.KeywordResult>();

        // Iterate over all classifications as the process is repeated for all classifications alike.
        for (int i = 0; i < classifications.length; i++) {
            String classification = classifications[i];

            // Iterate over all keywords in the result set
            result.forEach(new Consumer<HtmlDocument.KeywordResult>() {
                @Override
                public void accept(HtmlDocument.KeywordResult res) {

                    // Iterate over each other keyword so we get the combination of all keywords (n x n)
                    result.forEach(new Consumer<HtmlDocument.KeywordResult>() {
                        @Override
                        public void accept(HtmlDocument.KeywordResult res2) {
                            if (res == res2) return;

                            // Create a relationship between the two nodes.
                            if (!visitedList.contains(res2))
                                createOrCountKeywordRelationship(res, res2, classification, res.weight * res2.weight);
                        }
                    });

                    visitedList.add(res);
                }
            });
        }
    }

    public void createOrCountKeywordRelationship (HtmlDocument.KeywordResult k1, HtmlDocument.KeywordResult k2, String classification, double weight) {
        IntermediateKeywordRelationship r = relStore.findOrCreateRelationship(k1.keyword, k2.keyword, classification, weight);
    }
}
