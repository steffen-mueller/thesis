package me.steffenmueller.thesis.utils;

import me.steffenmueller.thesis.controller.DependencyContainer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Steffen on 07.03.2015.
 */
public class StopwordStore {

    protected DependencyContainer depCon;
    protected HashSet<String> stopWords;

    public StopwordStore (DependencyContainer container) {
        depCon = container;
        stopWords = new HashSet<String>();
    }

    public Set<String> getStopWordSet () {
        return stopWords;
    }

    public void loadStopWordsFromFile (String path) throws IOException {
        if (path == null)
            return;

        stopWords = new HashSet<String>();

        File file = new File(path);
        BufferedReader in = new BufferedReader(new FileReader(path));
        String line = "";
        while((line = in.readLine()) != null) {
            if (line != null)
                line = line.trim();
            stopWords.add(line);
        }
        in.close();

        depCon.out.debug("Read " + stopWords.size() + " stopwords from file '" + path + "'.");
    }

}
