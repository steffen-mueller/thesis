package me.steffenmueller.thesis.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Steffen on 15.03.2015.
 */
public class FileUtils {

    public static void removeRecursive(Path path) throws IOException
    {
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                // try to delete the file anyway, even if its attributes
                // could not be read, since delete-only access is
                // theoretically possible
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if (exc == null) {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                } else {
                    // directory iteration failed; propagate exception
                    throw exc;
                }
            }
        });
    }

    public static Map<String,String[]> parseMetaFile (File metaFile) throws IOException {
        String line = "";
        String cvsSplitBy = ",";

        BufferedReader br = null;

        HashMap<String,String[]> res = new HashMap<String,String[]>();

        try {
            br = new BufferedReader(new FileReader(metaFile));
            while ((line = br.readLine()) != null) {
                String[] lineParts = line.split(";");
                if (lineParts.length >= 2)
                    res.put(lineParts[0].trim(), lineParts[1].trim().split(","));
            }
        } finally {
            if (br != null) {
                br.close();
            }
        }

        return res;
    }

}
