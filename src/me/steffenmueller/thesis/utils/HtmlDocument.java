package me.steffenmueller.thesis.utils;

import me.steffenmueller.thesis.controller.DependencyContainer;
import me.steffenmueller.thesis.extractor.KeywordCorpusAnalyser;
import me.steffenmueller.thesis.extractor.KeywordExtractor;
import org.jsoup.Jsoup;
import org.tartarus.porter.PorterStemmer;

import java.io.File;
import java.util.*;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.function.Consumer;

/**
 * Created by Steffen on 28.12.2014.
 */
public class HtmlDocument {
    protected String html;
    protected String title;
    protected Locale locale = Locale.ENGLISH;
    protected Charset encoding = Charset.defaultCharset();

    protected double keywordThresholdFactor = 0.7;

    protected Set<String> stopWords;

    protected double totalWeight = 0;

    protected DependencyContainer depCon;
    protected KeywordCorpusAnalyser corpusAnalyser;

    public HtmlDocument (DependencyContainer container, KeywordCorpusAnalyser analyser) {
        depCon = container;
        corpusAnalyser = analyser;
    }

    public void loadFromFile (File file) throws IOException {
        byte[] encoded = Files.readAllBytes(file.toPath());
        loadFromString(new String(encoded, encoding), file.getName());
    }

    public void loadFromString (String htmlCode, String title) {
        this.html = htmlCode;
        this.title = title;
        totalWeight = 0;
    }

    public void setStopWords (Set<String> list) {
        stopWords = list;
    }

    public Collection<KeywordResult> parseKeywords () {

        depCon.out.info("Starting to parse keywords of document '"+this.title+"'");

        String text = Jsoup.parse(this.html).text();

        // Use the extractor to get a refined set of keywords
        KeywordExtractor extractor = new KeywordExtractor(depCon, corpusAnalyser);
        List<KeywordExtractor.Result> result = extractor.execute(text, this.locale, stopWords);
        depCon.out.info("... the extractor found "+result.size()+" keywords.");

        // Convert to html document keyword results
        Collection<KeywordResult> keywordCollection = new ArrayList<KeywordResult>();
        for (KeywordExtractor.Result res : result) {
            keywordCollection.add(new KeywordResult(res.getKeyword(), res.getWeight()));
        }

        if (depCon.out.isDebugEnabled()) {
            depCon.out.debug("... list of the "+keywordCollection.size()+" final keywords:");
            keywordCollection.forEach(new Consumer<KeywordResult>() {
                @Override
                public void accept(KeywordResult r) {
                    depCon.out.debug("...... " + r.keyword + ": " + r.weight);
                }
            });
        }
        else
            depCon.out.info("... found "+keywordCollection.size()+" final keywords.");

        return keywordCollection;
    }

    public class KeywordResult implements Comparable<KeywordResult> {
        public Double weight;
        public String keyword;

        public KeywordResult (String kw, double w) {
            keyword = kw;
            weight = w;
        }

        @Override
        public String toString () {
            return keyword+" ("+weight+")";
        }

        @Override
        public int compareTo(KeywordResult o) {
            return this.weight.compareTo(o.weight);
        }
    }
}
