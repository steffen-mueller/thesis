package me.steffenmueller.thesis.analyzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Steffen on 15.03.2015.
 */
public class DocumentAnalyzerResult {

    protected List<InternalResult> results;
    protected double totalResultWeight;

    protected DocumentAnalyzerResult (Map<String,Double> classMap) {

        totalResultWeight = 0;
        // Convert the map to an arraylist
        ArrayList<InternalResult> resList = new ArrayList<InternalResult>(classMap.size());
        for (Map.Entry<String, Double> entry : classMap.entrySet()) {
            InternalResult res = new InternalResult();
            res.weight = entry.getValue();
            res.classification = entry.getKey();
            resList.add(res);

            totalResultWeight += res.weight;
        }

        // Sort the results
        Collections.sort(resList, Collections.reverseOrder());

        results = resList;
    }

    public String getClassification () {
        if (results.size() > 0)
            return results.get(0).classification;
        else
            return "?";
    }

    public double getConfidence () {
        if (results.size() > 0)
            return results.get(0).weight / totalResultWeight;
        else
            return -1.0;
    }

    public int getConfidencePercent () {
        return (int)(getConfidence()*100);
    }

    @Override
    public String toString() {
        if (results.size() < 1)
            return "No classification available.";

        String s = "With "+getConfidencePercent()+"% confidence this is "+getClassification()+".\r\n";
        if (results.size() == 1)
            return s + "No other classifications possible.";

        s += "Other possible classifications:";

        for (int i = 1; i < results.size(); i++) {
            s += "... "+((int)(results.get(i).weight/totalResultWeight*100))+"% "+results.get(i).classification+"\r\n";
        }

        return s;
    }

    protected class InternalResult implements Comparable<InternalResult> {
        @Override
        public int compareTo(InternalResult o) {
            return weight.compareTo(o.weight);
        }

        public String classification;
        public Double weight;
    }
}
