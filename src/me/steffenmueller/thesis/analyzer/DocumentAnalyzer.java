package me.steffenmueller.thesis.analyzer;

import me.steffenmueller.thesis.controller.DependencyContainer;
import me.steffenmueller.thesis.extractor.KeywordCorpusAnalyser;
import me.steffenmueller.thesis.utils.HtmlDocument;
import me.steffenmueller.thesis.graphdb.GraphDatabase;
import me.steffenmueller.thesis.utils.StopwordStore;
import org.neo4j.cypher.javacompat.ExecutionResult;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.BiFunction;

/**
 * Analysiert ein Dokument auf Basis eines vom GraphAnalyzer vorberechneten Keywordgraphen. Errechnet eine Klassifikation
 * des Dokumentes.
 * Created by Steffen on 07.03.2015.
 */
public class DocumentAnalyzer {
    protected DependencyContainer depCon;
    protected GraphDatabase db;
    protected String graphLabel;
    protected StopwordStore stopWordStore;

    public DocumentAnalyzer(DependencyContainer container, GraphDatabase gdb, String glabel, StopwordStore stopstore) {
        depCon = container;
        db = gdb;
        graphLabel = glabel;
        stopWordStore = stopstore;
    }

    public DocumentAnalyzerResult analyzeFile (File path) throws IOException {

        // Create a corpus analysis on the single document. Yep, quite useless, but them are the rules.
        KeywordCorpusAnalyser analyser = new KeywordCorpusAnalyser(depCon, stopWordStore);
        analyser.addDocument(path);

        return analyzeFile(path, analyser);
    }

    public DocumentAnalyzerResult analyzeFile (File path, KeywordCorpusAnalyser analyser) throws IOException {
        depCon.out.info("Starting to analyze file '" + path.getName() + "'.");

        // Parse the keywords from the document
        HtmlDocument doc = new HtmlDocument(depCon, analyser);
        doc.setStopWords(stopWordStore.getStopWordSet());
        doc.loadFromFile(path);
        final Collection<HtmlDocument.KeywordResult> result = doc.parseKeywords();

        // First try: get the weighted classification map from the subgraphs
        Map<String,Double> classMap = getWeightedClassificationMapForKeywordResult(result);

        // If the document did not contain a single (!) connection, fall back to analyzing the keyword itself.
        if (classMap.isEmpty())
            classMap = getKeywordBasedFallbackClassificationMapForKeywordResult(result);

        // Show me what we got!
        classMap.forEach((k, v) -> depCon.out.info(k + "=" + v));

        return new DocumentAnalyzerResult(classMap);
    }

    protected Map<String,Double> getWeightedClassificationMapForKeywordResult (Collection<HtmlDocument.KeywordResult> result) {
        // Remember all keywords and their weights in a shortcut lookup map
        Map<String,Double> kwMap = new HashMap<String, Double>();
        for (HtmlDocument.KeywordResult res : result) {
            kwMap.put(res.keyword, res.weight);
        }

        // The result map
        Map<String,Double> classMap = new HashMap<String, Double>();

        // Get all relationships from neo4j that lie inside of the subgraph spanned by those keywords
        String cypher = "MATCH (node:"+graphLabel+") WHERE (";
        for (HtmlDocument.KeywordResult res : result) {
            cypher += "node.keyword='"+res.keyword+"' OR ";
        }
        cypher += "1=2) WITH collect(node) AS nodes MATCH (x)-[r]-(y) WHERE x IN nodes AND y IN nodes ";
        cypher += "RETURN type(r) AS rtype, x.keyword AS kw1, y.keyword AS kw2, r.weight AS weight";

        depCon.out.debug("Executing cypher: "+cypher);

        ExecutionResult res = db.executeQuery(cypher);

        // Iterate over result
        for (Map<String,Object> row : res) {
            String rType = (String) row.get("rtype");
            String kw1 = (String) row.get("kw1");
            String kw2 = (String) row.get("kw2");
            Double weight = (Double) row.get("weight");

            // Multiply the weight of the relation with the weight given by the keyword extraction algorithm
            weight *= kwMap.get(kw1) * kwMap.get(kw2);

            // Save into map
            if (classMap.containsKey(rType))
                classMap.replace(rType, classMap.getOrDefault(rType, 0.0) + weight);
            else
                classMap.put(rType, weight);
        }

        return classMap;
    }

    protected Map<String,Double> getKeywordBasedFallbackClassificationMapForKeywordResult (Collection<HtmlDocument.KeywordResult> result) {

        // Remember all keywords and their weights in a shortcut lookup map
        Map<String,Double> kwMap = new HashMap<String, Double>();
        for (HtmlDocument.KeywordResult res : result) {
            kwMap.put(res.keyword, res.weight);
        }

        // The result map
        Map<String,Double> classMap = new HashMap<String, Double>();

        // Get all relationships from neo4j that lie inside of the subgraph spanned by those keywords
        String cypher = "MATCH (node:"+graphLabel+") WHERE (";
        for (HtmlDocument.KeywordResult res : result) {
            cypher += "node.keyword='"+res.keyword+"' OR ";
        }
        cypher += "1=2) WITH collect(node) AS nodes MATCH (x)-[r]-(y) WHERE x IN nodes ";
        cypher += "RETURN type(r) AS rtype, x.keyword AS kw, r.weight AS weight";

        ExecutionResult res = db.executeQuery(cypher);

        // Iterate over result
        for (Map<String,Object> row : res) {
            String rType = (String) row.get("rtype");
            String kw = (String) row.get("kw");
            Double weight = (Double) row.get("weight");

            // Multiply the weight of the relation with the weight given by the keyword extraction algorithm
            weight *= kwMap.get(kw);

            // Save into map
            if (classMap.containsKey(rType))
                classMap.replace(rType, classMap.getOrDefault(rType, 0.0) + weight);
            else
                classMap.put(rType, weight);
        }

        return classMap;
    }
}